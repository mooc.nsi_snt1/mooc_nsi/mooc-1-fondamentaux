X_MIN = -320
Y_MIN = -240
X_MAX = -30
Y_MAX = 200
ZONE_PLAN_MINI = (X_MIN, Y_MIN)  # Coin inférieur gauche de la zone d'affichage du plan
ZONE_PLAN_MAXI = (X_MAX, Y_MAX)  # Coin supérieur droit de la zone d'affichage du plan
WIDTH = abs(X_MIN) + abs(X_MAX)
HEIGHT = abs(Y_MIN) + abs(Y_MAX)
POINT_AFFICHAGE_ANNONCES = (X_MIN, -X_MIN)  # Point d'origine de l'affichage des annonces
POINT_AFFICHAGE_INVENTAIRE = (X_MAX+40, Y_MAX)  # Point d'origine de l'affichage de l'inventaire

# Les valeurs ci-dessous définissent les couleurs des cases du plan
COULEUR_CASES = 'white'
COULEUR_COULOIR = 'white'
COULEUR_MUR = 'grey'
COULEUR_OBJECTIF = 'yellow'
COULEUR_PORTE = 'orange'
COULEUR_OBJET = 'green'
COULEUR_VUE = 'wheat'
COULEUR_INVISIBLE = 'black'
COULEURS = [COULEUR_COULOIR, COULEUR_MUR, COULEUR_OBJECTIF, COULEUR_PORTE, \
            COULEUR_OBJET, COULEUR_COULOIR, COULEUR_VUE]
COULEUR_EXTERIEUR = 'white'

# Couleur et dimension du personnage
COULEUR_PERSONNAGE = 'red'
RATIO_PERSONNAGE = 30  # Rapport entre diamètre du personnage et dimension des cases

# Types de Cellule 
VIDE = 0
MUR = 1
SORTIE = 2
PORTE = 3
OBJET = 4
ENTREE = 5
SEEN = 6

DISTANCE_MAX = 3

# Fichiers par défaut
FICHIER_PLAN = 'fichiers/plan_chateau.txt'
FICHIER_PORTES = 'fichiers/dico_portes.txt'
FICHIER_OBJETS = 'fichiers/dico_objets.txt'

# Touches fléchées
KEY_UP = 'Up'
KEY_DOWN = 'Down'
KEY_LEFT = 'Left'
KEY_RIGHT = 'Right'

# Directions
UP = (0, -1)
DOWN = (0, 1)
LEFT = (-1, 0)
RIGHT = (1, 0)

N = (0, -1)
NE = (1, -1)
E = (1, 0)
SE = (1, 1)
S = (0, 1)
SW = (-1, 1)
W = (-1, 0)
NW = (-1, 1) 

DIRECTIONS = N, NE, E, SE, S, SW, W, NW

# Textes
FONT = ('Verdana', 12, 'bold')
HAUTEUR_TEXTE = 20
MSG_VICTOIRE = 'Bravo ! Vous avez gagné... fermer la fenêtre pour quitter.'
MSG_DEBUT = "Début de partie. Amenez le héro en rouge jusqu'à la sortie en jaune"
MSG_PORTE_VERROUILLEE = 'Porte verrouillée. Répondez à la question pour ouvrir.'
MSG_PORTE_DEVERROUILLEE = 'Bravo ! Porte déverrouillée'
MSG_MAUVAISE_REPONSE = 'Désolé. Réponse fausse, la porte reste verrouillée'
MSG_INDICE_TROUVE = 'Vous avez ramassé un indice'
DELIMITER = ';'
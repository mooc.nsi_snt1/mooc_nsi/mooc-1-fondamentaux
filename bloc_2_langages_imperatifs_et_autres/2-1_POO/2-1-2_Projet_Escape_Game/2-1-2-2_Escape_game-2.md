## Escape game "Le château" 2/4

1. [Escape game "Le château" 1/4](2-1-2-1_Escape_game-1.md)  
2. **Escape game "Le château" 2/4**
3. [Escape game "Le château" 3/4](2-1-2-3_Escape_game-3.md)
4. [Escape game "Le château" 4/4](2-1-2-4_Escape_game-4.md)

####

* Sébastien Hoarau. 

[![Vidéo 2 B2-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M1-S2-video2.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M1-S2-video2.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M1/B2-M1-S2-video2b.srt" target="_blank">Sous-titre de la vidéo</a> - 

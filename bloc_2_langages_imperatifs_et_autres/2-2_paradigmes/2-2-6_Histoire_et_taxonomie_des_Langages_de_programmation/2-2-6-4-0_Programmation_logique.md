# Histoire et taxonomie des Langages de programmation. 4 : Programmation logique

1. Premiers langages de programmation
2. Langages impératifs
3. Langages fonctionnels
4. **Programmation logique**
5. Autres paradigmes de programmation

####

* **Thierry Massart**. 

[![Vidéo 4 B2-M2-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B2-M2-S6-V4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B2-M2-S6-V4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S6-video6d.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B2-M2/B2-M2-S6-script-video4.md" target="_blank">Transcription de la vidéo </a>


## Texte complémentaire 

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_2_langages_imperatifs_et_autres/2-2_paradigmes/2-2-6_Histoire_et_taxonomie_des_Langages_de_programmation/2-2-6-4-1_Histoire4-5.html" target="_blank">Texte complément à la video</a>

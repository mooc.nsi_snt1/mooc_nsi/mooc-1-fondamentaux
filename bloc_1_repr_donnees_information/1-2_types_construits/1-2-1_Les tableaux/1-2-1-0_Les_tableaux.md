## Une introduction aux 3 types construits. Les tableaux

Cette vidéo introduit le Module 2 qui va présenter les trois types construits que sont :

- **le tableau**,
- le p-uplet,
- le dictionnaire

puis enchaine par la première structure : le tableau.

**Pour chacune des structures, vous avez deux entrées possibles et complémentaires :**
- une vidéo présentant l'essentiel des manipulations sur la structure
- un texte pour approfondir

L'ordre dans lequel vous abordez les supports n'a pas vraiment d'importance._

####

- Sébastien Hoarau. Les tableaux.

[![Vidéo 1 B1-M2-S1 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M2-S1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M2-S1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M2/B1-M2-S1.srt" target="_blank">Sous-titre de la vidéo</a> 

## Texte complémentaire 

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/bloc_1_repr_donnees_information/1-2_types_construits/1-2-1_Les%20tableaux/1-2-1-1_Fiche-Tableaux.md" target="_blank">Texte complément à la video</a>










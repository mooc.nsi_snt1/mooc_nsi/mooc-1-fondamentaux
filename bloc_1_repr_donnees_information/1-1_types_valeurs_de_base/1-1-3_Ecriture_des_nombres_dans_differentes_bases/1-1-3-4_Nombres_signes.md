# Écriture des nombres dans différentes bases 4/5 : Nombres signés

1. Introduction : la notion de base
2. Changements de base
3. Manipuler les données en binaire
4. **Nombres signés**
5. Nombres avec partie fractionnaire

####

 Gilles Geeraerts.  Représenter des nombres négatifs en binaire 

[![Vidéo 4 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S7.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S7.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3_4.srt" target="_blank">Sous-titre de la vidéo</a> - <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3-script-video4.md" target="_blank">Transcription de la vidéo </a>


## Supports de présentation (diapos)

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/Slides/B1-M1-S7.pdf" target="_blank">Supports de présentation</a>


## Texte complémentaire

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-1_types_valeurs_de_base/1-1-3_Ecriture_des_nombres_dans_differentes_bases/1-1-3-4_texte.html" target="_blank">Texte complément à la video</a>






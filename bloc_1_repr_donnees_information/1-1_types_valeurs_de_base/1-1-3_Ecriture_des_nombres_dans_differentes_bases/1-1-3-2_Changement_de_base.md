# Écriture des nombres dans différentes bases 2/5 : Changements de base

1. Introduction : la notion de base
2. **Changements de base** (2 vidéos)
3. Manipuler les données en binaire
4. Nombres signés
5. Nombres avec partie fractionnaire



####

 Gilles Geeraerts.  Comment représenter en binaire les nombres.

[![Vidéo 2 part1 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S4.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S4.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3_2a.srt" target="_blank">Sous-titre de la vidéo</a> - <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3-script-video2part1.md" target="_blank">Transcription de la vidéo </a>


[![Vidéo 2 part2 B1-M1-S3 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B1-M1-S5.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B1-M1-S5.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3_2b.srt" target="_blank">Sous-titre de la vidéo</a> -  <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/B1-M1-S3-script-video2part2.md" target="_blank">Transcription de la vidéo </a>


## Supports de présentation (diapos)

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/Slides/B1-M1-S4.pdf" target="_blank">Supports de présentation vidéo partie 1</a>

<a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B1-M1/Slides/B1-M1-S5.pdf" target="_blank">Supports de présentation vidéo partie 2</a>


## Texte complémentaire

<a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-1_types_valeurs_de_base/1-1-3_Ecriture_des_nombres_dans_differentes_bases/1-1-3-2part1_texte.html" target="_blank">Texte complément à la video 2a</a> et  <a href="https://mooc-nsi-snt.gitlab.io/portail/3_Les_Fondamentaux/Le_Mooc/bloc_1_repr_donnees_information/1-1_types_valeurs_de_base/1-1-3_Ecriture_des_nombres_dans_differentes_bases/1-1-3-2part2_texte.html" target="_blank">Texte complément à la video 2b</a>




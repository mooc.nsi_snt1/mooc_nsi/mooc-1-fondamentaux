## SQL, récapitulatif 4/4 : Mises à jour

> **Supports complémentaires**
> Pas de vidéo sur cette partie triviale de SQL.


Les commandes de mise à jour (insertion, destruction, modification) sont
considérablement plus simples que les interrogations.

### Insertion


L'insertion s'effectue avec la commande `insert`, avec trois
variantes. Dans la première on indique la liste des valeurs à insérer
sans donner explicitement le nom des attributs. Le système suppose alors
qu'il y a autant de valeurs que d'attributs, et que l'ordre des
valeurs correspond à celui des attributs dans la table. On peut indiquer
`null` pour les valeurs inconnues.

``` {.sql}
insert into Immeuble
values (1 'Koudalou' '3 rue des Martyrs')
```

Si on veut insérer dans une partie seulement des attributs, il faut
donner la liste explicitement.

``` {.sql}
insert into Immeuble (id nom adresse)
values (1 'Koudalou' '3 rue des Martyrs')
```

Il est d'ailleurs préférable de toujours donner la liste des attributs.
La description d'une table peut changer par ajout d'attribut, et
l'ordre `insert` qui marchait un jour ne marchera plus le lendemain.

Enfin avec la troisième forme de `insert` il est possible d'insérer
dans une table le résultat d'une requête. Dans ce cas la partie
`values` est remplacée par la requête elle-même. Voici un exemple avec
une nouvelle table *Barabas* dans laquelle on insère uniquement les
informations sur l'immeuble "Barabas".

``` {.sql}
create table Barabas (id int not null,
                     nom varchar(100) not null,
                     adresse varchar(200),
                     primary key (id)
)

insert into Barabas
select * from Immeuble where nom='Barabas'
```

### Destruction

La destruction s'effectue avec la clause `delete` dont la syntaxe est :

``` {.sql}
delete from table
where condition
```

`table` étant bien entendu le nom de la table, et `condition` toute
condition ou liste de conditions valide pour une clause `where`. En
d'autres termes, si on effectue avant la destruction la requête

``` {.sql}
select * from table
where condition
```

on obtient l'ensemble des nuplets qui seront détruits par `delete`.
Procéder de cette manière est un des moyens de s'assurer que l'on va
bien détruire ce que l'on souhaite.

### Modification


La modification s'effectue avec la clause `update`. La syntaxe est
proche de celle du `delete`:

``` {.sql}
update table set A1=v1, A2=v2, ... An=vn
where condition
```

Comme précédemment `table` est la table, les `Ai` sont les attributs les
`vi` les nouvelles valeurs, et `condition` est toute condition valide
pour la clause `where`.


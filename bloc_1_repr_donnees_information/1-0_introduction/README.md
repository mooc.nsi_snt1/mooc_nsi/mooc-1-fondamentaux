# Sommaire du Bloc 1 DATA 

## Objectif :

La science informatique manipule une matière, abstraite, à savoir l'information. Cette information est représentée par des données qui permettent d'encoder cette information, c'est à dire la décrit de manière formelle pour pouvoir la mémoriser, transmettre, chiffrer, transformer, afficher, etc, bref la manipuler.
Pour apprendre ce volet de l'informatique, si vous débutez, le MOOC SNT est notre meilleur préalable pour se doter des notions de base, avant d'attaquer les choses au niveau NSI et même un peu au delà puisque qui enseigne une matière a besoin d'en savoir un peu plus que ce qui doit être transmis pour le faire avec du recul.
Ici, nous allons aller du plus simple à apréhender au plus complexe, des objets élémentaires, puis structurés, en travaillant une structure de données onmiprésente, avant de passer à un domaine de l'informatique à part entière : les bases de données. 

## Sommaire :


- 1.1 Représentation des données : types et valeurs de base
    - 1.1.0 Présentation du Module 
    - 1.1.1 Représentation binaire de l'information
    - 1.1.2 Unité de quantité d'information
    - 1.1.3 Ecriture des nombres dans différentes bases
    - 1.1.4 Représentation du texte
    - 1.1.5 Représentation d'images, de sons, de vidéos
    
- 1.2 Représentation des données : types construits
    - 1.2.0 Présentation du module
    - 1.2.1 Les tableaux
    - 1.2.2 Les p-uplets
    - 1.2.3 Les dictionnaires
    
- 1.3 Traitement des données en tables
    - 1.3.0 Présentation du module
    - 1.3.1 Introduction sur les données en table
    - 1.3.2 Recherches dans une table
    - 1.3.3 Trier des données
    - 1.3.4 Fusion de tables
    - 1.3.5 Sauvegarde des données
    - 1.3.6 Jointure
    
- 1.4 Bases de données
    - 1.4.0  Présentation du module
    - 1.4.1 Introduction aux bases de données
    - 1.4.2 Le modèle relationnel
    - 1.4.3 SQL, langage déclaratif
    - 1.4.4 SQL, langage algébrique
    - 1.4.5 SQL récapitulatif
    - 1.4.6 Conception d'une base de données
    - 1.4.7 Schémas relationnel
    - 1.4.8 Une étude de cas

## Temps d'investissement  : 

- 15h à 25h + 30h pour la partie 1.4 Bases de données

Ce temps est donné à titre indicatif. Il peut varier en fonction des participants.
# MOOC1_Fondamentaux
## Bloc 0 : introduction / prérequis
### Section 1 : prérequis
####  Fiche Apprendre à coder avec Python

- à relire / compléter
- pas de vidéo prévues

##### Contenu 

La fiche explique aux enseignants débutants ce qui est le plus important pour eux dans le MOOC « Apprendre à coder avec Python ». 

####  Extrait du glossaire Python

- à relire / compléter
- pas de vidéo prévues

##### Contenu 

Le glossaire donne aux enseignants une définition venant de python.org des termes qu’ils devraient ou pourraient utiliser (pas les concepts avancés de python qu’ils ne verront jamais)


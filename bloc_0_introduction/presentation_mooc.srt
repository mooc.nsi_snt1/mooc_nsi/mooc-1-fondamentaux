1
00:00:05,035 --> 00:00:06,710
Depuis 2019,

2
00:00:06,810 --> 00:00:09,915
l'informatique est reconnue par l'Éducation Nationale

3
00:00:10,083 --> 00:00:12,144
comme une discipline à part entière.

4
00:00:12,670 --> 00:00:14,934
Je suis ingénieure informaticienne

5
00:00:15,034 --> 00:00:16,579
et j'aimerais bien me reconvertir

6
00:00:16,679 --> 00:00:18,881
et enseigner l'informatique au lycée.

7
00:00:18,981 --> 00:00:20,205
Je suis prof en lycée,

8
00:00:20,305 --> 00:00:21,695
j'aimerais bien changer de discipline

9
00:00:21,795 --> 00:00:23,010
et enseigner l'informatique.

10
00:00:23,110 --> 00:00:24,785
Je suis étudiante en informatique

11
00:00:24,885 --> 00:00:26,821
et enseigner l'informatique me plairait beaucoup.

12
00:00:27,335 --> 00:00:28,457
(ensemble) Oui, mais comment ?

13
00:00:29,004 --> 00:00:30,933
Comment apprendre cette nouvelle discipline ?

14
00:00:31,033 --> 00:00:33,698
Comment apprendre à enseigner l'informatique ?

15
00:00:33,798 --> 00:00:36,136
Et pour le CAPES, on peut se former à distance ?

16
00:00:36,717 --> 00:00:38,662
Enseigner l'informatique en lycée en France,

17
00:00:38,762 --> 00:00:41,125
c'est devenir enseignant en NSI

18
00:00:41,225 --> 00:00:43,170
Numérique et Sciences de l'Informatique.

19
00:00:43,270 --> 00:00:45,623
Cette spécialité est maintenant intégrée

20
00:00:45,723 --> 00:00:47,436
au curricula des élèves au même titre

21
00:00:47,536 --> 00:00:49,488
que n'importe quelle autre discipline.

22
00:00:49,588 --> 00:00:51,579
Se former à cette nouvelle discipline

23
00:00:51,679 --> 00:00:53,410
est un challenge prenant

24
00:00:53,510 --> 00:00:54,636
et important.

25
00:00:55,034 --> 00:00:57,988
C'est pourquoi on vous propose de vous accompagner en ligne

26
00:00:58,088 --> 00:01:00,765
avec le MOOC NSI : Les fondamentaux.

27
00:01:01,190 --> 00:01:03,121
Dans ce MOOC, nous avons la chance

28
00:01:03,221 --> 00:01:04,559
que des enseignantes et enseignants,

29
00:01:04,659 --> 00:01:05,976
des chercheuses et chercheurs,

30
00:01:06,076 --> 00:01:07,964
vous proposent sous des formes diverses

31
00:01:08,064 --> 00:01:10,549
les essentiels du programme NSI.

32
00:01:10,833 --> 00:01:13,773
Vous pourrez vous former au codage de l'information,

33
00:01:14,235 --> 00:01:15,460
à la programmation,

34
00:01:15,560 --> 00:01:16,671
à l'algorithmique,

35
00:01:16,771 --> 00:01:19,137
compléter des autres aspects du programme

36
00:01:19,237 --> 00:01:21,081
tels que systèmes d'exploitation,

37
00:01:21,181 --> 00:01:22,976
réseaux, et cætera.

38
00:01:23,076 --> 00:01:24,353
C'est énorme,

39
00:01:24,453 --> 00:01:26,717
plus d'une centaine d'heures de cours.

40
00:01:26,817 --> 00:01:28,422
Nous vous offrons ici

41
00:01:28,522 --> 00:01:29,525
plus qu'un MOOC,

42
00:01:29,625 --> 00:01:31,430
c'est une formation complète

43
00:01:31,878 --> 00:01:33,190
pour vous permettre de

44
00:01:33,480 --> 00:01:36,750
vraiment vous former à cette nouvelle discipline

45
00:01:36,850 --> 00:01:38,412
qu'est NSI.

46
00:01:38,512 --> 00:01:41,240
C'est super, mais on va se retrouver seul.e derrière notre écran ?

47
00:01:41,720 --> 00:01:42,687
Pas tout à fait !

48
00:01:42,923 --> 00:01:45,041
Assister au MOOC, c'est également rejoindre

49
00:01:45,141 --> 00:01:48,172
la Communauté d'Apprentissage de l'Informatique

50
00:01:48,272 --> 00:01:49,220
CAI

51
00:01:49,320 --> 00:01:52,262
et un forum où sont déjà inscrits plus de mille collègues,

52
00:01:52,546 --> 00:01:53,594
pour s'entraider,

53
00:01:53,694 --> 00:01:55,263
partager des informations,

54
00:01:55,363 --> 00:01:57,546
des bonnes et des moins bonnes pratiques

55
00:01:57,646 --> 00:01:59,457
et poser toutes les questions.

56
00:01:59,557 --> 00:02:00,597
(Ensemble) Alors,

57
00:02:00,697 --> 00:02:02,420
à bientôt !


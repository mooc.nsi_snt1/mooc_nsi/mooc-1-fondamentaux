================
Glossaire
================

.. glossary::
    :sorted:

    >>>
      L'invite de commande utilisée par défaut dans l'interpréteur interactif. On la voit souvent dans des exemples de code qui peuvent être exécutés interactivement dans l'interpréteur.

    ...
      L'invite de commande utilisée par défaut dans l'interpréteur interactif lorsqu'on entre un bloc de code indenté, dans des délimiteurs fonctionnant par paires (parenthèses, crochets, accolades, triple guillemets).
      
    annotation
      Étiquette associée à une variable, un attribut de classe, un paramètre de fonction ou une valeur de retour. Elle est utilisée par convention comme annotation de type (:term:`annotation de type`) (voir :term:`annotation de variable`, :term:`annotation de fonction`).

    argument
      Valeur, donnée à une :term:`fonction` ou à une :term:`méthode` lors de son appel. Il existe deux types d'arguments :

      * :dfn:`argument nommé` : un argument précédé d'un identifiant (comme ``name=``) ou un dictionnaire précédé de ``**``, lors d'un appel de fonction. Par exemple, ``3`` et ``5`` sont tous les deux des arguments nommés dans l'appel à :func:`complex` ici ::

           complex(real=3, imag=5)
           complex(**{'real': 3, 'imag': 5})

      * :dfn:`argument positionnel` : un argument qui n'est pas nommé. Les arguments positionnels apparaissent au début de la liste des arguments, ou donnés sous forme d'un :term:`itérable` précédé par ``*``. Par exemple, ``3`` et ``5`` sont tous les deux des arguments positionnels dans les appels suivants ::

           complex(3, 5)
           complex(*(3, 5))

      Les arguments se retrouvent dans le corps de la fonction appelée parmi les variables locales.  Syntaxiquement, toute expression est acceptée comme argument, et c'est la valeur résultante de l'expression qui sera affectée à la variable locale (voir aussi :term:`paramètre` dans le glossaire).

    attribut
      Valeur associée à un objet et désignée par son nom via une notation utilisant des points. Par exemple, si un objet *o* possède un attribut *a*, il sera référencé par *o.a*.

    *BDFL*
      Dictateur bienveillant à vie (*Benevolent Dictator For Life* en anglais). Pseudonyme de `Guido van Rossum <https://gvanrossum.github.io/>`_, le créateur de Python.

    code intermédiaire (*bytecode*)
      Le code source, en Python, est compilé en un code intermédiaire (*bytecode* en anglais), la représentation interne à CPython d'un programme Python. Le code intermédiaire est mis en cache dans un fichier ``.pyc`` de manière à ce qu'une seconde exécution soit plus rapide (la compilation en code intermédiaire a déjà été faite). On dit que ce *langage intermédiaire* est exécuté sur une machine  virtuelle qui exécute des instructions machine pour chaque instruction du code intermédiaire. Notez que le code intermédiaire n'a pas vocation à fonctionner sur différentes machines virtuelles Python ou à être stable entre différentes versions de Python.

    classe
      Modèle pour créer des objets définis par l'utilisateur. Une définition de classe (*class*) contient normalement des définitions de méthodes qui agissent sur les instances de la classe.

    .. supprimer variable de classe [A supprimer ?]
        Une variable définie dans une classe et destinée à être modifiée uniquement au niveau de la classe (c'est-à-dire, pas dans une instance de la classe).

    coercition
      Conversion implicite d'une instance d'un type vers un autre lors d'une opération dont les deux opérandes doivent être de même type. Par exemple ``int(3.15)`` convertit explicitement le nombre à virgule flottante en nombre entier ``3``. Mais dans l'opération ``3 + 4.5``, les deux opérandes sont d'un type différent (un entier et un nombre à virgule flottante), alors qu'ils doivent avoir le même type pour être additionnés (sinon une exception :exc:`TypeError` serait levée). Sans coercition, tous les opérandes, même de types compatibles, devraient être convertis (on parle aussi de *cast*) explicitement par le développeur, par exemple : ``float(3) + 4.5`` au lieu du simple ``3 + 4.5``.

    nombre complexe
      Extension des nombres réels familiers, dans laquelle tous les nombres sont exprimés sous la forme d'une somme d'une partie réelle et d'une partie imaginaire. Les nombres imaginaires sont les nombres réels multipliés par l'unité imaginaire (la racine carrée de ``-1``, souvent écrite ``i`` en mathématiques ou ``j`` par les ingénieurs). Python comprend nativement les nombres complexes, écrits avec cette dernière notation : la partie imaginaire est écrite avec un suffixe ``j``, exemple, ``3+1j``. Pour utiliser les équivalents complexes de :mod:`math`, utilisez :mod:`cmath`. Les nombres complexes sont un concept assez avancé en mathématiques. Si vous ne connaissez pas ce concept, vous pouvez tranquillement les ignorer.

    CPython
      L'implémentation canonique du langage de programmation Python, tel que distribué sur `python.org <https://www.python.org>`_. Le terme \"CPython\" est utilisé dans certains contextes lorsqu'il est nécessaire de distinguer cette implémentation des autres comme *Jython* ou *IronPython*.

    .. supprimer descripteur [A supprimer ?]
       N'importe quel objet définissant les méthodes :meth:`__get__`, :meth: `__set__`, ou :meth:`__delete__`. Lorsque l'attribut d'une classe est un descripteur, son comportement spécial est déclenché lors de la recherche des attributs. Normalement, lorsque vous écrivez *a.b* pour obtenir, affecter ou effacer un attribut, Python recherche l'objet nommé *b* dans le dictionnaire de la classe de *a*. Mais si *b* est un descripteur, c'est la méthode de ce descripteur qui est alors appelée. Comprendre les descripteurs est requis pour avoir une compréhension approfondie de Python, ils sont la base de nombre de ses caractéristiques notamment les fonctions, méthodes, propriétés, méthodes de classes, méthodes statiques et les références aux classes parentes.

    dictionnaire
      Structure de donnée associant des clés à des valeurs. Les clés peuvent être n'importe quel objet possédant les méthodes :meth:`__hash__` et :meth:`__eq__`. 

    dictionnaire en compréhension
      Écriture concise pour manipuler tout ou partie des éléments d'une séquence et renvoyer un dictionnaire contenant les résultats. ``results = {n: n ** 2 for n in range(10)}`` génère un dictionnaire contenant les valeurs ``n``pour ``n`` allant de 0 à 9 comme clé et ``n ** 2`` comme valeur associée. 

    vue de dictionnaire
      Objets retournés par les méthodes :meth:`dict.keys`, :meth:`dict.values` et :meth:`dict.items`. Ils fournissent des vues dynamiques des entrées du dictionnaire, ce qui signifie que lorsque le dictionnaire change, la vue change. Pour transformer une vue en vraie liste, utilisez ``list(...)``. 

    *docstring* (chaîne de documentation)
      Première chaîne littérale qui apparaît dans l'expression d'une classe, fonction, ou module. Bien qu'ignorée à l'exécution, elle est reconnue par le compilateur et placée dans l'attribut :attr:`__doc__` de la classe, de la fonction ou du module. Comme cette chaîne est disponible par introspection, c'est l'endroit idéal pour documenter l'objet.

    EAFP
      Il est plus simple de demander pardon que demander la permission (*Easier to Ask for Forgiveness than Permission* en anglais). Ce style de développement Python fait l'hypothèse que le code est valide et traite les exceptions si cette hypothèse s'avère fausse. Ce style, propre et efficace, est caractérisé par la présence de beaucoup de mots clés ``try`` et ``except``. Cette technique de programmation contraste avec le style :term:`LBYL` utilisé couramment dans les langages tels que C.

    expression
      Suite logique de termes et chiffres conformes à la syntaxe Python dont l'évaluation fournit une valeur. En d'autres termes, une expression est une suite d'éléments tels que des noms, opérateurs, littéraux, accès d'attributs, méthodes ou fonctions qui aboutissent à une valeur. Contrairement à beaucoup d'autres langages, les différentes constructions du langage ne sont pas toutes des expressions. On trouve également des :term:`instructions <instruction>` qui ne peuvent pas être utilisées comme expressions, tel que ``while``. Les affectations sont également des instructions et non des expressions.

    f-string
      Chaîne littérale préfixée de ``'f'`` ou ``'F'``. Les \"f-strings\" sont un raccourci des chaînes de caractères formattées.

    objet fichier
      Objet exposant une ressource via une API orientée fichier (avec les méthodes :meth:`read()` ou :meth:`write()`). En fonction de la manière dont il a été créé, un objet fichier peut interfacer l'accès à un fichier sur le disque ou à un autre type de stockage ou de communication (typiquement l'entrée standard, la sortie standard, un tampon en mémoire, un connecteur réseau…). Les objets fichiers sont aussi appelés :dfn:`file-like-objects` ou :dfn:`streams`.


    division entière
      Division mathématique arrondissant à l'entier inférieur. L'opérateur de la division entière est ``//``. Par exemple l'expression ``11 // 4`` vaut ``2``, contrairement à ``11 / 4`` qui vaut ``2.75``. Notez que ``(-11) // 4`` vaut ``-3`` car l'arrondi se fait à l'entier inférieur. 

    fonction
      Suite d'instructions qui renvoie une valeur à son appelant. On peut lui passer des :term:`arguments <argument>` qui pourront être utilisés dans le corps de la fonction. Voir aussi :term:`paramètre` et :term:`méthode`.

    annotation de fonction
      :term:`annotation` d'un paramètre de fonction ou valeur de retour.

      Les annotations de fonctions sont généralement utilisées pour des :term:`annotations de types <annotation de type>` : par exemple, cette fonction devrait prendre deux arguments :class:`int` et devrait également avoir une valeur de retour de type :class:`int` ::

	
         def sum_two_numbers(a: int, b: int) -> int:
            return a + b

    ramasse-miettes
      (*garbage collection* en anglais) Mécanisme permettant de libérer de la mémoire lorsqu'elle n'est plus utilisée. Python utilise un ramasse-miettes par comptage de référence et un ramasse-miettes cyclique capable de détecter et casser les références circulaires. Le ramasse-miettes peut être contrôlé en utilisant le module :mod:`gc`.

    hachable
      Un objet est *hachable* s'il a une empreinte (*hash*) qui ne change jamais.

      La hachabilité permet à un objet d'être utilisé comme clé de dictionnaire ou en tant que membre d'un ensemble (type *set*), car ces structures de données utilisent ce *hash*.

      La plupart des types immuables natifs de Python sont hachables, mais les conteneurs muables (comme les listes ou les dictionnaires) ne le sont pas ; les conteneurs immuables (comme les n-uplets ou les ensembles figés) ne sont hachables que si leurs éléments sont hachables. Les instances de classes définies par les utilisateurs sont hachables par défaut. Elles sont toutes considérées différentes (sauf avec elles-mêmes) et leur valeur de hachage est calculée à partir de leur :func:`id`.

    immuable
      Objet dont la valeur ne change pas. Les nombres, les chaînes et les n-uplets sont immuables. Ils ne peuvent être modifiés. Un nouvel objet doit être créé si une valeur différente doit être stockée. Ils jouent un rôle important quand une valeur de *hash* constante est requise, typiquement en clé de dictionnaire.

    importer
      Processus rendant le code Python d'un module disponible dans un autre.

    interactif
      Python a un interpréteur interactif, ce qui signifie que vous pouvez écrire des expressions et des instructions à l'invite de l'interpréteur. L'interpréteur Python va les exécuter immédiatement et vous en présenter le résultat. Démarrez juste ``python`` (probablement depuis le menu principal de votre ordinateur). C'est un moyen puissant pour tester de nouvelles idées ou étudier de nouveaux modules (souvenez-vous de ``help(x)``).

    interprété
      Python est un langage interprété, en opposition aux langages compilés, bien que la frontière soit floue en raison de la présence d'un compilateur en code intermédiaire. Cela signifie que les fichiers sources peuvent être exécutés directement, sans avoir à compiler un fichier exécutable intermédiaire. Les langages interprétés ont généralement un cycle de développement / débogage plus court que les langages compilés. Cependant, ils s'exécutent généralement plus lentement. Voir aussi :term:`interactif`.

    itérable
      Objet capable de renvoyer ses éléments un à un. Par exemple, tous les types séquence (comme :class:`list`, :class:`str`, :class:`range` et :class:`tuple`), quelques autres types comme :class:`dict`, :term:`objets fichiers <objet fichier>` ou tout objet d'une classe ayant une méthode :meth:`__iter__` ou :meth:`__getitem__` qui implémente la sémantique d'une :term:`séquence`.

      Les itérables peuvent être utilisés dans des boucles ``for`` et à beaucoup d'autres endroits où une séquence est requise (:func:`zip`, :func:`map`…). Lorsqu'un itérable est passé comme argument à la fonction native :func:`iter`, celle-ci fournit en retour un itérateur sur cet itérable. Cet itérateur n'est valable que pour une seule passe sur le jeu de valeurs. Lors de l'utilisation d'itérables, il n'est habituellement pas nécessaire d'appeler :func:`iter` ou de s'occuper soi-même des objets itérateurs. L'instruction ``for`` le fait automatiquement pour vous, créant une variable temporaire anonyme pour garder l'itérateur durant la boucle. 

    itérateur
      Objet représentant un flux de donnée. Des appels successifs à la méthode :meth:`~iterator.__next__` de l'itérateur (ou le passer à la fonction native :func:`next`) donne successivement les objets du flux. Lorsque plus aucune donnée n'est disponible, une exception :exc:`StopIteration` est levée. À ce point, l'itérateur est épuisé et tous les appels suivants à sa méthode :meth:`__next__` lèveront encore une exception :exc:`StopIteration`.
      
    argument nommé
      Voir :term:`argument`.

    lambda
      Fonction anonyme sous la forme d'une :term:`expression` et ne contenant qu'une seule expression, exécutée lorsque la fonction est appelée. La syntaxe pour créer des fonctions lambda est : ``lambda [parameters]: expression``

    LBYL
      Regarde avant de sauter, (*Look before you leap* en anglais). Ce style de programmation consiste à vérifier des conditions avant d'effectuer des appels ou des accès. Ce style contraste avec le style :term:`EAFP` et se caractérise par la présence de beaucoup d'instructions ``if``.

    *list*
      Un type natif de :term:`séquence` dans Python. En dépit de son nom, une ``list`` ressemble plus à un tableau (*array* dans la plupart des langages) qu'à une liste chaînée puisque les accès se font en O(1).

    liste en compréhension (ou liste en intention)
      Écriture concise pour manipuler tout ou partie des éléments d'une séquence et renvoyer une liste contenant les résultats. ``result = ['{:#04x}'.format(i) for i in range(256) if i % 2 == 0]`` génère la liste composée des nombres pairs de 0 à 255 écrits sous formes de chaînes de caractères et en hexadécimal (``0x…``). La clause ``if`` est optionnelle. Si elle est omise, tous les éléments du ``range(256)`` seront utilisés.

    méthode
      Fonction définie à l'intérieur d'une classe. Lorsqu'elle est appelée comme un attribut d'une instance de cette classe, la méthode reçoit l'instance en premier :term:`argument` (qui, par convention, est habituellement nommé ``self``). Voir :term:`fonction` et :term:`portée imbriquée`.

    module
      Objet utilisé pour organiser une portion unitaire de code en Python. Les modules ont un espace de nommage et peuvent contenir n'importe quels objets Python. Charger des modules est appelé :term:`importer <importer>`.

    muable
      Un objet muable peut changer de valeur tout en gardant le même :func:`id`. Voir aussi :term:`immuable`.

    espace de nommage
      L'endroit où une variable est stockée. Les espaces de nommage sont implémentés avec des dictionnaires. Il existe des espaces de nommage globaux, natifs ou imbriqués dans les objets (dans les méthodes). Les espaces de nommage favorisent la modularité car ils permettent d'éviter les conflits de noms. Par exemple, les fonctions :func:`builtins.open <.open>` et :func:`os.open` sont différenciées par leurs espaces de nom. Les espaces de nommage aident aussi à la lisibilité et la maintenabilité en rendant clair quel module implémente une fonction. Par exemple, écrire :func:`random.seed` ou :func:`itertools.islice` affiche clairement que ces fonctions sont implémentées respectivement dans les modules :mod:`random` et :mod:`itertools`.

    portée imbriquée
      Possibilité de faire référence à une variable déclarée dans une définition englobante. Typiquement, une fonction définie à l'intérieur d'une autre fonction a accès aux variables de cette dernière. Souvenez-vous cependant que cela ne fonctionne que pour accéder à des variables, pas pour les assigner. Les variables locales sont lues et assignées dans l'espace de nommage le plus proche. Tout comme les variables globales qui sont stockés dans l'espace de nommage global, le mot clef ``nonlocal`` permet d'écrire dans l'espace de nommage dans lequel est déclarée la variable.

    objet
      N'importe quelle donnée comportant des états (sous forme d'attributs ou d'une valeur) et un comportement (des méthodes).

    paquet
      :term:`module` Python qui peut contenir des sous-modules ou des sous-paquets. Techniquement, un paquet est un module qui possède un attribut ``__path__``.

    paramètre
      Entité nommée dans la définition d'une :term:`fonction` (ou méthode), décrivant un :term:`argument` (ou dans certains cas des arguments) que la fonction accepte. Il existe cinq sortes de paramètres :

      :dfn:`positional-or-keyword` : l'argument peut être passé soit par sa :term:`position <argument>`, soit en tant qu' :term:`argument nommé <argument>`. C'est le type de paramètre par défaut. Par exemple, *foo* et *bar* dans l'exemple suivant ::

            def func(foo, bar=None): ...

      :dfn:`positional-only` : définit un argument qui ne peut être fourni que par position. Les paramètres *positional-only* peuvent être définis en insérant un caractère \"/\" dans la liste de paramètres de la définition de fonction après eux. Par exemple : *posonly1* et *posonly2* dans le code suivant ::

	    def func(posonly1, posonly2, /, positional_or_keyword): ...

      :dfn:`keyword-only` : l'argument ne peut être fourni que nommé. Les paramètres *keyword-only* peuvent être définis en utilisant un seul paramètre *var-positional*, ou en ajoutant une étoile (``*``) seule dans la liste des paramètres avant eux. Par exemple, *kw_only1* et *kw_only2* dans le code suivant ::

	    def func(arg, *, kw_only1, kw_only2): ...

      :dfn:`var-positional` : une séquence d'arguments positionnels peut être fournie (en plus de tous les arguments positionnels déjà acceptés par d'autres paramètres). Un tel paramètre peut être défini en préfixant son nom par une ``*``. Par exemple *args* ci-après ::

           def func(*args, **kwargs): ...

      :dfn:`var-keyword` : une quantité arbitraire d'arguments peut être passée, chacun étant nommé (en plus de tous les arguments nommés déjà acceptés par d'autres paramètres). Un tel paramètre est défini en préfixant le nom du paramètre par ``**``. Par exemple, *kwargs* ci-dessus.

      Les paramètres peuvent spécifier des arguments obligatoires ou optionnels, ainsi que des valeurs par défaut pour les arguments optionnels.

      Voir aussi :term:`argument` dans le glossaire.

    PEP
      *Python Enhancement Proposal* (Proposition d'amélioration Python). Un PEP est un document de conception fournissant des informations à la communauté Python ou décrivant une nouvelle fonctionnalité pour Python, ses processus ou son environnement. Les PEP doivent fournir une spécification technique concise et une justification des fonctionnalités proposées.

      Les PEPs sont censés être les principaux mécanismes pour proposer de nouvelles fonctionnalités majeures, pour recueillir les commentaires de la communauté sur une question et pour documenter les décisions de conception qui sont intégrées en Python. L’auteur du PEP est responsable de l’établissement d’un consensus au sein de la communauté et de documenter les opinions contradictoires.

    *Pythonique*
      Idée, ou bout de code, qui colle aux idiomes de Python plutôt qu'aux concepts communs rencontrés dans d'autres langages. Par exemple, il est idiomatique en Python de parcourir les éléments d'un itérable en utilisant `for``. Beaucoup d'autres langages n'ont pas cette possibilité, donc les gens qui ne sont pas habitués à Python utilisent parfois un compteur numérique à la place

    séquence
      :term:`itérable` qui offre un accès efficace à ses éléments par un indice sous forme de nombre entier via la méthode spéciale :meth:`__getitem__` et qui définit une méthode :meth:`__len__` donnant sa taille. Voici quelques séquences natives : :class:`list`, :class:`str`, :class:`tuple`, et :class:`bytes`. Notez que :class:`dict` possède aussi une méthode :meth:`__getitem__` et une méthode :meth:`__len__`, mais il est considéré comme un *mapping* plutôt qu'une séquence, car ses accès se font par une clé arbitraire :term:`immuable` plutôt qu'un nombre entier.

    ensemble en compréhension
      Écriture concise pour manipuler tout ou partie des éléments d'une séquence et renvoyer un ensemble contenant les résultats. ``results = {c for c in 'abracadabra' if c not in 'abc'}`` génère l'ensemble de chaînes de caractères ``{'r', 'd'}``.

    tranche
      (*slice* en anglais), un objet contenant habituellement une portion de :term:`séquence`. Une tranche est créée en utilisant la notation ``[]`` avec des ``:`` entre les nombres lorsque plusieurs sont fournis, comme dans ``variable_name[1:3:5]``. Cette notation utilise des objets :class:`slice` en interne.

    instruction
      Une instruction (*statement* en anglais) est un composant d'un \"bloc\" de code. Une instruction est soit une :term:`expression`, soit une ou plusieurs constructions basées sur un mot-clé, comme ``if``, ``while`` ou ``for``.

    encodage de texte
      Codec (codeur-décodeur) qui convertit des chaînes de caractères Unicode en octets (classe *bytes*).

    fichier texte
      Objet fichier capable de lire et d'écrire des objets :class:`str`. Souvent, un fichier texte (*text file* en anglais) accède en fait à un flux de donnée en octets et gère l':term:`encodage de texte` automatiquement. Des exemples de fichiers textes sont les fichiers ouverts en mode texte (``'r'`` ou ``'w'``), :data:`sys.stdin`, :data:`sys.stdout` et les instances de :class:`io.StringIO`.

    chaîne entre triple guillemets
      Chaîne qui est délimitée par trois guillemets simples (``'``) ou trois guillemets doubles (``"``). Bien qu'elle ne fournisse aucune fonctionnalité qui ne soit pas disponible avec une chaîne entre guillemets, elle est utile pour de nombreuses raisons. Elle vous autorise à insérer des guillemets simples et doubles dans une chaîne sans avoir à les protéger et elle peut s'étendre sur plusieurs lignes sans avoir à terminer chaque ligne par un ``\``. Elle est ainsi particulièrement utile pour les chaînes de documentation (*docstrings*).

    type
      Le type d'un objet Python détermine quel genre d'objet c'est. Tous les objets ont un type. Le type d'un objet peut être obtenu via son attribut :attr:`~instance.__class__` ou via ``type(obj)``.

    alias de type
      Synonyme d'un type, créé en affectant le type à un identifiant.

      Les alias de types sont utiles pour simplifier les :term:`annotations de types <annotation de type>`. Par exemple ::

       def remove_gray_shades(
                 colors: list[tuple[int, int, int]]) -> list[tuple[int, int, int]]:
             pass

      peut être rendu plus lisible comme ceci::

       Color = tuple[int, int, int]

       def remove_gray_shades(colors: list[Color]) -> list[Color]:
             pass


    annotation de type
      :term:`annotation` qui spécifie le type attendu pour une variable, un attribut de classe, un paramètre de fonction ou une valeur de retour.

      Les annotations de type sont facultatives et ne sont pas indispensables à l'interpréteur Python, mais elles sont utiles aux outils d'analyse de type statique et aident les IDE à compléter et à réusiner (*code refactoring* en anglais) le code.

    annotation de variable
      :term:`annotation` d'une variable ou d'un attribut de classe.

      Lorsque vous annotez une variable ou un attribut de classe, l'affectation est facultative ::

	class C:
             field: 'annotation'


      Les annotations de variables sont généralement utilisées pour des :term:`annotations de types <annotation de type>` : par exemple, cette variable devrait prendre des valeurs de type :class:`int` ::

	count: int = 0


    Le zen de Python
      Liste de principes et de préceptes utiles pour comprendre et utiliser le langage. Cette liste peut être obtenue en tapant \"``import this``\" dans une invite Python interactive.

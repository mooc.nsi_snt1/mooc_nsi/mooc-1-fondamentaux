;; RefTeX parse info file
;; File: /Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex
;; User: ggeeraer (Gilles Geeraerts)

(set reftex-docstruct-symbol '(


(xr nil "\\\\\\\\\\\\")

(index-tags)

(is-multi nil)

(bibview-cache)

(master-dir . "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/")

(label-numbers)

(bof "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex")

(toc "toc" "    1 Le processeur" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 2 "1" "\\section{Le processeur}" 1164)

(toc "toc" "          1.0.0.1 Data path" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.1" "\\paragraph{Data path}" 3014)

("fig:vonneumann" "f" "John Von Neumann ({\\tt images/Chap1/VonNeuman.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 4111)

(toc "toc" "          1.0.0.2 Exécution des instruction" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.2" "\\paragraph{Exécution des instruction}" 4760)

("item:2" "i" "Charger, dans le registre IR, l'instruction située à mémoire (M) à l'adresse donnée par PC: IR $\\lef" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil)

("item:4" "i" "Exécuter l'instruction (ceci peut modifier PC) " "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil)

(toc "toc" "          1.0.0.3 Machine à pile" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.3" "\\paragraph{Machine à pile}" 5850)

(toc "toc" "          1.0.0.4 Choix du jeu d'instructions machine" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.4" "\\paragraph{Choix du jeu d'instructions machine}" 7184)

("fig:ibm360" "f" "Le panneau de contrôle d'une IBM 360/91 ({\\tt % images/Chap1/IBM360.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 8958)

(toc "toc" "          1.0.0.5 Exécution en parallèle des instructions" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.5" "\\paragraph{Exécution en parallèle des instructions}" 13228)

(toc "toc" "          1.0.0.6 Architectures superscalaires" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.6" "\\paragraph{Architectures superscalaires}" 14062)

("fig:cdc6600" "f" "Un CDC 6600 au musée de l'informatique, à Mountain View, % Californie ({\\tt images/Chap1/CDC6600.jpg" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 15018)

(toc "toc" "          1.0.0.7 Processeurs vectoriels" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.7" "\\paragraph{Processeurs vectoriels}" 15055)

("fig:illiac4" "f" "Une vue de l'ILLIAC IV ({\\tt images/Chap1/ILLIAC4.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 16189)

("fig:cray1" "f" "Une Cray 1 préservée à Lausanne ({\\tt images/Chap1/Cray1.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 16403)

(toc "toc" "          1.0.0.8 Multiprocesseurs" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.8" "\\paragraph{Multiprocesseurs}" 16515)

(toc "toc" "          1.0.0.9 Systèmes multi-c\\oe urs" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.9" "\\paragraph{Systèmes multi-c\\oe urs}" 17169)

(toc "toc" "          1.0.0.10 Grappes et fermes de calcul" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 5 "1.0.0.10" "\\paragraph{Grappes et fermes de calcul}" 17733)

("fig:hydra" "f" "Le {\\it cluster} Hydra du centre de calcul de l' ULB/VUB % ({\\tt images/Chap1/Hydra.jpg}). " "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 18858)

(toc "toc" "    2 Mémoire primaire" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 2 "2" "\\section{Mémoire primaire\\label{sec:memoire-primaire}}" 18893)

("sec:memoire-primaire" "s" "La mémoire primaire est le composant de l'ordinateur où sont stockés les programmes en cours d'exécu" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil)

(toc "toc" "      2.1 Ordre des octets dans les mots" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "2.1" "\\subsection{Ordre des octets dans les mots}" 21130)

("fig:pbgb" "f" "Petit et gros boutistes" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil)

(toc "toc" "      2.2 Mémoire cache" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "2.2" "\\subsection{Mémoire cache}" 26240)

(toc "toc" "      2.3 Réalisation de la mémoire primaire" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "2.3" "\\subsection{Réalisation de la mémoire primaire}" 29414)

("fig:williams" "f" "Un tube Williams ({\\tt images/Chap1/Williams-tube.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 28919)

("fig:mm" "f" "Mémoire à ligne à délai à mercure de l'Univac 1, % 1951. Dimensions approximatives: 4m $\\times$ 2m $" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 30195)

("fig:toreferite" "f" "Détail d'une mémoire à Tore de ferrite ({\\tt % images/Chap1/MemoireToreFerite.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 30919)

("fig:ibm737" "f" "Une IBM 737 ({\\tt images/Chap1/ibm737.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 32310)

(toc "toc" "    3 Mémoire secondaire" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 2 "3" "\\section{Mémoire secondaire}" 34344)

(toc "toc" "      3.1 Principales formes de mémoire secondaire" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "3.1" "\\subsection{Principales formes de mémoire secondaire}" 35380)

("fig:punchedcard" "f" "Une carte perforée au format standard IBM ({\\tt % images/Chap1/Punch-card-5081.jpg})." "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 34300)

("fig:punchedtape" "f" "Du ruban perforé ({\\tt % images/Chap1/PaperTapeUnivac.jpg}). " "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" 34726)

(toc "toc" "    4 Les périphériques d'entrée/sortie" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 2 "4" "\\section{Les périphériques d'entrée/sortie}" 39207)

(toc "toc" "      4.1 Accès direct à la mémoire" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "4.1" "\\subsection{Accès direct à la mémoire}" 40553)

(toc "toc" "      4.2 Bus dédiés" "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex" nil 3 "4.2" "\\subsection{Bus dédiés}" 41743)

(eof "/Users/ggeeraer/ownCloud/Académique/Cours/FoncOrdi/from-svn/infof102/Notes/Lecons/03.tex")
))


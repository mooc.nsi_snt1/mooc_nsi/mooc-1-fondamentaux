\documentclass[a4paper]{scrartcl}
\usepackage{mathptmx}
\usepackage[scaled=.90]{helvet}
\usepackage{courier}

\usepackage[french]{babel}
\usepackage[T1]{fontenc}
%\usepackage{a4wide}
\usepackage[pdftex]{graphicx}
\usepackage{url}
\usepackage{../../foncordi}
%\usepackage{hyperref}
%\usepackage{times}
\usepackage{rotating}
\usepackage{amssymb,amsmath}
\newcommand{\xor}{\ensuremath{\operatorname{\mathrm{XOR}}}}
\newcommand{\desc}{\ensuremath{\textrm{desc}}}
\newcommand{\pres}{\ensuremath{\textrm{pr�sence}}}

\title{INFO-F-102 -- Fonctionnement des Ordinateurs\\ Le�on 15  --
  Syst�me d'exploitation}
\author{Gilles Geeraerts}
\date{Ann�e acad�mique 2015--2016}

\begin{document}
\maketitle
Dans toute la discussion que nous avons eue jusqu'� pr�sent, nous
avons suppos� que l'ordinateur ex�cute un et un seul programme
provenant d'un et un seul utilisateur, et nous avons volontairement
�lud� les probl�mes qui pourraient survenir si on d�cidait d'ex�cuter
plusieurs programmes appartenant � plusieurs utilisateurs (qui ont
chacun leurs donn�es et leurs caract�ristiques propres) sur la m�me
machine, comme on le rencontre sur les machines modernes.

Le principal probl�me pos� par l'ex�cution de plusieurs programmes sur
une m�me machine est le \emph{partage des ressources}, � savoir: la
m�moire, l'espace disque, le processeur, les p�riph�riques,\ldots

Comme exemple, consid�rons le cas d'un ordinateur install� dans une
universit�, et qui n'est utilis� que par un seul utilisateur, le
prof. X. Tous les matins, le prof. X. arrive avec de nouveaux
programmes � faire ex�cuter par l'ordinateur. Il charge le premier
programme dans la m�moire, en mettant la premi�re instruction machine
� l'adresse 0, demande � l'ordinateur de commencer � ex�cuter le
programme � partir de cette adresse, attend que le programme s'ex�cute
et passe au programme suivant. Durant son ex�cution, le programme lit
et �crit des donn�es sur le disque dur, � des endroits que le prof. X
a bien choisi de mani�re � pouvoir les retrouver ensuite.

Supposons maintenant que l'universit� engage un coll�gue, le prof. Y,
qui doit lui aussi utiliser l'ordinateur, et, lui aussi doit stocker
des donn�es sur le disque. Pour ce qui est de l'utilisation, X et Y se
mettent d'accord: chacun acc�de � l'ordinateur un jour sur deux. Pour
ce qui est du disque, l'accord est le suivant: la premi�re moiti� va �
X qui en dispose comme il le d�sire, et la seconde moiti� � Y.

Gr�ce � cet \emph{accord pr�alable}, les deux utilisateurs peuvent
sans probl�me exploiter la machine, mais imaginons maintenant que X et
Y se disputent, et que X d�sire nuire � son coll�gue. Rien ne
l'emp�che d'�crire un programme qui va d�truire sur le disque toutes
les donn�es de Y\ldots\ Une telle situation peut �galement se produire
par accident\ldots\ La situation se complique encore si plusieurs
autres personnes doivent avoir acc�s � l'ordinateur\ldots

Cet exemple simple montre que, si un partage coop�ratif des ressources
d'un ordinateur est possible, ce n'est pas une solution raisonnable en
pratique. C'est pourquoi, on pr�f�re ajouter un interm�diaire qui va
jouer le r�le \emph{d'arbitre}, � savoir, le \emph{syst�me
  d'exploitation} (OS -- \textit{operating system})

Dans notre exemple, le syst�me exploitera le fait que le processeur
poss�de des \emph{instructions privil�gi�es}, parmi lesquelles se
trouvent, par exemple, celles qui permettent de contr�ler les
p�riph�riques, et donc, en particulier, de lire ou �crire sur le
disque. On mettra le processeur en \emph{mode esclave} quand il doit
ex�cuter les programmes des utilisateurs. Quand un de ces programmes
d�sirera �crire sur le disque, par exemple, il ne pourra pas ex�cuter
directement les instructions (privil�gi�es) d'�criture sur disque. Il
devra faire appel au syst�me d'exploitation, en lui transmettant une
requ�te d'�criture. Le syst�me d'exploitation, par contre, s'ex�cutera
en \emph{mode ma�tre}. Quand il recevra un requ�te, il v�rifiera
d'abord si elle est l�gitime: le programme tente d'�crire dans une
zone du disque qui correspond � un fichier appartenant � l'utilisateur
qui a lanc� le programme. Si c'est le cas, l'OS ex�cutera les
instructions privil�gi�es pour le compte de l'utilisateur.

Le m�canisme d�crit bri�vement ci-dessus permet aux utilisateurs de
partager les ressources des disques, mais il peut aussi �tre appliqu�
pour:
\begin{itemize}
\item Partager la m�moire centrale entre les programmes et �viter
  qu'un programme n'�crive dans une zone m�moire r�serv�e � un autre
  programme.
\item Partager le processeur entre les programmes pour en ex�cuter
  plusieurs de telle mani�re qu'on ait l'impression qu'ils s'ex�cutent
  en parall�le.
\item \textit{etc}
\end{itemize}

\paragraph{Pour r�sumer} Le \emph{syst�me d'exploitation} est un
\emph{logiciel} qui a pour but de g�rer (de mani�re �quitable et s�re)
les ressources mat�rielles de l'ordinateur, en fournissant une
\emph{interface} entre le mat�riel et les applications. Pour ce faire,
le syst�me d'exploitation est en g�n�ral le seul � s'ex�cuter en
\emph{mode ma�tre}, les applications s'ex�cutent elles en \emph{mode
  esclave}\footnote{Remarquons qu'il y a eu plusieurs syst�mes
  d'exploitation avec lesquels les applications s'ex�cutaient elles
  aussi en mode ma�tre, DOS par exemple. Dans ce cas, la protection du
  syst�me repose sur la bonne volont� des programmeurs des
  applications, qui ont tout le loisir de contourner les garde-fous
  mis en place par l'OS.}.

\section{Types d'OS}
Il existe diff�rents types de syst�mes d'exploitation, en fonction des
fonctionnalit�s qu'ils proposent:
\begin{itemize}
\item \textbf{OS mono- ou multi- utilisateur(s)}: Sur un OS
  mono-utilisateur, on consid�re qu'il n'y a qu'une seule personne qui
  utilise la machine. Sur un OS multi-utilisateurs, l'OS est
  <<~conscient~>> qu'il existe diff�rents utilisateurs et propose des
  services en ce sens. Tout d'abord, les utilisateurs doivent
  s'identifier aupr�s de l'OS avant de pouvoir commencer � ex�cuter
  des programmes. Cela se fait typiquement � travers une proc�dure de
  \textit{login} � l'aide d'un mot de passe. Ensuite, l'OS peut
  retenir � quel utilisateur appartient chaque programme ex�cut�, et
  chaque fichier. Il peut emp�cher, le cas �ch�ant, un utilisateur
  d'acc�der aux fichiers d'un autre utilisateur (c'est ce qu'on
  appelle la \emph{gestion des permissions}). Enfin, l'OS peut
  distinguer diff�rentes cat�gories d'utilisateurs qui auront des
  privil�ges diff�rents. Typiquement, il y a un
  \emph{super-utilisateur} qui a tous les droits sur la machine et
  peut la g�rer enti�rement (ajout/suppression de programmes ou
  d'utilisateurs), alors que les utilisateurs normaux ne pourront
  ex�cuter que des programmes standards. Certaines cat�gories
  d'utilisateurs peuvent �galement se voir appliquer des restrictions
  (usage du mat�riel), des quotas (espace disque consomm�), ou des
  syst�mes de facturation (impression).
\item \textbf{OS mono- ou multi- t�che(s)}: Un OS multi-t�ches est
  capable d'ex�cuter plusieurs programmes en m�me temps
  (�ventuellement des programmes de diff�rents utilisateurs si le
  syst�me est multi-utilisateurs), alors que dans un syst�me
  mono-t�che, il faut attendre qu'un programme se soit compl�tement
  ex�cut� pour en lancer un autre. De mani�re g�n�rale, il y a moins
  de processeurs sur la machine que de programmes � ex�cuter en m�me
  temps. Il faut donc trouver une politique pour r�partir le(s)
  processeur(s) entre les programmes en cours d'ex�cution, et confier
  la gestion de cette politique � une partie de l'OS, qu'on appelle
  \emph{l'ordonnanceur} (ou \textit{scheduler} en anglais).
\item \textbf{OS \textit{batch} ou interactif}: Dans un OS
  \textit{batch}, on fournit � l'OS une s�quence de commandes �
  ex�cuter, et celui-ci s'en charge, sans permettre � l'utilisateur
  d'intervenir pendant l'ex�cution des commandes, ni entre les
  diff�rentes commandes. La partie de l'OS charg�e d'ex�cuter la
  s�quence de commande est appel�e \emph{l'interpr�teur de
    commande}. Dans un OS interactif, l'utilisateur peut dialoguer
  avec les programmes en cours d'ex�cution, � l'aide de commandes
  entr�es au clavier, ou � l'aide d'autres p�riph�riques, comme la
  souris.
\item \textbf{OS temps r�el}: Dans certains applications, le d�lai de
  r�ponse des applications est important (exemple: syst�mes de
  freinage dans une voiture,\ldots) Dans ce cas, l'OS doit �tre
  capable d'offrir des garanties sur le temps maximum au bout duquel
  une application donn�e sera termin�e. Ces OS sont appel�s
  \emph{temps r�el}, et sont obtenus en modifiant l'ordonnanceur.
\end{itemize}

La plupart des OS grand public (Windows, MacOS, Linux) sont
multi-t�ches, multi-utilisateurs, interactifs mais pas temps r�el.

\section{OS et interruptions}
Nous avons vu que la gestion d'une interruption se fait en ex�cutant
un \textit{handler} ou gestionnaire d'interruption qui doit avoir �t�
install� en m�moire. En pratique, les gestionnaires d'interruption
feront partir de l'OS: au d�marrage du syst�me, l'OS est charg� en
m�moire et se charge alors de placer les bons gestionnaires
d'interruption � l'endroit ad�quat (soit une adresse fixe en m�moire,
soit une adresse qui est indiqu�e dans un registre du
processeur). Ainsi, lors d'une interruption (par exemple: un
p�riph�rique a fini de lire), c'est un morceau de code de l'OS qu'on
ex�cutera, ce qui est bien coh�rent avec le r�le de l'OS, et avec le
fait qu'on a choisi de mettre la machine en mode ma�tre lors d'une
interruption.

\section{Appels syst�me}
L'appel syst�me est le m�canisme qui permet � un programme utilisateur
en cours d'ex�cution de faire appel � l'OS pour lui faire ex�cuter une
s�quence d'instructions en mode ma�tre. Concr�tement, le programme en
cours d'ex�cution doit d�cider volontairement d'interrompre son
ex�cution pour donner la main � l'OS. Cela peut se faire ais�ment
gr�ce au m�canisme d'interruption. La diff�rence par rapport aux
interruptions vues jusqu'� pr�sent, est \emph{le programme qui
  s'ex�cute} la d�clenche \emph{volontairement}: c'est ce qu'on
appelle une interruption \emph{interne}, par opposition aux
interruptions \emph{externes} qui sont d�clench�es par un �v�nement
ext�rieur.

Concr�tement le programme qui s'ex�cute et doit faire appel au syst�me
doit:
\begin{itemize}
\item Placer dans les registres les informations qu'il d�sire
  transmettre � l'OS: nature de la requ�te (une valeur num�rique pour
  chaque appel syst�me), param�tres. S'il n'y a pas assez de registre,
  on peut aussi utiliser le \textit{stack}.
\item Ex�cuter une instruction qui permet de d�clencher une
  interruption bien pr�cise (par exemple \verb-INT 80- sur les
  machines Intel).
\end{itemize}

� ce moment, le gestionnaire d'interruption correspondant aux appels
syst�me est ex�cut� (en mode ma�tre, on est dans l'OS). Sa premi�re
t�che est de regarder le contenu des registres pour d�terminer la
requ�te qui est faite. Ensuite, il ex�cute la requ�te. Si des valeurs
doivent �tre renvoy�es (par exemple, dans le cas d'une lecture de
p�riph�rique), celles-ci peuvent �tre plac�es dans les copies des
registres, comme d�crit pr�c�demment.
\bigskip

Dans la suite, nous verrons comment l'OS est utilis� pour partager
trois types de ressources:
\begin{enumerate}
\item La m�moire centrale
\item Le processeur
\item La m�moire secondaire
\end{enumerate}

\end{document}





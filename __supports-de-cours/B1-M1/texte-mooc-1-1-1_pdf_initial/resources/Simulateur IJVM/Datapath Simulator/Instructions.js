/*
 * Datapath simulator
 * 
 * Program by Meire Wouter, Master student at ULB
 * wmeire@ulb.ac.be
 */
/////////////////////////////// clicks ///////////////////////////////////////////////////////
$(document).ready(function(){
	$("#Nop").click(function(){
		initiateSimpleInstruction([new Nop()]);
	});
	
	$("#Iadd").click(function(){
		initiateSimpleInstruction([new Iadd()]);
	});

	$("#Isub").click(function(){
		initiateSimpleInstruction([new Isub()]);
	});

	$("#Iand").click(function(){
		initiateSimpleInstruction([new Iand()]);
	});

	$("#Ior").click(function(){
		initiateSimpleInstruction([new Ior()]);
	});

	$("#Dup").click(function(){
		initiateSimpleInstruction([new Dup()]);
	});

	$("#Pop").click(function(){
		initiateSimpleInstruction([new Pop()]);
	});

	$("#Swap").click(function(){
		initiateSimpleInstruction([new Swap()]);
	});	

	$("#Bipush").click(function(){
		initiateSimpleInstruction([new Bipush("0xf1")]);
	});

	$("#Iinc").click(function(){
		initiateSimpleInstruction([new Iinc("0x2", "0x13")]);
	});	

	$("#Goto").click(function(){
		initiateSimpleInstruction([new Goto("0x23c")]);
	});

	$("#Iflt_T").click(function(){
		var value = 0x86;
		intiateCndJmpInstruction([new Iflt("0xabc", value)], value);
	});

	$("#Iflt_F").click(function(){
		var value = 0x3a;
		intiateCndJmpInstruction([new Iflt("0x123", value)], value);
	});

	$("#Ifeq_T").click(function(){
		var value = 0;
		intiateCndJmpInstruction([new Ifeq("0xabc", value)], value);
	});

	$("#Ifeq_F").click(function(){
		var value = 1;
		intiateCndJmpInstruction([new Ifeq("0x123", value)], value);
	});

	$("#If_icmpeq_T").click(function(){
		var value1 = 0xd3;
		var value2 = 0xd3;
		intiateCndJmpInstruction([new If_icmpeq("0xabc", [value1, value2])], [value1, value2]);
	});

	$("#If_icmpeq_F").click(function(){
		var value1 = 0xd3;
		var value2 = 0xf6;
		intiateCndJmpInstruction([new If_icmpeq("0x123", [value1, value2])], [value1, value2]);
	});
});

/////////////////////////////// Nop ///////////////////////////////////////////////////////

function Nop(){
	this.opcode = "0x00";
	this.ID = nopID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["nop1"];
	this.assignOperations();
	this.setupInstruction = setupInstructionSimple;
}

Nop.prototype.assignOperations = function(){
	this.operations = new Array();
	
	this.operations.push(
			function(microcodeObject){
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Nop.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Iadd ///////////////////////////////////////////////////////

function Iadd(){
    //this.opcode = "0x60";
    this.opcode = "0x02";
	this.ID = iaddID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["iadd1", "iadd2", "iadd3"];
	this.assignOperations();
}

Iadd.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "d5";

		microcodeObject.state.insertInStack("1a");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Iadd.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());
	this.operations = this.operations.concat(getHEqTOSOperation());

	this.operations.push(
			function(microcodeObject){
				var addition = Math.abs(parseInt($(MDR).val(), 16) + parseInt($(H).val(), 16));

				addition = trunc(addition);
				testN(addition);
				testZ(addition);

				setNewRegValAndHighlight(MDR, addition.toString(16));
				setNewRegValAndHighlight(TOS, addition.toString(16));

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Iadd.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Isub ///////////////////////////////////////////////////////

function Isub(){
    //	this.opcode = "0x64";
    this.opcode = "0x05";
	this.ID = isubID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["isub1", "isub2", "isub3"];
	this.assignOperations();
}

Isub.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "e8";

		microcodeObject.state.insertInStack("f9");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Isub.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());

	this.operations = this.operations.concat(getHEqTOSOperation());

	this.operations.push(
			function(microcodeObject){
				var substraction = Math.abs(parseInt($(MDR).val(), 16) + 
						twosComplementNegateNumber(parseInt($(H).val(), 16)));

				substraction = trunc(substraction);
				testN(substraction);
				testZ(substraction);

				setNewRegValAndHighlight(MDR, substraction.toString(16));
				setNewRegValAndHighlight(TOS, substraction.toString(16));

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Isub.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Iand ///////////////////////////////////////////////////////

function Iand(){
	this.opcode = "0x7e";
	this.ID = iandID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["iand1", "iand2", "iand3"];
	this.assignOperations();
}

Iand.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "cc";

		microcodeObject.state.insertInStack("aa");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Iand.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());
	this.operations = this.operations.concat(getHEqTOSOperation());

	this.operations.push(
			function(microcodeObject){
				var and = (parseInt($(MDR).val(), 16) & parseInt($(H).val(), 16)).toString(16);

				setNewRegValAndHighlight(MDR, and);
				setNewRegValAndHighlight(TOS, and);

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Iand.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Ior ///////////////////////////////////////////////////////

function Ior(){
	this.opcode = "0x80";
	this.ID = iorID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["ior1", "ior2", "ior3"];
	this.assignOperations();
}

Ior.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "a8";

		microcodeObject.state.insertInStack("22");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Ior.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());
	this.operations = this.operations.concat(getHEqTOSOperation());

	this.operations.push(
			function(microcodeObject){
				var or = (parseInt($(MDR).val(), 16) | parseInt($(H).val(), 16)).toString(16);

				setNewRegValAndHighlight(MDR, or);
				setNewRegValAndHighlight(TOS, or);

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Ior.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Dup ///////////////////////////////////////////////////////

function Dup(){
    //	this.opcode = "0x59";
    this.opcode = "0x1c";
	this.ID = dupID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["dup1", "dup2"];
	this.assignOperations();
}

Dup.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var value = "2d";

		microcodeObject.state.insertInStack(value);

		microcodeObject.state.updateSP(1);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, value);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Dup.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations.push(
			function(microcodeObject){
				try {
					microcodeObject.state.updateSP(1);
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});
	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(MDR, $(TOS).val());

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Dup.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Pop ///////////////////////////////////////////////////////

function Pop(){
    //	this.opcode = "0x57";
    this.opcode = "0x08";
	this.ID = popID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["pop1", "pop2", "pop3"];
	this.assignOperations();
}

Pop.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "7b";

		microcodeObject.state.insertInStack("40");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Pop.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());

	this.operations.push(
			function(microcodeObject){
				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations = this.operations.concat(getTOSEqMDROperation());
};

Pop.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Swap ///////////////////////////////////////////////////////

function Swap(){
	this.opcode = "0x5f";
	this.ID = swapID;
	this.instructionInfo = [this.opcode, this.ID];
	this.operationIDs = ["swap1", "swap2", "swap3", "swap4", "swap5", "swap6"];
	this.assignOperations();
}

Swap.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "22";

		microcodeObject.state.insertInStack("11");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Swap.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations.push(
			function(microcodeObject){
				try{
					microcodeObject.state.updateMAR(-1);

					microcodeObject.initiateRead();
					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(
			function(microcodeObject){
				try{
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(H, $(MDR).val());

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(MDR, $(TOS).val());

				microcodeObject.initiateRead();
				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(
			function(microcodeObject){
				try{
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos - 1);

					microcodeObject.initiateWrite();
					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(TOS, $(H).val());

				microcodeObject.state.checkIfMemoryAccess();
			});
};

Swap.prototype.getInfoLines = function(){
	return new Array(this.instructionInfo);
};


/////////////////////////////// Bipush ///////////////////////////////////////////////////////

function Bipush(parameter){
    //	this.opcode = "0x10";
    this.opcode = "0x0b";
	this.ID = bipushID;
	this.instructionInfo = [this.opcode, this.ID, parameter];
	this.operationIDs = ["bipush1", "bipush2", "bipush3"];
	this.assignOperations();
	this.setupInstruction = setupInstructionSimple;
}

Bipush.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations.push(
			function(microcodeObject){
				try {
					microcodeObject.state.updateSP(1);
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(main1Operations[0]);

	this.operations.push(
			function(microcodeObject){	
				var value = $(MBR).val();
				setNewRegValAndHighlight(TOS, value);
				setNewRegValAndHighlight(MDR, value);

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Bipush.prototype.getInfoLines = function(){
	return new Array(
			this.instructionInfo,
			[this.instructionInfo[2]]
	);
};


/////////////////////////////// Iinc ///////////////////////////////////////////////////////

function Iinc(parameter1, parameter2){
	this.opcode = "0x84";
	this.ID = iincID;
	this.instructionInfo = [this.opcode, this.ID, parameter1, parameter2];
	this.operationIDs = ["iinc1", "iinc2", "iinc3", "iinc4", "iinc5", "iinc6"];
	this.assignOperations();
}

Iinc.prototype.setupInstruction = function(microcodeObject){
	try {
		microcodeObject.state.initialState();

		var secondValue = "b1";

		microcodeObject.state.insertInStack("56");
		microcodeObject.state.insertInStack(secondValue);

		microcodeObject.state.updateSP(2);
		setNewRegValAndHighlight(TOS, secondValue);

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Iinc.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(H, $(LV).val());

				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(
			function(microcodeObject){
				try{
					addition = parseInt($(MBR).val(), 16) + parseInt($(H).val(), 16);

					microcodeObject.state.setMARVal(addition);

					microcodeObject.initiateRead();
					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(main1Operations[0]);

	this.operations.push(
			function(microcodeObject){
				microcodeObject.state.checkIfMemoryAccess();

				setNewRegValAndHighlight(H, $(MDR).val());
			});

	this.operations.push(main1Operations[0]);

	this.operations.push(
			function(microcodeObject){
				var addition = Math.abs(parseInt($(MBR).val(), 16) + parseInt($(H).val(), 16));

				addition = trunc(addition);
				testN(addition);
				testZ(addition);

				setNewRegValAndHighlight(MDR, addition.toString(16));

				microcodeObject.initiateWrite();
				microcodeObject.state.checkIfMemoryAccess();
			});
};

Iinc.prototype.getInfoLines = function(){
	return new Array(
			this.instructionInfo,
			[this.instructionInfo[2]],
			[this.instructionInfo[3]]
	);
};


/////////////////////////////// Goto ///////////////////////////////////////////////////////

function Goto(parameter){
    //	this.opcode = "0xa7";
    this.opcode = "0x0e";
	this.ID = gotoID;
	this.instructionInfo = [this.opcode, this.ID, parameter];
	this.operationIDs = ["goto1", "goto2", "goto3", "goto4", "goto5", "goto6"];
	this.assignOperations();
	this.setupInstruction = setupInstructionSimple;
}

Goto.prototype.assignOperations = function(){
	this.operations = new Array();

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(OPC, (microcode.state.PCVal - 1).toString(16));

				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(main1Operations[0]);

	this.operations.push(
			function(microcodeObject){
				var shiftedHexa = (parseInt($(MBR).val(), 16) << 8).toString(16);
				setNewRegValAndHighlight(H, shiftedHexa);

				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(
			function(microcodeObject){
				var newVal = (parseInt($(MBR).val(), 16) | parseInt($(H).val(), 16)).toString(16);
				setNewRegValAndHighlight(H, newVal);

				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations.push(
			function(microcodeObject){
				microcodeObject.state.checkIfMemoryAccess();

				microcodeObject.state.PCVal = parseInt($(OPC).val(), 16) + parseInt($(H).val(), 16);
				setNewRegValAndHighlight(PC, microcode.state.PCVal.toString(16));

				microcodeObject.initiateFetch();
			});

	this.operations.push(
			function(microcodeObject){
				try{
					microcodeObject.handlePCJmp();

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});
};

Goto.prototype.getInfoLines = function(){
	var parameter = this.instructionInfo[2];
	var parameterH = "0x" + ((parseInt(parameter) >>  8) & 0x00ff).toString(16);
	var parameterL = "0x" + (parseInt(parameter) & 0x00ff).toString(16);
	return new Array(
			this.instructionInfo,
			[parameterH],
			[parameterL]
	);
};


/////////////////////////////// Iflt ///////////////////////////////////////////////////////

function Iflt(parameter, previousMicrocodeObjectsOrNumber){
	this.opcode = "0x9b";
	this.ID = ifltID;
	this.instructionInfo = [this.opcode, this.ID, parameter];
	this.operationIDs = ["iflt1", "iflt2", "iflt3", "iflt4"];
	this.assignOperations(previousMicrocodeObjectsOrNumber);
}

Iflt.prototype.setupInstruction = function(microcodeObject, number){
	try {
		microcodeObject.state.initialState();

		microcodeObject.state.insertInStack(number.toString(16));

		microcodeObject.state.updateSP(1);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

		setNewRegValAndHighlight(TOS, number.toString(16));

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Iflt.prototype.assignOperations = function(previousMicrocodeObjectsOrNumber){
	var simulationMicrocode = new Microcode(new Array());
	if(typeof(previousMicrocodeObjectsOrNumber) != "number")
		simulationMicrocode = new Microcode(previousMicrocodeObjectsOrNumber);
	else{
		this.number = previousMicrocodeObjectsOrNumber;
		this.setupInstruction(simulationMicrocode, this.number);
	}

	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());

	this.operations = this.operations.concat(getSaveTOSInOPCOperation());

	this.operations = this.operations.concat(getTOSEqMDROperation());

	this.operations.push(
			function(microcodeObject){
				var val = parseInt($(OPC).val(), 16);

				val = (val & 0x80);

				if(val != 0)
					setNewRegValAndHighlight(N, 1);
				else
					setNewRegValAndHighlight(N, 0);

				microcodeObject.state.checkIfMemoryAccess();
			});

	executeToCurrentEnd(simulationMicrocode);

	var val = parseInt($(TOS).val(), 16);

	val = (val & 0x80);

	if(val != 0){
		this.operationIDs = this.operationIDs.concat(tOperationIDs);
		this.operations = this.operations.concat(tOperations);
	} else {
		this.operationIDs = this.operationIDs.concat(fOperationIDs);
		this.operations = this.operations.concat(fOperations);
	}

	resetRegValues();
	resetRegColors();
};

Iflt.prototype.getInfoLines = function(){
	var parameter = this.instructionInfo[2];
	var parameterH = "0x" + ((parseInt(parameter) >>  8) & 0x00ff).toString(16);
	var parameterL = "0x" + (parseInt(parameter) & 0x00ff).toString(16);
	return new Array(
			this.instructionInfo,
			[parameterH],
			[parameterL]
	);
};


/////////////////////////////// Ifeq ///////////////////////////////////////////////////////

function Ifeq(parameter, previousMicrocodeObjectsOrNumber){
    //	this.opcode = "0x99";
    	this.opcode = "0x14";
	this.ID = ifeqID;
	this.instructionInfo = [this.opcode, this.ID, parameter];
	this.operationIDs = ["ifeq1", "ifeq2", "ifeq3", "ifeq4"];
	this.assignOperations(previousMicrocodeObjectsOrNumber);
}

Ifeq.prototype.setupInstruction = function(microcodeObject, number){
	try {
		microcodeObject.state.initialState();

		microcodeObject.state.insertInStack(number.toString(16));

		microcodeObject.state.updateSP(1);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

		setNewRegValAndHighlight(TOS, number.toString(16));

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

Ifeq.prototype.assignOperations = function(previousMicrocodeObjectsOrNumber){
	var simulationMicrocode = new Microcode(new Array());
	if(typeof(previousMicrocodeObjectsOrNumber) != "number")
		simulationMicrocode = new Microcode(previousMicrocodeObjectsOrNumber);
	else{
		this.number = previousMicrocodeObjectsOrNumber;
		this.setupInstruction(simulationMicrocode, this.number);
	}

	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());

	this.operations = this.operations.concat(getSaveTOSInOPCOperation());

	this.operations = this.operations.concat(getTOSEqMDROperation());

	this.operations.push(
			function(microcodeObject){
				if($(OPC).val() == 0)
					setNewRegValAndHighlight(Z, 1);
				else
					setNewRegValAndHighlight(Z, 0);

				microcodeObject.state.checkIfMemoryAccess();
			});

	executeToCurrentEnd(simulationMicrocode);

	if($(TOS).val() != 0){
		this.operationIDs = this.operationIDs.concat(fOperationIDs);
		this.operations = this.operations.concat(fOperations);
	} else {
		this.operationIDs = this.operationIDs.concat(tOperationIDs);
		this.operations = this.operations.concat(tOperations);
	}

	resetRegValues();
	resetRegColors();
};

Ifeq.prototype.getInfoLines = function(){
	var parameter = this.instructionInfo[2];
	var parameterH = "0x" + ((parseInt(parameter) >>  8) & 0x00ff).toString(16);
	var parameterL = "0x" + (parseInt(parameter) & 0x00ff).toString(16);
	return new Array(
			this.instructionInfo,
			[parameterH],
			[parameterL]
	);
};


/////////////////////////////// If_icmpeq ///////////////////////////////////////////////////////

function If_icmpeq(parameter, previousMicrocodeObjectsOrArray){
	this.opcode = "0x9f";
	this.ID = if_icmpeqID;
	this.instructionInfo = [this.opcode, this.ID, parameter];
	this.operationIDs = ["if_icmpeq1", "if_icmpeq2","if_icmpeq3","if_icmpeq4","if_icmpeq5", "if_icmpeq6"];
	this.assignOperations(previousMicrocodeObjectsOrArray);
}

If_icmpeq.prototype.setupInstruction = function(microcodeObject, numberArray){
	try {
		microcodeObject.state.initialState();

		microcodeObject.state.insertInStack(numberArray[0].toString(16));
		microcodeObject.state.insertInStack(numberArray[1].toString(16));

		microcodeObject.state.updateSP(2);
		microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

		setNewRegValAndHighlight(TOS, numberArray[1].toString(16));

		fillAllDisplays();

		$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
	} catch(err) {
		alert(err);
	}
};

If_icmpeq.prototype.assignOperations = function(previousMicrocodeObjectsOrArray){
	var simulationMicrocode = new Microcode(new Array());
	if(previousMicrocodeObjectsOrArray.length > 0 &&
			typeof(previousMicrocodeObjectsOrArray[0]) == "number"){
		this.number1 = previousMicrocodeObjectsOrArray[0];
		this.number2 = previousMicrocodeObjectsOrArray[1];
		this.setupInstruction(simulationMicrocode, [this.number1, this.number2]);
	}
	else
		simulationMicrocode = new Microcode(previousMicrocodeObjectsOrArray);

	this.operations = new Array();

	this.operations = this.operations.concat(getRdNextToTopWordOnStackOperation());

	this.operations.push(
			function(microcodeObject){
				try {
					microcodeObject.state.updateSP(-1);
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});

	this.operations.push(
			function(microcodeObject){
				setNewRegValAndHighlight(H, $(MDR).val()); // TODO esperer qu'mdr n'ai pas besoin d'update avant

				microcodeObject.initiateRead();

				microcodeObject.state.checkIfMemoryAccess();
			});

	this.operations = this.operations.concat(getSaveTOSInOPCOperation());

	this.operations = this.operations.concat(getTOSEqMDROperation());

	this.operations.push(
			function(microcodeObject){

				var substraction = parseInt($(OPC).val(), 16) + 
				twosComplementNegateNumber(parseInt($(H).val(), 16));

				substraction = trunc(substraction);

				if(substraction == 0)
					setNewRegValAndHighlight(Z, 1);
				else
					setNewRegValAndHighlight(Z, 0);

				microcodeObject.state.checkIfMemoryAccess();
			});

	executeToCurrentEnd(simulationMicrocode);

	var substraction;
	if(previousMicrocodeObjectsOrArray.length > 0 && 
			typeof(previousMicrocodeObjectsOrArray[0]) != "number"){
		var val1 = parseInt(simulationMicrocode.state.stack[simulationMicrocode.state.SPPos], 16);
		var val2 = parseInt(simulationMicrocode.state.stack[simulationMicrocode.state.SPPos-1], 16);
		
		substraction = val1 + twosComplementNegateNumber(val2);
	} else
		substraction = this.number1 - this.number2;

	substraction = trunc(substraction);

	if(substraction == 0){
		this.operationIDs = this.operationIDs.concat(tOperationIDs);
		this.operations = this.operations.concat(tOperations);
	} else {
		this.operationIDs = this.operationIDs.concat(fOperationIDs);
		this.operations = this.operations.concat(fOperations);
	}

	resetRegValues();
	resetRegColors();
};

If_icmpeq.prototype.getInfoLines = function(){
	var parameter = this.instructionInfo[2];
	var parameterH = "0x" + ((parseInt(parameter) >>  8) & 0x00ff).toString(16);
	var parameterL = "0x" + (parseInt(parameter) & 0x00ff).toString(16);
	return new Array(
			this.instructionInfo,
			[parameterH],
			[parameterL]
	);
};


/////////////////////////////// Main1 & F & T ///////////////////////////////////////////////////////

var main1OperationIDs = ["Main1"];

var main1Operations = new Array(
		function(microcodeObject){
			microcodeObject.state.checkIfMemoryAccess();

			microcodeObject.state.PCVal += 1;
			setNewRegValAndHighlight(PC, microcodeObject.state.PCVal.toString(16));

			microcodeObject.initiateFetch();
		}
);
var fOperationIDs = ["F", "F2", "F3"];

var fOperations = new Array(
		function(microcodeObject){
			microcodeObject.state.checkIfMemoryAccess();

			microcodeObject.state.PCVal += 1;
			setNewRegValAndHighlight(PC, microcodeObject.state.PCVal);
		},

		main1Operations[0],

		function(microcodeObject){
			microcodeObject.state.checkIfMemoryAccess();
		}
);

var tOperationIDs = ["T", "goto2", "goto3", "goto4", "goto5", "goto6"];

var tGoto = new Goto("0x0");

var tOperations = new Array(
		function(microcodeObject){

			setNewRegValAndHighlight(OPC, microcodeObject.state.PCVal - 1);

			microcodeObject.state.checkIfMemoryAccess();
		},

		tGoto.operations[1],
		tGoto.operations[2],
		tGoto.operations[3],
		tGoto.operations[4],
		tGoto.operations[5]
);


/////////////////////////////// Other ///////////////////////////////////////////////////////

function setupInstructionSimple(microcodeObject){
	microcodeObject.state.initialState();

	fillAllDisplays();

	$("#instructionOperationStepTracker").html("0/" + microcodeObject.nbrOfSteps);
}

function initiateSimpleInstruction(instructionInArray){
	resetThumbnailParameters();
	setMicrointructionParameters();

	microcode = new Microcode(instructionInArray);
	microcode.instructionObjects[0].setupInstruction(microcode);
}

function intiateCndJmpInstruction(instructionInArray, value){
	resetThumbnailParameters();
	setMicrointructionParameters();

	microcode = new Microcode(instructionInArray);
	microcode.instructionObjects[0].setupInstruction(microcode, value);
}

function getRdNextToTopWordOnStackOperation(){
	return new Array(
			function(microcodeObject){
				try {
					microcodeObject.state.updateSP(-1);
					microcodeObject.state.setMARVal(microcodeObject.state.SPPos);

					microcodeObject.initiateRead();

					microcodeObject.state.checkIfMemoryAccess();
				} catch(err) {
					alert(err);
				}
			});
}

function getHEqTOSOperation(){
	return new Array(
			function(microcodeObject){
				setNewRegValAndHighlight(H, $(TOS).val());

				microcodeObject.state.checkIfMemoryAccess();
			});
}

function getSaveTOSInOPCOperation(){
	return new Array(
			function(microcodeObject){
				setNewRegValAndHighlight(OPC, $(TOS).val());

				microcodeObject.state.checkIfMemoryAccess();
			});
}

function getTOSEqMDROperation(){
	return new Array(
			function(microcodeObject){
				setNewRegValAndHighlight(TOS, $(MDR).val());

				microcodeObject.state.checkIfMemoryAccess();
			});
}

function executeToCurrentEnd(simulationMicrocode){
	while(simulationMicrocode.state.currentStep < simulationMicrocode.nbrOfSteps){
		var tmpStep = simulationMicrocode.state.currentStep;
		simulationMicrocode.executeNextStep();
		if((tmpStep + 1) != simulationMicrocode.state.currentStep)// needed for when errors are throw that reset currentStep to 0
			break;
	}
}


% $ biblatex auxiliary file $
% $ biblatex bbl format version 3.1 $
% Do not modify the above lines!
%
% This is an auxiliary file used by the 'biblatex' package.
% This file may safely be deleted. It will be recreated by
% biber as required.
%
\begingroup
\makeatletter
\@ifundefined{ver@biblatex.sty}
  {\@latex@error
     {Missing 'biblatex' package}
     {The bibliography requires the 'biblatex' package.}
      \aftergroup\endinput}
  {}
\endgroup


\refsection{0}
  \datalist[entry]{nty/global//global/global}
    \entry{i8259}{manual}{}
      \list{organization}{1}{%
        {Intel Corporation}%
      }
      \field{sortinit}{8}
      \field{sortinithash}{1b24cab5087933ef0826a7cd3b99e994}
      \field{labeltitlesource}{title}
      \field{title}{8259A Programmable Interrupt Controller}
      \field{year}{1988}
      \verb{urlraw}
      \verb https://pdos.csail.mit.edu/6.828/2010/readings/hardware/8259A.pdf
      \endverb
      \verb{url}
      \verb https://pdos.csail.mit.edu/6.828/2010/readings/hardware/8259A.pdf
      \endverb
    \endentry
    \entry{cpu-fpga}{article}{}
      \name{author}{3}{}{%
        {{hash=4ac6a5fac360a89f9d39c56b9dace16c}{%
           family={Andrews},
           familyi={A\bibinitperiod},
           given={David},
           giveni={D\bibinitperiod}}}%
        {{hash=9cade841af4b4a6f9c8b9b81541d2678}{%
           family={Niehaus},
           familyi={N\bibinitperiod},
           given={D},
           giveni={D\bibinitperiod}}}%
        {{hash=ebaef3b035d3a3a46cb13c752944022a}{%
           family={Ashenden},
           familyi={A\bibinitperiod},
           given={P},
           giveni={P\bibinitperiod}}}%
      }
      \strng{namehash}{75d340d7c9405f13641826638baf24fb}
      \strng{fullhash}{75d340d7c9405f13641826638baf24fb}
      \strng{bibnamehash}{75d340d7c9405f13641826638baf24fb}
      \strng{authorbibnamehash}{75d340d7c9405f13641826638baf24fb}
      \strng{authornamehash}{75d340d7c9405f13641826638baf24fb}
      \strng{authorfullhash}{75d340d7c9405f13641826638baf24fb}
      \field{sortinit}{A}
      \field{sortinithash}{a3dcedd53b04d1adfd5ac303ecd5e6fa}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{Computer}
      \field{month}{02}
      \field{title}{Programming models for hybrid CPU/FPGA chips}
      \field{volume}{37}
      \field{year}{2004}
      \field{pages}{118\bibrangedash 120}
      \range{pages}{3}
      \verb{doi}
      \verb 10.1109/MC.2004.1260732
      \endverb
    \endentry
    \entry{Cbook}{book}{}
      \name{author}{3}{}{%
        {{hash=0fd087aae9e8f68c1288ea58f32e324b}{%
           family={Banahan},
           familyi={B\bibinitperiod},
           given={Mike},
           giveni={M\bibinitperiod}}}%
        {{hash=0956b97c85fba743a242d7e43af16cba}{%
           family={Brady},
           familyi={B\bibinitperiod},
           given={Declan},
           giveni={D\bibinitperiod}}}%
        {{hash=4f6f3b8a84b647786b47fdf84ef967d8}{%
           family={Doran},
           familyi={D\bibinitperiod},
           given={Mark},
           giveni={M\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Addison Wesley}%
      }
      \strng{namehash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \strng{fullhash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \strng{bibnamehash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \strng{authorbibnamehash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \strng{authornamehash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \strng{authorfullhash}{f13262a6c5ebc3caaa37204e01f6e9ac}
      \field{sortinit}{B}
      \field{sortinithash}{8de16967003c7207dae369d874f1456e}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{The C Book}
      \field{year}{1991}
      \verb{urlraw}
      \verb http://publications.gbdirect.co.uk/c_book/
      \endverb
      \verb{url}
      \verb http://publications.gbdirect.co.uk/c_book/
      \endverb
    \endentry
    \entry{Boole}{book}{}
      \name{author}{1}{}{%
        {{hash=11e54e87435aa3c53aa25a448424f022}{%
           family={Boole},
           familyi={B\bibinitperiod},
           given={George},
           giveni={G\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Walton \& Maberly}%
      }
      \strng{namehash}{11e54e87435aa3c53aa25a448424f022}
      \strng{fullhash}{11e54e87435aa3c53aa25a448424f022}
      \strng{bibnamehash}{11e54e87435aa3c53aa25a448424f022}
      \strng{authorbibnamehash}{11e54e87435aa3c53aa25a448424f022}
      \strng{authornamehash}{11e54e87435aa3c53aa25a448424f022}
      \strng{authorfullhash}{11e54e87435aa3c53aa25a448424f022}
      \field{sortinit}{B}
      \field{sortinithash}{8de16967003c7207dae369d874f1456e}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{note}{Une {é}dition r{é}cente (1954) est disponible en ligne {à} l'adresse \url{http://www.gutenberg.org/etext/15114}.}
      \field{title}{An Investigation of the Laws of Thought on Which are Founded the Mathematical Theories of Logic and Probabilities}
      \field{year}{1854}
    \endentry
    \entry{wiki:cfp737}{url}{}
      \field{sortinit}{C}
      \field{sortinithash}{4c244ceae61406cdc0cc2ce1cb1ff703}
      \field{labeltitlesource}{title}
      \field{title}{Code page 737 --- Wikipedia{,} The Free Encyclopedia}
      \field{urlday}{1}
      \field{urlmonth}{9}
      \field{urlyear}{2017}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb https://en.wikipedia.org/w/index.php?title=Code_page_737&oldid=782268129
      \endverb
      \verb{url}
      \verb https://en.wikipedia.org/w/index.php?title=Code_page_737&oldid=782268129
      \endverb
    \endentry
    \entry{hamming50}{article}{}
      \name{author}{1}{}{%
        {{hash=cb982834b1b1f07b8d1c242d0a3e2552}{%
           family={Hamming},
           familyi={H\bibinitperiod},
           given={Richard\bibnamedelima W.},
           giveni={R\bibinitperiod\bibinitdelim W\bibinitperiod}}}%
      }
      \strng{namehash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \strng{fullhash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \strng{bibnamehash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \strng{authorbibnamehash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \strng{authornamehash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \strng{authorfullhash}{cb982834b1b1f07b8d1c242d0a3e2552}
      \field{sortinit}{H}
      \field{sortinithash}{6db6145dae8dc9e1271a8d556090b50a}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{Bell System Technical Journal}
      \field{number}{2}
      \field{title}{Error detecting and error correcting codes}
      \field{volume}{29}
      \field{year}{1950}
      \field{pages}{147\bibrangedash 160}
      \range{pages}{14}
    \endentry
    \entry{i486}{manual}{}
      \list{organization}{1}{%
        {Intel Corporation}%
      }
      \field{sortinit}{i}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{title}{i486 Microprocessor Programmer's Reference Manual}
      \field{year}{1990}
      \verb{urlraw}
      \verb http://bitsavers.trailing-edge.com/components/intel/80486/i486_Processor_Programmers_Reference_Manual_1990.pdf
      \endverb
      \verb{url}
      \verb http://bitsavers.trailing-edge.com/components/intel/80486/i486_Processor_Programmers_Reference_Manual_1990.pdf
      \endverb
    \endentry
    \entry{ibm360}{manual}{}
      \list{organization}{1}{%
        {IBM}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{title}{IBM System/360 Reference data}
      \verb{urlraw}
      \verb http://www.bitsavers.org/pdf/ibm/360/referenceCard/GX20-1703-9_System360_Reference_Data_2up.pdf
      \endverb
      \verb{url}
      \verb http://www.bitsavers.org/pdf/ibm/360/referenceCard/GX20-1703-9_System360_Reference_Data_2up.pdf
      \endverb
    \endentry
    \entry{ieee754}{report}{}
      \list{institution}{1}{%
        {IEEE}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{month}{08}
      \field{number}{754-2008}
      \field{title}{IEEE Standard for Floating-Point Arithmetic}
      \field{type}{techreport}
      \field{year}{2008}
      \field{pages}{1\bibrangedash 70}
      \range{pages}{70}
      \verb{doi}
      \verb 10.1109/IEEESTD.2008.4610935
      \endverb
      \verb{urlraw}
      \verb https://ieeexplore.ieee.org/servlet/opac?punumber=4610933
      \endverb
      \verb{url}
      \verb https://ieeexplore.ieee.org/servlet/opac?punumber=4610933
      \endverb
    \endentry
    \entry{iso-qr}{report}{}
      \list{institution}{1}{%
        {International Organization for Standardization}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{number}{ISO/IEC 18004:2015}
      \field{title}{Information technology -- Automatic identification and data capture techniques -- QR Code bar code symbology specification}
      \field{type}{techreport}
      \field{year}{2015}
      \verb{urlraw}
      \verb https://www.iso.org/standard/62021.html
      \endverb
      \verb{url}
      \verb https://www.iso.org/standard/62021.html
      \endverb
    \endentry
    \entry{iso-png}{report}{}
      \list{institution}{1}{%
        {International Organization for Standardization}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{number}{ISO/IEC 15948:2004}
      \field{title}{Information technology -- Computer graphics and image processing -- Portable Network Graphics (PNG): Functional specification}
      \field{type}{techreport}
      \field{year}{2004}
      \verb{urlraw}
      \verb https://www.iso.org/standard/29581.html
      \endverb
      \verb{url}
      \verb https://www.iso.org/standard/29581.html
      \endverb
    \endentry
    \entry{iso-jpeg}{report}{}
      \list{institution}{1}{%
        {International Organization for Standardization}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{number}{ISO/IEC 10918-1:1994}
      \field{title}{Information technology -- Digital compression and coding of continuous-tone still images: Requirements and guidelines}
      \field{type}{techreport}
      \field{year}{1994}
      \verb{urlraw}
      \verb https://www.iso.org/standard/18902.html
      \endverb
      \verb{url}
      \verb https://www.iso.org/standard/18902.html
      \endverb
    \endentry
    \entry{intel-isa}{manual}{}
      \list{organization}{1}{%
        {Intel Corporation}%
      }
      \field{sortinit}{I}
      \field{sortinithash}{9417e9a1288a9371e2691d999083ed39}
      \field{labeltitlesource}{title}
      \field{title}{ISA bus specification and application notes}
      \field{year}{1989}
      \verb{urlraw}
      \verb https://archive.org/details/bitsavers_intelbusSpep89_3342148
      \endverb
      \verb{url}
      \verb https://archive.org/details/bitsavers_intelbusSpep89_3342148
      \endverb
    \endentry
    \entry{jvm-spec}{url}{}
      \name{author}{4}{}{%
        {{hash=efc7d9d3d26034142724398ad4b4467e}{%
           family={Lindholm},
           familyi={L\bibinitperiod},
           given={Tim},
           giveni={T\bibinitperiod}}}%
        {{hash=b7f6c816ff8aadf3e93769013ec384bc}{%
           family={Yellin},
           familyi={Y\bibinitperiod},
           given={Frank},
           giveni={F\bibinitperiod}}}%
        {{hash=075676db46622d5a64b6ff1011b9b7d0}{%
           family={Bracha},
           familyi={B\bibinitperiod},
           given={Gilad},
           giveni={G\bibinitperiod}}}%
        {{hash=1c34391bfa51bee4b9dacbc295281374}{%
           family={Buckley},
           familyi={B\bibinitperiod},
           given={Alex},
           giveni={A\bibinitperiod}}}%
      }
      \strng{namehash}{9e2a4c38bd880568b88d56a17eae9f6e}
      \strng{fullhash}{88c9dddf89f8f9660970b70f561a4e06}
      \strng{bibnamehash}{9e2a4c38bd880568b88d56a17eae9f6e}
      \strng{authorbibnamehash}{9e2a4c38bd880568b88d56a17eae9f6e}
      \strng{authornamehash}{9e2a4c38bd880568b88d56a17eae9f6e}
      \strng{authorfullhash}{88c9dddf89f8f9660970b70f561a4e06}
      \field{sortinit}{L}
      \field{sortinithash}{dad3efd0836470093a7b4a7bb756eb8c}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{The Java Virtual Machine Specification (Java SE 7 Edition)}
      \field{urlday}{28}
      \field{urlmonth}{2}
      \field{urlyear}{2013}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb https://docs.oracle.com/javase/specs/jvms/se7/html/index.html
      \endverb
      \verb{url}
      \verb https://docs.oracle.com/javase/specs/jvms/se7/html/index.html
      \endverb
    \endentry
    \entry{6502}{manual}{}
      \list{organization}{1}{%
        {MOS Technology Inc.}%
      }
      \field{sortinit}{M}
      \field{sortinithash}{2e5c2f51f7fa2d957f3206819bf86dc3}
      \field{labeltitlesource}{title}
      \field{month}{1}
      \field{title}{MCS6500 Micorcomputer Family Programming Manual}
      \field{year}{1976}
      \verb{urlraw}
      \verb http://archive.6502.org/books/mcs6500_family_programming_manual.pdf
      \endverb
      \verb{url}
      \verb http://archive.6502.org/books/mcs6500_family_programming_manual.pdf
      \endverb
    \endentry
    \entry{vn45}{report}{}
      \name{author}{1}{}{%
        {{hash=f8744afb2b8eb53c5804cbac615dbad1}{%
           family={Neumann},
           familyi={N\bibinitperiod},
           given={John},
           giveni={J\bibinitperiod},
           prefix={von},
           prefixi={v\bibinitperiod}}}%
      }
      \list{institution}{1}{%
        {University of Pennsylvania}%
      }
      \strng{namehash}{f8744afb2b8eb53c5804cbac615dbad1}
      \strng{fullhash}{f8744afb2b8eb53c5804cbac615dbad1}
      \strng{bibnamehash}{f8744afb2b8eb53c5804cbac615dbad1}
      \strng{authorbibnamehash}{f8744afb2b8eb53c5804cbac615dbad1}
      \strng{authornamehash}{f8744afb2b8eb53c5804cbac615dbad1}
      \strng{authorfullhash}{f8744afb2b8eb53c5804cbac615dbad1}
      \field{sortinit}{N}
      \field{sortinithash}{98cf339a479c0454fe09153a08675a15}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{First Draft of a Report on the EDVAC}
      \field{type}{techreport}
      \field{year}{1945}
      \verb{urlraw}
      \verb https://archive.org/download/firstdraftofrepo00vonn/firstdraftofrepo00vonn.pdf
      \endverb
      \verb{url}
      \verb https://archive.org/download/firstdraftofrepo00vonn/firstdraftofrepo00vonn.pdf
      \endverb
    \endentry
    \entry{wiki:ordinateur}{url}{}
      \field{sortinit}{O}
      \field{sortinithash}{ff8d4eeb5101e3cf3809959b3592d942}
      \field{labeltitlesource}{title}
      \field{title}{Ordinateur --- Wikip{é}dia{,} l'encyclop{é}die libre}
      \field{urlday}{16}
      \field{urlmonth}{8}
      \field{urlyear}{2018}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb https://fr.wikipedia.org/w/index.php?title=Ordinateur&oldid=150970460
      \endverb
      \verb{url}
      \verb https://fr.wikipedia.org/w/index.php?title=Ordinateur&oldid=150970460
      \endverb
    \endentry
    \entry{Setun}{url}{}
      \name{author}{4}{}{%
        {{hash=84b6a2fd98d164d6c7f9621334a10f4e}{%
           family={P.},
           familyi={P\bibinitperiod},
           given={Brousentsov\bibnamedelima N.},
           giveni={B\bibinitperiod\bibinitdelim N\bibinitperiod}}}%
        {{hash=87f5e000f1986cea018eb745aac6622d}{%
           family={P.},
           familyi={P\bibinitperiod},
           given={Maslov\bibnamedelima S.},
           giveni={M\bibinitperiod\bibinitdelim S\bibinitperiod}}}%
        {{hash=0797b743f31843e3e4d466c2ad6c1cbf}{%
           family={J.},
           familyi={J\bibinitperiod},
           given={Ramil\bibnamedelima Alvarez},
           giveni={R\bibinitperiod\bibinitdelim A\bibinitperiod}}}%
        {{hash=36b4fa344781f1e8256610adcaa1dd8c}{%
           family={E.A.},
           familyi={E\bibinitperiod},
           given={Zhogolev},
           giveni={Z\bibinitperiod}}}%
      }
      \strng{namehash}{a4dfbde87ca2a0d715a06f2af7a0e8db}
      \strng{fullhash}{f99c2bf330268f82a0df184157cc87c2}
      \strng{bibnamehash}{a4dfbde87ca2a0d715a06f2af7a0e8db}
      \strng{authorbibnamehash}{a4dfbde87ca2a0d715a06f2af7a0e8db}
      \strng{authornamehash}{a4dfbde87ca2a0d715a06f2af7a0e8db}
      \strng{authorfullhash}{f99c2bf330268f82a0df184157cc87c2}
      \field{sortinit}{P}
      \field{sortinithash}{bb5b15f2db90f7aef79bb9e83defefcb}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{Development of ternary computers at Moscow State University}
      \field{urlday}{8}
      \field{urlmonth}{8}
      \field{urlyear}{2018}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb http://www.computer-museum.ru/english/setun.htm
      \endverb
      \verb{url}
      \verb http://www.computer-museum.ru/english/setun.htm
      \endverb
    \endentry
    \entry{PD80}{article}{}
      \name{author}{2}{}{%
        {{hash=95486d328725c1c6a933929c9442fe2a}{%
           family={Patterson},
           familyi={P\bibinitperiod},
           given={David\bibnamedelima A.},
           giveni={D\bibinitperiod\bibinitdelim A\bibinitperiod}}}%
        {{hash=94076a01a60d1d67e0b266557532717e}{%
           family={Ditzel},
           familyi={D\bibinitperiod},
           given={David\bibnamedelima R.},
           giveni={D\bibinitperiod\bibinitdelim R\bibinitperiod}}}%
      }
      \list{location}{1}{%
        {New York, NY, USA}%
      }
      \list{publisher}{1}{%
        {ACM}%
      }
      \strng{namehash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \strng{fullhash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \strng{bibnamehash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \strng{authorbibnamehash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \strng{authornamehash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \strng{authorfullhash}{db5d1aed6e1b05a5f5251f43b51231b3}
      \field{sortinit}{P}
      \field{sortinithash}{bb5b15f2db90f7aef79bb9e83defefcb}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{issn}{0163-5964}
      \field{journaltitle}{SIGARCH Comput. Archit. News}
      \field{month}{10}
      \field{number}{6}
      \field{title}{The Case for the Reduced Instruction Set Computer}
      \field{volume}{8}
      \field{year}{1980}
      \field{pages}{25\bibrangedash 33}
      \range{pages}{9}
      \verb{doi}
      \verb 10.1145/641914.641917
      \endverb
      \verb{urlraw}
      \verb http://doi.acm.org/10.1145/641914.641917
      \endverb
      \verb{url}
      \verb http://doi.acm.org/10.1145/641914.641917
      \endverb
    \endentry
    \entry{ENIAC}{misc}{}
      \field{sortinit}{P}
      \field{sortinithash}{bb5b15f2db90f7aef79bb9e83defefcb}
      \field{labeltitlesource}{title}
      \field{howpublished}{Press Release, War Department, Bureau of Public Relations}
      \field{title}{Physical aspects, operations of ENIAC are described}
      \verb{urlraw}
      \verb https://americanhistory.si.edu/comphist/pr4.pdf
      \endverb
      \verb{url}
      \verb https://americanhistory.si.edu/comphist/pr4.pdf
      \endverb
    \endentry
    \entry{iso}{report}{}
      \list{institution}{1}{%
        {International Organization for Standardization}%
      }
      \field{sortinit}{Q}
      \field{sortinithash}{c54e16deb53fc904a621fa8fd8703b5a}
      \field{labeltitlesource}{title}
      \field{number}{IEC 80000-13:2008}
      \field{title}{Quantities and units -- Part 13: Information science and technology}
      \field{type}{techreport}
      \field{year}{2008}
      \verb{urlraw}
      \verb https://www.iso.org/standard/31898.html
      \endverb
      \verb{url}
      \verb https://www.iso.org/standard/31898.html
      \endverb
    \endentry
    \entry{RS60}{article}{}
      \name{author}{2}{}{%
        {{hash=37f57c5f7585b47da1946352cee4d6dd}{%
           family={Reed},
           familyi={R\bibinitperiod},
           given={Irving\bibnamedelima S.},
           giveni={I\bibinitperiod\bibinitdelim S\bibinitperiod}}}%
        {{hash=abd0d5108903894427446c09ce75de64}{%
           family={Solomon},
           familyi={S\bibinitperiod},
           given={Gustave},
           giveni={G\bibinitperiod}}}%
      }
      \strng{namehash}{582d8799d79ba58b6cbb33707a573404}
      \strng{fullhash}{582d8799d79ba58b6cbb33707a573404}
      \strng{bibnamehash}{582d8799d79ba58b6cbb33707a573404}
      \strng{authorbibnamehash}{582d8799d79ba58b6cbb33707a573404}
      \strng{authornamehash}{582d8799d79ba58b6cbb33707a573404}
      \strng{authorfullhash}{582d8799d79ba58b6cbb33707a573404}
      \field{sortinit}{R}
      \field{sortinithash}{b9c68a358aea118dfa887b6e902414a7}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{journaltitle}{Journal of the Society for Industrial and Applied Mathematics}
      \field{number}{2}
      \field{title}{Polynomial Codes Over Certain Finite Fields}
      \field{volume}{8}
      \field{year}{1960}
      \field{pages}{300\bibrangedash 304}
      \range{pages}{5}
    \endentry
    \entry{ieeeunits}{report}{}
      \name{author}{1}{}{%
        {{hash=e3289954d382af2fd0e419efd37339fb}{%
           family={{SCC14.3 - Unit Symbols Subcommittee}},
           familyi={S\bibinitperiod}}}%
      }
      \list{institution}{1}{%
        {IEEE}%
      }
      \strng{namehash}{e3289954d382af2fd0e419efd37339fb}
      \strng{fullhash}{e3289954d382af2fd0e419efd37339fb}
      \strng{bibnamehash}{e3289954d382af2fd0e419efd37339fb}
      \strng{authorbibnamehash}{e3289954d382af2fd0e419efd37339fb}
      \strng{authornamehash}{e3289954d382af2fd0e419efd37339fb}
      \strng{authorfullhash}{e3289954d382af2fd0e419efd37339fb}
      \field{sortinit}{S}
      \field{sortinithash}{c319cff79d99c853d775f88277d4e45f}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{number}{260.1-2004}
      \field{title}{IEEE Standard Letter Symbols for Units of Measurement (SI Customary Inch-Pound Units, and Certain Other Units)}
      \field{type}{techreport}
      \field{year}{2004}
      \verb{urlraw}
      \verb https://standards.ieee.org/standard/260_1-2004.html
      \endverb
      \verb{url}
      \verb https://standards.ieee.org/standard/260_1-2004.html
      \endverb
    \endentry
    \entry{Shannon37}{thesis}{}
      \name{author}{1}{}{%
        {{hash=536c91678d1dde76c9acd47047556291}{%
           family={Shannon},
           familyi={S\bibinitperiod},
           given={Claude\bibnamedelima E.},
           giveni={C\bibinitperiod\bibinitdelim E\bibinitperiod}}}%
      }
      \list{institution}{1}{%
        {MIT}%
      }
      \strng{namehash}{536c91678d1dde76c9acd47047556291}
      \strng{fullhash}{536c91678d1dde76c9acd47047556291}
      \strng{bibnamehash}{536c91678d1dde76c9acd47047556291}
      \strng{authorbibnamehash}{536c91678d1dde76c9acd47047556291}
      \strng{authornamehash}{536c91678d1dde76c9acd47047556291}
      \strng{authorfullhash}{536c91678d1dde76c9acd47047556291}
      \field{sortinit}{S}
      \field{sortinithash}{c319cff79d99c853d775f88277d4e45f}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{A symbolic analysis of relay and switching circuits}
      \field{type}{mathesis}
      \field{year}{1937}
      \verb{urlraw}
      \verb https://dspace.mit.edu/handle/1721.1/11173
      \endverb
      \verb{url}
      \verb https://dspace.mit.edu/handle/1721.1/11173
      \endverb
    \endentry
    \entry{cppbook}{book}{}
      \name{author}{1}{}{%
        {{hash=4975813514faee20fb6fcdb39a08af6f}{%
           family={Stroustrup},
           familyi={S\bibinitperiod},
           given={Bjarne},
           giveni={B\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Addison-Wesley}%
      }
      \strng{namehash}{4975813514faee20fb6fcdb39a08af6f}
      \strng{fullhash}{4975813514faee20fb6fcdb39a08af6f}
      \strng{bibnamehash}{4975813514faee20fb6fcdb39a08af6f}
      \strng{authorbibnamehash}{4975813514faee20fb6fcdb39a08af6f}
      \strng{authornamehash}{4975813514faee20fb6fcdb39a08af6f}
      \strng{authorfullhash}{4975813514faee20fb6fcdb39a08af6f}
      \field{sortinit}{S}
      \field{sortinithash}{c319cff79d99c853d775f88277d4e45f}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{A Tour of C++}
      \field{year}{2018}
    \endentry
    \entry{gulliver}{book}{}
      \name{author}{1}{}{%
        {{hash=e0fdf0b5513f7c742f83801aec72a3d8}{%
           family={Swift},
           familyi={S\bibinitperiod},
           given={Jonhathan},
           giveni={J\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Balliet, Thomas M. (Thomas Minard)}%
      }
      \strng{namehash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \strng{fullhash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \strng{bibnamehash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \strng{authorbibnamehash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \strng{authornamehash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \strng{authorfullhash}{e0fdf0b5513f7c742f83801aec72a3d8}
      \field{sortinit}{S}
      \field{sortinithash}{c319cff79d99c853d775f88277d4e45f}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{note}{Version en ligne du projet Gutenberg}
      \field{title}{Gulliver's Travels Into Several Remote Regions of the World}
      \verb{urlraw}
      \verb http://www.gutenberg.org/ebooks/17157
      \endverb
      \verb{url}
      \verb http://www.gutenberg.org/ebooks/17157
      \endverb
    \endentry
    \entry{tagbanwa}{report}{}
      \list{institution}{1}{%
        {The Unicode Consortium}%
      }
      \field{sortinit}{T}
      \field{sortinithash}{51f9faf24c60c62ca764a77f78cf5666}
      \field{labeltitlesource}{title}
      \field{title}{Tagbanwa}
      \field{type}{techreport}
      \field{year}{1991}
      \verb{urlraw}
      \verb http://www.unicode.org/charts/PDF/U1760.pdf
      \endverb
      \verb{url}
      \verb http://www.unicode.org/charts/PDF/U1760.pdf
      \endverb
    \endentry
    \entry{wiki:tagbanwa}{url}{}
      \field{sortinit}{T}
      \field{sortinithash}{51f9faf24c60c62ca764a77f78cf5666}
      \field{labeltitlesource}{title}
      \field{title}{Tagbanwa (Unicode block) --- Wikipedia{,} The Free Encyclopedia}
      \field{urlday}{1}
      \field{urlmonth}{9}
      \field{urlyear}{2017}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb https://en.wikipedia.org/w/index.php?title=Tagbanwa_(Unicode_block)&oldid=775143371
      \endverb
      \verb{url}
      \verb https://en.wikipedia.org/w/index.php?title=Tagbanwa_(Unicode_block)&oldid=775143371
      \endverb
    \endentry
    \entry{tanenbaum}{book}{}
      \name{author}{1}{}{%
        {{hash=f5468b55b742e14a341a50be425d3715}{%
           family={Tanenbaum},
           familyi={T\bibinitperiod},
           given={Andrew},
           giveni={A\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Prentice Hall}%
      }
      \strng{namehash}{f5468b55b742e14a341a50be425d3715}
      \strng{fullhash}{f5468b55b742e14a341a50be425d3715}
      \strng{bibnamehash}{f5468b55b742e14a341a50be425d3715}
      \strng{authorbibnamehash}{f5468b55b742e14a341a50be425d3715}
      \strng{authornamehash}{f5468b55b742e14a341a50be425d3715}
      \strng{authorfullhash}{f5468b55b742e14a341a50be425d3715}
      \field{sortinit}{T}
      \field{sortinithash}{51f9faf24c60c62ca764a77f78cf5666}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{Structured Computer Organisation, 5th edition}
    \endentry
    \entry{ferrantimark1}{url}{}
      \field{sortinit}{T}
      \field{sortinithash}{51f9faf24c60c62ca764a77f78cf5666}
      \field{labeltitlesource}{title}
      \field{title}{The Ferranti Mark 1}
      \field{urlyear}{2003}
      \field{urldateera}{ce}
      \verb{urlraw}
      \verb http://curation.cs.manchester.ac.uk/computer50/www.computer50.org/mark1/FM1.html
      \endverb
      \verb{url}
      \verb http://curation.cs.manchester.ac.uk/computer50/www.computer50.org/mark1/FM1.html
      \endverb
    \endentry
    \entry{Turing36}{inproceedings}{}
      \name{author}{1}{}{%
        {{hash=1210277649f3c7541e2750ee21da6237}{%
           family={Turing},
           familyi={T\bibinitperiod},
           given={Alan\bibnamedelima M.},
           giveni={A\bibinitperiod\bibinitdelim M\bibinitperiod}}}%
      }
      \strng{namehash}{1210277649f3c7541e2750ee21da6237}
      \strng{fullhash}{1210277649f3c7541e2750ee21da6237}
      \strng{bibnamehash}{1210277649f3c7541e2750ee21da6237}
      \strng{authorbibnamehash}{1210277649f3c7541e2750ee21da6237}
      \strng{authornamehash}{1210277649f3c7541e2750ee21da6237}
      \strng{authorfullhash}{1210277649f3c7541e2750ee21da6237}
      \field{sortinit}{T}
      \field{sortinithash}{51f9faf24c60c62ca764a77f78cf5666}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{booktitle}{Proceedings of the London Mathematical Society}
      \field{title}{On Computable Numbers, with an Application to the Entscheidungsproblem}
      \field{volume}{42}
      \field{year}{1936}
      \field{pages}{230\bibrangedash 265}
      \range{pages}{36}
      \verb{urlraw}
      \verb https://doi.org/10.1112/plms/s2-42.1.230
      \endverb
      \verb{url}
      \verb https://doi.org/10.1112/plms/s2-42.1.230
      \endverb
    \endentry
    \entry{vax}{report}{}
      \list{institution}{1}{%
        {Digital Equipment Corporation}%
      }
      \field{sortinit}{V}
      \field{sortinithash}{02432525618c08e2b03cac47c19764af}
      \field{labeltitlesource}{title}
      \field{number}{revision 6.1}
      \field{title}{VAX-11 Architecture Reference Manual}
      \field{type}{techreport}
      \field{year}{1982}
      \verb{urlraw}
      \verb http://www.bitsavers.org/pdf/dec/vax/archSpec/EK-VAXAR-RM-001_Arch_May82.pdf
      \endverb
      \verb{url}
      \verb http://www.bitsavers.org/pdf/dec/vax/archSpec/EK-VAXAR-RM-001_Arch_May82.pdf
      \endverb
    \endentry
    \entry{wilkes51}{inproceedings}{}
      \name{author}{1}{}{%
        {{hash=8eba62450f7aedebd6589930fa96d3bb}{%
           family={Wilkes},
           familyi={W\bibinitperiod},
           given={Maurice},
           giveni={M\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Ferranti Ltd.}%
      }
      \strng{namehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{fullhash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{bibnamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authorbibnamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authornamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authorfullhash}{8eba62450f7aedebd6589930fa96d3bb}
      \field{extraname}{1}
      \field{sortinit}{W}
      \field{sortinithash}{1af34bd8c148ffb32de1494636b49713}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{booktitle}{Manchester University Computer Inaugural Conference}
      \field{title}{The best way to design an automated calculating machine}
      \field{year}{1951}
      \verb{urlraw}
      \verb https://www.cs.princeton.edu/courses/archive/fall10/cos375/BestWay.pdf
      \endverb
      \verb{url}
      \verb https://www.cs.princeton.edu/courses/archive/fall10/cos375/BestWay.pdf
      \endverb
    \endentry
    \entry{wilkes86}{article}{}
      \name{author}{1}{}{%
        {{hash=8eba62450f7aedebd6589930fa96d3bb}{%
           family={Wilkes},
           familyi={W\bibinitperiod},
           given={Maurice},
           giveni={M\bibinitperiod}}}%
      }
      \strng{namehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{fullhash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{bibnamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authorbibnamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authornamehash}{8eba62450f7aedebd6589930fa96d3bb}
      \strng{authorfullhash}{8eba62450f7aedebd6589930fa96d3bb}
      \field{extraname}{2}
      \field{sortinit}{W}
      \field{sortinithash}{1af34bd8c148ffb32de1494636b49713}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{issn}{0164-1239}
      \field{journaltitle}{Annals of the History of Computing}
      \field{month}{4}
      \field{number}{2}
      \field{title}{The Genesis of Microprogramming}
      \field{volume}{8}
      \field{year}{1986}
      \field{pages}{116\bibrangedash 126}
      \range{pages}{11}
      \verb{doi}
      \verb 10.1109/MAHC.1986.10035
      \endverb
      \keyw{Microprogramming;Permission;Circuits;History;Laboratories;Information processing;Digital arithmetic;Mouth;Read-write memory;Computer aided instruction}
    \endentry
    \entry{wolper06}{book}{}
      \name{author}{1}{}{%
        {{hash=f33cf2a41e0fc51eaf5ebebc8aaf6062}{%
           family={Wolper},
           familyi={W\bibinitperiod},
           given={Pierre},
           giveni={P\bibinitperiod}}}%
      }
      \list{publisher}{1}{%
        {Dunod}%
      }
      \strng{namehash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \strng{fullhash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \strng{bibnamehash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \strng{authorbibnamehash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \strng{authornamehash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \strng{authorfullhash}{f33cf2a41e0fc51eaf5ebebc8aaf6062}
      \field{sortinit}{W}
      \field{sortinithash}{1af34bd8c148ffb32de1494636b49713}
      \field{labelnamesource}{author}
      \field{labeltitlesource}{title}
      \field{title}{Introduction {à} la calculabilit{é}: cours et exercices corrig{é}s}
      \field{year}{2006}
    \endentry
  \enddatalist
\endrefsection
\endinput


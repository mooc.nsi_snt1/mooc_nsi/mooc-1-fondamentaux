# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 1.1.3.1 : Introduction : la notion de base

Dans cette vidéo, nous allons voir comment représenter, en binaire, des informations très simples, à savoir les nombres. Pour ce faire, nous allons d'abord étudier la notion de base, en partant d'un exemple.

Considérons le nombre 314,15. 

On peut le décomposer en
- 3 centaines
- 2 dizaines
- 4 unité
- 1 dixième
- 5 centièmes

On voit que cette décomposition se réfère à des puissances de 10, qui sont comptées à partir de la virgule. En effet:
- 1 centaine = 10^2
- 1 dizaine = 10^1
- 1 unité = 10^0
- 1 dixième = 10^-1
- 1 centième = 10^-2

On dit alors que cette valeur de référence, 10, est la base. On parle de représentation décimale et positionnelle.

On peut aussi observer qu'en base 10, on utilise les chiffres de 0 à 9 dans cette décomposition. C'est logique, car, quand on dépassé 9, on passe à la puissance de 10 suivante. 


Que se passe-t-il si nous considérons maintenant la base 2 ?

Tout d'abord, on n'utilisera que les chiffres 0 et 1, c'est-à-dire des bits. Ensuite, ces chiffres se
 référeront à des puissances de 2.

Ainsi, le nombre 101,11 en base 2 se décompose en:
- 1 fois 2^2
- 0 fois 2^1
- 1 fois 2^0
- 1 fois 2^-1
- 1 fois 2^-2

Soit 4 + 0 + 1 + 1/2 + 1/4 ce qui fait 5,75 en base 10

Pour être certains que nous interprétons correctement les nombres,  nous noterons souvent la base en indice. 

Enfin, notons que le bit le plus à droite est appelé "bit de poids faible", et que celui le plus à gauche est appelé "bit de poids fort".


En informatique, les bases généralement utilisées sont la base 2 (qu'on appelle binaire), la base 8 ( qu'on appelle octale), la base 10 (qu'on appelle décimale) et la base 16. Pour cette dernière, on parle d'hexadécimal, et on remplace les "chiffres" de 10 à 15 par les lettres de a à f pour éviter toute confusion.

Enfin, notons qu'en python, on peut utiliser les préfixes 0b, 0o et 0x pour indiquer que des entiers sont exprimés en binaire, en octal ou en hexadécimal.

Et on peut aussi utiliser les fonctions bin, oct et hex pour obtenir la chaîne de caractères qui représente entier en binaire, en octal ou en hexadécimal.





1
00:00:00,884 --> 00:00:01,834
Dans cette vidéo,

2
00:00:01,934 --> 00:00:03,537
nous allons voir quels types d'opérations

3
00:00:03,637 --> 00:00:04,595
on peut appliquer

4
00:00:04,695 --> 00:00:06,444
à des nombres en binaire.

5
00:00:06,694 --> 00:00:08,337
Tout d'abord, nous pouvons effectuer

6
00:00:08,437 --> 00:00:10,042
des opérations arithmétiques classiques

7
00:00:10,142 --> 00:00:10,926
en binaire.

8
00:00:11,442 --> 00:00:14,813
Faisons la somme de 111 et 110.

9
00:00:15,300 --> 00:00:16,666
Comme en base 10,

10
00:00:16,766 --> 00:00:18,398
on commence par la colonne de droite.

11
00:00:18,498 --> 00:00:19,985
1 plus 0 égale 1.

12
00:00:20,482 --> 00:00:22,044
Passons à la colonne suivante

13
00:00:22,144 --> 00:00:23,533
1 plus 1 égale 2

14
00:00:23,633 --> 00:00:25,575
qui s'écrit 10 en binaire.

15
00:00:25,675 --> 00:00:28,078
On recopie donc le bit de poids faible 0

16
00:00:28,178 --> 00:00:29,057
dans la solution

17
00:00:29,157 --> 00:00:31,065
et on reporte le bit de poids fort 1.

18
00:00:31,516 --> 00:00:32,609
Pour la dernière colonne,

19
00:00:32,709 --> 00:00:35,279
1 plus 1 plus 1 égale 11 en binaire.

20
00:00:35,379 --> 00:00:37,926
On ajoute donc ces deux bits à la solution.

21
00:00:38,373 --> 00:00:40,337
De même, la soustraction,

22
00:00:40,437 --> 00:00:42,827
la multiplication et la division écrites

23
00:00:42,927 --> 00:00:44,269
fonctionnent toujours en binaire.

24
00:00:44,582 --> 00:00:46,683
Comme les symboles binaires 1 et 0

25
00:00:46,783 --> 00:00:48,500
peuvent être interprétés comme vrai et faux,

26
00:00:48,600 --> 00:00:51,135
on peut aussi utiliser des opérateurs logiques.

27
00:00:51,385 --> 00:00:52,859
Les quatre opérateurs de base

28
00:00:52,959 --> 00:00:55,159
sont le et, qu'on note and en Python,

29
00:00:55,259 --> 00:00:57,703
le ou inclusif, or en Python,

30
00:00:57,803 --> 00:01:00,617
le ou exclusif, accent circonflexe en Python,

31
00:01:00,717 --> 00:01:03,244
et la négation, not en Python.

32
00:01:03,673 --> 00:01:05,303
Pour définir ces opérateurs,

33
00:01:05,403 --> 00:01:07,298
on utilise une table de vérité.

34
00:01:07,627 --> 00:01:09,697
Voici celle de l'opérateur et.

35
00:01:09,797 --> 00:01:12,211
Elle donne la valeur de a et b

36
00:01:12,311 --> 00:01:14,534
pour toute combinaison de valeurs binaires

37
00:01:14,634 --> 00:01:15,735
a et b.

38
00:01:16,306 --> 00:01:18,775
La définition du et correspond à son intuition,

39
00:01:18,875 --> 00:01:20,480
a et b vaut 1

40
00:01:20,580 --> 00:01:23,543
uniquement quand a et b valent 1 en même temps.

41
00:01:23,989 --> 00:01:25,814
Voici les tables de vérité

42
00:01:25,914 --> 00:01:27,364
des trois autres opérateurs.

43
00:01:27,508 --> 00:01:30,400
Vous noterez la différence entre les deux types de ou

44
00:01:30,500 --> 00:01:33,572
et le fait que le not est un opérateur unaire.

45
00:01:34,179 --> 00:01:35,969
On peut maintenant appliquer ces opérateurs

46
00:01:36,069 --> 00:01:37,611
à des nombres en binaire bit à bit

47
00:01:37,711 --> 00:01:38,999
c'est-à-dire colonne par colonne.

48
00:01:39,146 --> 00:01:42,525
Voici un exemple avec le et de deux nombres binaires.

49
00:01:43,608 --> 00:01:46,410
Le langage Python dispose également d'opérateurs

50
00:01:46,510 --> 00:01:48,446
pour réaliser ces opérations.

51
00:01:50,787 --> 00:01:52,374
Notons que l'opérateur et

52
00:01:52,474 --> 00:01:54,684
permet de réaliser un masque.

53
00:01:54,784 --> 00:01:57,391
Cela revient à sélectionner certains bits d'un nombre

54
00:01:57,491 --> 00:01:59,690
et à remplacer les autres par des 0.

55
00:02:00,021 --> 00:02:01,751
Les bits sélectionnés sont ceux

56
00:02:01,851 --> 00:02:03,131
où le masque est à 1.

57
00:02:04,346 --> 00:02:06,665
Une autre opération très utile en binaire

58
00:02:06,765 --> 00:02:07,801
et dans les autres bases

59
00:02:07,901 --> 00:02:09,082
est le décalage.

60
00:02:09,182 --> 00:02:10,865
Le décalage à gauche

61
00:02:10,965 --> 00:02:13,460
revient à ajouter des 0 à droite du nombre

62
00:02:13,560 --> 00:02:15,592
de manière à décaler les chiffres vers la gauche.

63
00:02:15,692 --> 00:02:17,708
Supposons qu'on veuille, en base 10,

64
00:02:17,808 --> 00:02:19,867
multiplier le nombre 13 par 1000.

65
00:02:20,076 --> 00:02:22,630
Cela revient à multiplier par 10 exposant 3

66
00:02:22,730 --> 00:02:26,340
donc à décaler 13 de trois positions vers la gauche.

67
00:02:26,903 --> 00:02:28,478
Cela fonctionne également en binaire.

68
00:02:28,578 --> 00:02:31,512
Si l'on veut multiplier 1101 par 8,

69
00:02:31,612 --> 00:02:32,838
cela revient à un décalage

70
00:02:32,938 --> 00:02:34,616
de trois positions vers la gauche en binaire.

71
00:02:35,046 --> 00:02:36,466
On décale de trois positions

72
00:02:36,566 --> 00:02:38,611
car huit est égal à 2 exposant 3.

73
00:02:39,101 --> 00:02:40,390
De manière générale,

74
00:02:40,490 --> 00:02:42,595
ajouter un 0 à droite d'un nombre

75
00:02:42,695 --> 00:02:44,611
revient à multiplier par la base.

76
00:02:45,224 --> 00:02:46,490
De façon symétrique,

77
00:02:46,590 --> 00:02:47,700
le décalage à droite

78
00:02:47,800 --> 00:02:49,747
revient à effacer des chiffres de poids faible

79
00:02:49,847 --> 00:02:51,453
pour tout décaler vers la droite.

80
00:02:51,689 --> 00:02:53,544
Cela revient à diviser par la base

81
00:02:53,644 --> 00:02:55,035
et les chiffres effacés constituent

82
00:02:55,135 --> 00:02:56,619
le reste de la division.

83
00:02:56,719 --> 00:03:00,141
Par exemple, 1101 0110

84
00:03:00,241 --> 00:03:01,257
divisé par 8

85
00:03:01,357 --> 00:03:03,377
donne 11010

86
00:03:03,477 --> 00:03:05,683
avec un reste de 110.


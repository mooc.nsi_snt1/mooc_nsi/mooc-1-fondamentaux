1
00:00:01,067 --> 00:00:02,650
Dans la vidéo précédente,

2
00:00:02,750 --> 00:00:04,160
nous avons vu qu'un même nombre

3
00:00:04,260 --> 00:00:06,332
peut être exprimé dans plusieurs bases.

4
00:00:06,432 --> 00:00:08,077
Dans cette vidéo, nous allons voir

5
00:00:08,177 --> 00:00:09,264
comment changer de base

6
00:00:09,364 --> 00:00:10,648
pour exprimer un nombre

7
00:00:10,748 --> 00:00:12,250
dans une base b donnée.

8
00:00:12,757 --> 00:00:14,422
Pour comprendre la technique,

9
00:00:14,522 --> 00:00:15,810
nous allons voir un exemple

10
00:00:15,910 --> 00:00:17,819
où l'on veut exprimer la valeur 13

11
00:00:17,919 --> 00:00:19,194
en base 2.

12
00:00:19,776 --> 00:00:21,036
Pour ce faire,

13
00:00:21,136 --> 00:00:23,815
nous allons commencer par diviser 13 par 2.

14
00:00:23,915 --> 00:00:26,204
Nous allons faire une division entière.

15
00:00:26,304 --> 00:00:27,955
Nous obtenons donc comme résultat

16
00:00:28,055 --> 00:00:28,871
la valeur 6

17
00:00:28,971 --> 00:00:30,041
et un reste de 1.

18
00:00:30,262 --> 00:00:31,772
Comme nous divisons par 2,

19
00:00:31,872 --> 00:00:33,350
nous pouvons observer que le reste

20
00:00:33,450 --> 00:00:35,829
sera toujours soit 0, soit 1.

21
00:00:36,242 --> 00:00:38,575
Cette division nous permet d'exprimer la valeur 13

22
00:00:38,675 --> 00:00:40,014
comme 1, qui est le reste

23
00:00:40,114 --> 00:00:41,179
plus 6 fois 2.

24
00:00:41,667 --> 00:00:43,339
Nous allons maintenant recommencer

25
00:00:43,439 --> 00:00:45,528
en divisant à nouveau par 2 le résultat 6.

26
00:00:45,628 --> 00:00:47,901
Nous obtenons 3 et un reste de 0.

27
00:00:48,317 --> 00:00:51,883
Cela nous permet d'affirmer que 6 vaut 0 plus 3 fois 2.

28
00:00:51,983 --> 00:00:54,515
En injectant ce résultat dans l'expression de 13

29
00:00:54,615 --> 00:00:55,634
trouvée précédemment,

30
00:00:55,734 --> 00:00:57,426
on voit maintenant que 13 vaut 1

31
00:00:57,526 --> 00:00:58,502
plus 0 fois 2

32
00:00:58,602 --> 00:01:00,667
plus 3 fois 2 exposant 2.

33
00:01:00,767 --> 00:01:03,014
Et on voit que les restes qui sont en rouge sur l'écran

34
00:01:03,114 --> 00:01:05,483
apparaissent multipliés par des puissances de 2

35
00:01:05,583 --> 00:01:07,499
dans cette décomposition que nous sommes occupés à calculer.

36
00:01:08,013 --> 00:01:09,348
On continue de cette façon.

37
00:01:09,458 --> 00:01:11,956
3 divisé par 2 donne 1 reste 1

38
00:01:12,056 --> 00:01:13,520
qu'on reporte dans l'expression de 13.

39
00:01:13,620 --> 00:01:15,640
Et finalement, 1 divisé par 2

40
00:01:15,740 --> 00:01:17,538
donne 0 reste 1.

41
00:01:17,842 --> 00:01:20,436
L'expression qu'on obtient de la valeur d'origine 13

42
00:01:20,536 --> 00:01:22,089
est bien une décomposition

43
00:01:22,189 --> 00:01:24,483
en puissances de cette nouvelle base 2.

44
00:01:24,634 --> 00:01:28,565
Et on voit donc que 13 s'exprime comme 1101 en base 2,

45
00:01:28,665 --> 00:01:30,923
ce qui revient à lire la suite des restes calculés

46
00:01:31,023 --> 00:01:32,740
du dernier au premier.

47
00:01:33,399 --> 00:01:35,327
Certains changements de base sont plus faciles

48
00:01:35,427 --> 00:01:36,364
quand on passe à une base

49
00:01:36,464 --> 00:01:38,021
qui est une puissance de la base d'origine.

50
00:01:38,497 --> 00:01:40,476
C'est le cas avec les bases usuelles en informatique

51
00:01:40,576 --> 00:01:43,441
à savoir l'hexadécimal, l'octal et le binaire.

52
00:01:43,541 --> 00:01:45,802
En effet, 16 vaut 2 exposant 4

53
00:01:45,902 --> 00:01:47,271
et 8 vaut 2 exposant 3.

54
00:01:47,724 --> 00:01:50,876
Convertir de la base 2 vers les bases 8 et 16

55
00:01:50,976 --> 00:01:51,744
et vice-versa

56
00:01:51,844 --> 00:01:53,081
est donc beaucoup plus simple.

57
00:01:53,484 --> 00:01:56,004
Par exemple, si on veut convertir le nombre binaire

58
00:01:56,104 --> 00:01:58,600
1101111 en base 8,

59
00:01:58,700 --> 00:02:01,566
on commence par diviser ce nombre en paquets de 3 bits

60
00:02:01,666 --> 00:02:03,318
en commençant par le bit de poids faible.

61
00:02:03,418 --> 00:02:05,037
On choisit des paquets de 3 bits 

62
00:02:05,137 --> 00:02:06,646
car 8 vaut 2 exposant 3.

63
00:02:06,944 --> 00:02:09,519
On convertit ensuite chacun de ces paquets

64
00:02:09,619 --> 00:02:10,545
en un chiffre en octal.

65
00:02:10,645 --> 00:02:12,948
Par exemple, 111 en binaire

66
00:02:13,048 --> 00:02:14,397
se représente 7 en octal.

67
00:02:14,497 --> 00:02:15,147
Et ainsi de suite.

68
00:02:15,247 --> 00:02:16,314
Et on obtient le résultat

69
00:02:16,414 --> 00:02:18,163
157 en base 8.

70
00:02:18,756 --> 00:02:20,149
Pour la base 16,

71
00:02:20,249 --> 00:02:21,656
on procède en groupant les bits

72
00:02:21,756 --> 00:02:22,612
par paquets de 4

73
00:02:22,712 --> 00:02:24,500
puisque 16 vaut 2 exposant 4.

74
00:02:24,600 --> 00:02:27,430
Et on trouve que 1101111

75
00:02:27,530 --> 00:02:29,759
se note 6F en hexadécimal.


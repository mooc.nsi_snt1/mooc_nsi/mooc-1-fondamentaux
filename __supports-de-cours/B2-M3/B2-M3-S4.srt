1
00:00:00,000 --> 00:00:01,559
Les exemples précédents

2
00:00:01,659 --> 00:00:03,112
montrent qu'il est souvent très difficile

3
00:00:03,212 --> 00:00:04,793
de calculer le temps moyen ou maximal

4
00:00:04,893 --> 00:00:06,159
d'exécution d'un algorithme.

5
00:00:06,508 --> 00:00:08,004
Ces calculs précis

6
00:00:08,104 --> 00:00:09,177
sont de plus inutiles

7
00:00:09,277 --> 00:00:10,544
puisqu'ils se basent

8
00:00:10,644 --> 00:00:12,203
sur des approximations grossières

9
00:00:12,303 --> 00:00:14,275
du temps d'exécution des actions élémentaires,

10
00:00:14,375 --> 00:00:17,038
évaluations qui ne tiennent pas non plus compte

11
00:00:17,138 --> 00:00:18,500
par exemple de l'ordinateur,

12
00:00:18,600 --> 00:00:20,405
de l'interpréteur utilisé, et caetera.

13
00:00:20,823 --> 00:00:23,044
Une évaluation du temps d'exécution d'un algorithme

14
00:00:23,144 --> 00:00:24,609
ne devient généralement intéressante

15
00:00:24,709 --> 00:00:26,328
que lorsque la taille du jeu de données,

16
00:00:26,428 --> 00:00:28,836
c'est-à-dire n dans nos exemples,

17
00:00:28,936 --> 00:00:29,708
est grande.

18
00:00:29,963 --> 00:00:31,686
En effet, pour n petit,

19
00:00:31,786 --> 00:00:33,598
les algorithmes s'exécutent généralement

20
00:00:33,698 --> 00:00:34,623
très rapidement.

21
00:00:34,964 --> 00:00:38,024
Supposons que la complexité maximale d'un algorithme A

22
00:00:38,124 --> 00:00:39,924
soit égale à 100 fois n,

23
00:00:40,024 --> 00:00:43,450
la complexité d'un algorithme B

24
00:00:43,550 --> 00:00:45,900
soit au maximum de 15 fois n carré,

25
00:00:46,000 --> 00:00:47,462
celle de C, n exposant 4,

26
00:00:47,562 --> 00:00:49,954
et celle de D, 2 exposant n.

27
00:00:51,276 --> 00:00:53,416
Le graphique montre qu'à partir

28
00:00:53,516 --> 00:00:55,736
de n valant quelques dizaines,

29
00:00:55,836 --> 00:00:58,681
2 exposant n prend le pas sur n exposant 4

30
00:00:58,781 --> 00:01:01,041
qui prend le pas sur 15 n carré,

31
00:01:01,141 --> 00:01:02,992
et encore sur 100 fois n.

32
00:01:03,970 --> 00:01:05,392
La table suivante

33
00:01:05,492 --> 00:01:07,178
donne les valeurs de complexité

34
00:01:07,278 --> 00:01:12,635
pour n valant 1, 10, 100, mille, dix-mille, un million et un milliard.

35
00:01:13,932 --> 00:01:15,900
Ces valeurs doivent être divisées

36
00:01:16,000 --> 00:01:17,890
par le nombre d'instructions élémentaires par seconde

37
00:01:17,990 --> 00:01:20,050
pour obtenir un temps exprimé en secondes.

38
00:01:21,010 --> 00:01:22,685
Les microprocesseurs actuels

39
00:01:22,785 --> 00:01:24,236
permettent d'exécuter de l'ordre de

40
00:01:24,336 --> 00:01:27,051
10 exposant 9 instructions par seconde,

41
00:01:27,151 --> 00:01:30,471
soit environ 10 exposant 14 instructions par jour.

42
00:01:30,571 --> 00:01:32,364
Cela signifie qu'en un jour,

43
00:01:32,464 --> 00:01:34,222
si aucune autre limitation ne perturbe

44
00:01:34,322 --> 00:01:35,868
l'exécution des algorithmes,

45
00:01:36,194 --> 00:01:38,493
par exemple, l'espace mémoire disponible,

46
00:01:38,593 --> 00:01:39,072
et cætera,

47
00:01:39,172 --> 00:01:40,691
avec un microprocesseur,

48
00:01:40,791 --> 00:01:43,194
on peut résoudre, avec l'algorithme A,

49
00:01:43,994 --> 00:01:45,301
un problème pour n

50
00:01:45,401 --> 00:01:48,161
valant approximativement 10 exposant 12,

51
00:01:48,261 --> 00:01:51,357
B, un problème valant approximativement

52
00:01:51,457 --> 00:01:52,594
10 exposant 7,

53
00:01:52,694 --> 00:01:55,525
C, approximativement 3 000

54
00:01:55,625 --> 00:01:57,578
et pour D, un problème

55
00:01:57,678 --> 00:01:59,444
pour un n valant 50.

56
00:01:59,710 --> 00:02:02,540
Et donc A est meilleur que B,

57
00:02:02,640 --> 00:02:03,662
qui est meilleur que C,

58
00:02:03,762 --> 00:02:05,770
lui-même étant meilleur que D,

59
00:02:06,364 --> 00:02:08,356
sauf pour des petites valeurs de n évidemment.

60
00:02:08,456 --> 00:02:09,984
Meilleur voulant dire qu'il prend

61
00:02:10,084 --> 00:02:12,003
moins de temps pour s'exécuter

62
00:02:12,103 --> 00:02:14,814
ou peut traiter, dans un certain temps,

63
00:02:14,914 --> 00:02:16,083
plus de données.

64
00:02:16,744 --> 00:02:17,927
De façon générale,

65
00:02:18,027 --> 00:02:20,167
quand on regarde les complexités,

66
00:02:20,267 --> 00:02:22,178
on voit un classement dans les fonctions,

67
00:02:22,278 --> 00:02:25,107
n exposant n étant plus grand

68
00:02:25,207 --> 00:02:26,582
que 2 exposant n,

69
00:02:26,682 --> 00:02:29,251
lui-même plus grand que n exposant 3,

70
00:02:30,196 --> 00:02:32,333
supérieur à n exposant 2,

71
00:02:32,433 --> 00:02:33,884
plus grand que n log n,

72
00:02:33,984 --> 00:02:34,714
plus grand que n,

73
00:02:34,814 --> 00:02:36,160
plus grand que log n

74
00:02:36,260 --> 00:02:37,640
et évidemment plus grand que 1.

75
00:02:38,319 --> 00:02:40,513
En vertu des remarques précédentes,

76
00:02:40,613 --> 00:02:41,537
et de ces chiffres,

77
00:02:41,637 --> 00:02:43,579
on préfère donner un ordre de grandeur

78
00:02:43,679 --> 00:02:45,812
permettant d'avoir une idée du type d'algorithme

79
00:02:45,912 --> 00:02:47,643
en terme de complexité.

80
00:02:48,074 --> 00:02:50,856
Pour cela, on utilise la notation O

81
00:02:50,956 --> 00:02:52,345
qui permet de déterminer

82
00:02:52,445 --> 00:02:55,111
si un algorithme a une chance de s'exécuter

83
00:02:55,361 --> 00:02:57,065
pour un n donné,

84
00:02:57,371 --> 00:02:59,636
et connaissant le temps d'exécution

85
00:02:59,736 --> 00:03:01,904
pour un n donné, de faire une approximation

86
00:03:02,004 --> 00:03:04,051
de ce temps pour une autre valeur de n.

87
00:03:04,151 --> 00:03:05,377
La notion de O

88
00:03:05,477 --> 00:03:06,498
est définie comme suit.

89
00:03:07,074 --> 00:03:09,497
Une complexité T(n)

90
00:03:09,597 --> 00:03:11,806
est en O(f(n))

91
00:03:11,906 --> 00:03:16,121
s'il existe des valeurs naturelles N et c

92
00:03:16,221 --> 00:03:17,870
strictement supérieur à 0

93
00:03:17,970 --> 00:03:21,171
pour que tout n supérieur à N,

94
00:03:21,271 --> 00:03:25,224
T(n) soit inférieur ou égal à c fois f(n).

95
00:03:25,324 --> 00:03:27,176
Cette définition permet d'effectuer

96
00:03:27,276 --> 00:03:29,989
de nombreuses simplifications dans les calculs.

97
00:03:30,089 --> 00:03:32,808
En effet, de cette définition, il résulte

98
00:03:32,908 --> 00:03:35,198
que les facteurs constants ne sont pas importants,

99
00:03:35,298 --> 00:03:40,439
par exemple, si T(n) est égal à n ou est égal à 100 fois n,

100
00:03:40,539 --> 00:03:42,294
T(n) est toujours en O(n),

101
00:03:42,795 --> 00:03:45,329
et que les termes d'ordre inférieur sont négligeables.

102
00:03:45,459 --> 00:03:51,277
Par exemple, si T(n) est égal à n cube  + 4 n carré + 20 n + 100,

103
00:03:51,377 --> 00:03:55,512
on peut voir que pour un n supérieur à 5,

104
00:03:55,612 --> 00:03:58,301
n cube est supérieur à 4 n carré,

105
00:03:58,401 --> 00:04:01,755
n cube est supérieur à 20 n et n cube est supérieur à 100.

106
00:04:01,855 --> 00:04:05,092
Et donc, pour un n supérieur à 5,

107
00:04:05,192 --> 00:04:09,414
T(n) est inférieur à 4 fois n cube.

108
00:04:09,514 --> 00:04:11,277
Et donc, de ce fait,

109
00:04:11,377 --> 00:04:13,895
T(n) est en O(n cube).


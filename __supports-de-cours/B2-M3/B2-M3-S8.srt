1
00:00:00,000 --> 00:00:02,456
Les complexités de structures de données Python

2
00:00:02,556 --> 00:00:04,244
telles que des listes ou des dictionnaires

3
00:00:04,900 --> 00:00:08,048
dépendent de l'implémentation utilisée.

4
00:00:08,148 --> 00:00:09,844
Si on prend l'implémentation CPython

5
00:00:09,944 --> 00:00:12,791
qui est l'implémentation utilisée de façon classique,

6
00:00:13,611 --> 00:00:15,410
de façon interne, une liste est représentée

7
00:00:15,510 --> 00:00:16,492
sous forme de vecteur

8
00:00:16,592 --> 00:00:18,646
où les composantes sont contiguës en mémoire

9
00:00:18,746 --> 00:00:19,815
et avec en moyenne,

10
00:00:19,915 --> 00:00:21,995
de la place libre pour allonger la liste.

11
00:00:22,714 --> 00:00:25,493
La pire opération est donc une insertion

12
00:00:25,593 --> 00:00:27,864
ou une suppression en début de liste,

13
00:00:27,964 --> 00:00:30,261
car toutes les composantes doivent bouger.

14
00:00:30,540 --> 00:00:32,597
Malheureusement, au pire des cas,

15
00:00:33,058 --> 00:00:34,301
un simple append

16
00:00:34,401 --> 00:00:36,694
peut demander de recopier toute la liste

17
00:00:37,424 --> 00:00:39,914
s'il n'y a pas de place libre juste après la liste.

18
00:00:40,330 --> 00:00:43,661
Le tableau suivant donne les complexités moyennes

19
00:00:43,761 --> 00:00:45,950
et maximales des différentes opérations

20
00:00:46,050 --> 00:00:49,465
en supposant qu'on a une liste de taille n

21
00:00:49,565 --> 00:00:52,190
et que, si on manipule des slices,

22
00:00:52,290 --> 00:00:54,270
les slices ont une taille k.

23
00:00:54,370 --> 00:00:56,311
On peut en particulier voir

24
00:00:56,411 --> 00:00:59,455
que le sort est en O de n log n

25
00:00:59,555 --> 00:01:01,474
puisque l'implémentation du sort,

26
00:01:01,574 --> 00:01:03,461
c'est l'implémentation du tri rapide

27
00:01:03,561 --> 00:01:05,522
qui est un tri récursif

28
00:01:05,622 --> 00:01:08,459
et qui a une complexité remarquable.

29
00:01:10,962 --> 00:01:13,775
Toujours avec l'implémentation CPython utilisée,

30
00:01:14,245 --> 00:01:15,776
un dictionnaire Python est stocké

31
00:01:15,876 --> 00:01:17,061
sous forme de table.

32
00:01:17,498 --> 00:01:18,625
Alors pour simplifier,

33
00:01:18,725 --> 00:01:20,132
supposons que cette table

34
00:01:20,308 --> 00:01:21,818
ait max composantes,

35
00:01:21,918 --> 00:01:24,124
par exemple, max est égal à 100.

36
00:01:24,367 --> 00:01:26,512
Et donc, lorsqu'on crée un dictionnaire

37
00:01:26,612 --> 00:01:27,650
on crée cette table

38
00:01:27,750 --> 00:01:29,299
vide en quelque sorte

39
00:01:29,399 --> 00:01:30,654
avec 100 composantes.

40
00:01:30,754 --> 00:01:32,471
L'accès à une composante du dictionnaire,

41
00:01:32,571 --> 00:01:33,500
donc le getitem,

42
00:01:33,600 --> 00:01:34,541
se fait par hashing.

43
00:01:34,641 --> 00:01:36,300
On prend la clé,

44
00:01:36,400 --> 00:01:37,643
on fait le hash de la clé

45
00:01:37,743 --> 00:01:39,020
modulo 100 dans ce cas-ci,

46
00:01:39,120 --> 00:01:42,251
ça calcule un indice entre 0 et max - 1

47
00:01:42,351 --> 00:01:44,745
et en moyenne, cet indice correspond à l'emplacement

48
00:01:44,845 --> 00:01:49,153
dans la table où l'élément clé/valeur va être stocké.

49
00:01:49,650 --> 00:01:53,412
Cette opération, hashing, modulo et accès à la table,

50
00:01:53,512 --> 00:01:55,282
prend un temps qui est constant

51
00:01:55,382 --> 00:01:56,793
donc qui est en O(1).

52
00:01:57,628 --> 00:02:00,813
La complexité moyenne suppose que les collisions sont rares

53
00:02:00,913 --> 00:02:02,315
avec la fonction de hashage

54
00:02:02,415 --> 00:02:03,580
ce qui est généralement le cas.

55
00:02:04,130 --> 00:02:06,564
Le cas moyen suppose que les paramètres

56
00:02:06,664 --> 00:02:08,826
sont choisis uniformément aléatoirement

57
00:02:09,371 --> 00:02:10,619
parmi les clés possibles.

58
00:02:10,719 --> 00:02:12,961
Malheureusement, il peut y avoir des collisions.

59
00:02:13,506 --> 00:02:14,453
Au pire des cas,

60
00:02:15,026 --> 00:02:16,978
pour un dictionnaire de n éléments,

61
00:02:17,078 --> 00:02:19,101
l'accès à un élément

62
00:02:19,201 --> 00:02:21,992
pourrait avoir n -  1 collisions

63
00:02:23,420 --> 00:02:24,984
plus le dernier hashing

64
00:02:25,084 --> 00:02:26,966
qui prend donc un temps

65
00:02:27,066 --> 00:02:28,669
n fois O(1)

66
00:02:28,769 --> 00:02:30,474
donc un temps en O(n).

67
00:02:30,993 --> 00:02:33,168
En terme de complexité, placer un élément

68
00:02:33,268 --> 00:02:35,613
setitem, revient à y accéder,

69
00:02:35,713 --> 00:02:37,339
suivi du stockage de l'élément

70
00:02:37,509 --> 00:02:39,472
qui est en O(1) en moyenne

71
00:02:39,572 --> 00:02:42,186
et en O(n) en complexité maximale,

72
00:02:42,286 --> 00:02:45,777
soit la même complexité que l'accès à un élément.

73
00:02:47,222 --> 00:02:50,510
La table montre les différentes complexités

74
00:02:50,610 --> 00:02:52,321
où on se rend compte que, effectivement,

75
00:02:53,550 --> 00:02:55,844
beaucoup de manipulations sont en O(1)

76
00:02:55,944 --> 00:02:58,033
avec un dictionnaire, ce qui est remarquable,

77
00:02:58,133 --> 00:03:00,656
mais la complexité maximale

78
00:03:00,756 --> 00:03:01,851
est souvent en O(n).


1
00:00:05,634 --> 00:00:07,970
Nous continuons à travailler avec plusieurs tables.

2
00:00:08,070 --> 00:00:10,333
Ici, nous allons parler de jointures.

3
00:00:10,873 --> 00:00:13,344
La jointure est un produit cartésien en fait,

4
00:00:13,444 --> 00:00:16,691
c'est-à-dire que, avec ici deux tables,

5
00:00:16,791 --> 00:00:19,779
nous allons croiser chacun des enregistrements d'une table

6
00:00:19,879 --> 00:00:23,126
avec chacun des enregistrements de l'autre table.

7
00:00:23,226 --> 00:00:25,125
Alors évidemment, en faisant des filtres,

8
00:00:25,886 --> 00:00:29,941
et en gardant ou en ne gardant pas d'ailleurs

9
00:00:30,041 --> 00:00:31,532
chacun des croisements,

10
00:00:31,632 --> 00:00:32,804
et ce qui nous donne lieu

11
00:00:32,904 --> 00:00:34,580
c'est pour ça qu'on a jointures au pluriel

12
00:00:34,680 --> 00:00:36,105
puisqu'il y a plusieurs jointures possibles

13
00:00:36,205 --> 00:00:39,876
mais encore une fois, c'est un concept

14
00:00:39,976 --> 00:00:42,108
une notion que nous allons rencontrer

15
00:00:42,208 --> 00:00:44,624
au module sur les bases de données.

16
00:00:45,634 --> 00:00:50,045
Donc ici, nous allons voir une première jointure

17
00:00:50,145 --> 00:00:51,573
qui consiste en fait

18
00:00:51,673 --> 00:00:53,836
à extraire des informations

19
00:00:53,936 --> 00:00:55,164
croisées des deux tables

20
00:00:55,264 --> 00:00:56,366
en faisant un filtre.

21
00:00:56,466 --> 00:00:59,286
C'est-à-dire que tous les croisements ne nous seront pas utiles

22
00:00:59,750 --> 00:01:01,308
et donc nous allons filtrer.

23
00:01:01,408 --> 00:01:03,388
Par exemple, la question qu'on peut se poser,

24
00:01:03,488 --> 00:01:10,126
donc on a notre carnet de contacts commun

25
00:01:10,226 --> 00:01:12,474
que nous avons créé tout à l'heure

26
00:01:13,000 --> 00:01:15,059
des contacts de Pierre et de Marie,

27
00:01:15,159 --> 00:01:17,997
avec le nom, le prénom, le téléphone et l'email.

28
00:01:18,097 --> 00:01:22,083
Et puis Pierre a retrouvé un fichier

29
00:01:22,183 --> 00:01:24,304
qui contient une information supplémentaire

30
00:01:24,404 --> 00:01:25,142
qui est l'adresse.

31
00:01:25,242 --> 00:01:28,237
Et donc nous avons trois descripteurs communs

32
00:01:28,337 --> 00:01:30,947
avec le carnet

33
00:01:31,772 --> 00:01:33,584
qui sont donc le nom, le prénom et le téléphone.

34
00:01:33,684 --> 00:01:38,074
Et une information supplémentaire qui est l'adresse.

35
00:01:40,457 --> 00:01:42,826
Et donc la question qu'on se pose, c'est

36
00:01:43,194 --> 00:01:46,790
à quelle adresse email correspond chaque adresse postale ?

37
00:01:50,329 --> 00:01:53,196
Nous allons construire une requête

38
00:01:53,296 --> 00:01:56,938
où nous voulons récupérer les informations

39
00:01:57,038 --> 00:01:58,861
de l'adresse postale et de l'email

40
00:01:59,523 --> 00:02:01,506
mais évidemment, chacune de ces informations

41
00:02:01,606 --> 00:02:02,892
vient de deux tables différentes.

42
00:02:04,642 --> 00:02:05,970
Nous allons commencer par

43
00:02:06,433 --> 00:02:10,456
comme tout à l'heure par lire nos deux tables

44
00:02:10,556 --> 00:02:15,903
donc LC pour la table des emails

45
00:02:17,424 --> 00:02:23,003
et puis LA pour la table des adresses.

46
00:02:23,922 --> 00:02:26,368
Ce que nous voulons, c'est récupérer

47
00:02:27,458 --> 00:02:30,247
l'adresse postale et l'adresse email

48
00:02:30,347 --> 00:02:34,343
avec a qui parcourt l'ensemble de notre table,

49
00:02:34,443 --> 00:02:36,174
donc a est un enregistrement

50
00:02:36,274 --> 00:02:38,250
de la table des adresses,

51
00:02:38,350 --> 00:02:44,166
et c est un enregistrement de la table des emails.

52
00:02:45,905 --> 00:02:48,723
Et donc ici, cette double boucle fait que

53
00:02:48,823 --> 00:02:52,081
pour chaque enregistrement de la table LA

54
00:02:52,181 --> 00:02:56,287
on va parcourir chaque enregistrement de la table LC.

55
00:02:56,689 --> 00:02:59,895
C'est une opération qui est particulièrement coûteuse

56
00:02:59,995 --> 00:03:01,683
et qu'il faut absolument optimiser

57
00:03:01,783 --> 00:03:02,819
dès que c'est possible

58
00:03:02,919 --> 00:03:04,917
et surtout quand on travaille avec de vraies tables

59
00:03:05,017 --> 00:03:07,180
de taille réelle.

60
00:03:08,491 --> 00:03:12,174
Et ici, qu'est-ce qui fait qu'on ne va pas garder tous les enregistrements ?

61
00:03:12,274 --> 00:03:20,943
puisque si on exécutait la requête telle quelle,

62
00:03:21,043 --> 00:03:24,014
on aurait effectivement un croisement

63
00:03:24,114 --> 00:03:27,163
avec toutes les adresses et tous les emails possibles.

64
00:03:27,609 --> 00:03:29,860
C'est bien un produit cartésien.

65
00:03:29,960 --> 00:03:31,443
Ça, ça ne nous intéresse pas du tout

66
00:03:31,620 --> 00:03:32,865
et donc on va filtrer

67
00:03:32,965 --> 00:03:36,282
en gardant l'information pertinente.

68
00:03:36,382 --> 00:03:38,544
Quelle est la question qu'on se pose ?

69
00:03:38,644 --> 00:03:41,524
C'est qu'est-ce qui fait le lien entre la table des adresses

70
00:03:41,624 --> 00:03:42,622
et la table des emails ?

71
00:03:43,020 --> 00:03:45,213
Comme tout à l'heure, on va considérer

72
00:03:45,313 --> 00:03:48,786
que le couple nom et prénom constitue une clé

73
00:03:48,886 --> 00:03:54,581
et donc on va récupérer les croisements

74
00:03:54,681 --> 00:03:56,706
qui préservent le nom et le prénom,

75
00:03:56,806 --> 00:04:01,727
donc tels que le nom de la table des adresses

76
00:04:01,827 --> 00:04:04,799
est le même que le nom de la table des emails

77
00:04:04,899 --> 00:04:05,831
et pareil pour le prénom.

78
00:04:07,405 --> 00:04:09,162
Et effectivement, en filtrant comme cela,

79
00:04:09,262 --> 00:04:11,404
on ne garde que les croisements pertinents

80
00:04:11,504 --> 00:04:14,866
c'est-à-dire l'email de Michel avec son adresse,

81
00:04:14,966 --> 00:04:17,106
l'email d'Anaïs avec son adresse, et cætera.

82
00:04:17,707 --> 00:04:23,306
Donc voilà un exemple de jointure.

83
00:04:24,107 --> 00:04:27,528
On pourrait faire également une jointure

84
00:04:27,628 --> 00:04:31,859
donc considérer le croisement de ces deux tables

85
00:04:31,959 --> 00:04:34,936
pour évidemment, augmenter comme tout à l'heure

86
00:04:35,036 --> 00:04:40,157
nous avions augmenté la table de Pierre

87
00:04:40,257 --> 00:04:41,124
ou celle de Marie

88
00:04:41,224 --> 00:04:43,839
en rajoutant des enregistrements

89
00:04:43,939 --> 00:04:45,136
là, nous pourrions faire la même chose

90
00:04:45,236 --> 00:04:49,053
augmenter la table des emails

91
00:04:49,153 --> 00:04:50,563
en rajoutant un champ.

92
00:04:50,663 --> 00:04:54,248
Et en rajoutant donc un champ adresse.

93
00:04:55,094 --> 00:04:57,839
Évidemment, il faudrait faire une sorte de fusion

94
00:04:57,939 --> 00:05:03,015
et rajouter, dans le parcours de cette table,

95
00:05:03,115 --> 00:05:05,276
rajouter l'information d'adresse

96
00:05:05,376 --> 00:05:08,761
sur chacun des enregistrements de cette table.

97
00:05:10,527 --> 00:05:13,578
Voilà, nous avons manipulé ensemble

98
00:05:13,678 --> 00:05:14,753
les données en table,

99
00:05:14,853 --> 00:05:16,834
nous avons fait des tris, des fusions.

100
00:05:17,934 --> 00:05:19,627
Comme d'habitude, je vous conseille

101
00:05:19,727 --> 00:05:21,660
de relire le document écrit

102
00:05:21,772 --> 00:05:24,254
et de surtout beaucoup tester,

103
00:05:24,354 --> 00:05:26,489
de beaucoup faire par vous-mêmes

104
00:05:26,589 --> 00:05:27,952
ces manipulations.


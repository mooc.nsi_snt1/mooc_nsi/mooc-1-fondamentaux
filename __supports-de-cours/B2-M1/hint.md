# Quelques indications

## L'objet `Duree`

Cet objet comporte trois attributs qui sont des entiers et qui sont des paramètres de l'initialisateur.

Ne pas oublier le premier paramètre `self` :

```python
class Duree:

    def __init__(self, heure, minute, seconde):
        # attention ici penser à ramener minute et seconde dans des valeurs acceptables

        # à compléter pour initialiser vos attributs
        self.heure = ...
``` 

Cette première version de la classe `Duree`  va vous permettre de répondre aux questions 1 et 2 du quiz. Pour la question 3, il vous faudra définir l'opérateur `+`, c'est-à-dire la méthode `__add__` (le code ci-dessous est à compléter) :

```python
def __add__(self, d):
    """Crée une durée en sommant la durée appelante et la durée d"""
    return Duree(...)
``` 

Lorsqu'on regarde les données de la base _Pink Floyd_, on se rend compte que les durées sont représentées par des chaînes de caractères de la forme _mm:ss_ où _mm_ sont des minutes et _ss_ des secondes.

On peut alors se donner une méthode de classe qui permette la construction d'une `Duree`  à partir d'une telle chaîne de caractères. Voici un début possible pour une telle méthode :

```python
    @classmethod
    def duree(cls, duree_str, delimiter=':'):
        h, m, s = 0, 0, 0
        if delimiter in duree_str:
            duree_liste = duree_str.strip().split(delimiter)
            if len(duree_liste) == 3:
                h, m, s = duree_liste
            elif ...
            ...
            if h.isdigit() and m.isdigit() and s.isdigit():
                h, m, s = int(h), int(m), int(s)
        return ...
``` 

## L'objet `Titre` 

Celui-là est très simple, c'est surtout l'objet `PlayList` qui va l'utiliser.

```python
class Titre:

    def __init__(self, annee, album, titre, duree):
        # compléter pour définir les 4 attributs
``` 
La question 4 demande de créer une fonction de lecture de la base pour en créer une liste (au sens `list` de Python) des `Titre`. Voici le début de cette fonction :

```python
def lecture(filename, delimiter=';'):
    """Lit le fichier texte contenant les titres au format csv et retourne une liste de Titre"""
    liste_de_titres = []
    with open(filename, 'r', encoding='utf-8') as datas:
        for line in datas:
            # à compléter
``` 

C'est lors de la construction de l'objet `Titre`, au moment de traiter la chaîne de caractères représentant la durée du morceau qu'on utilisera un appel à la méthode de classe : `Duree.duree(duree_str)` 

## L'objet `PlayList` 

Il est défini par une liste de `Titre`  qui constitue la base de la _playlist_ et d'un ensemble d'identifiants (les indices des éléments de la liste de base) des titres qui constituent la _playlist_.

```python
class PlayList:

    def __init__(self, base, ids):
        self.base = base
        self.ids = set(ids)
``` 

l'action principale que nous allons effectuer est la constitution d'une _playlist_ commune entre deux _playlist_. On supposera pour simplifier que la base est bien la même. Compléter le code suivant avec la bonne opération ensembliste sur les identifiants :

```python
def commun(self, playlist):
    return PlayList(self.base, ...)
``` 


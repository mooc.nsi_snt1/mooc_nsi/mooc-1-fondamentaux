1
00:00:04,381 --> 00:00:05,480
Après le tableau,

2
00:00:05,580 --> 00:00:07,945
nous allons passer à notre deuxième type construit, le p-uplet,

3
00:00:08,045 --> 00:00:09,470
également appelé n-uplet.

4
00:00:09,708 --> 00:00:11,828
La différence fondamentale avec le tableau,

5
00:00:11,928 --> 00:00:13,938
c'est que le p-uplet n'est pas modifiable,

6
00:00:14,228 --> 00:00:15,335
non mutable.

7
00:00:15,852 --> 00:00:18,694
Sinon, il s'agit également d'une collection d'objets

8
00:00:18,794 --> 00:00:20,082
indexés par les entiers.

9
00:00:20,493 --> 00:00:22,346
Je vous propose de passer tout de suite

10
00:00:22,446 --> 00:00:23,689
dans un interprète interactif

11
00:00:23,789 --> 00:00:24,969
et de manipuler des tuples,

12
00:00:25,069 --> 00:00:27,128
qui est la version Python du p-uplet.

13
00:00:27,540 --> 00:00:29,972
Nous commençons donc notre manipulation des p-uplets

14
00:00:30,072 --> 00:00:32,927
par les différentes façon de construire un p-uplet.

15
00:00:33,027 --> 00:00:34,233
En extension d'abord,

16
00:00:34,333 --> 00:00:35,588
en listant les éléments

17
00:00:36,348 --> 00:00:38,269
et en les séparant par des virgules,

18
00:00:38,369 --> 00:00:40,229
les parenthèses sont facultatives

19
00:00:40,329 --> 00:00:41,911
sauf bien sûr s'il y a ambiguïté

20
00:00:42,011 --> 00:00:43,734
comme par exemple le cas

21
00:00:43,834 --> 00:00:45,842
où un des éléments est lui-même un tuple.

22
00:00:46,295 --> 00:00:49,725
Notez que lorsque Python nous affiche la valeur d'un tuple,

23
00:00:49,825 --> 00:00:50,980
il mettra toujours des parenthèses.

24
00:00:52,767 --> 00:00:54,506
La concaténation multiple est possible

25
00:00:54,606 --> 00:00:55,693
mais elle n'a que peu d'intérêt

26
00:00:55,793 --> 00:00:57,312
puisque le tuple est non mutable.

27
00:00:58,478 --> 00:01:01,494
Par contre, la concaténation à partir du tuple vide

28
00:01:02,105 --> 00:01:03,403
est utilisée.

29
00:01:03,843 --> 00:01:05,739
Voici un exemple

30
00:01:08,219 --> 00:01:10,611
où nous construisons un tuple,

31
00:01:10,711 --> 00:01:13,950
un triplet, à partir du tuple vide.

32
00:01:14,050 --> 00:01:17,042
Le tuple vide peut s'écrire comme ceci

33
00:01:17,142 --> 00:01:19,404
ou directement parenthèse ouvrante parenthèse fermante.

34
00:01:19,635 --> 00:01:22,053
Attention à la syntaxe du singleton,

35
00:01:22,153 --> 00:01:26,146
il ne faut pas oublier la virgule juste après l'élément

36
00:01:26,246 --> 00:01:28,930
sinon, ce ne sera pas un tuple.

37
00:01:29,522 --> 00:01:31,502
Donc ici, nous avons bien notre triplet.

38
00:01:31,879 --> 00:01:34,975
L'utilisation de la fonction tuple()

39
00:01:35,075 --> 00:01:37,500
avec un itérable,

40
00:01:37,600 --> 00:01:39,109
ici, par exemple, l'objet range

41
00:01:39,209 --> 00:01:40,463
qui va nous fournir

42
00:01:40,563 --> 00:01:44,466
les codes ASCII des caractères de A à Z.

43
00:01:45,758 --> 00:01:47,682
Et la fonction tuple()

44
00:01:47,782 --> 00:01:50,433
va nous construire notre tuple.

45
00:01:50,975 --> 00:01:54,415
Une deuxième utilisation, avec une chaîne de caractères ici

46
00:01:54,936 --> 00:01:58,018
et nous obtenons donc le tuple constitué

47
00:01:59,161 --> 00:02:00,661
de ses caractères.

48
00:02:01,176 --> 00:02:02,785
En compréhension,

49
00:02:02,885 --> 00:02:05,627
nous utilisons la même syntaxe

50
00:02:05,727 --> 00:02:08,160
avec l'expression génératrice

51
00:02:08,260 --> 00:02:11,044
mais nous devons coupler cette expression

52
00:02:11,144 --> 00:02:13,736
explicitement avec la fonction tuple()

53
00:02:13,836 --> 00:02:16,074
pour obtenir notre tuple,

54
00:02:16,174 --> 00:02:19,071
ici, celui des carrés de 1 à 100.

55
00:02:19,802 --> 00:02:22,086
En effet, si on n'utilise pas la fonction tuple()

56
00:02:22,186 --> 00:02:24,941
et qu'on croit construire un tuple de cette façon-là,

57
00:02:25,041 --> 00:02:26,495
simplement donc avec les parenthèses,

58
00:02:27,146 --> 00:02:28,587
il y a une erreur puisqu'en fait,

59
00:02:28,687 --> 00:02:31,176
nous obtenons un objet qui est un itérateur

60
00:02:31,276 --> 00:02:32,313
et qui n'est pas du tout un tuple,

61
00:02:32,413 --> 00:02:35,167
certes qui peut être utilisé par ailleurs.

62
00:02:36,855 --> 00:02:40,570
Je vous propose à partir de la prochaine vidéo

63
00:02:40,670 --> 00:02:42,547
de voir comment, concrètement,

64
00:02:42,647 --> 00:02:44,523
on manipule et on utilise les tuples

65
00:02:44,623 --> 00:02:46,062
dans des petits exemples.

66
00:02:47,184 --> 00:02:49,710
Voici quelques utilisations concrètes

67
00:02:49,810 --> 00:02:52,775
des p-uplets, des tuples de Python.

68
00:02:53,250 --> 00:02:55,950
Première utilisation, c'est lorsque

69
00:02:56,496 --> 00:02:59,089
on crée une fonction qui doit retourner plusieurs valeurs.

70
00:02:59,577 --> 00:03:02,424
On va retourner en réalité un tuple

71
00:03:02,524 --> 00:03:03,905
ici, par exemple, un couple

72
00:03:05,276 --> 00:03:08,952
de la valeur min et de la valeur max

73
00:03:09,052 --> 00:03:09,991
de notre tableau.

74
00:03:10,399 --> 00:03:12,117
Deuxième utilisation,

75
00:03:12,217 --> 00:03:15,531
c'est ce qu'on appelle l'affectation multiple Python.

76
00:03:15,631 --> 00:03:19,602
Ici, dans cette version itérative de Fibonacci,

77
00:03:21,086 --> 00:03:23,161
nous affectons les variables a et b

78
00:03:23,261 --> 00:03:25,056
aux valeurs initiales de la suite

79
00:03:25,156 --> 00:03:26,324
et ensuite, dans le corps de la boucle,

80
00:03:26,424 --> 00:03:28,674
nous réutilisons cette affectation multiple

81
00:03:28,920 --> 00:03:30,916
pour passer aux éléments suivants,

82
00:03:31,016 --> 00:03:33,601
donc a devient b, et b devient a + b.

83
00:03:34,686 --> 00:03:37,252
En Python, nous utilisons aussi cette affectation multiple

84
00:03:37,352 --> 00:03:40,383
par exemple, pour échanger le contenu de deux variables x, y

85
00:03:40,824 --> 00:03:42,531
en écrivant tout simplement

86
00:03:44,126 --> 00:03:46,960
x, y = y, x

87
00:03:48,390 --> 00:03:49,581
Le décompactage,

88
00:03:49,681 --> 00:03:50,922
c'est un petit peu le même principe.

89
00:03:51,022 --> 00:03:55,409
Donnons-nous ici un tableau de triplets

90
00:03:56,811 --> 00:03:59,860
et le décompactage ici utilisé dans la boucle for

91
00:03:59,960 --> 00:04:02,605
va permettre de parcourir nos triplets

92
00:04:04,022 --> 00:04:05,680
contenus dans le tableau langages

93
00:04:05,919 --> 00:04:10,855
mais plutôt que de manipuler une variable pour l'ensemble du triplet,

94
00:04:10,955 --> 00:04:16,354
nous décompactons au moment du parcours

95
00:04:16,454 --> 00:04:18,318
le triplet en trois éléments

96
00:04:18,925 --> 00:04:21,981
avec des noms explicites pour les variables,

97
00:04:22,081 --> 00:04:23,720
langage, prenom et nom

98
00:04:23,820 --> 00:04:25,885
qui nous permet d'obtenir

99
00:04:26,674 --> 00:04:29,883
une boucle assez élégante et facile à lire.

100
00:04:30,004 --> 00:04:32,408
À comparer par exemple avec cette version

101
00:04:32,508 --> 00:04:34,758
où nous parcourons les indices d'une part

102
00:04:34,858 --> 00:04:36,891
et nous allons récupérer

103
00:04:38,036 --> 00:04:41,835
langages[i] correspond au triplet

104
00:04:41,935 --> 00:04:43,814
et nous sommes obligés de rajouter

105
00:04:43,914 --> 00:04:47,221
une deuxième utilisation de l'opérateur crochet

106
00:04:47,321 --> 00:04:49,384
pour obtenir chacune de nos composantes.

107
00:04:50,023 --> 00:04:52,706
On peut noter une différence de lisibilité

108
00:04:52,806 --> 00:04:54,730
évidente entre les deux versions.

109
00:04:56,755 --> 00:04:59,596
Une deuxième utilisation du décompactage

110
00:05:01,102 --> 00:05:02,933
qui est souvent utilisée

111
00:05:03,033 --> 00:05:04,705
nous manipulons des points géométriques

112
00:05:04,805 --> 00:05:09,715
et ici, nous décompactons les couples pt_a, pt_b

113
00:05:09,815 --> 00:05:11,854
pour obtenir leurs coordonnées

114
00:05:11,954 --> 00:05:13,365
et faire le calcul de la distance

115
00:05:13,465 --> 00:05:14,716
de façon plus lisible

116
00:05:14,816 --> 00:05:18,862
plutôt que d'utiliser l'opérateur crochet.

117
00:05:20,240 --> 00:05:21,629
Le décompactage se fait aussi

118
00:05:21,729 --> 00:05:23,934
lorsque l'on appelle une fonction

119
00:05:24,034 --> 00:05:26,091
qui retourne un p-uplet.

120
00:05:26,642 --> 00:05:29,825
Plutôt que de mettre une simple variable

121
00:05:29,925 --> 00:05:33,274
pour récupérer le p-uplet en question,

122
00:05:33,438 --> 00:05:35,279
on peut bien sûr décompacter

123
00:05:35,379 --> 00:05:37,676
et nommer chacune des composantes du p-uplet.

124
00:05:45,162 --> 00:05:47,564
Enfin, la dernière utilisation du décompactage,

125
00:05:47,664 --> 00:05:51,338
c'est lors de l'utilisation de la fonction prédéfinie enumerate.

126
00:05:51,845 --> 00:05:54,745
enumerate, si on lui donne un itérable,

127
00:05:54,845 --> 00:05:57,741
va nous renvoyer un couple,

128
00:05:57,841 --> 00:06:01,502
le premier est l'indice de la valeur et le deuxième est la valeur.

129
00:06:01,602 --> 00:06:03,058
Ça peut être parfois bien utile

130
00:06:03,158 --> 00:06:06,557
pour avoir à la fois l'indice et à la fois la valeur

131
00:06:06,657 --> 00:06:08,532
de l'itérable qu'on parcourt.

132
00:06:10,590 --> 00:06:13,242
Dernière utilisation du tuple,

133
00:06:13,783 --> 00:06:17,073
c'est lors de la définition d'une fonction

134
00:06:17,173 --> 00:06:19,860
qui accepte un nombre variable de paramètres.

135
00:06:21,212 --> 00:06:24,400
La syntaxe, c'est *args,

136
00:06:24,500 --> 00:06:26,028
le nom args est une convention,

137
00:06:29,497 --> 00:06:30,747
et à ce moment-là,

138
00:06:31,940 --> 00:06:34,249
args est un p-uplet,

139
00:06:34,349 --> 00:06:36,241
est un tuple qu'on peut parcourir,

140
00:06:36,341 --> 00:06:38,167
ici par exemple, pour faire donc la somme

141
00:06:38,267 --> 00:06:39,603
des différents éléments.

142
00:06:41,972 --> 00:06:46,430
Voilà, cela conclut notre manipulation des tuples

143
00:06:46,530 --> 00:06:48,602
et nous allons passer maintenant

144
00:06:48,702 --> 00:06:50,526
au troisième et dernier type construit,

145
00:06:50,626 --> 00:06:51,561
le dictionnaire.


# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 2.2.6.2 : Langages impératifs

Résumons ici les caractéristiques des langages impératifs et dérivés

Nous avons déjà parlés des langages impératifs 

Cette  famille, de loin la plus répandue, est à l’origine  une formalisation plus lisible par un humain du code machine binaire ou de sa version textuelle en langage d’assemblage.

Un programme impératif qui s’exécute n’est qu’une suite de modifications de l’état de la mémoire en modifiant une donnée ou une variable, et l'indication de l'instruction suivante.

Le nouvel état d’une variable est lié à une évaluation : une opération arithmétique, logique ou booléenne effectuée sur une ou plusieurs variables.

Le jeu d’instructions des langages impératifs, quoique formel et a priori universel, reste proche de celui d’un langage de bas niveau,

mais les adresses et les registres disparaissent au profit de variables — y compris de pointeurs parfois — et d’expressions, donc de valeurs temporaires invisibles et anonymes.

Très rapidement, deux améliorations des langages impératifs ont été introduites avec les langages structurés et procéduraux.

D'abord, la programmation structurée constitue un sous-ensemble de la programmation impérative.

Elle recommande une organisation hiérarchique simple du code.

Pour faire simple, on y trouve généralement les structures de contrôles while, repeat, for, if .. then .. else.  

Ensuite les langages procéduraux, comme on l'a déjà vu, sont des langages de programmation impératifs qui permettent une découpe du code en fonctions ou un sous-programme parmi les nombreuses variantes.




Parlons maintenant des langages orientés objet

dont nous avons déjà présenté certains aspects en Python.

Les langages orienté-objet sont les plus utilisés aujourd’hui;

Ils permettent d'utiliser le paradigme de programmation par objets, qui change la façon de concevoir les programmes informatiques via l’assemblage de briques logicielles appelées objets.


L'histoire des langages orientés objets commence en 1967.

En implantant les Record Class de Hoare, le langage Simula 67 pose les constructions qui seront celles des langages orientés objet à classes :
classe, polymorphisme, héritage, etc.

Mais c'est réellement par et avec Smalltalk 71 puis Smalltalk 80, résultat des travaux d'Alan Kay,
inspiré en grande partie par Simula 67 et Lisp,
que les principes de la programmation par objets sont pleinement définis :
objet,
encapsulation,
messages,
typage
et polymorphisme via la sous-classification ;

les autres principes, comme l'héritage, sont dérivés de ceux-ci.

À partir des années 80, commence l'effervescence des langages à objets :

C++ (en 1983),
Objective-C (en 1984),
Eiffel (en 1986),
Common Lisp Object System (en 1988),
etc.

Les années quatre-vingt dix voient l'âge d'or de l'extension de la programmation par objets dans les différents secteurs du développement logiciel.



Essayons de résumer les principes des langages orienté-objet

Concrètement, un objet est une structure de données qui répond à un ensemble de messages.

Cette structure de données définit son état tandis que l'ensemble des messages qu'il comprend décrit son comportement :

- les données, ou champs, qui décrivent sa structure interne sont appelées ses attributs ;

- l'ensemble des messages forme ce que l'on appelle l'interface de l'objet ;

c'est seulement au travers de celle-ci que les objets interagissent entre eux.

Une méthode donne la réponse au message reçu.


 Le principe d'encapsulation consiste à cacher certains attributs ou méthodes.

Ainsi, le programme peut modifier la structure interne des objets ou leurs méthodes associées sans avoir d'impact sur les utilisateurs de l'objet.


Dans la programmation par objets, chaque objet est typé.

Le polymorphisme  permet d'utiliser des objets de types différents là où est attendu un objet d'un certain type.

Une façon de réaliser le polymorphisme est le sous-typage appelé aussi héritage de type :

on raffine un type-père en un autre type par des restrictions sur les valeurs possibles des attributs.

Notons qu'il existe d'autres types de polymorphisme.


On distingue dans les langages objets deux mécanismes de sous-typage :

l'héritage simple (comme c'est le cas avec Smalltalk, Java, C#)

et l'héritage multiple (comme c'est le cas avec C++, Python, Common Lisp, Eiffel).

Notons également le principe de redéfinition des messages (ou overriding en anglais) qui permet à un objet de raffiner une méthode des objets d'un type parent.

Enfin, parmi les critères de classement des langages orientés-objet, on distingue les langages orientés-objets purs ou non.

Dans un langage orienté objet pur, toute la syntaxe et la sémantique sont construites autour de la notion d’objet, sans exception :

tous les types, même les primitifs, sont des classes,
toutes les opérations, même prédéfinies, sont des méthodes de classes,
toutes les classes sont des objets, instances de métaclasses,
et le code du programme lui-même est un objet ;

il y a donc réflexivité complète.

Des exemples sont Smalltalk, évidemment, mais aussi Ruby, Raku, Self, ou encore Eiffel.


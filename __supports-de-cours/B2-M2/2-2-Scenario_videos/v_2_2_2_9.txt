


2_2_2_9
Les langages déclaratifs




Parlons maintenant des Langages déclaratifs 

Contrairement aux langages impératifs qui décrivent COMMENT faire pour calculer un résultat,
les langages déclaratifs décrivent CE qu’il faut calculer.

Les langages déclaratifs ont donc une syntaxe très différente de celle des langages impératifs.

Fini les boucles, les tests, les « CALL-RETURN » et vive les fonctions « mathématiques », les variables formelles , etc. 


Cette grande et très ancienne famille
puisque LISP date de 1958
peut être divisée en  :

- langages fonctionnels et
- langages logiques


Les premiers langages déclaratifs,
tant en nombre qu’historiquement,
sont dits « fonctionnels ».

Ils sont issus des travaux de John McCarthy qui,
dès 1960,
sur base des premiers développements de LISP,
proposait une définition de langages fondés sur la notation du λ-calcul ,
inventé vers 1930 par Alonzo Church.

Un langage fonctionnel est
un langage dans lequel un programme est décrit par composition de fonctions. 

Le langage LISP en a été le point de départ,
un sujet d’étude propre
et une inspiration riche et fertile pour d’autres.

Puis, cette famille de langages est tombée partiellement en désuétude
en partie pour des raisons d'efficacité d'exéution de ses programmes.

Mais en mille neuf cent soixante dix-huit,
John Backus,
pourtant connu comme le co-créateur de FORTRAN
et pour ses travaux précurseurs sur la définition et la compilation des langages impératifs,
publie une longue communication

Can Programming Be Liberated
from the von Neumann Style ?
A Functional Style
and Its Algebra of Programs

qui lui valut un Turing Award
et relança véritablement l’intérêt
pour cette famille de langages
et le développement d’outils plus performants.

Aujourd’hui, il existe effectivement de nombreux langages fonctionnels qui peuvent,
dans certaines circonstances,
rivaliser en efficacité avec les langages impératifs.

LISP a été développé  initialement comme un simple outil de manipulation de listes généralisées.

Sa syntaxe y reste toujours attachée :
tout y est une liste simple d’atomes ou de sous-listes.

Une brève explication sur Lisp accompagnés de quelques exemples sont disponibles dans le support écrit.


D’autres langages fonctionnels ont bien sûr été créés,
notamment ML en mille neuf cent soixante treize,
un langage fortement et statiquement typé,
surtout connu par son dialecte Caml
et son lointain descendant orienté objet : OCaml apparu
en mille neuf cent quatre-vingt seize.

Mais également Scheme, Erlang, Haskell, etcetera


1
00:00:00,405 --> 00:00:01,999
Résumons ici les caractéristiques

2
00:00:02,099 --> 00:00:03,459
des langages impératifs et dérivés.

3
00:00:04,358 --> 00:00:06,441
Nous avons déjà parlé des langages impératifs.

4
00:00:06,641 --> 00:00:07,820
Cette famille,

5
00:00:07,920 --> 00:00:09,648
de loin la plus répandue,

6
00:00:09,748 --> 00:00:11,346
est à l'origine d'une formalisation

7
00:00:11,446 --> 00:00:12,908
plus lisible par un humain

8
00:00:13,008 --> 00:00:14,475
du code machine binaire

9
00:00:14,575 --> 00:00:16,418
ou de sa version textuelle

10
00:00:16,518 --> 00:00:17,694
en langage d'assemblage.

11
00:00:17,794 --> 00:00:19,476
Un programme impératif qui s'exécute

12
00:00:19,576 --> 00:00:21,066
n'est qu'une suite de modifications

13
00:00:21,166 --> 00:00:22,145
de l'état de la mémoire

14
00:00:22,245 --> 00:00:24,087
modifiant une donnée ou une variable

15
00:00:24,187 --> 00:00:26,376
et l'indication de l'instruction suivante.

16
00:00:26,692 --> 00:00:28,298
Le nouvel état d'une variable

17
00:00:28,398 --> 00:00:30,192
est lié à une évaluation,

18
00:00:30,292 --> 00:00:32,033
une opération arithmétique, logique ou booléenne

19
00:00:32,133 --> 00:00:34,599
effectuée sur une ou plusieurs variables.

20
00:00:34,699 --> 00:00:35,803
Le jeu d'instructions

21
00:00:35,903 --> 00:00:36,721
des langages impératifs

22
00:00:36,821 --> 00:00:39,126
quoique formels et a priori universels,

23
00:00:39,226 --> 00:00:41,756
reste proche de celui d'un langage de bas niveau,

24
00:00:42,025 --> 00:00:44,838
mais les adresses et les registres disparaissent

25
00:00:44,938 --> 00:00:46,069
au profit de variables,

26
00:00:46,169 --> 00:00:47,671
y compris les pointeurs parfois,

27
00:00:47,771 --> 00:00:48,808
et d'expressions,

28
00:00:48,908 --> 00:00:50,187
donc de valeurs temporaires

29
00:00:50,287 --> 00:00:51,847
invisibles et anonymes.

30
00:00:52,937 --> 00:00:54,012
Très rapidement,

31
00:00:54,112 --> 00:00:56,103
deux améliorations des langages impératifs

32
00:00:56,203 --> 00:00:57,078
ont été introduites

33
00:00:57,178 --> 00:00:58,720
avec les langages structurés

34
00:00:58,820 --> 00:01:00,027
et procéduraux.

35
00:01:00,127 --> 00:01:02,264
D'abord la programmation structurée

36
00:01:02,364 --> 00:01:04,252
constitue un sous-ensemble

37
00:01:04,352 --> 00:01:05,826
de la programmation impérative.

38
00:01:05,926 --> 00:01:07,109
Elle recommande

39
00:01:07,370 --> 00:01:09,413
une organisation hiérarchique

40
00:01:09,513 --> 00:01:10,712
simple du code.

41
00:01:10,812 --> 00:01:11,562
Pour faire simple,

42
00:01:11,662 --> 00:01:12,646
on y trouve généralement

43
00:01:12,746 --> 00:01:14,120
les instructions de contrôle

44
00:01:14,220 --> 00:01:16,962
while, repeat, for, if/then/else

45
00:01:17,062 --> 00:01:17,666
et cætera.

46
00:01:18,110 --> 00:01:19,987
Ensuite, les langages procéduraux,

47
00:01:20,087 --> 00:01:21,286
comme on l'a déjà vu,

48
00:01:21,386 --> 00:01:23,954
sont les langages de programmation impératifs

49
00:01:24,054 --> 00:01:25,390
qui permettent une découpe du code

50
00:01:25,490 --> 00:01:27,390
en fonctions ou en sous-programmes

51
00:01:27,490 --> 00:01:30,132
parmi les nombreuses variantes que nous avons déjà vues.

52
00:01:30,668 --> 00:01:33,123
Parlons maintenant des langages orientés objet

53
00:01:33,223 --> 00:01:35,548
dont nous avons déjà présenté certains aspects en Python.

54
00:01:36,423 --> 00:01:37,763
Les langages orientés objet

55
00:01:37,863 --> 00:01:39,413
sont les plus utilisés aujourd'hui.

56
00:01:39,813 --> 00:01:41,709
Ils permettent d'utiliser le paradigme

57
00:01:41,809 --> 00:01:43,067
de programmation par objet

58
00:01:43,167 --> 00:01:44,823
qui change la façon de concevoir

59
00:01:44,923 --> 00:01:45,874
les programmes informatiques

60
00:01:45,974 --> 00:01:47,770
via l'assemblage de briques logicielles

61
00:01:47,870 --> 00:01:49,323
appelées objets.

62
00:01:50,349 --> 00:01:52,599
L'histoire des langages orientés objet

63
00:01:52,699 --> 00:01:54,778
commence en 1967.

64
00:01:54,878 --> 00:01:57,250
En implantant les Record Class de Hoare,

65
00:01:57,350 --> 00:01:58,952
le langage Simula 67

66
00:01:59,052 --> 00:02:01,264
pose les constructions qui seront celles

67
00:02:01,364 --> 00:02:03,233
des langages orientés objet à classes,

68
00:02:03,333 --> 00:02:05,916
notions de classe, polymorphisme, héritage, et cætera.

69
00:02:06,346 --> 00:02:09,226
Mais c'est réellement par et avec Smalltalk 71

70
00:02:09,326 --> 00:02:10,545
puis Smalltalk 80,

71
00:02:10,645 --> 00:02:12,081
résultats des travaux d'Alan Kay,

72
00:02:12,181 --> 00:02:15,171
inspirés en grande partie par Simula 67 et Lisp,

73
00:02:15,271 --> 00:02:17,293
que les principes de la programmation par objet

74
00:02:17,393 --> 00:02:18,772
sont pleinement définis.

75
00:02:18,872 --> 00:02:20,469
Objet, encapsulation,

76
00:02:20,569 --> 00:02:21,946
messages, typage

77
00:02:22,046 --> 00:02:25,277
et polymorphisme via la sous-classification.

78
00:02:26,292 --> 00:02:28,189
Les autres principes comme l'héritage

79
00:02:28,289 --> 00:02:29,215
sont dérivés de ceux-ci.

80
00:02:29,315 --> 00:02:30,872
À partir des années 80,

81
00:02:30,972 --> 00:02:33,437
commence l'effervescence des langages à objets.

82
00:02:33,537 --> 00:02:35,395
C++ en 83,

83
00:02:35,495 --> 00:02:37,406
Objective-C en 84,

84
00:02:37,506 --> 00:02:38,474
Eiffel en 86,

85
00:02:38,574 --> 00:02:40,982
Common Lisp Object System en 88

86
00:02:41,082 --> 00:02:41,682
et cætera.

87
00:02:41,962 --> 00:02:43,240
Les années 90

88
00:02:43,340 --> 00:02:45,180
voient l'âge d'or de l'extension

89
00:02:45,280 --> 00:02:46,411
de la programmation par objet

90
00:02:46,511 --> 00:02:49,347
dans les différents secteurs du développement logiciel.

91
00:02:50,117 --> 00:02:51,893
Essayons de résumer les principes

92
00:02:51,993 --> 00:02:53,171
des langages orientés objet.

93
00:02:54,550 --> 00:02:55,609
Concrètement,

94
00:02:55,709 --> 00:02:57,883
un objet est une structure de données

95
00:02:57,983 --> 00:02:59,975
qui répond à un ensemble de messages.

96
00:03:00,075 --> 00:03:01,288
Cette structure de données

97
00:03:01,388 --> 00:03:02,938
définit son état

98
00:03:03,038 --> 00:03:05,471
tandis que l'ensemble des messages qu'il comprend

99
00:03:05,571 --> 00:03:06,861
décrit son comportement.

100
00:03:07,118 --> 00:03:08,365
Les données, ou champs,

101
00:03:08,465 --> 00:03:10,146
qui décrivent sa structure interne

102
00:03:10,246 --> 00:03:11,724
sont appelés ses attributs ;

103
00:03:11,824 --> 00:03:13,359
l'ensemble des messages forme

104
00:03:13,459 --> 00:03:15,027
ce qu'on appelle l'interface de l'objet.

105
00:03:15,532 --> 00:03:17,804
C'est seulement au travers de celle-ci

106
00:03:17,904 --> 00:03:19,949
que les objets interagissent entre eux.

107
00:03:20,049 --> 00:03:22,665
Une méthode donne la réponse au message reçu.

108
00:03:24,213 --> 00:03:26,005
Le principe d'encapsulation

109
00:03:26,105 --> 00:03:28,806
consiste à cacher certains attributs ou méthodes.

110
00:03:29,132 --> 00:03:31,017
Ainsi, le programme peut modifier

111
00:03:31,117 --> 00:03:32,556
la structure interne des objets

112
00:03:32,656 --> 00:03:34,071
ou leurs méthodes associées

113
00:03:34,171 --> 00:03:37,377
sans avoir d'impact sur les utilisateurs de l'objet.

114
00:03:37,663 --> 00:03:39,026
Dans la programmation par objet,

115
00:03:39,126 --> 00:03:41,352
chaque objet est typé.

116
00:03:41,452 --> 00:03:43,412
Le polymorphisme permet d'utiliser

117
00:03:43,512 --> 00:03:45,097
des objets de types différents

118
00:03:45,197 --> 00:03:47,935
là où est attendu un objet d'un certain type.

119
00:03:48,242 --> 00:03:49,983
Une façon de réaliser le polymorphisme

120
00:03:50,083 --> 00:03:53,138
est le sous-typage, appelé aussi héritage de type.

121
00:03:53,621 --> 00:03:55,536
On raffine un type père

122
00:03:55,803 --> 00:03:57,198
en un autre type

123
00:03:57,298 --> 00:04:00,259
par des restrictions sur les valeurs possibles des attributs.

124
00:04:00,359 --> 00:04:02,862
Notons qu'il existe d'autres types de polymorphisme.

125
00:04:02,962 --> 00:04:05,467
On distingue, dans les langages objet,

126
00:04:05,567 --> 00:04:07,253
deux mécanismes de sous-typage.

127
00:04:07,353 --> 00:04:08,583
L'héritage simple,

128
00:04:08,683 --> 00:04:11,680
comme c'est le cas pour Smalltalk, Java, C#,

129
00:04:12,067 --> 00:04:13,853
et l'héritage multiple

130
00:04:13,953 --> 00:04:17,686
comme c'est le cas pour C++, Python, Common Lisp, Eiffel.

131
00:04:17,993 --> 00:04:20,645
Notons également le principe de redéfinition des messages,

132
00:04:20,745 --> 00:04:22,184
ou overriding en anglais,

133
00:04:22,284 --> 00:04:23,087
qui permet à un objet

134
00:04:23,187 --> 00:04:26,293
de raffiner une méthode des objets d'un type parent.

135
00:04:26,393 --> 00:04:27,908
Enfin, parmi les critères de classement

136
00:04:28,008 --> 00:04:29,088
des langages orientés objet,

137
00:04:29,188 --> 00:04:32,559
on distingue les langages orientés objet purs ou non.

138
00:04:32,809 --> 00:04:34,558
Dans un langage orienté objet pur,

139
00:04:34,658 --> 00:04:36,376
toutes la syntaxe et la sémantique

140
00:04:36,476 --> 00:04:38,265
sont construites autour de la notion d'objet

141
00:04:38,365 --> 00:04:39,384
sans exception.

142
00:04:39,484 --> 00:04:41,876
Tous les types, mêmes les primitifs,

143
00:04:41,976 --> 00:04:42,914
sont des classes.

144
00:04:43,014 --> 00:04:43,900
Toutes les opérations,

145
00:04:44,000 --> 00:04:44,903
même prédéfinies,

146
00:04:45,003 --> 00:04:46,162
sont des méthodes de classe.

147
00:04:46,377 --> 00:04:48,268
Toutes les classes sont des objets

148
00:04:48,368 --> 00:04:49,570
instances de métaclasses,

149
00:04:49,670 --> 00:04:51,604
et le code du programme lui-même

150
00:04:51,704 --> 00:04:52,461
est un objet.

151
00:04:52,561 --> 00:04:54,821
Il y a donc réflexivité complète.

152
00:04:54,936 --> 00:04:57,560
Des exemples sont Smalltalk évidemment,

153
00:04:57,660 --> 00:05:00,826
mais aussi Ruby, Raku, Self ou encore Eiffel.


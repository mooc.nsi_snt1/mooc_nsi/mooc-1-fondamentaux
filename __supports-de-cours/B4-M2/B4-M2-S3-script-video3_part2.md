# MOOC NSI - fondamentaux. 
## Transcription de la vidéo

## 4.2.3.3 part2 : Programmer des threads en Python: fonction join

**[00:00:01]**

Nous allons maintenant voir la fonction de joint qui nous permet de mettre un peu de l'ordre lors de l'ordonnancement des raid. Nous pouvons utiliser la fonction de jeune qui permet à un raid d'attendre la terminaison d'un autre raid. Ainsi, si un Cerep dispose de l'identifiant d'un autre raid, par exemple, Tia est dit 2 s'il fait tiédi deux points joint. Si l'autre raid est toujours en train de s'exécuter, le premier raid va se bloquer jusqu'à ce que l'autre raid soit terminé. Bien sûr, si l'autre ce raid s'est déjà arrêté le premier, ce raid va continuer son exécution sans être bloqué. Illustrant ce fonctionnement avec le même programme qui était celui considéré dans la séquence vidéo précédente. Et si nous avons la définition du comportement de notre raid dans la classe Raid, nous avons enrichi le constructeur Ignite par un argument. En plus, cet argument to wait va être sauvegardé dans un autre attribut de notre raid qui a le même nom Self to Wait et où nous allons stocker l'identifiant du raid que ce raid devrait attendre. Ainsi, pendant son exécution, après l'affichage du début et la boucle où nous voyons le numéro d'itération une phrase qui, je vous rappelle une phrase de salutation, le serait de vérifier s'il dispose d'un identifiant de ce raid valide. Et si c'est le cas, il va attendre sa terminaison avant de se terminer lui même. Le, ce raid initial lui va comme dans l'exemple de la séquence précédente, un fichier qu'il démarre. Il va créer 4 ce raid qui, en premier argument, vous voyez, prennent des phrases différentes pour nous saluer en différentes langues et ensuite, en deuxième argument, prennent des identifiants de ce raid.


**[00:02:11]**

Donc, le raid 1 n'a pas d'identifiants valides. Un deuxième argument, donc il ne devra attendre personne. Le raid 2 devrait attendre le raid 1. Le raid 3 devra attendre le raid 2 et le serait de 4 devrait attendre le serait. 3 c'est 4. Ce raid sont lancés et à la fin, même le Maine devra attendre un raid. Il va attendre le raid. 4. Voyons! 2 Exécution de ce programme. A gauche, un extrait de trace d'exécution où nous voyons la fin, où il y a bien le seraient. 2 qui se termine et ensuite le raid 3 le serait de 4 et le serait de demain. Le serait de 1 dut se terminer quelque part plus haut, dont la trace que la capture n'a pas pu prendre et à droite, une autre exécution où les terminaisons sont un peu plus espacées et où nous avons des affichages et des salutations qui sont sont glissées entre les terminaisons de ce raid. Ceci vous montre bien que l'entrelacement et l'ordonnancement des raids d'un programme multi raider changent d'une exécution à l'autre et donc non déterministe. Nous avons vu que l'exécution d'un programme multi raider et non déterministe plus il y a de thread, plus leur exécution est longue et plus ce non déterminisme est important. Toutefois, nous disposons de la fonction de joint qui permet à un thread d'attendre la terminaison d'un autre raid et donc d'influencer de manière simple l'ordonnancement de ce raid.




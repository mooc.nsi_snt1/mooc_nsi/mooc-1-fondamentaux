#!/usr/bin/env python3
import threading
import time
import random 

class Parking:
    def __init__(self, nbtotal):
        self.NB_PLACES_TOTAL = nbtotal
        self.NB_PLACES_LIBRE = nbtotal
        #toutes les places sont libres
        self.PLACES = []
        for i in range(self.NB_PLACES_TOTAL):
            self.PLACES.append(0)
        #self.condition_parking = threading.Condition()
    
        
# voiture -------------------------------------
class VoitureThread (threading.Thread):

    def __init__(self, parking, iterations):
        threading.Thread.__init__(self)
        #le no de place que la voiture accupe,
        #-1 veut dire aucun 
        self.noplace = -1
        #le parking pour se garer
        self.parking = parking
        #le nombre d'aller-retours
        self.iter = iterations

    def entrer_parking(self):
        #self.parking.condition_parking.acquire()

        while (self.parking.NB_PLACES_LIBRE==0):
            pass
            #enlever pass et mettre la ligne suivante
            #self.parking.condition_parking.wait()

        print(self.name, "J'AI UNE PLACE")
        i=0
        #trouver le no d'une place libre
        while (i<self.parking.NB_PLACES_TOTAL) and (self.parking.PLACES[i]!=0):
            i+=1

        #avec synchro, le if ne devrait jamais être vrai    
        if i==self.parking.NB_PLACES_TOTAL:
            print(self.name, "MA PLACE = ERREUR")
        else:
            print(self.name, "MA PLACE EST ", i)
            self.parking.NB_PLACES_LIBRE-=1
            self.noplace=i
            self.parking.PLACES[self.noplace]=1

        #condition_parking.release()    

    def sortir_parking(self):       
        #self.parking.condition_parking.acquire()
        self.parking.NB_PLACES_LIBRE+=1
        self.parking.PLACES[self.noplace]=0
        self.noplace = -1
        #self.parking.condition_parking.notify()
        #self.parking.condition_parking.release()      
        
    def run(self):
        for i in range(0,self.iter):
            self.entrer_parking()
            print(i, self.name, "IN <--", self.noplace, "/ nb libres ", self.parking.NB_PLACES_LIBRE)
            time.sleep(random.randrange(1, 2))
            print(i, self.name, "OUT --->", self.noplace)            
            self.sortir_parking()


     
# main function ------------------------------------
def main():
    ITERATIONS = 1
    NB_VOITURES = 5


    NB_PLACES_TOTAL=4

    parking = Parking(NB_PLACES_TOTAL)
   
    # Create new threads
    threads = []
    for t in range(0,NB_VOITURES):
        thread = VoitureThread(parking, ITERATIONS)
        threads.append(thread)

    # Start new threads
    for t in threads:
        t.start()

    # Wait for threads
    for t in threads:
        t.join()

    print("PARKING %d places libres sur %d" %( parking.NB_PLACES_LIBRE,parking.NB_PLACES_TOTAL) );    

    print("THIS IS THE END")

# MAIN -----------------------------------------


main()

import threading
import random

ITERATIONS = 1000

def addMyNumbers(l,m):
    for i in range(0,ITERATIONS):
        l[i]=l[i]+m

#  ------------------------------------
class AThread (threading.Thread):
    def __init__(self, nums):
        threading.Thread.__init__(self)
        self.nums = nums
        
    def run(self):
        for i in range(0,ITERATIONS):
            addMyNumbers(self.nums, random.randrange(1,49))


     
#  ------------------------------------
def main():
       
    numbers = []
    for i in range(0,ITERATIONS):
        numbers.append(0)
    t = AThread(numbers)
    t.start()
    for i in range(0,ITERATIONS):
        addMyNumbers(numbers,  random.randrange(1,49))
    t.join()
    verif(numbers)

#  -----------------------------------------
main()

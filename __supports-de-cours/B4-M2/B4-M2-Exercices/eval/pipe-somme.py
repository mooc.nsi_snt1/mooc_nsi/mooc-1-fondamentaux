#!/usr/bin/env python3

import os, sys

def main():

    args = sys.argv[1:]
    if len(args)!=2:
        print("Please enter two arguments.")
        os._exit(-1)
        
    a = args[0]
    b = args[1]
    print("Arguments :  %s et %s\n "%(a,b))
    
    rpf, wpf = os.pipe()
    rfp,wfp = os.pipe()
    
    pid = os.fork()
    if pid==0:  # FILS
        os.close(wpf)
        os.close(rfp)

        riopf = os.fdopen(rpf)
        r1 = riopf.readline()
        r2 = riopf.readline()
        riopf.close()

        wiofp = os.fdopen(wfp,'w')
        wiofp.write(str(int(r1)+int(r2))+'\n')
        wiofp.close()
        os._exit(0)
    else: # PERE
        os.close(rpf)
        os.close(wfp)

        wiopf = os.fdopen(wpf,'w')
        wiopf.write(a+'\r\n')
        wiopf.write(b+'\r\n')
        wiopf.close()

        riofp = os.fdopen(rfp, 'r')
        res = riofp.readline()
        riofp.close()
        
        os.waitpid(pid,0)
        print('Le père se termine avec résultat %d\n'%(int(res)))
        os._exit(0)
   
main()

1
00:00:02,700 --> 00:00:05,471
Au fait, c'est quoi la conception d'algorithme ?

2
00:00:05,654 --> 00:00:08,016
C'est cette difficulté que nous allons

3
00:00:08,253 --> 00:00:10,845
tenter d'aborder dans cette petite vidéo,

4
00:00:11,130 --> 00:00:13,236
en l'illustrant sur un exemple classique,

5
00:00:13,336 --> 00:00:14,617
qui est le crêpier psycho-rigide

6
00:00:14,717 --> 00:00:16,376
que l'on retrouvera dans de nombreux ouvrages

7
00:00:17,006 --> 00:00:19,173
et sur de nombreux sites,

8
00:00:19,273 --> 00:00:20,735
ça a été très utilisé

9
00:00:21,100 --> 00:00:23,073
dans les petites classes puis dans les plus grandes,

10
00:00:23,173 --> 00:00:25,397
même à l'université, nous l'utilisons comme exemple,

11
00:00:25,786 --> 00:00:27,596
et ça va nous permettre d'illustrer

12
00:00:27,696 --> 00:00:29,868
ce que l'on attend quand on parle de

13
00:00:29,968 --> 00:00:31,158
concevoir un algorithme

14
00:00:31,258 --> 00:00:32,619
et qu'est-ce qui est attendu derrière.

15
00:00:33,597 --> 00:00:36,801
Le problème est un problème qui se scénarise très bien,

16
00:00:37,828 --> 00:00:39,727
le problème scénarisé dirait :

17
00:00:40,120 --> 00:00:41,296
on a un crêpier,

18
00:00:41,396 --> 00:00:42,908
donc c'est quelqu'un qui fabrique des crêpes,

19
00:00:43,008 --> 00:00:43,968
un breton par exemple,

20
00:00:44,068 --> 00:00:46,018
mais je pense que les bretons sont habiles.

21
00:00:46,144 --> 00:00:47,669
Celui-ci n'est pas très habile,

22
00:00:47,769 --> 00:00:49,283
c'est-à-dire que quand il construit ses crêpes,

23
00:00:49,445 --> 00:00:52,144
et bien, ses crêpes ne sont pas bien rangées

24
00:00:52,244 --> 00:00:53,479
sur le tas où il pose les crêpes

25
00:00:53,579 --> 00:00:54,794
au fur et à mesure où il les fait.

26
00:00:55,168 --> 00:00:56,814
Donc l'objectif du crêpier,

27
00:00:56,914 --> 00:00:57,659
c'est de se dire :

28
00:00:57,759 --> 00:01:00,391
j'ai un tas de crêpes là

29
00:01:00,491 --> 00:01:02,358
qui n'est pas très joli donc pour la vente,

30
00:01:02,458 --> 00:01:03,485
ce n'est pas terrible,

31
00:01:03,898 --> 00:01:04,794
donc ce que j'aimerais,

32
00:01:04,894 --> 00:01:06,631
ce serait le transformer

33
00:01:06,731 --> 00:01:08,252
en un tas de crêpes qui soient

34
00:01:08,775 --> 00:01:10,188
joliment rangées.

35
00:01:10,521 --> 00:01:11,536
Alors pour faire ça,

36
00:01:11,636 --> 00:01:12,825
je vais avoir

37
00:01:13,265 --> 00:01:15,944
juste la place pour poser un tas de crêpes,

38
00:01:16,044 --> 00:01:18,207
je ne peux pas en mettre à droite, à gauche, et cætera,

39
00:01:18,307 --> 00:01:19,514
je n'ai qu'une petite table

40
00:01:19,970 --> 00:01:22,106
sur laquelle je mets mes crêpes une fois qu'elles sont faites,

41
00:01:22,428 --> 00:01:24,742
et donc je vais avoir un certain nombre d'opérations.

42
00:01:24,993 --> 00:01:26,521
Ces opérations sont : 

43
00:01:27,332 --> 00:01:29,490
je peux glisser une spatule

44
00:01:29,590 --> 00:01:30,508
entre deux crêpes,

45
00:01:30,894 --> 00:01:33,845
j'ai un tas qui est au-dessus de la spatule,

46
00:01:33,945 --> 00:01:35,261
un petit tas de crêpes, quelques crêpes,

47
00:01:35,361 --> 00:01:37,672
et je peux retourner ce tas qui est

48
00:01:37,772 --> 00:01:39,368
au-dessus de la spatule,

49
00:01:39,468 --> 00:01:42,312
donc avec un mouvement tout à fait classique.

50
00:01:42,412 --> 00:01:44,719
L'objectif donc de ce travail,

51
00:01:44,819 --> 00:01:47,029
ça va être de concevoir un algorithme

52
00:01:47,129 --> 00:01:49,477
permettant de transformer le tas de crêpes de gauche

53
00:01:49,577 --> 00:01:50,930
en un nouveau tas de crêpes

54
00:01:51,030 --> 00:01:53,084
où les crêpes sont empilées par tailles décroissantes.

55
00:01:53,358 --> 00:01:55,558
Alors je vous donnerai les références,

56
00:01:55,658 --> 00:01:57,196
donc vous pourrez aller les chercher

57
00:01:58,221 --> 00:02:01,769
sur le site de Marie Duflot

58
00:02:01,869 --> 00:02:07,418
qui est une grande, grande info sans ordi devant l'éternel,

59
00:02:07,819 --> 00:02:09,535
et qui a cette activité-là,

60
00:02:09,635 --> 00:02:10,366
qui la propose.

61
00:02:10,701 --> 00:02:12,416
Et puis la page de l'IREM de Grenoble

62
00:02:12,516 --> 00:02:13,494
que nous avons faite

63
00:02:13,594 --> 00:02:15,577
par rapport à ce problème-là.

64
00:02:15,865 --> 00:02:17,513
Donc pour illustrer ce problème,

65
00:02:17,613 --> 00:02:18,670
moi, ce que je vous propose,

66
00:02:19,049 --> 00:02:22,271
on va reprendre

67
00:02:22,371 --> 00:02:23,443
notre tas de crêpes,

68
00:02:23,543 --> 00:02:25,149
on va ici

69
00:02:26,028 --> 00:02:28,643
placer notre tas de crêpes,

70
00:02:28,743 --> 00:02:29,883
le voici.

71
00:02:32,694 --> 00:02:34,080
Alors ici, il est bien rangé

72
00:02:34,180 --> 00:02:36,267
donc on va supposer qu'il est mélangé

73
00:02:38,236 --> 00:02:40,100
dans tous les sens,

74
00:02:42,322 --> 00:02:44,542
et donc l'idée, c'est de prendre une spatule,

75
00:02:44,642 --> 00:02:46,447
vous voyez, j'ai tous les instruments qu'il faut,

76
00:02:46,857 --> 00:02:52,134
de glisser la spatule entre deux crêpes

77
00:02:52,234 --> 00:02:53,469
et puis de retourner

78
00:02:53,934 --> 00:02:55,750
l'ensemble par dessus.

79
00:02:56,217 --> 00:02:57,938
Et puis je peux faire l'opération

80
00:02:58,381 --> 00:02:59,574
plusieurs fois.

81
00:03:02,000 --> 00:03:03,958
Donc, à l'aide de ces opérations-là,

82
00:03:04,537 --> 00:03:07,269
je voudrais avoir les crêpes

83
00:03:07,369 --> 00:03:08,379
super bien rangées,

84
00:03:10,149 --> 00:03:11,922
voilà, comme ça.

85
00:03:12,298 --> 00:03:14,239
Pour l'instant, vous ne vous préoccupez pas

86
00:03:14,339 --> 00:03:17,407
de la face qui est blanche ou pas blanche,

87
00:03:17,507 --> 00:03:18,618
ça représente la face brûlée,

88
00:03:18,718 --> 00:03:20,169
donc c'est une amélioration de l'algorithme,

89
00:03:20,269 --> 00:03:21,876
mais déjà, si on peut les ranger

90
00:03:21,976 --> 00:03:23,374
comme il faut, ça serait pas mal.

91
00:03:23,778 --> 00:03:25,316
Donc, ce que je vous propose, c'est

92
00:03:25,416 --> 00:03:27,298
de faire un stop ici sur la vidéo,

93
00:03:27,600 --> 00:03:28,885
donc on arrête,

94
00:03:28,985 --> 00:03:31,671
et vous prenez des petits morceaux de carton,

95
00:03:31,771 --> 00:03:32,640
des morceaux de papier

96
00:03:32,740 --> 00:03:33,703
de tailles différentes,

97
00:03:33,803 --> 00:03:34,794
et vous vous posez la question :

98
00:03:35,200 --> 00:03:36,550
comment est-ce que je peux

99
00:03:36,650 --> 00:03:37,713
écrire un algorithme

100
00:03:37,813 --> 00:03:38,730
qui va me permettre

101
00:03:38,881 --> 00:03:40,456
de faire le retournement

102
00:03:40,556 --> 00:03:41,512
du tas de crêpes.


1
00:00:01,347 --> 00:00:02,145
Quand on va

2
00:00:03,820 --> 00:00:05,251
prendre nos algorithmes,

3
00:00:05,351 --> 00:00:06,471
on a bien vu que c'était

4
00:00:06,571 --> 00:00:08,228
une approximation qu'on allait faire.

5
00:00:08,538 --> 00:00:10,286
C'est-à-dire que la complexité,

6
00:00:10,795 --> 00:00:11,976
elle ne vous donne pas tout.

7
00:00:12,387 --> 00:00:15,490
Et donc, on espère que c'est une prédiction.

8
00:00:15,590 --> 00:00:18,636
Si je regarde de façon un peu plus précise,

9
00:00:18,736 --> 00:00:20,246
une fois que vous avez écrit votre algorithme,

10
00:00:20,346 --> 00:00:21,774
il y a plein d'étapes.

11
00:00:22,043 --> 00:00:23,598
Et ces étapes, à chaque fois,

12
00:00:23,698 --> 00:00:24,763
vous mettez du bruit,

13
00:00:25,326 --> 00:00:27,165
il y a des choses qui vont se passer

14
00:00:27,493 --> 00:00:28,765
et donc ce n'est pas la peine

15
00:00:28,865 --> 00:00:31,270
de calculer avec une précision extrême

16
00:00:31,691 --> 00:00:33,734
la complexité, le nombre d'opérations.

17
00:00:33,834 --> 00:00:35,718
C'est intéressant parce que ça vous permet de comprendre

18
00:00:36,020 --> 00:00:37,943
mais de fait, ça ne va pas être forcément

19
00:00:39,619 --> 00:00:42,216
le détail qui va être important.

20
00:00:42,316 --> 00:00:43,692
Ce qui va compter,

21
00:00:43,792 --> 00:00:45,559
c'est les ordres de grandeur qui vont être derrière.

22
00:00:46,161 --> 00:00:47,308
Donc là, par exemple, je dis

23
00:00:47,408 --> 00:00:49,568
vous avez le langage de programmation qui va jouer,

24
00:00:50,206 --> 00:00:52,463
l'interpréteur, le compilateur que vous allez prendre,

25
00:00:52,732 --> 00:00:55,691
si vous êtes en C ou dans d'autres langages,

26
00:00:55,791 --> 00:00:57,392
vous avez, associées au compilateur,

27
00:00:57,492 --> 00:00:58,587
des options de compilation,

28
00:00:59,260 --> 00:01:00,808
qui vous permettent d'optimiser le code,

29
00:01:01,426 --> 00:01:03,381
Ensuite, vous avez un exécutable

30
00:01:03,481 --> 00:01:05,196
que vous allez exécuter sur une machine.

31
00:01:05,296 --> 00:01:06,374
Alors, c'est quoi, votre machine ?

32
00:01:06,474 --> 00:01:08,760
Est-ce que c'est une machine virtuelle Java ?

33
00:01:08,860 --> 00:01:11,283
Est-ce que c'est directement un processeur ?

34
00:01:11,383 --> 00:01:12,777
Un processeur embarqué ? Et cætera.

35
00:01:13,059 --> 00:01:14,937
Donc ça dépend de l'architecture du processeur,

36
00:01:15,037 --> 00:01:17,101
le coût des opérations peut être très, très différent

37
00:01:17,201 --> 00:01:19,239
en fonction du type de mémoire que vous avez.

38
00:01:19,500 --> 00:01:21,126
Il y a vraiment beaucoup, beaucoup de paramètres

39
00:01:21,226 --> 00:01:23,401
qui vont faire que c'est une approximation,

40
00:01:23,501 --> 00:01:24,484
il faut le prendre comme tel.

41
00:01:26,113 --> 00:01:28,325
L'objectif de l'informatique, c'est de traiter de l'information.

42
00:01:28,690 --> 00:01:30,724
Et donc, ce qui va nous intéresser le plus,

43
00:01:30,824 --> 00:01:32,752
c'est la notion de taille d'information

44
00:01:33,040 --> 00:01:35,220
parce que si c'est traiter un tableau avec 5 éléments,

45
00:01:35,320 --> 00:01:36,210
c'est sans intérêt,

46
00:01:36,310 --> 00:01:37,869
ce n'est pas la peine d'utiliser un ordinateur.

47
00:01:38,199 --> 00:01:40,032
Par contre, si c'est traiter

48
00:01:40,132 --> 00:01:41,407
un tableau de taille 10 000,

49
00:01:41,699 --> 00:01:43,154
ou de taille un million

50
00:01:43,254 --> 00:01:45,068
ou plusieurs millions si on regarde

51
00:01:45,973 --> 00:01:47,784
les personnes à la Sécurité Sociale,

52
00:01:47,980 --> 00:01:49,394
il est clair que là,

53
00:01:49,494 --> 00:01:51,606
on n'est plus du tout sur les mêmes ordres de grandeur,

54
00:01:51,949 --> 00:01:53,612
impossible de faire ça à la main,

55
00:01:53,883 --> 00:01:55,599
par contre, on va être capable

56
00:01:55,699 --> 00:01:57,639
d'utiliser une machine pour faire ce traitement-là.

57
00:02:00,286 --> 00:02:02,431
Le vade-mecum là-dessus, c'est

58
00:02:02,531 --> 00:02:04,139
ce qui est important, c'est l'ordre de grandeur,

59
00:02:04,409 --> 00:02:05,966
et pouvoir comparer les algorithmes.

60
00:02:08,505 --> 00:02:09,160
Exemples.

61
00:02:10,134 --> 00:02:11,438
Le crêpier psychorigide,

62
00:02:11,538 --> 00:02:12,890
nous avons vu qu'on a quelque chose

63
00:02:12,990 --> 00:02:13,874
qui est de l'ordre de

64
00:02:14,093 --> 00:02:16,242
n(n - 1)/2 opérations.

65
00:02:16,487 --> 00:02:17,948
Et j'arrive à atteindre la borne.

66
00:02:18,824 --> 00:02:21,027
La somme des éléments d'un tableau de taille n,

67
00:02:21,127 --> 00:02:22,153
c'est n opérations,

68
00:02:22,253 --> 00:02:23,902
c'est exactement la même chose que la puissance.

69
00:02:24,555 --> 00:02:26,107
Si je fais un tri par insertion,

70
00:02:26,207 --> 00:02:28,086
vous avez peut-être vu auparavant,

71
00:02:28,186 --> 00:02:29,472
on reviendra dessus par la suite,

72
00:02:30,000 --> 00:02:31,612
vous avez quelque chose de l'ordre de

73
00:02:32,071 --> 00:02:34,761
n au carré opérations

74
00:02:34,861 --> 00:02:36,182
de comparaison entre des éléments.

75
00:02:38,173 --> 00:02:41,115
Si je regarde les vecteurs de booléens,

76
00:02:41,215 --> 00:02:43,393
les vecteurs de bits de taille n,

77
00:02:43,493 --> 00:02:45,069
je vais en avoir 2 puissance n.

78
00:02:45,675 --> 00:02:46,854
Donc en fait, on se rend compte

79
00:02:46,954 --> 00:02:48,077
qu'il y a des problèmes

80
00:02:48,177 --> 00:02:49,805
qui seront de natures différentes,

81
00:02:53,024 --> 00:02:54,668
de temps d'exécution différents,

82
00:02:54,768 --> 00:02:56,879
et on peut avoir des différences énormes.

83
00:02:58,077 --> 00:02:59,829
La différence, pour n égal à 10,

84
00:02:59,929 --> 00:03:01,661
entre 10 et 2 puissance 10,

85
00:03:02,084 --> 00:03:03,666
c'est un facteur 100.

86
00:03:05,548 --> 00:03:09,249
Et donc, si on veut calculer par exemple la puissance,

87
00:03:10,322 --> 00:03:12,631
on a vu qu'on a un calcul itératif

88
00:03:14,293 --> 00:03:16,212
qui vous fait n opérations

89
00:03:16,551 --> 00:03:19,100
et cet algorithme a un coût linéaire donc

90
00:03:19,200 --> 00:03:20,247
dans la valeur de n.

91
00:03:22,215 --> 00:03:23,811
Coût linéaire dans la valeur de n

92
00:03:23,911 --> 00:03:25,652
donc on se dit c'est plutôt sympa.

93
00:03:27,311 --> 00:03:28,800
Maintenant, on va un peu plus loin.

94
00:03:29,120 --> 00:03:30,711
C'est quoi, la taille des données ?

95
00:03:31,622 --> 00:03:33,134
La taille des données ici,

96
00:03:33,234 --> 00:03:36,133
c'est la taille pour stocker n.

97
00:03:37,922 --> 00:03:39,017
Donc en gros, on pourrait dire,

98
00:03:39,117 --> 00:03:40,573
c'est le nombre de bits nécessaires

99
00:03:40,673 --> 00:03:41,685
pour stocker n.

100
00:03:42,987 --> 00:03:45,067
Donc ce n'est pas n la taille des données,

101
00:03:45,167 --> 00:03:46,726
c'est le logarithme en base 2 de n.

102
00:03:47,717 --> 00:03:48,888
Donc en fait, ce que j'ai,

103
00:03:48,988 --> 00:03:51,230
j'ai un algorithme de coût exponentiel

104
00:03:51,539 --> 00:03:52,701
dans la taille des données

105
00:03:52,801 --> 00:03:55,411
puisque vous avez le coût

106
00:03:56,941 --> 00:03:59,508
C pour une taille de k

107
00:03:59,964 --> 00:04:02,135
ici, n est égal à 

108
00:04:02,742 --> 00:04:03,908
2 puissance k,

109
00:04:04,533 --> 00:04:05,569
C(k), c'est

110
00:04:05,669 --> 00:04:07,839
2 puissance k.

111
00:04:10,097 --> 00:04:12,704
Donc c'est très, très important

112
00:04:12,804 --> 00:04:14,458
par rapport à la taille des données en entrée.

113
00:04:15,602 --> 00:04:17,803
Donc la question qui se pose, c'est

114
00:04:18,606 --> 00:04:21,142
est-ce que je peux effectivement comparer ça ?

115
00:04:21,242 --> 00:04:22,414
Bref,

116
00:04:22,514 --> 00:04:24,091
on va essayer de réfléchir à voir

117
00:04:24,191 --> 00:04:25,774
comment est-ce qu'on peut améliorer la situation.

118
00:04:26,663 --> 00:04:27,867
Mais avant de faire ça,

119
00:04:27,967 --> 00:04:30,142
on va passer sur les ordres de grandeur

120
00:04:30,242 --> 00:04:31,167
et sur les échelles

121
00:04:31,557 --> 00:04:32,633
qui sont possibles,

122
00:04:34,428 --> 00:04:35,890
On va définir les ordres de grandeur

123
00:04:35,990 --> 00:04:37,095
et puis

124
00:04:37,195 --> 00:04:39,847
regarder, lorsqu'on a un programme,

125
00:04:40,231 --> 00:04:41,314
un algorithme,

126
00:04:41,414 --> 00:04:43,381
quelles sont les comparaisons que l'on peut faire

127
00:04:43,481 --> 00:04:45,402
et comment situer ces programmes sur une échelle.

128
00:04:51,002 --> 00:04:52,353
Alors,

129
00:04:52,453 --> 00:04:54,596
lorsque je me place sur des échelles,

130
00:04:54,931 --> 00:04:57,916
on reviendra après sur les différentes échelles,

131
00:04:58,270 --> 00:04:59,277
je vais avoir à dire

132
00:04:59,377 --> 00:05:01,304
ça, c'est plus grand ou c'est plus petit que n.

133
00:05:02,866 --> 00:05:04,382
Ce n'est pas plus grand ou plus petit que n,

134
00:05:04,482 --> 00:05:06,163
ça va être plus grand ou plus petit que linéaire.

135
00:05:06,509 --> 00:05:08,340
Ou plus grand ou plus petit que quadratique.

136
00:05:08,649 --> 00:05:10,824
Et donc, on va essayer de généraliser

137
00:05:10,924 --> 00:05:13,131
les choses que l'on a classiquement,

138
00:05:13,451 --> 00:05:14,430
et de dire que

139
00:05:14,530 --> 00:05:17,792
si j'ai une fonction f,

140
00:05:18,678 --> 00:05:22,191
je vais dire qu'elle est majorée

141
00:05:22,291 --> 00:05:23,691
asymptotiquement par g

142
00:05:24,003 --> 00:05:25,727
si je peux

143
00:05:25,827 --> 00:05:27,743
majorer f

144
00:05:28,045 --> 00:05:30,658
par une constante fois g.

145
00:05:31,669 --> 00:05:33,380
C'est ce que j'ai écrit de façon formelle.

146
00:05:33,747 --> 00:05:35,579
grand O de g,

147
00:05:35,751 --> 00:05:38,142
c'est en fait un ensemble de fonctions

148
00:05:38,450 --> 00:05:40,310
c'est l'ensemble des fonctions f

149
00:05:40,410 --> 00:05:42,751
telles que il existe un c et un n0

150
00:05:42,851 --> 00:05:44,883
tels que, quel que soit n plus grand que n0,

151
00:05:44,983 --> 00:05:47,766
f(n) est plus petit que c fois g(n).

152
00:05:48,055 --> 00:05:49,842
Par exemple, si vous avez

153
00:05:53,113 --> 00:05:54,391
une fonction f,

154
00:05:55,525 --> 00:05:56,301
qui est comme ça,

155
00:05:56,654 --> 00:05:58,861
et que vous avez une fonction g

156
00:06:00,273 --> 00:06:01,855
qui est comme ça,

157
00:06:02,255 --> 00:06:03,617
et bien

158
00:06:03,717 --> 00:06:05,250
il va exister un moment,

159
00:06:05,350 --> 00:06:07,462
alors il faudrait que je continue beaucoup plus loin,

160
00:06:07,828 --> 00:06:09,799
il va exister un moment où 

161
00:06:10,108 --> 00:06:12,340
ma fonction bleue va être plus petite

162
00:06:12,440 --> 00:06:16,274
qu'une constante fois la fonction rouge.

163
00:06:18,048 --> 00:06:20,263
Je ne l'ai peut-être pas fait de la bonne façon,

164
00:06:21,469 --> 00:06:23,198
si je reprends mon dessin,

165
00:06:25,725 --> 00:06:27,096
donc ici, j'ai n,

166
00:06:27,450 --> 00:06:29,930
je vais avoir ma fonction bleue

167
00:06:30,931 --> 00:06:31,879
qui est comme ça,

168
00:06:32,303 --> 00:06:33,789
donc ça, c'est f,

169
00:06:35,672 --> 00:06:36,896
je vais prendre ma fonction g,

170
00:06:36,996 --> 00:06:39,071
c'est une droite

171
00:06:39,171 --> 00:06:40,686
et bien il existe un moment

172
00:06:40,786 --> 00:06:42,773
où ma fonction f sera

173
00:06:42,873 --> 00:06:46,245
tout le temps en dessous de la droite

174
00:06:46,560 --> 00:06:48,436
que je peux multiplier par un coefficient

175
00:06:48,536 --> 00:06:49,490
si j'ai envie.

176
00:06:50,114 --> 00:06:53,497
Ça, c'est ce qu'on appelle la borne supérieure asymptotique,

177
00:06:53,783 --> 00:06:55,503
c'est-à-dire que je dis que f

178
00:06:56,074 --> 00:06:57,177
appartient

179
00:06:58,943 --> 00:07:01,459
à O(g).

180
00:07:02,508 --> 00:07:03,460
Donc  par exemple,

181
00:07:03,560 --> 00:07:05,135
si j'ai quelque chose qui est linéaire,

182
00:07:05,235 --> 00:07:08,557
ça appartiendra à O(n^2).

183
00:07:09,280 --> 00:07:11,490
Il y a un abus de notation et d'écriture

184
00:07:11,590 --> 00:07:12,933
qui dit que le "appartient",

185
00:07:13,033 --> 00:07:14,745
c'est mis avec un "égal",

186
00:07:15,083 --> 00:07:16,988
c'est ce que je sous-entends ici.

187
00:07:18,017 --> 00:07:20,133
Là, on dit que f égale O(g),

188
00:07:20,233 --> 00:07:21,452
ça sous-entend

189
00:07:21,757 --> 00:07:23,909
que f appartient à O(g).

190
00:07:24,643 --> 00:07:29,067
Donc n appartient à O(n^3),

191
00:07:29,167 --> 00:07:33,353
n^2 appartient à O(2^n), et cætera.

192
00:07:33,621 --> 00:07:35,198
Donc on est capable de faire,

193
00:07:35,298 --> 00:07:36,534
et puis on a des règles algébriques

194
00:07:36,905 --> 00:07:37,987
de manipulation.

195
00:07:40,049 --> 00:07:42,352
Ces règles algébriques, ça vous dit que

196
00:07:43,151 --> 00:07:45,608
si vous avez f et g qui appartiennent

197
00:07:46,350 --> 00:07:49,437
à O(h) par exemple,

198
00:07:49,537 --> 00:07:51,965
et bien f + g appartient à O(h).

199
00:07:52,279 --> 00:07:53,199
Ce genre de choses.

200
00:07:53,495 --> 00:07:56,046
Donc effectivement, ça se manipule bien

201
00:07:56,146 --> 00:07:58,208
et vous trouverez dans les références dans les ouvrages

202
00:07:58,308 --> 00:08:00,540
tout ce qui est nécessaire par rapport à ça.

203
00:08:01,680 --> 00:08:03,446
Ici, on a fait

204
00:08:03,745 --> 00:08:05,327
la borne supérieure asymptotique,

205
00:08:05,923 --> 00:08:07,945
on peut faire exactement de la même façon

206
00:08:08,416 --> 00:08:09,896
la borne inférieure asymptotique

207
00:08:09,996 --> 00:08:11,724
avec la notation grand oméga

208
00:08:12,083 --> 00:08:13,401
c'est-à-dire que

209
00:08:13,703 --> 00:08:15,559
vous ne pourrez pas faire mieux

210
00:08:15,877 --> 00:08:17,074
que grand oméga.

211
00:08:17,721 --> 00:08:19,806
Donc grand oméga, c'est l'ensemble des fonctions

212
00:08:19,906 --> 00:08:21,121
qui sont minorées

213
00:08:22,488 --> 00:08:24,190
par une constante fois g.

214
00:08:24,290 --> 00:08:26,393
C'est exactement la même idée

215
00:08:27,789 --> 00:08:29,274
et quand vous pouvez encadrer

216
00:08:30,112 --> 00:08:32,506
vous avez ce qu'on appelle la fonction théta,

217
00:08:32,606 --> 00:08:34,473
c'est le même ordre de grandeur,

218
00:08:34,690 --> 00:08:36,730
f et g sont du même ordre de grandeur

219
00:08:37,490 --> 00:08:39,373
s'il existe deux constantes

220
00:08:39,760 --> 00:08:40,870
telles que je peux

221
00:08:41,198 --> 00:08:46,276
mettre ma fonction f

222
00:08:47,303 --> 00:08:49,126
dans une sorte de tube

223
00:08:50,292 --> 00:08:52,750
qui est lié à ma fonction g.

224
00:08:53,162 --> 00:08:54,765
Donc j'ai une fonction f

225
00:08:55,185 --> 00:08:56,403
par exemple,

226
00:09:01,592 --> 00:09:05,045
une fonction f un peu compliquée

227
00:09:06,990 --> 00:09:07,982
mais par contre,

228
00:09:09,911 --> 00:09:12,905
je suis capable de la mettre entre

229
00:09:16,590 --> 00:09:17,784
deux droites

230
00:09:18,149 --> 00:09:19,638
et donc je suis capable de dire

231
00:09:19,738 --> 00:09:21,347
j'ai un grand oméga de n

232
00:09:21,719 --> 00:09:22,677
dans ce cas-là.

233
00:09:23,423 --> 00:09:25,939
Alors, une fois qu'on a ça,

234
00:09:26,269 --> 00:09:27,964
c'est quoi les fonctions rouges

235
00:09:28,064 --> 00:09:29,174
qui vont nous être intéressantes ?

236
00:09:29,399 --> 00:09:31,140
En fait, c'est les fonctions que tout le monde connaît

237
00:09:31,240 --> 00:09:32,053
qui sont classiques.

238
00:09:32,426 --> 00:09:35,024
On a n la taille des données

239
00:09:35,189 --> 00:09:36,664
et puis on va avoir des

240
00:09:36,996 --> 00:09:39,268
n, n au carré, racine de n,

241
00:09:39,368 --> 00:09:41,677
n au cube, n puissance alpha, et cætera

242
00:09:42,031 --> 00:09:44,442
Tout ça, c'est des puissances de n

243
00:09:44,791 --> 00:09:46,487
et c'est ce qu'on appelle l'échelle polynomiale.

244
00:09:46,615 --> 00:09:48,655
Alors polynomiale, c'est pris au sens large ici,

245
00:09:49,000 --> 00:09:50,923
puisque n puissance un demi, racine de n,

246
00:09:51,222 --> 00:09:52,526
serait considérée là-dedans.

247
00:09:52,626 --> 00:09:53,973
Après, on peut avoir

248
00:09:54,310 --> 00:09:55,770
une échelle logarithmique,

249
00:09:55,870 --> 00:09:57,891
log(n), log(log(n)), et cætera.

250
00:09:58,548 --> 00:09:59,657
Puis une échelle exponentielle,

251
00:10:02,761 --> 00:10:03,815
exponentielle de n,

252
00:10:03,915 --> 00:10:06,518
2 puissance n, factorielle de n, et cætera.

253
00:10:06,892 --> 00:10:08,607
Et puis on peut même avoir des mélanges.

254
00:10:08,982 --> 00:10:10,373
On peut avoir du n log n,

255
00:10:10,692 --> 00:10:12,666
n fois 2 puissance n, et cætera.

256
00:10:13,044 --> 00:10:14,993
Maintenant, regardons d'un peu plus près

257
00:10:15,093 --> 00:10:16,645
ce que représentent ces différentes fonctions

258
00:10:16,745 --> 00:10:17,706
et ces différentes échelles.

259
00:10:18,704 --> 00:10:20,569
Donc je vais prendre trois exemples,

260
00:10:20,669 --> 00:10:22,707
un exemple sur les trois échelles,

261
00:10:23,061 --> 00:10:25,647
n, log(n) et 2 puissance n.

262
00:10:26,927 --> 00:10:29,210
Si je vous demande de tracer

263
00:10:29,310 --> 00:10:32,087
n, log(n) et 2^n sur un même schéma,

264
00:10:32,482 --> 00:10:34,312
en général, les gens vont dire

265
00:10:34,412 --> 00:10:35,435
c'est facile,

266
00:10:36,646 --> 00:10:38,149
il y en a une qui est comme ça,

267
00:10:38,841 --> 00:10:40,097
il y en a une qui est comme ça,

268
00:10:41,316 --> 00:10:44,357
il y en a une qui est comme ça

269
00:10:44,781 --> 00:10:47,210
à peu de choses près, on a quelque chose comme ça.

270
00:10:48,217 --> 00:10:49,204
En fait, c'est faux.

271
00:10:49,304 --> 00:10:50,547
Ce type de dessin

272
00:10:50,647 --> 00:10:52,623
que l'on retrouve très fréquemment avec les élèves,

273
00:10:53,607 --> 00:10:54,879
il est complètement faux

274
00:10:54,979 --> 00:10:57,031
parce que la réalité

275
00:10:57,131 --> 00:10:58,761
et ce qui nous intéresse nous en informatique,

276
00:10:58,861 --> 00:10:59,681
ce n'est pas du tout ça.

277
00:11:00,011 --> 00:11:03,554
Là, je vais tracer les trois fonctions

278
00:11:03,905 --> 00:11:07,399
J'ai logarithme de n,

279
00:11:07,499 --> 00:11:08,736
2 puissance n et n

280
00:11:08,836 --> 00:11:09,727
sur un même diagramme.

281
00:11:09,827 --> 00:11:11,054
Ici, n varie de 0 à 10.

282
00:11:11,997 --> 00:11:13,430
Vous allez me dire, évidemment,

283
00:11:14,222 --> 00:11:15,167
si je fais de l'informatique,

284
00:11:15,267 --> 00:11:17,280
ce n'est pas pour traiter des tableaux ayant 10 entrées.

285
00:11:17,575 --> 00:11:18,606
Ce qu'il faut faire,

286
00:11:18,706 --> 00:11:19,577
c'est augmenter la taille

287
00:11:19,677 --> 00:11:20,981
pour voir la vraie différence

288
00:11:21,323 --> 00:11:23,844
qui peut exister entre les différentes courbes.

289
00:11:25,010 --> 00:11:26,367
Je vous laisse réfléchir.

290
00:11:26,467 --> 00:11:28,707
Voilà pour n de 0 à 100,

291
00:11:29,310 --> 00:11:31,170
vous avez cette figure-là.

292
00:11:33,600 --> 00:11:34,746
Ça devient compliqué, là.

293
00:11:34,846 --> 00:11:36,332
Déjà, je ne peux pas comparer

294
00:11:36,432 --> 00:11:37,920
quelque chose qui est en 2 puissance n

295
00:11:38,020 --> 00:11:38,956
et quelque chose qui est en n.

296
00:11:39,820 --> 00:11:41,011
C'est incomparable.

297
00:11:41,349 --> 00:11:44,771
L'un est gigantesquement plus grand que l'autre.

298
00:11:45,086 --> 00:11:47,097
Et a fortiori, 2 puissance n et log(n)

299
00:11:47,361 --> 00:11:48,916
c'est incomparable aussi.

300
00:11:49,257 --> 00:11:51,511
Si je vais encore un peu plus loin,

301
00:11:51,611 --> 00:11:53,241
parce que 100, c'est encore petit comme tableau,

302
00:11:53,341 --> 00:11:54,122
si je vais à 1000,

303
00:11:54,751 --> 00:11:56,303
mon diagramme, il sera

304
00:11:57,038 --> 00:11:57,941
sous cette forme-là.

305
00:11:58,901 --> 00:12:00,013
Ça veut dire quoi ?

306
00:12:00,113 --> 00:12:01,030
Ça veut dire que

307
00:12:01,384 --> 00:12:03,217
si j'ai quelque chose qui est en log(n)

308
00:12:03,317 --> 00:12:04,773
par rapport à quelque chose qui serait en n,

309
00:12:04,873 --> 00:12:06,746
c'est négligeable, c'est zéro.

310
00:12:07,394 --> 00:12:08,985
Donc quand, par exemple, vous me dites

311
00:12:09,520 --> 00:12:11,722
je fais une recherche dichotomique

312
00:12:11,822 --> 00:12:13,991
qui est en log(n), on verra ça éventuellement plus tard,

313
00:12:15,125 --> 00:12:16,506
une dichotomie dans un tableau,

314
00:12:16,606 --> 00:12:18,740
où je fais une recherche en parcourant les éléments du tableau,

315
00:12:19,388 --> 00:12:21,144
vous passez de n à log(n)

316
00:12:22,056 --> 00:12:24,531
donc si vous avez un tableau de taille 1000,

317
00:12:26,126 --> 00:12:28,446
vous passez de 1000 à 10.

318
00:12:29,676 --> 00:12:31,766
Si vous avez un tableau de taille un million,

319
00:12:31,866 --> 00:12:33,861
vous passez de un million à 20.

320
00:12:34,940 --> 00:12:36,103
Donc effectivement,

321
00:12:36,203 --> 00:12:38,368
ça devient quelque chose de négligeable

322
00:12:38,468 --> 00:12:39,794
si dans le reste de votre algorithme,

323
00:12:39,894 --> 00:12:41,684
vous avez quelque chose qui est lié à la taille.

324
00:12:42,317 --> 00:12:44,234
Et donc, cette forme de courbe,

325
00:12:44,334 --> 00:12:45,420
il faut bien la retenir,

326
00:12:45,520 --> 00:12:46,976
c'est-à-dire qu'on ne peut pas tracer

327
00:12:47,799 --> 00:12:49,634
de façon non triviale

328
00:12:49,959 --> 00:12:52,938
des courbes sur des échelles qui sont suffisantes,

329
00:12:53,806 --> 00:12:56,319
donc sur des axes des x et des y

330
00:12:56,419 --> 00:12:57,503
qui sont suffisamment grands,

331
00:12:57,868 --> 00:12:59,366
on ne peut pas tracer ces courbes

332
00:12:59,466 --> 00:13:01,362
sans que ce soit la représentation triviale

333
00:13:01,462 --> 00:13:03,540
que je vous ai mise sur le transparent.

334
00:13:04,293 --> 00:13:06,796
C'est ça, la complexité derrière.

335
00:13:08,230 --> 00:13:09,785
Et donc, on a effectivement

336
00:13:09,885 --> 00:13:10,915
tout un tas d'échelles

337
00:13:11,015 --> 00:13:12,228
et des comparaisons,

338
00:13:13,722 --> 00:13:14,985
et là, j'ai mis, l'ordre

339
00:13:15,085 --> 00:13:16,879
dans lequel vous avez du plus petit au plus grand.

340
00:13:17,206 --> 00:13:19,184
Et bien sûr, il y a des choses entre les deux

341
00:13:19,284 --> 00:13:20,894
et il y a des choses qui, et cætera.

342
00:13:21,604 --> 00:13:23,292
Ce n'est pas exhaustif.

343
00:13:24,302 --> 00:13:26,854
Alors, on est arrivé là,

344
00:13:27,140 --> 00:13:28,811
on a des échelles

345
00:13:29,382 --> 00:13:31,210
et donc on a des outils

346
00:13:31,310 --> 00:13:36,605
pour comparer les différents algorithmes.


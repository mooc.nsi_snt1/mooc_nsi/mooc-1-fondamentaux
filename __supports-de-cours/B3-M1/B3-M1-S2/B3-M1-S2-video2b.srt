1
00:00:01,442 --> 00:00:03,648
On va prendre ici trois petites opérations.

2
00:00:04,258 --> 00:00:05,677
La première opération,

3
00:00:05,777 --> 00:00:07,370
c'est les instructions de séquence.

4
00:00:07,698 --> 00:00:09,627
Donc quand vous avez une succession

5
00:00:09,727 --> 00:00:10,776
d'opérations,

6
00:00:11,091 --> 00:00:12,696
ce qui est facile à calculer,

7
00:00:12,796 --> 00:00:17,672
le coût de mon petit bout de code

8
00:00:17,772 --> 00:00:18,674
que je vous ai mis,

9
00:00:18,774 --> 00:00:20,133
c'est la somme des coûts

10
00:00:20,233 --> 00:00:21,897
des deux lignes que vous avez.

11
00:00:21,997 --> 00:00:24,296
Donc en fait, dès que vous avez une séquence

12
00:00:24,396 --> 00:00:26,044
d'instructions, d'opérations,

13
00:00:26,387 --> 00:00:27,826
et bien, vous faites la somme des coûts

14
00:00:27,926 --> 00:00:28,815
qui sont associés.

15
00:00:29,140 --> 00:00:30,669
Donc ça, c'est la première étape.

16
00:00:30,769 --> 00:00:32,241
C'est la séquence,

17
00:00:32,727 --> 00:00:34,024
le coût d'une séquence.

18
00:00:34,350 --> 00:00:36,376
Évidemment, tout ça, ça vous permet

19
00:00:36,476 --> 00:00:39,147
de regarder le calcul de coût

20
00:00:39,247 --> 00:00:41,306
de toute forme d'itération,

21
00:00:41,406 --> 00:00:42,775
que ce soit des instructions for,

22
00:00:42,875 --> 00:00:44,884
des instructions while, des instructions repeat,

23
00:00:45,211 --> 00:00:47,084
c'est juste des sommes

24
00:00:47,403 --> 00:00:49,500
de nombres d'opérations

25
00:00:49,600 --> 00:00:50,898
à l'intérieur du corps de la boucle.

26
00:00:51,242 --> 00:00:52,645
Donc ça, ça va assez bien.

27
00:00:52,972 --> 00:00:55,327
Là où ça va être un peu plus compliqué,

28
00:00:55,660 --> 00:00:57,706
c'est dès qu'on va avoir une conditionnelle.

29
00:00:57,806 --> 00:00:59,285
Supposons que j'ai une conditionnelle

30
00:00:59,385 --> 00:01:02,077
if condition, je fais un calcul a,

31
00:01:02,177 --> 00:01:04,041
et sinon, je fais un calcul b.

32
00:01:06,134 --> 00:01:08,482
Là, à l'exécution de mon algorithme,

33
00:01:08,582 --> 00:01:09,999
à l'exécution du programme

34
00:01:10,099 --> 00:01:11,334
implémentant mon algorithme,

35
00:01:11,603 --> 00:01:12,905
je vais avoir un petit problème.

36
00:01:13,005 --> 00:01:13,874
Je ne sais pas

37
00:01:14,602 --> 00:01:15,783
quelle va être la branche

38
00:01:15,883 --> 00:01:17,480
empruntée par mon exécution.

39
00:01:18,029 --> 00:01:19,482
Ce qu'il va se passer,

40
00:01:19,582 --> 00:01:21,258
c'est que je peux juste donner,

41
00:01:21,605 --> 00:01:24,143
si je connais le coût de calcul de machin

42
00:01:24,243 --> 00:01:25,376
et le coût de traitement de bidule,

43
00:01:25,476 --> 00:01:26,423
je peux juste donner

44
00:01:26,766 --> 00:01:27,908
une sorte d'encadrement

45
00:01:29,548 --> 00:01:30,349
des choses,

46
00:01:30,449 --> 00:01:31,262
je peux vous dire : oui,

47
00:01:31,362 --> 00:01:32,824
ça sera compris entre

48
00:01:33,544 --> 00:01:36,146
le minimum de calcul(machin)

49
00:01:36,246 --> 00:01:37,062
et traitement(bidule)

50
00:01:37,162 --> 00:01:38,648
et le maximum

51
00:01:39,426 --> 00:01:40,330
des deux branches.

52
00:01:40,588 --> 00:01:42,902
Donc en fait,on perd de l'information

53
00:01:43,002 --> 00:01:44,269
en ayant des conditionnelles

54
00:01:44,369 --> 00:01:45,615
donc on perd quelque chose

55
00:01:45,715 --> 00:01:47,452
donc on va avoir quelque chose de moins précis.

56
00:01:51,332 --> 00:01:53,616
Si je m'intéresse uniquement à la majoration

57
00:01:53,955 --> 00:01:55,121
du coût, c'est-à-dire

58
00:01:55,221 --> 00:01:57,206
si je m'intéresse à la complexité au pire,

59
00:01:57,818 --> 00:01:58,871
je vais pouvoir dire

60
00:01:58,971 --> 00:02:00,336
la complexité au pire

61
00:02:00,436 --> 00:02:01,945
d'une condition,

62
00:02:02,269 --> 00:02:04,294
ça va être

63
00:02:04,394 --> 00:02:05,950
le coût d'évaluation

64
00:02:06,050 --> 00:02:07,607
des paramètres de la condition,

65
00:02:09,570 --> 00:02:12,303
en gros, vous avez à regarder

66
00:02:12,628 --> 00:02:15,271
le coût d'évaluation de ça,

67
00:02:16,707 --> 00:02:18,192
c'est cette partie-là,

68
00:02:19,657 --> 00:02:21,288
et puis, une fois que vous avez

69
00:02:22,079 --> 00:02:22,945
fait ça,

70
00:02:23,300 --> 00:02:24,690
vous avez à regarder

71
00:02:24,790 --> 00:02:26,295
le coût de chacune des branches

72
00:02:26,582 --> 00:02:28,397
et donc le coût de chacune des branches,

73
00:02:28,497 --> 00:02:32,210
C(A), ça correspondrait à la branche a

74
00:02:33,276 --> 00:02:34,139
C(B),

75
00:02:37,095 --> 00:02:38,700
c'est la branche b.

76
00:02:39,096 --> 00:02:40,649
Et le mieux que vous puissiez faire,

77
00:02:40,749 --> 00:02:42,435
à ce niveau-là de programmation,

78
00:02:42,763 --> 00:02:45,178
sauf si vous avez des informations supplémentaires

79
00:02:45,278 --> 00:02:47,456
et si vous savez que la branche, elle va passer par là ou par là,

80
00:02:47,754 --> 00:02:49,168
mais à ce moment-là,

81
00:02:49,268 --> 00:02:50,786
vous ne pouvez pas faire autre chose que dire

82
00:02:50,886 --> 00:02:52,881
c'est majoré par le max des deux.

83
00:02:53,554 --> 00:02:56,145
Et donc après, quand vous avez des if emboîtés, et cætera,

84
00:02:56,245 --> 00:02:57,795
ça fait des max de max de max de max

85
00:02:57,895 --> 00:02:59,467
donc vous risquez de vous éloigner

86
00:02:59,809 --> 00:03:01,895
sans doute du coût réel

87
00:03:02,244 --> 00:03:04,185
que vous aurez lorsque vous allez l'implémenter.

88
00:03:07,743 --> 00:03:10,550
Alors, j'ai mis un petit "aïe" ici

89
00:03:10,650 --> 00:03:12,489
parce que, ce qui était avant,

90
00:03:12,589 --> 00:03:13,741
on a été capable de compter

91
00:03:13,841 --> 00:03:15,468
mais ici, maintenant, on n'est plus capable de compter,

92
00:03:15,568 --> 00:03:16,754
on va dire c'est entre ça et ça.

93
00:03:17,833 --> 00:03:19,673
On a des encadrements, on a des majorations.

94
00:03:20,228 --> 00:03:21,737
Donc ce n'est pas si clair que ça

95
00:03:22,032 --> 00:03:25,292
que je sois capable de faire les choses.

96
00:03:25,734 --> 00:03:29,925
Il reste une dernière étape,

97
00:03:30,025 --> 00:03:33,091
si j'ai des appels de fonctions et procédures,

98
00:03:33,433 --> 00:03:34,938
et bien c'est toujours la même chose,

99
00:03:35,038 --> 00:03:38,103
le coût associé à un appel de procédure,

100
00:03:38,203 --> 00:03:40,138
c'est le coût qui correspond

101
00:03:40,238 --> 00:03:41,593
à l'évaluation de ses paramètres

102
00:03:42,219 --> 00:03:45,053
plus le coût du bloc interne à la procédure.

103
00:03:45,849 --> 00:03:48,462
Alors ici, j'ai caché le fait que

104
00:03:48,784 --> 00:03:50,432
ma machine sous-jacente

105
00:03:50,842 --> 00:03:53,938
était capable de gérer des appels de procédure

106
00:03:54,038 --> 00:03:55,356
et que ce n'était pas coûteux.

107
00:03:56,421 --> 00:03:58,282
Donc ici, il y a des hypothèses aussi

108
00:03:58,382 --> 00:03:59,161
qui sont cachées.

109
00:04:02,724 --> 00:04:03,990
Si je prends maintenant

110
00:04:04,490 --> 00:04:06,730
la méthode de calcul de complexité,

111
00:04:06,830 --> 00:04:08,936
en fait, je vais avoir deux méthodes,

112
00:04:09,282 --> 00:04:12,962
la méthode par assemblage et reconstruction,

113
00:04:13,062 --> 00:04:17,693
c'est-à-dire je vais partir des éléments les plus petits

114
00:04:17,793 --> 00:04:19,046
et grossir petit à petit

115
00:04:19,146 --> 00:04:22,079
et calculer ma complexité petit à petit,

116
00:04:22,377 --> 00:04:23,992
ou alors je peux composer,

117
00:04:24,092 --> 00:04:27,612
je vais avoir la composition,

118
00:04:27,712 --> 00:04:29,791
ou alors je fais une approche top-down,

119
00:04:30,114 --> 00:04:31,902
c'est-à-dire je vais

120
00:04:32,002 --> 00:04:34,003
définir la complexité de

121
00:04:34,313 --> 00:04:35,412
ma fonction

122
00:04:35,512 --> 00:04:37,975
comme étant une fonction de complexité

123
00:04:38,075 --> 00:04:40,195
des fonctions appelées en interne, et cætera.

124
00:04:40,941 --> 00:04:42,286
Les deux approches sont possibles,

125
00:04:42,386 --> 00:04:43,714
les deux façons de voir sont possibles,

126
00:04:43,814 --> 00:04:45,448
une constructive

127
00:04:45,548 --> 00:04:46,361
à partir du code,

128
00:04:46,669 --> 00:04:49,207
et une, je dirais, par décomposition du code

129
00:04:50,311 --> 00:04:52,870
qui permet d'avoir une autre vision

130
00:04:52,970 --> 00:04:54,358
de la complexité.

131
00:04:54,458 --> 00:04:56,162
Le résultat final sera le même.

132
00:04:56,648 --> 00:04:58,748
C'est juste deux façons d'appréhender le programme.

133
00:04:59,088 --> 00:05:01,763
Dans la partie sur la programmation,

134
00:05:02,129 --> 00:05:04,083
vous avez des calculs aussi

135
00:05:04,183 --> 00:05:06,035
de complexité de programmes

136
00:05:06,135 --> 00:05:07,993
et vous faites, petit à petit,

137
00:05:08,093 --> 00:05:08,997
d'abord les instructions,

138
00:05:09,097 --> 00:05:11,004
puis les boucles qui encapsulent les instructions,

139
00:05:11,104 --> 00:05:12,594
puis et cætera, et vous construisez comme ça

140
00:05:12,694 --> 00:05:14,303
la complexité du programme.

141
00:05:16,937 --> 00:05:21,188
Toujours garder à l'idée

142
00:05:21,550 --> 00:05:24,606
que ce calcul de complexité,

143
00:05:24,706 --> 00:05:25,726
son objectif

144
00:05:25,826 --> 00:05:26,884
n'est pas seulement

145
00:05:27,300 --> 00:05:29,804
de calculer le coût associé à l'algorithme.

146
00:05:30,869 --> 00:05:32,729
C'est quelque chose, cette démarche-là,

147
00:05:32,829 --> 00:05:34,224
qui vous permet d'avoir une analyse

148
00:05:34,324 --> 00:05:36,128
plus fine de l'algorithme,

149
00:05:36,228 --> 00:05:38,733
de comprendre mieux ce qu'il y a à l'intérieur de l'algorithme.

150
00:05:39,480 --> 00:05:40,885
Ça, c'est la première chose.

151
00:05:40,985 --> 00:05:41,999
La deuxième chose,

152
00:05:42,237 --> 00:05:43,812
c'est qu'ici, vous avez

153
00:05:44,154 --> 00:05:46,186
défini une mesure de qualité

154
00:05:46,286 --> 00:05:47,559
de votre algorithme, vous avez dit

155
00:05:47,659 --> 00:05:48,664
tiens, il coûte ça.

156
00:05:48,998 --> 00:05:50,455
Donc c'est une mesure de qualité

157
00:05:50,555 --> 00:05:52,984
et si vous avez une mesure de qualité,

158
00:05:53,084 --> 00:05:55,502
vous êtes capables de comparer différents algorithmes.

159
00:05:55,909 --> 00:05:57,256
C'est-à-dire vous êtes capables de

160
00:05:57,356 --> 00:05:58,742
dire celui-là,

161
00:05:58,842 --> 00:06:00,132
dans le pire cas,

162
00:06:00,232 --> 00:06:01,973
il va être meilleur que celui-là.

163
00:06:02,988 --> 00:06:04,388
Mais le meilleur cas de celui-là,

164
00:06:04,488 --> 00:06:06,404
peut-être qu'il est plus petit que, et cætera.

165
00:06:06,761 --> 00:06:09,203
Donc ça permet de comparer des algorithmes entre eux

166
00:06:09,303 --> 00:06:11,044
pour résoudre un même problème.

167
00:06:11,385 --> 00:06:13,809
Donc c'est sans doute une des façons,

168
00:06:14,178 --> 00:06:15,563
je dirais, de comparer

169
00:06:15,663 --> 00:06:19,053
ce qu'on appellera l'efficacité des algorithmes,

170
00:06:19,153 --> 00:06:20,165
leur complexité,

171
00:06:20,498 --> 00:06:22,373
de manière à pouvoir dire

172
00:06:22,473 --> 00:06:24,941
celui-ci prend moins de mémoire,

173
00:06:25,041 --> 00:06:26,317
d'espace mémoire que celui-là,

174
00:06:26,674 --> 00:06:28,020
donc je vais le préférer.

175
00:06:28,415 --> 00:06:30,485
Celui-ci a un temps d'exécution,

176
00:06:30,585 --> 00:06:31,850
une complexité,

177
00:06:32,359 --> 00:06:34,072
qui est linéaire dans la taille des entrées,

178
00:06:34,172 --> 00:06:35,916
celui-là a une complexité quadratique

179
00:06:36,016 --> 00:06:36,776
dans la taille des entrées,

180
00:06:36,876 --> 00:06:38,799
je vais prendre celui qui a une complexité linéaire.

181
00:06:39,645 --> 00:06:40,743
Donc c'est cette idée-là

182
00:06:41,750 --> 00:06:44,863
que l'on peut regarder.


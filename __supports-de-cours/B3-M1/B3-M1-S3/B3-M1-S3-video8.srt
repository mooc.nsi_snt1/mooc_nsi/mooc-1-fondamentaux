1
00:00:00,937 --> 00:00:02,276
Exemple suivant,

2
00:00:02,596 --> 00:00:03,941
le tri par insertion.

3
00:00:04,041 --> 00:00:06,225
Le tri par insertion, c'est

4
00:00:06,603 --> 00:00:10,087
je dirais le dual du tri par sélection

5
00:00:10,187 --> 00:00:10,833
qu'on a vu avant,

6
00:00:11,203 --> 00:00:13,625
c'est-à-dire qu'au lieu de prendre tout le paquet

7
00:00:13,725 --> 00:00:14,701
et de regarder le plus grand,

8
00:00:15,047 --> 00:00:17,951
ici, on va commencer petit

9
00:00:18,051 --> 00:00:19,778
et puis augmenter la taille du paquet.

10
00:00:20,096 --> 00:00:22,331
Donc on va insérer successivement

11
00:00:23,290 --> 00:00:24,638
de nouveaux éléments

12
00:00:24,934 --> 00:00:26,484
parmi les éléments déjà triés

13
00:00:27,563 --> 00:00:30,070
et puis l'ensemble des éléments triés

14
00:00:30,170 --> 00:00:33,153
va augmenter jusqu'à arriver à trier tous les éléments.

15
00:00:33,440 --> 00:00:35,730
Là encore, on peut avoir une vision récursive.

16
00:00:36,760 --> 00:00:38,482
La vision récursive

17
00:00:38,772 --> 00:00:40,644
se fait de la façon suivante.

18
00:00:42,272 --> 00:00:43,796
Soit l'ensemble est vide

19
00:00:43,896 --> 00:00:45,011
et dans ce cas-là, je n'ai rien à faire,

20
00:00:45,478 --> 00:00:47,030
sinon, je choisis

21
00:00:49,010 --> 00:00:50,141
un x donné

22
00:00:50,753 --> 00:00:52,754
et puis qu'est-ce que je vais faire avec ce x ?

23
00:00:52,955 --> 00:00:54,072
Je vais le mettre de côté

24
00:00:54,408 --> 00:00:56,582
et je vais trier par insertion

25
00:00:57,057 --> 00:00:58,328
le reste du paquet.

26
00:00:58,670 --> 00:00:59,854
Et donc en fait,

27
00:00:59,954 --> 00:01:01,407
je vais redescendre,

28
00:01:01,507 --> 00:01:02,922
je vais dire le tri par insertion,

29
00:01:03,290 --> 00:01:04,349
ça consiste à

30
00:01:04,449 --> 00:01:05,673
avoir un élément x

31
00:01:05,773 --> 00:01:06,643
et à l'insérer

32
00:01:07,034 --> 00:01:09,441
dans l'ensemble qui a été trié par insertion

33
00:01:09,541 --> 00:01:10,156
précédemment.

34
00:01:11,685 --> 00:01:12,996
On peut exprimer également

35
00:01:13,096 --> 00:01:14,941
insert(L, x)

36
00:01:15,125 --> 00:01:18,834
de façon récursive,

37
00:01:18,934 --> 00:01:19,670
ça ne pose pas de souci,

38
00:01:20,002 --> 00:01:21,008
mais cette idée-là,

39
00:01:21,108 --> 00:01:22,516
c'est de changer le point de vue

40
00:01:22,616 --> 00:01:23,333
c'est-à-dire que

41
00:01:23,493 --> 00:01:25,385
je fais mon opération d'insertion

42
00:01:25,485 --> 00:01:29,191
à la fin de mon appel récursif.

43
00:01:32,058 --> 00:01:35,124
On va passer aux travaux pratiques.

44
00:01:35,473 --> 00:01:36,754
Je reprends mon exemple.

45
00:01:36,854 --> 00:01:38,933
Ici, qu'est-ce que je vais avoir à ma disposition ?

46
00:01:39,033 --> 00:01:39,895
Je vais avoir des chalumeaux,

47
00:01:39,995 --> 00:01:41,310
et je vais les prendre un par un.

48
00:01:41,814 --> 00:01:43,045
J'ai un premier élément.

49
00:01:45,448 --> 00:01:45,999
Le voilà.

50
00:01:46,446 --> 00:01:47,852
J'en prends un autre.

51
00:01:50,321 --> 00:01:51,620
Le voilà et là,

52
00:01:52,005 --> 00:01:54,303
je l'insére, c'est-à-dire que je le mets

53
00:01:54,677 --> 00:01:56,206
à l'endroit où il devrait être

54
00:01:56,306 --> 00:01:57,504
dans mon tableau.

55
00:01:58,190 --> 00:01:59,568
Je prends un troisième élément.

56
00:02:00,492 --> 00:02:01,482
Et je vais l'insérer.

57
00:02:01,582 --> 00:02:02,459
Donc je vais le comparer.

58
00:02:02,559 --> 00:02:03,464
Là, il est plus grand,

59
00:02:03,779 --> 00:02:04,900
là, il est plus grand,

60
00:02:05,463 --> 00:02:08,591
donc je vais le mettre en toute première position.

61
00:02:11,263 --> 00:02:12,042
Continuons.

62
00:02:13,422 --> 00:02:14,464
J'ai un rose.

63
00:02:15,047 --> 00:02:16,203
Là, il est plus grand,

64
00:02:16,650 --> 00:02:17,828
là, il est plus petit,

65
00:02:18,169 --> 00:02:19,428
donc je l'insère

66
00:02:19,783 --> 00:02:20,643
entre les deux.

67
00:02:21,471 --> 00:02:22,235
J'ai le vert.

68
00:02:23,195 --> 00:02:25,139
Je regarde. Là, il est plus grand,

69
00:02:25,239 --> 00:02:26,203
là, il est plus grand,

70
00:02:27,145 --> 00:02:28,451
là, il est plus petit,

71
00:02:28,902 --> 00:02:29,830
donc je l'insère.

72
00:02:30,979 --> 00:02:31,928
J'ai un autre vert.

73
00:02:32,909 --> 00:02:34,061
Là, il est plus grand,

74
00:02:35,017 --> 00:02:35,863
là, il est plus petit,

75
00:02:36,307 --> 00:02:37,242
je l'insère.

76
00:02:39,826 --> 00:02:40,953
Je prends le rose,

77
00:02:41,053 --> 00:02:42,747
il est plus petit

78
00:02:42,847 --> 00:02:44,730
donc je le pose en toute première position.

79
00:02:46,624 --> 00:02:47,216
Je prends le vert,

80
00:02:47,347 --> 00:02:48,128
il est plus grand,

81
00:02:48,650 --> 00:02:49,586
il est plus petit,

82
00:02:50,075 --> 00:02:51,402
donc je l'insère.

83
00:02:53,793 --> 00:02:54,638
Un autre vert,

84
00:02:54,738 --> 00:02:55,660
il est plus grand que le rose,

85
00:02:56,250 --> 00:02:57,658
et je l'insère.

86
00:03:00,363 --> 00:03:01,292
Le bleu,

87
00:03:03,083 --> 00:03:04,052
je compare,

88
00:03:04,152 --> 00:03:04,761
je compare,

89
00:03:04,861 --> 00:03:08,869
là, il est plus petit,

90
00:03:09,305 --> 00:03:10,145
je l'insère.

91
00:03:11,364 --> 00:03:12,164
Le rose,

92
00:03:12,726 --> 00:03:13,567
il est plus grand,

93
00:03:13,667 --> 00:03:14,449
il est plus grand,

94
00:03:14,549 --> 00:03:15,245
il est plus petit,

95
00:03:16,151 --> 00:03:17,148
je l'insère.

96
00:03:17,317 --> 00:03:18,569
Et un dernier jaune,

97
00:03:19,049 --> 00:03:20,101
il est plus grand,

98
00:03:20,600 --> 00:03:21,567
il est plus petit,

99
00:03:21,895 --> 00:03:23,113
je l'insère.

100
00:03:23,608 --> 00:03:25,826
Donc en fait, ici,

101
00:03:26,187 --> 00:03:27,293
c'est chaque élément

102
00:03:27,393 --> 00:03:29,653
que je vais insérer au fur et à mesure.

103
00:03:29,753 --> 00:03:31,349
Je n'ai pas de connaissance globale

104
00:03:31,618 --> 00:03:32,787
sur mon tableau,

105
00:03:32,887 --> 00:03:34,599
je vais juste le comparer

106
00:03:34,699 --> 00:03:36,640
successivement à tous les éléments

107
00:03:36,740 --> 00:03:39,001
qui sont devant lui.

108
00:03:39,101 --> 00:03:40,714
Alors, j'aurais pu faire dans l'autre sens,

109
00:03:40,814 --> 00:03:42,744
j'aurais pu partir du plus petit

110
00:03:42,844 --> 00:03:43,499
au plus grand,

111
00:03:43,847 --> 00:03:46,391
le tri par insertion tel qu'on le présente d'habitude

112
00:03:46,529 --> 00:03:48,871
consiste à regarder le plus grand

113
00:03:48,971 --> 00:03:49,697
et à ordonner

114
00:03:49,797 --> 00:03:51,616
et à insérer en partant du plus grand.

115
00:03:51,866 --> 00:03:53,268
C'est ce que j'aurais pu faire ici

116
00:03:53,443 --> 00:03:54,518
mais c'est un peu pénible

117
00:03:54,618 --> 00:03:56,590
au niveau de la présentation et de la mise en œuvre.

118
00:03:57,019 --> 00:03:58,926
Revenons à notre exemple.

119
00:03:59,254 --> 00:04:01,179
Ici, on a vu le tri par insertion

120
00:04:01,279 --> 00:04:03,034
et on l'a vu de façon, je dirais,

121
00:04:03,722 --> 00:04:04,607
sans ordi,

122
00:04:05,766 --> 00:04:07,469
on l'a vu avec des objets.

123
00:04:07,790 --> 00:04:09,523
Et après, on va essayer de le traduire

124
00:04:09,623 --> 00:04:11,396
sur une écriture dans un tableau.

125
00:04:11,836 --> 00:04:13,714
Ça, c'est l'étape suivante.

126
00:04:14,142 --> 00:04:16,191
L'étape suivante consiste à

127
00:04:16,513 --> 00:04:19,440
écrire le tri par insertion.

128
00:04:19,904 --> 00:04:22,205
Là encore, c'est tout à fait intéressant.

129
00:04:22,711 --> 00:04:25,024
L'idée, c'est de  regarder le tableau

130
00:04:25,333 --> 00:04:27,073
et puis d'insérer petit à petit.

131
00:04:27,173 --> 00:04:29,453
Donc on va avoir un ensemble qui va être croissant

132
00:04:30,253 --> 00:04:31,654
dans mon tableau.

133
00:04:32,889 --> 00:04:36,108
Ce que je vais faire, c'est pour i égale 1 jusqu'à n - 1,

134
00:04:36,208 --> 00:04:38,893
je vous rappelle que mon tableau commence à l'indice 0,

135
00:04:39,324 --> 00:04:43,180
je vais regarder l'élément qui est en i

136
00:04:43,280 --> 00:04:44,925
et je vais voir où est-ce que je dois l'insérer.

137
00:04:45,419 --> 00:04:48,176
Alors ici aussi, un dessin

138
00:04:48,276 --> 00:04:50,634
va être tout à fait intéressant.

139
00:04:51,259 --> 00:04:52,410
J'ai mon tableau.

140
00:04:54,247 --> 00:04:55,927
Ici, j'ai mon indice i.

141
00:04:56,413 --> 00:04:58,682
Et ici déjà, initialement,

142
00:04:59,101 --> 00:05:00,530
tout ça, c'est trié.

143
00:05:03,822 --> 00:05:04,992
Et donc l'idée,

144
00:05:05,307 --> 00:05:07,025
c'est de prendre l'élément qui est là

145
00:05:07,926 --> 00:05:11,333
le comparer avec celui qui précède.

146
00:05:13,423 --> 00:05:15,153
Si jamais il est plus petit,

147
00:05:15,253 --> 00:05:16,090
on échange.

148
00:05:16,451 --> 00:05:18,302
Si jamais il est plus petit, on échange.

149
00:05:18,402 --> 00:05:19,992
Si jamais il est plus petit, on échange.

150
00:05:21,058 --> 00:05:22,576
Ici, je vais le mettre ici.

151
00:05:22,916 --> 00:05:25,370
Et qu'est-ce que j'ai sur l'élément qui est là ?

152
00:05:27,966 --> 00:05:29,809
En fait, cet élément-là

153
00:05:29,909 --> 00:05:32,442
est plus petit que l'élément qui était en position i.

154
00:05:32,880 --> 00:05:34,937
C'est une façon de faire

155
00:05:35,242 --> 00:05:36,996
qui permet effectivement

156
00:05:37,096 --> 00:05:38,435
de réaliser le tri par insertion,

157
00:05:38,535 --> 00:05:41,346
mais c'est exactement ce que j'ai fait avec mes baguettes.

158
00:05:41,601 --> 00:05:42,745
Il n'y a rien de plus,

159
00:05:43,160 --> 00:05:44,532
sauf que c'est des jeux d'indices,

160
00:05:44,632 --> 00:05:45,722
on essaie de faire attention.

161
00:05:46,095 --> 00:05:48,238
Ici, pour faire mon itération,

162
00:05:48,861 --> 00:05:49,912
je vais avoir j

163
00:05:50,158 --> 00:05:51,807
qui correspond à l'indice

164
00:05:52,149 --> 00:05:53,730
de l'élément que j'examine

165
00:05:54,036 --> 00:05:56,505
pour voir s'il est plus petit,

166
00:05:56,849 --> 00:05:59,023
donc c'est cet indice j qui est là,

167
00:05:59,338 --> 00:06:01,204
qui correspond à i - 1 au début

168
00:06:01,304 --> 00:06:02,404
puis qui va remonter.

169
00:06:02,679 --> 00:06:04,287
Et puis je vais échanger

170
00:06:04,387 --> 00:06:05,497
à chaque étape,

171
00:06:05,597 --> 00:06:07,609
cette opération d'échange qui est là,

172
00:06:08,047 --> 00:06:10,751
qui correspond à un échange que je fais

173
00:06:11,054 --> 00:06:13,139
à chacune des étapes.

174
00:06:13,239 --> 00:06:15,854
Effectivement, mon dessin est un peu cryptique maintenant

175
00:06:15,954 --> 00:06:18,513
mais ça n'est pas bien grave.

176
00:06:20,807 --> 00:06:23,663
Si je regarde l'exemple qu'on avait tout à l'heure,

177
00:06:23,763 --> 00:06:25,769
on va faire exactement la même chose.

178
00:06:26,201 --> 00:06:27,639
Je vais avoir machine

179
00:06:27,739 --> 00:06:30,522
qui va être mon exemple fétiche.

180
00:06:30,936 --> 00:06:33,043
Je regarde une première itération,

181
00:06:33,143 --> 00:06:35,784
le 'm' est en place évidemment,

182
00:06:35,884 --> 00:06:38,054
là, j'ai un tableau qui est trié à gauche.

183
00:06:38,154 --> 00:06:39,981
Et en rouge, j'ai mis les éléments

184
00:06:40,081 --> 00:06:41,753
qu'il va falloir que j'insère successivement.

185
00:06:42,414 --> 00:06:44,367
Ici, je vais insérer le 'a'.

186
00:06:44,823 --> 00:06:46,022
Pour insérer le 'a',

187
00:06:46,122 --> 00:06:47,243
je vais décaler

188
00:06:47,343 --> 00:06:49,263
et le mettre en position 1.

189
00:06:49,824 --> 00:06:54,234
Puis je vais insérer le 'c'.

190
00:06:54,951 --> 00:06:57,168
Ça, ça va me faire la première partie à gauche.

191
00:06:57,671 --> 00:07:00,182
Puis je vais insérer le 'h'.

192
00:07:04,330 --> 00:07:10,380
Et puis le 'i'.

193
00:07:10,480 --> 00:07:13,090
Et puis le 'n'.

194
00:07:13,190 --> 00:07:15,681
Et puis, finalement, le 'e'.

195
00:07:16,004 --> 00:07:17,471
Donc on voit ici

196
00:07:17,837 --> 00:07:19,395
la belle décroissance

197
00:07:19,495 --> 00:07:21,291
de l'ensemble des éléments

198
00:07:22,531 --> 00:07:25,145
qui restent à trier.

199
00:07:25,245 --> 00:07:26,996
Je ne suis pas très habile de mes doigts.

200
00:07:27,151 --> 00:07:29,588
Voilà, on voit bien le front

201
00:07:29,688 --> 00:07:31,533
qui va effectivement faire que

202
00:07:32,562 --> 00:07:34,732
l'algorithme trie au fur et à mesure.

203
00:07:35,679 --> 00:07:37,208
Comment on fait la preuve ?

204
00:07:37,308 --> 00:07:39,713
J'ai exactement fait la même preuve que tout à l'heure.

205
00:07:40,133 --> 00:07:42,364
On est vraiment sur une structure

206
00:07:42,464 --> 00:07:44,243
qui est du même acabit.

207
00:07:44,543 --> 00:07:47,570
La preuve va se faire en regardant des invariants.

208
00:07:47,835 --> 00:07:50,976
Donc là encore, j'ai une boucle interne

209
00:07:51,328 --> 00:07:53,309
qui va insérer

210
00:07:53,409 --> 00:07:54,619
l'élément dans sa place.

211
00:07:56,667 --> 00:07:59,128
Ça, ce n'est pas un souci.

212
00:07:59,286 --> 00:08:02,949
T[0..j] est trié en amont

213
00:08:03,275 --> 00:08:09,096
et T[j + 1..i] est trié.

214
00:08:09,196 --> 00:08:11,552
Les éléments de 0 à i sont inférieurs, et cætera.

215
00:08:11,917 --> 00:08:14,815
Donc là, j'ai véritablement mon invariant.

216
00:08:14,915 --> 00:08:17,241
Je pense qu'il faut le reprendre à tête reposée

217
00:08:17,341 --> 00:08:19,150
pour bien comprendre comment ça fonctionne.

218
00:08:20,469 --> 00:08:22,384
Et puis le variant,

219
00:08:22,484 --> 00:08:23,823
c'est évidemment que vous avez

220
00:08:23,923 --> 00:08:25,136
une boucle qui se termine.

221
00:08:26,426 --> 00:08:27,863
Quand vous sortez de la boucle,

222
00:08:27,963 --> 00:08:28,840
qu'est-ce qu'il va se passer ?

223
00:08:29,167 --> 00:08:31,437
Vous sortez de la boucle, soit j

224
00:08:32,188 --> 00:08:33,630
est négatif, donc

225
00:08:33,730 --> 00:08:35,633
vous avez tout parcouru et c'est fini,

226
00:08:36,406 --> 00:08:40,381
soit T[j] est plus petit que T[j + 1]

227
00:08:40,682 --> 00:08:41,790
et dans ce cas-là,

228
00:08:41,890 --> 00:08:43,756
vous avez votre tableau,

229
00:08:45,253 --> 00:08:50,328
vous avez ici i,

230
00:08:50,853 --> 00:08:56,923
là, vous avez l'élément qui était initialement en i

231
00:08:57,269 --> 00:08:58,167
qui était là,

232
00:08:58,737 --> 00:08:59,879
cet élément-là

233
00:09:00,921 --> 00:09:02,697
va être plus petit que celui-là.

234
00:09:02,979 --> 00:09:04,966
Celui-là va être plus petit que celui-là,

235
00:09:05,066 --> 00:09:06,288
celui-là, celui-là, celui-là.

236
00:09:06,638 --> 00:09:08,323
Et donc votre tableau finalement

237
00:09:08,726 --> 00:09:10,560
ici, est trié.

238
00:09:11,387 --> 00:09:14,200
C'est ça le dessin que l'on pourrait faire.

239
00:09:14,751 --> 00:09:17,672
Évidemment, on peut l'animer,

240
00:09:17,772 --> 00:09:18,967
on peut mettre des couleurs,

241
00:09:19,067 --> 00:09:20,227
on peut faire des choses en plus

242
00:09:20,578 --> 00:09:23,536
pour rendre la présentation plus riche.

243
00:09:23,800 --> 00:09:27,171
Si maintenant je regarde l'itération externe,

244
00:09:27,575 --> 00:09:29,091
à la fin de la boucle interne,

245
00:09:29,191 --> 00:09:30,578
à chaque fois mon tableau est trié.

246
00:09:30,939 --> 00:09:32,781
Donc effectivement, je vais avoir

247
00:09:33,089 --> 00:09:34,766
à répéter cette opération-là

248
00:09:34,954 --> 00:09:36,485
du tableau de taille 1

249
00:09:36,585 --> 00:09:37,788
jusqu'au tableau de taille n,

250
00:09:38,781 --> 00:09:40,384
jusqu'à l'indice n - 1

251
00:09:40,484 --> 00:09:42,672
pour avoir effectivement trié mon tableau.

252
00:09:43,026 --> 00:09:44,759
Donc on a de cette façon-là

253
00:09:45,373 --> 00:09:48,312
montré que le tri par insertion est correct.

254
00:09:48,630 --> 00:09:50,484
Alors là encore, je rappelle

255
00:09:50,696 --> 00:09:53,773
ici, quand on va travailler avec les invariants et cætera,

256
00:09:53,873 --> 00:09:55,756
moi, je pense qu'un beau dessin

257
00:09:55,856 --> 00:09:58,091
qui explique bien comment ça se passe

258
00:09:58,432 --> 00:10:00,076
est sans doute plus informatif

259
00:10:00,176 --> 00:10:03,685
que l'écriture de l'invariant

260
00:10:03,785 --> 00:10:05,647
avec toutes les chances de se tromper dans les indices.

261
00:10:06,360 --> 00:10:07,691
J'espère ne pas m'être trompé,

262
00:10:07,791 --> 00:10:09,245
s'il y a des fautes, il faudra les corriger.

263
00:10:09,705 --> 00:10:11,867
Mais l'idée, c'est vraiment de faire le dessin

264
00:10:11,967 --> 00:10:13,507
et de savoir exactement ce que l'on fait.

265
00:10:14,554 --> 00:10:15,914
Alors, pour la complexité,

266
00:10:16,014 --> 00:10:17,215
on a pareil,

267
00:10:17,315 --> 00:10:20,391
on va compter l'opérateur comparaison.

268
00:10:20,700 --> 00:10:22,392
Ici, on a des données de taille n,

269
00:10:22,492 --> 00:10:23,427
la taille du tableau,

270
00:10:24,144 --> 00:10:26,645
et puis l'itération interne va faire

271
00:10:26,939 --> 00:10:29,298
somme de i - 1 à 0

272
00:10:29,677 --> 00:10:31,737
au maximum - ah oui, mince,

273
00:10:34,137 --> 00:10:35,633
je ne me suis pas rendu compte mais

274
00:10:35,733 --> 00:10:38,215
là, en fait, quand j'exécutais mon algorithme,

275
00:10:39,412 --> 00:10:44,031
les opérations dépendaient des valeurs des clés

276
00:10:44,131 --> 00:10:45,325
c'est-à-dire que

277
00:10:46,313 --> 00:10:49,139
autant avec le tri par sélection

278
00:10:49,239 --> 00:10:54,357
nous avions quelque chose qui était constant,

279
00:10:54,457 --> 00:10:56,615
autant là, ça dépend des valeurs que j'ai.

280
00:10:56,949 --> 00:10:58,217
Donc c'est un peu compliqué.

281
00:10:59,059 --> 00:11:00,888
En fait, ce que je vais pouvoir dire,

282
00:11:00,988 --> 00:11:02,837
c'est que je peux m'arrêter tout de suite,

283
00:11:03,187 --> 00:11:05,381
dans ce cas-là, j'aurai la valeur 1,

284
00:11:05,481 --> 00:11:07,706
ou alors je peux redescendre jusqu'au bout

285
00:11:07,806 --> 00:11:10,605
si j'ai l'élément de valeur minimale,

286
00:11:10,705 --> 00:11:14,377
donc je vais m'arrêter au bout de i comparaisons.

287
00:11:16,464 --> 00:11:18,043
C'est embêtant, ça.

288
00:11:18,143 --> 00:11:21,776
Donc en fait, je n'ai pas une évaluation du coût

289
00:11:21,876 --> 00:11:23,740
de mon algorithme qui est exacte,

290
00:11:23,840 --> 00:11:24,846
ça dépend des données.

291
00:11:25,390 --> 00:11:28,483
Je sais juste que c'est compris entre 1 et i.

292
00:11:29,023 --> 00:11:31,480
Et je peux trouver un exemple qui donne 1

293
00:11:31,580 --> 00:11:33,518
et je peux trouver un exemple qui donne i.

294
00:11:35,474 --> 00:11:37,249
Maintenant, pour l'itération externe,

295
00:11:37,349 --> 00:11:39,893
je ne fais que la borne supérieure dans un premier temps.

296
00:11:40,406 --> 00:11:43,524
Et bien je vais avoir un coût qui va être de

297
00:11:44,049 --> 00:11:46,326
i à chacune des itérations internes

298
00:11:46,426 --> 00:11:47,840
et je retrouve la même formule qu'avant,

299
00:11:48,449 --> 00:11:49,549
sauf qu'ici, j'ai

300
00:11:51,727 --> 00:11:54,656
C(n) qui est plus petit que n(n - 1)/2

301
00:11:55,117 --> 00:11:56,434
donc j'ai un majorant

302
00:11:56,906 --> 00:11:58,772
et je peux montrer d'un autre côté

303
00:11:58,872 --> 00:12:00,128
qu'il est toujours plus grand

304
00:12:00,568 --> 00:12:02,073
que n -  1.

305
00:12:02,783 --> 00:12:04,955
Ce qui va être important ici à noter,

306
00:12:05,055 --> 00:12:06,300
c'est trouver des exemples

307
00:12:06,400 --> 00:12:08,048
qui donnent ces deux valeurs-là.

308
00:12:08,573 --> 00:12:11,186
On peut trouver facilement ces deux exemples.

309
00:12:11,359 --> 00:12:15,792
Le premier, c'est : le tableau est trié en ordre inverse.

310
00:12:15,892 --> 00:12:17,774
Dans ce cas-là, chaque fois que vous avez un élément,

311
00:12:17,874 --> 00:12:19,345
vous devez le ramener tout au début

312
00:12:19,445 --> 00:12:21,007
donc faire toutes les comparaisons.

313
00:12:22,061 --> 00:12:24,614
Et dans l'autre cas, c'est si le tableau est déjà trié.

314
00:12:24,714 --> 00:12:26,497
Dans ce cas-là, vous comparez avec le précédent

315
00:12:26,597 --> 00:12:27,825
mais comme vous êtes plus grand

316
00:12:27,925 --> 00:12:30,016
vous n'avez pas besoin d'avancer.

317
00:12:30,632 --> 00:12:32,718
Donc on a les deux.

318
00:12:32,818 --> 00:12:34,147
Et donc ce qu'on peut dire,

319
00:12:34,247 --> 00:12:35,572
c'est que, au pire,

320
00:12:35,689 --> 00:12:38,809
vous avez n(n - 1)/2 opérations,

321
00:12:39,906 --> 00:12:42,670
que vous avez un exemple qui vous donne du n(n - 1)/2

322
00:12:42,770 --> 00:12:44,562
donc vous êtes en O(n^2).

323
00:12:44,918 --> 00:12:46,904
En fait, vous êtes dans du thêta de n^2

324
00:12:47,004 --> 00:12:51,091
parce que vous avez vraiment un exemple.


1
00:00:01,906 --> 00:00:03,072
On va voir maintenant

2
00:00:03,404 --> 00:00:05,121
deux exemples

3
00:00:05,444 --> 00:00:07,137
de structures de données

4
00:00:07,237 --> 00:00:08,385
qui sont classiques,

5
00:00:08,656 --> 00:00:10,238
qui sont au programme de terminale,

6
00:00:10,338 --> 00:00:13,957
c'est la pile et la file

7
00:00:14,209 --> 00:00:17,162
qui sont en fait deux outils

8
00:00:17,262 --> 00:00:18,743
qui vont servir en particulier

9
00:00:18,843 --> 00:00:19,836
pour les parcours de graphes

10
00:00:19,936 --> 00:00:21,355
et dans plein d'autres situations

11
00:00:21,455 --> 00:00:22,161
comme on va le voir.

12
00:00:22,534 --> 00:00:24,412
Qu'est-ce que c'est qu'une pile ?

13
00:00:24,787 --> 00:00:27,226
Intuitivement, une pile, la pile d'assiettes,

14
00:00:27,507 --> 00:00:29,555
moi, j'aime bien utiliser l'analogie

15
00:00:29,655 --> 00:00:31,798
avec un jeu de cartes,

16
00:00:32,145 --> 00:00:33,467
vous avez un jeu de cartes en main,

17
00:00:33,567 --> 00:00:35,005
le paquet de cartes est en main,

18
00:00:35,260 --> 00:00:37,903
et vous voyez la carte qui est sur le dessus,

19
00:00:38,284 --> 00:00:39,767
vous ne voyez pas les autres cartes

20
00:00:39,867 --> 00:00:40,900
qui sont cachées derrière,

21
00:00:41,166 --> 00:00:42,539
et les seules opérations que vous pouvez faire,

22
00:00:42,639 --> 00:00:43,805
c'est retirer une carte

23
00:00:44,196 --> 00:00:46,213
donc vous avez alors accès à la carte qui est derrière,

24
00:00:46,412 --> 00:00:49,118
et donc vous traitez la carte et vous la perdez quelque part,

25
00:00:49,783 --> 00:00:52,115
ou alors, vous pouvez rajouter une carte par dessus.

26
00:00:52,420 --> 00:00:54,723
Cette analogie est, je trouve, assez sympa

27
00:00:55,017 --> 00:00:56,421
au sens où on se rend bien compte

28
00:00:56,521 --> 00:01:00,245
que la seule manière d'avoir accès à votre collection,

29
00:01:00,600 --> 00:01:03,678
c'est d'arriver par le dessus de la pile.

30
00:01:04,453 --> 00:01:08,601
Cette discipline de traitement de données,

31
00:01:08,701 --> 00:01:10,702
c'est ce qu'on appelle la discipline LIFO,

32
00:01:10,802 --> 00:01:11,938
last in, first out,

33
00:01:12,178 --> 00:01:13,879
dernier arrivé, premier servi.

34
00:01:14,878 --> 00:01:18,705
Alors, à quoi sert effectivement la notion de pile ?

35
00:01:19,088 --> 00:01:23,557
En fait, dès que vous allez faire un appel de fonction

36
00:01:23,887 --> 00:01:26,201
dans votre exécution,

37
00:01:26,301 --> 00:01:28,036
et bien vous aurez une pile

38
00:01:28,347 --> 00:01:30,021
qui va vous permettre de dire

39
00:01:30,121 --> 00:01:31,531
je vais mettre sur la pile

40
00:01:31,779 --> 00:01:33,537
quelle est la fonction qui est appelée,

41
00:01:33,807 --> 00:01:35,432
quels sont les paramètres qui sont associés,

42
00:01:35,751 --> 00:01:37,899
éventuellement sauvegarder le contexte

43
00:01:37,999 --> 00:01:39,940
dans lequel la fonction a été appelée,

44
00:01:40,258 --> 00:01:43,764
et puis vous allez passer dans l'exécution de votre fonction.

45
00:01:44,058 --> 00:01:47,271
En fait, à chaque fois qu'on va avoir un appel de fonction,

46
00:01:47,371 --> 00:01:48,386
on va empiler

47
00:01:48,486 --> 00:01:52,378
un objet un peu complexe

48
00:01:52,478 --> 00:01:54,943
de façon à pouvoir après

49
00:01:55,043 --> 00:01:57,198
qu'on a terminé l'exécution de l'appel de fonction,

50
00:01:57,475 --> 00:01:59,740
retrouver le contexte tel qu'il doit être

51
00:02:00,031 --> 00:02:02,304
et continuer l'exécution du programme.

52
00:02:02,725 --> 00:02:04,744
Quand vous avez des appels récursifs,

53
00:02:04,844 --> 00:02:06,940
on a effectivement la pile de récursivité

54
00:02:07,259 --> 00:02:08,311
qui vous permet de

55
00:02:08,411 --> 00:02:10,477
stocker les différentes valeurs

56
00:02:10,707 --> 00:02:12,842
au fur et à mesure que vous descendez dans la récursion.

57
00:02:14,285 --> 00:02:16,057
Sous-entendus à la pile,

58
00:02:16,370 --> 00:02:17,433
vous avez également

59
00:02:17,533 --> 00:02:19,875
des parcours, en particulier le parcours d'arbre,

60
00:02:20,191 --> 00:02:22,691
le parcours de graphe avec le retour en arrière,

61
00:02:22,987 --> 00:02:24,856
au fur et à mesure que vous voyez des éléments,

62
00:02:24,956 --> 00:02:25,728
vous les empilez

63
00:02:26,013 --> 00:02:29,517
et après, vous dépilez au fur et à mesure que vous voulez visiter.

64
00:02:29,853 --> 00:02:32,399
Et on verra en petit exemple, à la fin du cours,

65
00:02:32,499 --> 00:02:33,989
comment traiter les parenthésages

66
00:02:34,245 --> 00:02:35,546
à l'aide d'une pile.

67
00:02:36,746 --> 00:02:38,946
Alors les opérations sont relativement simples.

68
00:02:40,104 --> 00:02:42,245
Vous avez des opérations d'insertion.

69
00:02:42,669 --> 00:02:44,465
Par exemple ici, l'opération a

70
00:02:44,565 --> 00:02:45,850
consiste à insérer

71
00:02:46,176 --> 00:02:47,082
4 dans la pile

72
00:02:47,182 --> 00:02:49,449
donc ici, je prends cette pile-là,

73
00:02:51,034 --> 00:02:53,276
et donc je mets l'élément 4

74
00:02:56,420 --> 00:02:57,858
à l'intérieur de la pile.

75
00:02:59,461 --> 00:03:01,725
Et puis ici, je vais empiler 2

76
00:03:02,149 --> 00:03:04,396
et puis ici, je vais dépiler.

77
00:03:10,729 --> 00:03:11,910
Dépiler ou extraire,

78
00:03:12,010 --> 00:03:13,373
ça dépend des livres

79
00:03:13,727 --> 00:03:14,706
dans lesquels vous êtes,

80
00:03:14,806 --> 00:03:16,892
ici, il aurait plutôt fallu mettre dépiler

81
00:03:17,209 --> 00:03:19,699
dans le texte, mais on comprend très bien ce que ça veut dire.

82
00:03:21,517 --> 00:03:23,656
Une spécification, ça va consister en quoi ?

83
00:03:25,038 --> 00:03:26,225
Ça va consister

84
00:03:26,578 --> 00:03:28,367
à définir un certain nombre de fonctions,

85
00:03:28,694 --> 00:03:30,956
d'opérations que vous allez pouvoir faire sur votre structure.

86
00:03:31,986 --> 00:03:33,854
Première opération que vous pouvez faire,

87
00:03:33,954 --> 00:03:35,020
c'est créer une pile vide

88
00:03:35,120 --> 00:03:36,772
dans laquelle vous allez pouvoir mettre des éléments.

89
00:03:37,190 --> 00:03:41,155
Deuxième opération, vous allez pouvoir empiler un élément.

90
00:03:41,462 --> 00:03:43,701
Donc vous avez deux arguments ici,

91
00:03:44,076 --> 00:03:46,873
un élément, qui n'est pas spécifié,

92
00:03:46,973 --> 00:03:50,513
c'est-à-dire qu'une pile va dépendre d'un type d'élément,

93
00:03:50,871 --> 00:03:51,943
et une pile

94
00:03:52,287 --> 00:03:54,475
et ça vous ressort une pile.

95
00:03:56,303 --> 00:03:58,352
Je pense que cet exercice est important,

96
00:03:58,452 --> 00:04:00,895
bien voir exactement ce que font vos opérations.

97
00:04:01,611 --> 00:04:04,397
Ça, c'est le premier constructeur qui est Empiler.

98
00:04:04,723 --> 00:04:07,642
Et le deuxième, ça va être Dépiler,

99
00:04:07,742 --> 00:04:10,195
qui va prendre une pile en argument

100
00:04:10,295 --> 00:04:12,614
et qui va renvoyer une autre pile

101
00:04:12,714 --> 00:04:14,301
à laquelle on aura enlevé le premier élément,

102
00:04:14,716 --> 00:04:15,852
l'élément du sommet.

103
00:04:16,133 --> 00:04:18,707
Et on a deux observateurs,

104
00:04:18,807 --> 00:04:20,738
c'est-à-dire qui vous permettent de déterminer

105
00:04:20,838 --> 00:04:22,281
des propriétés ou des états

106
00:04:22,633 --> 00:04:23,583
de votre pile.

107
00:04:24,201 --> 00:04:26,010
Le premier observateur, c'est EstVide,

108
00:04:26,451 --> 00:04:27,909
il vous teste si la pile est vide,

109
00:04:28,174 --> 00:04:29,897
et le deuxième opérateur, c'est Sommet,

110
00:04:30,178 --> 00:04:31,543
qui vous renvoie la valeur

111
00:04:31,643 --> 00:04:33,416
de la carte qui est au sommet du tas,

112
00:04:33,516 --> 00:04:34,565
du paquet de cartes.

113
00:04:34,931 --> 00:04:37,138
Ce sont ces deux-là.

114
00:04:38,941 --> 00:04:40,927
On a un troisième constructeur, évidemment,

115
00:04:41,027 --> 00:04:43,171
qui est le constructeur PileVide.

116
00:04:47,189 --> 00:04:49,035
Alors on a des préconditions évidemment.

117
00:04:49,656 --> 00:04:51,400
Il faut bien faire la différence

118
00:04:51,792 --> 00:04:53,434
entre la notion de précondition

119
00:04:53,846 --> 00:04:56,623
et les contraintes liées à l'implémentation.

120
00:04:57,147 --> 00:04:58,941
Si vous implémentez votre pile

121
00:04:59,041 --> 00:05:02,808
à l'intérieur, je dirais, de votre machine

122
00:05:02,908 --> 00:05:04,286
dans un espace mémoire qui est fini,

123
00:05:04,544 --> 00:05:06,885
vous pouvez avoir des problèmes de débordement,

124
00:05:06,985 --> 00:05:08,386
c'est-à-dire que vous voulez empiler trop de choses

125
00:05:08,486 --> 00:05:09,245
et que ça ne rentre pas.

126
00:05:09,527 --> 00:05:11,211
Ça, c'est une erreur d'implémentation.

127
00:05:11,504 --> 00:05:14,948
Ici, on parlera plutôt de précondition

128
00:05:15,048 --> 00:05:17,850
et d'erreur algorithmique

129
00:05:17,950 --> 00:05:20,091
ou de programmation de base,

130
00:05:21,146 --> 00:05:25,146
si par exemple, vous voulez consulter la pile,

131
00:05:25,246 --> 00:05:28,782
Sommet(p) par exemple ici,

132
00:05:30,210 --> 00:05:32,345
vous voulez voir Sommet(p),

133
00:05:32,445 --> 00:05:34,405
il faut que la pile soit non vide.

134
00:05:34,621 --> 00:05:36,591
De la même façon, vous ne pouvez pas dépiler

135
00:05:36,900 --> 00:05:37,845
une pile qui est vide.

136
00:05:37,945 --> 00:05:39,775
C'est une erreur de conception.

137
00:05:40,246 --> 00:05:43,079
Pas de la pile, mais de votre conception d'algorithme

138
00:05:43,433 --> 00:05:45,321
si vous avez à un moment donné

139
00:05:45,421 --> 00:05:46,531
quelque chose qui est de ce type-là.

140
00:05:50,081 --> 00:05:51,666
Et puis vous avez les axiomes.

141
00:05:51,766 --> 00:05:53,746
Donc les axiomes sont relativement simples.

142
00:05:54,285 --> 00:05:57,035
Ici, à chaque constructeur, on va avoir

143
00:05:57,676 --> 00:05:59,710
des axiomes et puis on va comparer.

144
00:06:00,044 --> 00:06:03,849
Par exemple, si vous regardez EstVide,

145
00:06:04,187 --> 00:06:07,615
EstVide(PileVide) va renvoyer vrai

146
00:06:07,966 --> 00:06:11,780
et EstVide(Empiler(e, p)) va renvoyer faux.

147
00:06:12,057 --> 00:06:15,389
Ça vous permet de spécifier les propriétés de PileVide.

148
00:06:16,818 --> 00:06:18,177
Si vous prenez Sommet,

149
00:06:18,277 --> 00:06:21,459
Sommet(Empiler(e, p)), ça doit être égal à e.

150
00:06:22,017 --> 00:06:23,154
Et pour Dépiler,

151
00:06:23,292 --> 00:06:25,556
Dépiler(Empiler(e, p))

152
00:06:25,891 --> 00:06:26,815
doit être égal à p.

153
00:06:27,440 --> 00:06:29,746
Donc on exprime les axiomes

154
00:06:29,846 --> 00:06:31,019
comme des propriétés

155
00:06:31,119 --> 00:06:33,682
que doivent vérifier les différents constructeurs

156
00:06:33,999 --> 00:06:37,196
de votre structure.

157
00:06:38,215 --> 00:06:40,001
Si je prends maintenant la file,

158
00:06:40,101 --> 00:06:41,401
c'est exactement pareil.

159
00:06:41,890 --> 00:06:44,506
Ça correspond à la file d'attente du supermarché.

160
00:06:45,080 --> 00:06:46,269
Ça correspond également

161
00:06:46,369 --> 00:06:48,035
à une file d'attente de processus

162
00:06:48,135 --> 00:06:49,680
en attente d'impression par exemple.

163
00:06:50,080 --> 00:06:53,226
Donc vous avez cette notion de client

164
00:06:53,539 --> 00:06:55,774
et de premier arrivé, premier servi.

165
00:06:55,874 --> 00:06:57,859
La discipline qu'on appelle FIFO,

166
00:06:57,959 --> 00:06:59,260
first in, first out.

167
00:07:00,005 --> 00:07:03,333
Vous pouvez regarder

168
00:07:03,433 --> 00:07:06,008
la façon de l'utiliser,

169
00:07:07,435 --> 00:07:09,095
à l'aide des exemples que je vous ai donnés

170
00:07:09,469 --> 00:07:11,000
de files classiques.

171
00:07:11,688 --> 00:07:14,205
Et puis ça servira également

172
00:07:14,305 --> 00:07:16,480
dans tout ce qui va être parcours de graphe et d'arbre

173
00:07:16,805 --> 00:07:18,939
puisque ça vous permet de traiter

174
00:07:19,316 --> 00:07:22,662
les objets dans l'ordre dans lequel

175
00:07:22,762 --> 00:07:24,114
vous les avez vus pour la première fois.

176
00:07:24,806 --> 00:07:27,450
C'est vraiment un outil également important.

177
00:07:28,121 --> 00:07:31,391
Et donc, pour une file, on a une entrée et une sortie.

178
00:07:31,859 --> 00:07:34,567
Et puis on va enfiler.

179
00:07:35,090 --> 00:07:37,519
Ici, j'enfile l'élément 4,

180
00:07:37,619 --> 00:07:38,632
entre là et là.

181
00:07:39,330 --> 00:07:41,128
Là, je vais enfiler l'élément 4.

182
00:07:41,810 --> 00:07:43,782
Là, je vais enfiler l'élément 2.

183
00:07:44,663 --> 00:07:46,264
Et là, je vais défiler

184
00:07:46,364 --> 00:07:48,078
et vous voyez que vous avez pris l'élément 5

185
00:07:48,450 --> 00:07:51,610
et que l'élément 5, vous l'avez fait sortir.

186
00:07:55,095 --> 00:07:56,461
Alors, les spécifications,

187
00:07:56,561 --> 00:07:59,012
ça sera un petit peu plus compliqué, complexe.

188
00:07:59,253 --> 00:08:03,050
Là, je vous laisse regarder les spécifications sur les slides.

189
00:08:03,503 --> 00:08:06,060
On va avoir exactement le même genre d'axiomes.

190
00:08:06,465 --> 00:08:09,848
On va avoir le même genre de préconditions associées.

191
00:08:10,272 --> 00:08:12,627
Et puis on va avoir des choses du genre

192
00:08:13,523 --> 00:08:16,390
lorsqu'on va regarder Tête(Enfiler(e, ...))

193
00:08:16,666 --> 00:08:19,077
on va avoir effectivement des tas de règles

194
00:08:19,177 --> 00:08:20,928
qui vont nous permettre de dire

195
00:08:21,028 --> 00:08:22,679
exactement ce que font les opérateurs.

196
00:08:24,418 --> 00:08:25,381
Vous vous rendez compte que,

197
00:08:25,481 --> 00:08:26,520
avec cette vision-là,

198
00:08:29,136 --> 00:08:30,373
vous avez une vision

199
00:08:30,473 --> 00:08:31,611
où vous avez des fonctions

200
00:08:31,711 --> 00:08:32,878
de fonctions de fonctions,

201
00:08:32,978 --> 00:08:34,802
et des appels qui s'imbriquent les uns dans les autres.

202
00:08:35,499 --> 00:08:38,873
Donc on perd la notion d'état,

203
00:08:38,973 --> 00:08:40,197
on est juste sur des fonctions

204
00:08:40,491 --> 00:08:41,801
qui s'appellent les unes les autres.

205
00:08:42,103 --> 00:08:43,427
C'est souvent plus simple

206
00:08:43,527 --> 00:08:44,513
pour décrire les choses

207
00:08:44,913 --> 00:08:47,253
et après, on a un passage

208
00:08:50,017 --> 00:08:51,223
dans un langage de programmation

209
00:08:51,563 --> 00:08:53,353
dans lequel on va être plus pragmatique

210
00:08:53,453 --> 00:08:54,844
et on va avoir des notions d'état

211
00:08:55,168 --> 00:08:58,639
mais le principe, il est exactement le même.

212
00:08:59,600 --> 00:09:02,108
Voilà, ça, c'est la dernière de défilée.

213
00:09:03,628 --> 00:09:06,916
Alors, la dernière structure qu'on va voir là,

214
00:09:07,016 --> 00:09:07,720
qui est importante,

215
00:09:07,820 --> 00:09:09,523
qui est ce qu'on appelle la file à priorité,

216
00:09:10,038 --> 00:09:12,941
consiste à dire lorsque vous allez mettre des objets

217
00:09:13,574 --> 00:09:14,741
dans la collection,

218
00:09:14,841 --> 00:09:16,297
en fait, vous allez leur allouer

219
00:09:16,397 --> 00:09:18,339
une priorité qui correspond

220
00:09:18,686 --> 00:09:20,136
quelque chose qui a du sens pour vous,

221
00:09:20,438 --> 00:09:22,648
et vous avez envie de récupérer

222
00:09:22,900 --> 00:09:25,308
les éléments dans un certain ordre de priorité.

223
00:09:26,062 --> 00:09:28,457
Par exemple, si vous êtes sur des files d'attente,

224
00:09:28,557 --> 00:09:30,472
vous avez des personnes

225
00:09:30,572 --> 00:09:32,383
qui peuvent être prioritaires par rapport à vous

226
00:09:33,211 --> 00:09:34,357
dans la file d'attente,

227
00:09:35,012 --> 00:09:37,132
par exemple une personne en situation de handicap

228
00:09:37,232 --> 00:09:38,341
devrait être prioritaire,

229
00:09:39,115 --> 00:09:42,130
une femme enceinte, et cætera,

230
00:09:42,230 --> 00:09:43,517
c'est normal

231
00:09:43,790 --> 00:09:45,035
qu'il y ait des priorités

232
00:09:45,359 --> 00:09:48,607
et que ces priorités soient gérées par le système.

233
00:09:48,953 --> 00:09:49,897
Et dans ce cas-là,

234
00:09:49,997 --> 00:09:51,604
on a ce qu'on appelle des files à priorité.

235
00:09:53,333 --> 00:09:56,211
C'est les processus que vous avez dans les systèmes d'exploitation,

236
00:09:56,515 --> 00:09:59,098
et puis on verra plusieurs algorithmes en fin de semestre

237
00:09:59,198 --> 00:10:00,891
dans lesquels on utilise la file à priorité

238
00:10:01,196 --> 00:10:03,881
et on en a déjà utilisé une auparavant

239
00:10:03,981 --> 00:10:05,845
quand on a fait par exemple le tri par sélection.

240
00:10:06,624 --> 00:10:09,528
Vous mettez vos éléments dans une file à priorité

241
00:10:09,836 --> 00:10:12,031
et vous retirez les éléments

242
00:10:12,131 --> 00:10:13,932
par priorité croissante.

243
00:10:15,140 --> 00:10:17,506
Vous avez le tri par sélection.

244
00:10:17,787 --> 00:10:19,217
C'est exactement celui-là.

245
00:10:19,317 --> 00:10:22,222
Donc une vision de la file à priorité,

246
00:10:22,423 --> 00:10:24,252
une interprétation qu'on peut en faire

247
00:10:24,352 --> 00:10:25,427
pourrait être le tri par sélection.

248
00:10:26,076 --> 00:10:29,193
De la même façon, on peut spécifier

249
00:10:29,491 --> 00:10:31,512
et on va donc complexifier le schéma

250
00:10:31,612 --> 00:10:32,760
que l'on avait pour la file.

251
00:10:33,040 --> 00:10:34,648
Ici,on a la file à priorité

252
00:10:34,748 --> 00:10:36,146
avec pareil, les opérations

253
00:10:36,246 --> 00:10:38,323
Insérer, Prioritaire

254
00:10:38,423 --> 00:10:42,332
qui vous donne un élément prioritaire de la file

255
00:10:42,432 --> 00:10:45,110
et l'extraction qui consiste à extraire

256
00:10:45,210 --> 00:10:47,578
l'élément prioritaire que vous avez trouvé auparavant

257
00:10:48,942 --> 00:10:49,704
de la file.

258
00:10:50,461 --> 00:10:52,470
Évidemment, on a les mêmes types de préconditions.

259
00:10:52,740 --> 00:10:54,071
Par contre, vous voyez que

260
00:10:54,171 --> 00:10:55,510
pour les axiomes

261
00:10:55,845 --> 00:10:58,387
c'est nettement plus compliqué

262
00:10:58,487 --> 00:11:00,855
puisqu'on doit regarder en fonction de ce qu'on insère

263
00:11:01,247 --> 00:11:03,259
si on change la priorité, et cætera.

264
00:11:05,732 --> 00:11:08,165
C'est une généralisation de la pile et de la file.

265
00:11:09,025 --> 00:11:10,218
Quand on prend une pile,

266
00:11:10,318 --> 00:11:12,641
l'élément prioritaire, c'est le dernier qui est arrivé.

267
00:11:13,075 --> 00:11:14,936
Donc il suffit d'avoir un petit compteur

268
00:11:15,851 --> 00:11:17,975
qui vous dit votre ordre d'arrivée

269
00:11:18,233 --> 00:11:19,994
et vous sortez les éléments

270
00:11:20,094 --> 00:11:23,434
en fonction de l'ordre inverse de l'ordre d'arrivée.

271
00:11:23,850 --> 00:11:25,178
Si vous avez une file,

272
00:11:25,278 --> 00:11:26,817
vous avez pareil un petit compteur

273
00:11:26,917 --> 00:11:28,116
qui vous permet de dire

274
00:11:28,709 --> 00:11:30,100
quel est l'ordre d'arrivée,

275
00:11:30,200 --> 00:11:31,834
et vous faites sortir

276
00:11:31,934 --> 00:11:33,551
l'élément le plus ancien,

277
00:11:33,822 --> 00:11:35,524
celui qui a le plus petit numéro d'arrivée.

278
00:11:35,861 --> 00:11:37,383
Donc en fait, la file à priorité

279
00:11:37,483 --> 00:11:39,481
est tout simplement une généralisation

280
00:11:39,836 --> 00:11:41,004
de la pile et de la file.

281
00:11:42,378 --> 00:11:44,497
Alors il faut faire attention

282
00:11:45,257 --> 00:11:48,991
que dans la spécification de la file à priorité,

283
00:11:50,833 --> 00:11:53,141
on n'a pas dit ce qu'il pouvait se passer

284
00:11:53,241 --> 00:11:55,055
dans le cas d'une égalité de priorité,

285
00:11:56,651 --> 00:11:58,210
lequel on va faire sortir

286
00:11:58,310 --> 00:11:59,760
si j'ai deux priorités qui sont égales.

287
00:12:00,372 --> 00:12:03,192
Et là, on est sur un exemple de type

288
00:12:03,292 --> 00:12:04,292
dans lequel on ne spécifie pas tout.

289
00:12:05,851 --> 00:12:07,753
C'est-à-dire que c'est à l'utilisateur

290
00:12:07,853 --> 00:12:14,231
de dire si n'importe quel élément de la file

291
00:12:14,586 --> 00:12:16,801
ayant la priorité maximale

292
00:12:17,265 --> 00:12:18,246
peut sortir

293
00:12:18,519 --> 00:12:20,751
ou si on doit en choisir un particulier

294
00:12:20,975 --> 00:12:24,692
ou si on doit coupler à la priorité l'ancienneté,

295
00:12:24,986 --> 00:12:26,256
ou et cætera, et cætera.

296
00:12:26,356 --> 00:12:29,225
Mais ce n'est pas du ressort du type file à priorité.

297
00:12:29,386 --> 00:12:32,261
La file à priorité va juste vous offrir l'interface,

298
00:12:32,361 --> 00:12:35,536
vous prenez un élément de priorité maximale,

299
00:12:35,636 --> 00:12:39,053
et c'est éventuellement l'utilisateur qui doit être derrière

300
00:12:39,153 --> 00:12:43,565
qui doit se dire est-ce que je peux résoudre mon problème avec ça

301
00:12:43,815 --> 00:12:47,075
ou est-ce que je suis obligé de spécifier effectivement

302
00:12:47,313 --> 00:12:50,706
la priorité entre des éléments de même priorité,

303
00:12:50,806 --> 00:12:53,691
donc généraliser la priorité avec une priorité plus large.

304
00:12:57,176 --> 00:12:59,099
Voilà, on extrait un élément,

305
00:12:59,199 --> 00:13:00,567
et on se fiche éperdument

306
00:13:00,944 --> 00:13:02,857
a priori de cet élément-là.


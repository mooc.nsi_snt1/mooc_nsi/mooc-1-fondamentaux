1
00:00:01,690 --> 00:00:03,090
En informatique,

2
00:00:04,076 --> 00:00:06,273
on utilise effectivement de l'information,

3
00:00:06,373 --> 00:00:07,632
on transforme de l'information

4
00:00:07,972 --> 00:00:09,373
et souvent, cette information,

5
00:00:09,473 --> 00:00:11,736
on n'a pas envie de la garder sous forme de séquence de bits

6
00:00:11,836 --> 00:00:13,739
ce qui est l'aspect machine,

7
00:00:14,022 --> 00:00:17,622
et on a plutôt envie d'avoir de l'information structurée,

8
00:00:17,722 --> 00:00:21,363
ordonnée, compréhensible, lisible, transformable

9
00:00:21,683 --> 00:00:23,340
et ça, ça va nous pousser

10
00:00:23,548 --> 00:00:25,582
à travailler sur ce qu'on appelle

11
00:00:25,917 --> 00:00:27,138
des types de données,

12
00:00:28,309 --> 00:00:30,637
avoir des types de données

13
00:00:30,737 --> 00:00:32,134
qui nous permettent des manipulations.

14
00:00:32,823 --> 00:00:33,835
Ces types de données,

15
00:00:33,935 --> 00:00:35,563
on va les rendre de plus en plus complexes

16
00:00:35,663 --> 00:00:37,612
ce qui va donner des types qu'on appelle abstraits

17
00:00:37,966 --> 00:00:40,448
qu'on va manipuler tels quels.

18
00:00:40,548 --> 00:00:42,437
Un tableau, par exemple, est un type abstrait,

19
00:00:42,696 --> 00:00:45,059
et puis on va pouvoir travailler sur ce tableau,

20
00:00:45,159 --> 00:00:47,742
il y a un certain nombre d'opérations

21
00:00:47,842 --> 00:00:48,776
qui sont définies dessus

22
00:00:48,876 --> 00:00:51,804
et on va être capable de gérer

23
00:00:51,904 --> 00:00:53,407
ce tableau, de l'utiliser

24
00:00:53,507 --> 00:00:55,079
pour transformer de l'information.

25
00:00:56,239 --> 00:00:57,747
Et derrière,

26
00:00:58,639 --> 00:00:59,881
on va avoir

27
00:01:03,273 --> 00:01:06,084
des structures qui vont être de plus en plus complexes

28
00:01:06,419 --> 00:01:08,252
pour aboutir à la notion d'objet

29
00:01:08,352 --> 00:01:09,496
qui est utilisée en Python,

30
00:01:09,819 --> 00:01:12,717
on a des références

31
00:01:12,817 --> 00:01:14,698
qui vont aller vers quelque part

32
00:01:15,022 --> 00:01:16,922
pour nous dire : voilà, à tel endroit,

33
00:01:17,022 --> 00:01:19,281
on a un objet, cet objet, il est comme ceci,

34
00:01:19,567 --> 00:01:22,313
on accède à ses éléments internes comme cela,

35
00:01:22,413 --> 00:01:24,169
on a des méthodes pour traiter cet objet.

36
00:01:24,681 --> 00:01:26,812
Donc l'objectif de toute cette séance,

37
00:01:26,912 --> 00:01:28,091
ça va être justement

38
00:01:28,191 --> 00:01:30,675
d'aborder de manière très élémentaire

39
00:01:31,490 --> 00:01:32,566
ces notions-là

40
00:01:32,867 --> 00:01:34,870
et de le voir du côté algorithmique

41
00:01:34,970 --> 00:01:37,439
c'est-à-dire comment moi, j'exprime un algorithme

42
00:01:38,422 --> 00:01:39,817
de façon très simple

43
00:01:40,614 --> 00:01:42,279
en ayant une abstraction

44
00:01:43,107 --> 00:01:45,685
qui permette de manipuler les données.

45
00:01:46,338 --> 00:01:47,828
On va avoir donc plusieurs étapes,

46
00:01:47,928 --> 00:01:50,647
une première étape sur les types simples un peu rapide.

47
00:01:50,936 --> 00:01:53,007
Après, on va se poser la question

48
00:01:53,107 --> 00:01:54,352
qu'est-ce que c'est qu'un type abstrait de données,

49
00:01:54,452 --> 00:01:55,617
comment on le spécifie.

50
00:01:55,953 --> 00:01:57,785
Et on va enchaîner derrière

51
00:01:57,885 --> 00:01:59,431
avec une présentation

52
00:01:59,531 --> 00:02:01,840
de structures qu'on appelle séquentielles

53
00:02:01,940 --> 00:02:03,752
c'est-à-dire dans lesquelles on a un certain ordre

54
00:02:03,852 --> 00:02:07,299
sur les éléments qu'on a envie de stocker dans la structure.

55
00:02:07,644 --> 00:02:09,579
On verra en particulier

56
00:02:09,679 --> 00:02:11,254
le tableau et la liste

57
00:02:11,354 --> 00:02:13,308
qui sont deux structures qui sont à connaître.

58
00:02:13,622 --> 00:02:17,445
Et on enchaînera avec des types un peu plus compliqués

59
00:02:17,545 --> 00:02:19,738
qui sont la pile, la file, la file à priorité,

60
00:02:20,056 --> 00:02:23,536
qui sont normalement au programme de première et terminale,

61
00:02:23,885 --> 00:02:25,268
ces deux notions-là,

62
00:02:25,368 --> 00:02:26,707
alors à différents niveaux,

63
00:02:26,807 --> 00:02:27,939
au niveau de la programmation,

64
00:02:28,039 --> 00:02:31,316
au niveau de la conception d'algorithmes,

65
00:02:31,416 --> 00:02:33,922
de l'utilisation comme outil pour résoudre des problèmes.

66
00:02:34,267 --> 00:02:37,409
Et puis, dans une deuxième partie qui sera plus importante,

67
00:02:38,529 --> 00:02:40,984
nous aborderons la structure d'arbre

68
00:02:41,308 --> 00:02:45,191
qui, elle, va nous occuper sur plusieurs séances

69
00:02:45,985 --> 00:02:47,933
pour voir comment on utilise

70
00:02:48,269 --> 00:02:49,964
le concept d'arbre

71
00:02:50,064 --> 00:02:51,387
pour résoudre des problèmes.

72
00:02:51,808 --> 00:02:53,094
Dans un premier temps,

73
00:02:53,194 --> 00:02:55,477
on regardera dans cette séquence 4

74
00:02:55,840 --> 00:02:58,791
les arbres binaires simples

75
00:02:58,891 --> 00:03:01,076
et puis peut-être qu'on aura une petite excursion

76
00:03:01,176 --> 00:03:02,558
vers les arbres binaires de recherche.


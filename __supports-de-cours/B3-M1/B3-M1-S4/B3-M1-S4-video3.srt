1
00:00:01,482 --> 00:00:03,013
Un type abstrait de données,

2
00:00:03,113 --> 00:00:05,180
c'est quoi, pourquoi, pour qui,

3
00:00:05,280 --> 00:00:07,575
est-ce qu'on fait des types abstraits de données ?

4
00:00:07,675 --> 00:00:09,872
En fait, si on réfléchit,

5
00:00:10,347 --> 00:00:12,896
on a besoin d'avoir un objet,

6
00:00:12,996 --> 00:00:13,693
un type abstrait,

7
00:00:14,261 --> 00:00:15,547
pour l'utiliser

8
00:00:15,647 --> 00:00:17,016
et pour effectivement après

9
00:00:17,116 --> 00:00:18,445
construire sur cet objet-là.

10
00:00:18,994 --> 00:00:20,539
Donc ce qui va être important,

11
00:00:20,639 --> 00:00:21,795
c'est l'interface

12
00:00:22,046 --> 00:00:24,235
entre l'objet

13
00:00:24,335 --> 00:00:26,350
et l'environnement qui va être extérieur.

14
00:00:26,829 --> 00:00:28,514
Cette interface-là,

15
00:00:29,384 --> 00:00:30,613
c'est en fait,

16
00:00:31,545 --> 00:00:32,338
on peut la voir

17
00:00:32,722 --> 00:00:34,347
comme un contrat

18
00:00:34,722 --> 00:00:38,138
entre le chef de projet qui décide

19
00:00:38,238 --> 00:00:39,494
et qui spécifie, il va dire

20
00:00:39,594 --> 00:00:40,772
voilà, l'objet, il va faire ça,

21
00:00:41,157 --> 00:00:42,939
le développeur qui va le coder,

22
00:00:43,543 --> 00:00:45,698
et l'utilisateur qui va utiliser

23
00:00:46,174 --> 00:00:47,229
l'objet

24
00:00:47,329 --> 00:00:50,369
sans se soucier de la façon dont c'est codé.

25
00:00:51,228 --> 00:00:53,890
C'est en gros la séparation

26
00:00:54,165 --> 00:00:57,189
entre celui qui va coder l'objet

27
00:00:57,540 --> 00:00:59,646
et celui qui va utiliser l'objet.

28
00:01:00,103 --> 00:01:02,389
Et cette séparation-là, elle est importante.

29
00:01:02,805 --> 00:01:04,123
C'est la même distinction

30
00:01:04,223 --> 00:01:05,474
qu'il y a entre l'utilisateur

31
00:01:05,789 --> 00:01:07,367
et celui qui développe le code.

32
00:01:08,318 --> 00:01:09,658
Et le type abstrait,

33
00:01:09,758 --> 00:01:12,479
c'est vraiment ce genre d'idée qui est derrière.

34
00:01:14,763 --> 00:01:17,786
La spécification doit dire comment

35
00:01:18,631 --> 00:01:19,814
on va faire,

36
00:01:19,914 --> 00:01:21,430
comment on va s'en servir,

37
00:01:23,436 --> 00:01:26,012
quel est l'usage qu'on va en avoir,

38
00:01:26,648 --> 00:01:28,544
mais ne dit pas comment

39
00:01:28,644 --> 00:01:30,372
l'objet est réalisé.

40
00:01:30,913 --> 00:01:32,022
Avec les étudiants,

41
00:01:32,122 --> 00:01:33,269
c'est un vrai problème

42
00:01:33,369 --> 00:01:35,546
ou avec les élèves, c'est un vrai problème,

43
00:01:35,737 --> 00:01:37,075
parce que par exemple, on va dire

44
00:01:37,175 --> 00:01:38,214
on va utiliser,

45
00:01:38,510 --> 00:01:40,426
je ne sais pas, une pile ou une file

46
00:01:40,526 --> 00:01:41,473
ou quelque chose comme ça,

47
00:01:41,762 --> 00:01:43,172
et tout de suite,

48
00:01:43,638 --> 00:01:44,916
on a envie de se rabattre

49
00:01:45,016 --> 00:01:46,766
et de se dire comment elle est codée, la file.

50
00:01:46,866 --> 00:01:48,335
Mais en fait, ce n'est pas le problème.

51
00:01:48,818 --> 00:01:50,026
Le problème, c'est

52
00:01:50,126 --> 00:01:51,553
j'ai une file, j'ai des opérateurs,

53
00:01:51,653 --> 00:01:53,276
donc je vais utiliser ces opérateurs

54
00:01:53,660 --> 00:01:56,215
pour travailler sur cet objet-là.

55
00:01:58,031 --> 00:02:01,315
Alors, si on réfléchit un peu plus loin,

56
00:02:01,415 --> 00:02:03,694
déjà, dans ce que vous utilisez en Python,

57
00:02:04,112 --> 00:02:08,181
avec, je ne sais pas, les objets

58
00:02:08,281 --> 00:02:10,618
de type réels, entiers, et cætera,

59
00:02:10,718 --> 00:02:12,462
déjà, ça c'est une abstraction

60
00:02:12,562 --> 00:02:14,091
de représentation concrète,

61
00:02:14,349 --> 00:02:15,773
vous ne vous souciez pas

62
00:02:15,873 --> 00:02:16,960
de la manière dont

63
00:02:17,463 --> 00:02:19,989
le booléen est codé à l'intérieur de votre machine,

64
00:02:20,472 --> 00:02:21,915
vous ne vous souciez pas du tout

65
00:02:22,015 --> 00:02:24,920
de la façon dont l'addition se fait sur des réels,

66
00:02:25,371 --> 00:02:27,104
vous avez l'opérateur +

67
00:02:27,204 --> 00:02:30,064
et vous faites 3.1 + 4.2

68
00:02:30,355 --> 00:02:32,389
et ça vous donne le résultat.

69
00:02:32,726 --> 00:02:35,506
3.1 est déjà interprété comme il faut,

70
00:02:35,685 --> 00:02:37,890
4.2 est déjà interprété comme il faut,

71
00:02:38,244 --> 00:02:39,742
et donc, dessous,

72
00:02:39,842 --> 00:02:41,621
ça a été implémenté d'une certaine façon

73
00:02:41,825 --> 00:02:43,107
et vous ne vous en souciez pas.

74
00:02:43,492 --> 00:02:45,649
Donc en fait, c'est la généralisation de ce principe-là

75
00:02:45,749 --> 00:02:46,551
qu'on va regarder.

76
00:02:47,493 --> 00:02:49,059
Alors si je prends ça

77
00:02:50,298 --> 00:02:52,847
d'un point de vue plus général,

78
00:02:52,947 --> 00:02:53,936
d'un point de vue développement,

79
00:02:54,678 --> 00:02:56,244
en phase de conception,

80
00:02:56,943 --> 00:02:59,588
le fait de travailler sur des objets abstraits,

81
00:02:59,688 --> 00:03:00,433
des types abstraits,

82
00:03:00,724 --> 00:03:02,666
ça permet juste de se concentrer

83
00:03:02,766 --> 00:03:04,795
sur ce qu'on attend comme fonctionnalités

84
00:03:05,311 --> 00:03:06,817
et donc de séparer

85
00:03:07,274 --> 00:03:09,004
les différentes zones de code.

86
00:03:09,453 --> 00:03:10,946
Donc on va vérifier déjà

87
00:03:11,661 --> 00:03:13,478
que l'objet est bien implémenté,

88
00:03:13,578 --> 00:03:15,364
et puis une fois que l'objet est bien implémenté,

89
00:03:15,681 --> 00:03:18,200
on ne se soucie plus de la vérification de sa qualité,

90
00:03:18,487 --> 00:03:20,203
on va juste travailler avec cet objet

91
00:03:20,303 --> 00:03:21,988
et on se concentre sur la manipulation

92
00:03:22,323 --> 00:03:23,300
de ces objets.

93
00:03:24,650 --> 00:03:25,679
Évidemment, ce que je dis,

94
00:03:25,779 --> 00:03:27,080
c'est indépendant du langage

95
00:03:27,180 --> 00:03:29,945
parce que, une fois que vous avez spécifié,

96
00:03:30,045 --> 00:03:32,691
vous pouvez avoir un objet qui est codé en C,

97
00:03:33,310 --> 00:03:34,248
même en Python,

98
00:03:35,568 --> 00:03:37,764
dans une librairie, il peut très bien être codé en C,

99
00:03:38,133 --> 00:03:39,469
ça ne vous regarde pas

100
00:03:39,569 --> 00:03:41,505
et vous utilisez les objets de la librairie.

101
00:03:43,871 --> 00:03:46,504
D'autre part, si vous définissez un objet,

102
00:03:46,604 --> 00:03:49,479
quand vous allez effectivement coder cet objet,

103
00:03:50,768 --> 00:03:53,217
vous n'avez pas en tête complètement

104
00:03:53,376 --> 00:03:55,776
le scénario d'utilisation de cet objet.

105
00:03:56,422 --> 00:03:59,574
C'est-à-dire on vous demande de coder une file ou une liste,

106
00:03:59,847 --> 00:04:01,243
on vous donne les spécifications,

107
00:04:02,445 --> 00:04:03,996
vous codez la file ou la liste,

108
00:04:04,096 --> 00:04:06,511
vous ne vous souciez pas du fait que la liste va être utilisée

109
00:04:06,869 --> 00:04:08,148
dans le système d'exploitation

110
00:04:08,248 --> 00:04:09,373
pour gérer des processus.

111
00:04:09,662 --> 00:04:11,290
Ça n'a aucun intérêt

112
00:04:11,390 --> 00:04:12,852
donc vous ne vous posez pas cette question.

113
00:04:13,541 --> 00:04:15,342
Et donc derrière,

114
00:04:15,839 --> 00:04:17,322
ce que je dis, en développement,

115
00:04:17,715 --> 00:04:19,589
on a plein de tests à tous les niveaux

116
00:04:19,689 --> 00:04:21,121
pour vérifier à chaque fois

117
00:04:21,221 --> 00:04:23,273
que l'abstraction que l'on a produite

118
00:04:23,373 --> 00:04:24,050
est correcte.

119
00:04:25,220 --> 00:04:27,144
Et lorsque vous êtes en phase d'utilisation,

120
00:04:27,244 --> 00:04:28,139
vous êtes à un haut niveau,

121
00:04:28,239 --> 00:04:29,544
vous manipulez vos objets

122
00:04:30,151 --> 00:04:33,279
comme si c'était des objets qui existent dans la machine.

123
00:04:33,701 --> 00:04:35,996
Vous ne vous souciez pas de toute la hiérarchie

124
00:04:36,096 --> 00:04:37,824
de ce qui a été programmé en dessous

125
00:04:38,173 --> 00:04:39,615
pour utiliser cet objet.

126
00:04:42,058 --> 00:04:44,246
Alors, quelle est la méthodologie

127
00:04:44,346 --> 00:04:45,496
de construction de ces objets ?

128
00:04:45,986 --> 00:04:49,484
Premièrement, il faut lister les opérations autorisées avec leurs profils,

129
00:04:49,952 --> 00:04:52,677
donc ça, c'est dire exactement

130
00:04:52,777 --> 00:04:54,236
qu'est-ce que je peux faire sur mes objets

131
00:04:54,962 --> 00:04:55,909
ou mes types abstraits.

132
00:04:56,279 --> 00:04:58,165
Deuxièmement, il faut préciser

133
00:04:58,826 --> 00:05:00,672
quelles sont les préconditions

134
00:05:00,772 --> 00:05:02,200
pour faire ces opérations.

135
00:05:03,129 --> 00:05:04,430
Vous ne pouvez pas,

136
00:05:05,263 --> 00:05:06,493
si vous avez un tableau

137
00:05:06,593 --> 00:05:07,752
qui est de taille fixe,

138
00:05:07,852 --> 00:05:10,344
vous ne pouvez pas rajouter un élément

139
00:05:10,444 --> 00:05:13,259
en position n + 1 ou n + 2.

140
00:05:16,077 --> 00:05:17,548
Changer un élément,

141
00:05:18,028 --> 00:05:20,384
ça veut dire que vous supposez que

142
00:05:20,484 --> 00:05:22,102
l'index est bien défini

143
00:05:22,202 --> 00:05:23,538
par rapport à votre tableau.

144
00:05:24,089 --> 00:05:26,783
Et vous allez spécifier le comportement

145
00:05:26,883 --> 00:05:27,957
des différentes opérations

146
00:05:28,432 --> 00:05:31,882
en fonction, je dirais, des entrées,

147
00:05:31,982 --> 00:05:33,790
et donc ça, c'est ce qu'on appelle les axiomes

148
00:05:33,890 --> 00:05:35,797
qui permettent de dire exactement ce qu'on va faire.

149
00:05:36,654 --> 00:05:38,900
Alors les axiomes vont vous expliquer

150
00:05:39,000 --> 00:05:42,803
ce que font vos opérations.

151
00:05:43,791 --> 00:05:45,944
Évidemment, ils ne doivent pas être contradictoires

152
00:05:46,044 --> 00:05:49,082
et ils doivent être exhaustifs, complets,

153
00:05:49,182 --> 00:05:51,572
c'est-à-dire prévoir le résultat de toute opération.

154
00:05:54,125 --> 00:05:56,451
En fait, on va regarder

155
00:05:56,551 --> 00:05:59,598
les constructeurs, comment ça opère sur la structure,

156
00:05:59,806 --> 00:06:02,856
et voir comment on généralise ça.

157
00:06:04,650 --> 00:06:08,992
Si je reprends les spécifications

158
00:06:09,092 --> 00:06:10,640
d'un type abstrait ou d'un objet,

159
00:06:11,023 --> 00:06:14,130
en fait, on peut avoir plusieurs manières de voir les choses.

160
00:06:14,445 --> 00:06:15,850
Là, je vous présente la plus simple

161
00:06:15,950 --> 00:06:17,189
qui est le point de vue fonctionnel,

162
00:06:17,524 --> 00:06:18,781
qui consiste à dire

163
00:06:19,163 --> 00:06:21,068
de toute façon, je vais avoir des objets

164
00:06:21,168 --> 00:06:23,827
mais ils vont être pris en arguments de fonctions

165
00:06:24,106 --> 00:06:26,026
qui vont avoir des valeurs de retour

166
00:06:26,126 --> 00:06:28,056
et je ne travaille pas sur un état.

167
00:06:28,452 --> 00:06:30,951
Donc une opération sur un objet

168
00:06:31,051 --> 00:06:32,081
renvoie un nouvel objet,

169
00:06:32,268 --> 00:06:33,495
souvent de même type,

170
00:06:33,811 --> 00:06:34,895
je n'ai pas d'effets de bord,

171
00:06:34,995 --> 00:06:36,641
je n'ai pas de donnée-résultat,

172
00:06:37,920 --> 00:06:41,710
de problème entre changement de ma structure, et cætera.

173
00:06:42,403 --> 00:06:45,230
Mais ça, c'est du point de vue juste de la conception.

174
00:06:46,292 --> 00:06:48,066
d'un point de vue très pratique,

175
00:06:48,166 --> 00:06:49,972
en fait, ce que l'on fait,

176
00:06:50,072 --> 00:06:52,448
on va dire je vais prendre un objet,

177
00:06:52,672 --> 00:06:56,001
je vais faire une opération dessus

178
00:06:56,101 --> 00:06:57,519
donc j'ai un nouvel objet

179
00:06:57,879 --> 00:07:00,098
et je vais mettre cet objet à la place de l'ancien.

180
00:07:00,571 --> 00:07:02,650
Souvent, c'est comme cela qu'on procède,

181
00:07:02,750 --> 00:07:04,655
donc c'est la notion d'état qui est associée.

182
00:07:05,496 --> 00:07:08,129
Donc les paramètres sont modifiables

183
00:07:08,475 --> 00:07:11,060
et puis, souvent dans les langages,

184
00:07:11,160 --> 00:07:13,033
par exemple en C, on passe par référence.

185
00:07:13,466 --> 00:07:14,938
Un pointeur sur l'objet

186
00:07:15,038 --> 00:07:17,471
et puis on travaille avec des références.

187
00:07:17,830 --> 00:07:20,152
Lorsque l'on va être en Python,

188
00:07:20,873 --> 00:07:23,293
on va avoir des objets et des méthodes

189
00:07:24,684 --> 00:07:26,520
et on sous-entend en particulier

190
00:07:26,620 --> 00:07:28,444
que les méthodes peuvent modifier l'objet.

191
00:07:29,580 --> 00:07:31,654
Ça évite plein de choses,

192
00:07:31,754 --> 00:07:33,908
donc c'est plus du confort de programmation

193
00:07:34,240 --> 00:07:36,471
et la forme de pensée qui est derrière reste la même,

194
00:07:36,837 --> 00:07:40,082
et on a tout plein de techniques

195
00:07:40,182 --> 00:07:41,292
qui sont cachées là.

196
00:07:41,607 --> 00:07:43,921
Alors, ce n'est pas l'objectif dans ce cours-là

197
00:07:44,270 --> 00:07:46,275
de parler de ces aspects-là

198
00:07:46,583 --> 00:07:47,962
mais il faut les avoir en tête

199
00:07:48,062 --> 00:07:49,557
quand on va effectivement développer

200
00:07:49,657 --> 00:07:51,819
et implémenter des algorithmes.


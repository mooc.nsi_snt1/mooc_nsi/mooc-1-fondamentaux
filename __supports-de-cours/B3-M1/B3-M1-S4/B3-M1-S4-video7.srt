1
00:00:00,993 --> 00:00:04,245
Les arbres, c'est vraiment omniprésent

2
00:00:04,345 --> 00:00:06,050
dans toute l'informatique.

3
00:00:06,150 --> 00:00:08,966
Alors, pour bien comprendre pourquoi,

4
00:00:09,517 --> 00:00:11,854
c'est lié au fait que

5
00:00:11,954 --> 00:00:15,093
un arbre représente en gros une décision, un choix

6
00:00:15,400 --> 00:00:17,004
que l'on est capable de faire.

7
00:00:17,279 --> 00:00:19,275
Ce que l'on voit dans les petites classes,

8
00:00:19,375 --> 00:00:21,380
c'est typiquement des arbres de décision,

9
00:00:22,430 --> 00:00:25,415
typiquement des arbres qui servent à déterminer des choix

10
00:00:25,515 --> 00:00:26,598
et en fonction des choix, on va

11
00:00:26,881 --> 00:00:29,250
explorer une des branches de l'arbre.

12
00:00:29,831 --> 00:00:33,137
Donc je pense qu'il faut en être conscient,

13
00:00:33,519 --> 00:00:35,158
les arbres existaient bien avant

14
00:00:35,448 --> 00:00:37,479
la formalisation de l'informatique,

15
00:00:37,770 --> 00:00:40,377
mais c'était déjà des outils qui servaient à

16
00:00:40,637 --> 00:00:42,126
définir par exemple

17
00:00:42,867 --> 00:00:44,585
l'arborescence des plus courts chemins

18
00:00:44,685 --> 00:00:46,676
quand vous cherchiez à aller d'un endroit à un autre,

19
00:00:46,776 --> 00:00:49,011
ou ce genre d'objet.

20
00:00:49,316 --> 00:00:51,012
Si je reprends maintenant

21
00:00:51,112 --> 00:00:55,407
dans le domaine je dirais plus fondamental de l'informatique,

22
00:00:55,781 --> 00:00:57,475
quand vous faites du système,

23
00:00:57,699 --> 00:01:00,249
vous êtes en train de regarder

24
00:01:00,349 --> 00:01:02,141
un système par exemple de gestion de fichiers,

25
00:01:02,241 --> 00:01:04,943
vous avez une structuration sous forme d'arbre.

26
00:01:05,522 --> 00:01:07,400
Cette structuration sous forme d'arbre

27
00:01:07,500 --> 00:01:09,751
doit être efficace pour vous permettre d'accéder

28
00:01:10,004 --> 00:01:12,669
facilement à n'importe quel fichier

29
00:01:12,769 --> 00:01:13,889
que vous avez sur votre machine.

30
00:01:14,502 --> 00:01:16,933
On voit bien qu'ici l'utilisation,

31
00:01:17,033 --> 00:01:18,899
c'est le chemin d'accès qui va compter

32
00:01:18,999 --> 00:01:20,715
et qui va être plutôt court

33
00:01:20,815 --> 00:01:24,040
si vous avez des arbres qui sont bien fichus.

34
00:01:25,346 --> 00:01:27,601
Si on reprend par exemple du calcul,

35
00:01:27,701 --> 00:01:30,662
on reprend une expression par exemple de calcul arithmétique,

36
00:01:31,329 --> 00:01:33,241
je ne sais pas, vous faites

37
00:01:33,341 --> 00:01:38,645
trois plus deux fois cinq,

38
00:01:38,988 --> 00:01:41,966
et bien évidemment, vous avez derrière

39
00:01:42,070 --> 00:01:44,293
un arbre qui consiste à dire

40
00:01:44,575 --> 00:01:46,547
que vous avez une opération de multiplication,

41
00:01:47,143 --> 00:01:49,223
là, vous avez une opération d'addition,

42
00:01:49,628 --> 00:01:51,963
et puis, en bas,

43
00:01:52,293 --> 00:01:54,760
vous avez 3, vous avez 2

44
00:01:55,082 --> 00:01:57,797
et là, vous avez 5.

45
00:01:58,532 --> 00:02:03,203
Donc en fait, déjà dans les expressions arithmétiques,

46
00:02:03,303 --> 00:02:05,794
on avait une structure d'arbre qui était sous-jacente.

47
00:02:07,053 --> 00:02:09,331
Ensuite, dans tout ce qui est document structuré

48
00:02:09,431 --> 00:02:12,574
que vous utilisez par exemple en HTML ou XML,

49
00:02:12,868 --> 00:02:15,689
sous-entendues, vous avez des structures d'arbre

50
00:02:15,789 --> 00:02:18,102
qui correspondent aux imbrications

51
00:02:18,202 --> 00:02:22,022
des contextes que vous avez dans votre page XML.

52
00:02:23,222 --> 00:02:25,019
Les arbres, vous les retrouvez également

53
00:02:25,119 --> 00:02:26,665
dans plein d'autres endroits,

54
00:02:26,985 --> 00:02:28,756
on utilise beaucoup l'analogie 

55
00:02:28,856 --> 00:02:31,414
avec les arbres généalogiques,

56
00:02:31,823 --> 00:02:34,177
alors que ce soit l'arbre des ascendants

57
00:02:34,490 --> 00:02:36,483
qui est un arbre binaire

58
00:02:36,817 --> 00:02:38,293
ou l'arbre des descendants

59
00:02:38,393 --> 00:02:40,646
qui est un arbre qui n'est pas forcément binaire.

60
00:02:41,449 --> 00:02:44,394
On l'a aussi dans les organigrammes,

61
00:02:44,494 --> 00:02:46,675
derrière on peut avoir des structures d'arbre.

62
00:02:46,775 --> 00:02:48,739
On l'a dans la description

63
00:02:48,839 --> 00:02:51,977
d'un flux, par exemple un réseau fluvial,

64
00:02:52,077 --> 00:02:56,009
c'est un arbre qui est effectivement utilisé.

65
00:02:56,481 --> 00:02:59,090
Donc tout ça,

66
00:03:00,718 --> 00:03:01,955
ça nous permet de dire

67
00:03:02,055 --> 00:03:04,244
que les arbres sont partout

68
00:03:04,724 --> 00:03:06,345
et en particulier,

69
00:03:06,445 --> 00:03:08,085
nous allons voir quelques exemples,

70
00:03:08,512 --> 00:03:11,544
on va regarder dans les arbres pour faire de la décision,

71
00:03:11,644 --> 00:03:13,479
dans la compression des images et des textes,

72
00:03:13,579 --> 00:03:16,721
ça, ça va être les séquences un peu plus tard,

73
00:03:17,159 --> 00:03:18,808
aussi dans de la recherche de motifs,

74
00:03:18,908 --> 00:03:20,826
on peut avoir des constructions de l'arbre des préfixes.

75
00:03:20,926 --> 00:03:22,729
On peut avoir plein de choses

76
00:03:22,829 --> 00:03:24,917
qui apparaissent avec les arbres au fur et à mesure.

77
00:03:26,080 --> 00:03:27,748
Alors il faut faire attention

78
00:03:27,848 --> 00:03:32,097
par exemple quand vous parlez d'une page XML,

79
00:03:32,455 --> 00:03:33,807
d'un document XML,

80
00:03:34,850 --> 00:03:36,414
l'arbre n'est pas explicite,

81
00:03:36,514 --> 00:03:37,301
c'est-à-dire on ne vous dit pas

82
00:03:37,401 --> 00:03:39,060
quel est le père, quel est le fils, et cætera.

83
00:03:39,160 --> 00:03:40,188
On vous dit juste,

84
00:03:40,397 --> 00:03:41,849
enfin, ce que l'on dit ici,

85
00:03:41,949 --> 00:03:43,349
c'est que l'arbre est défini

86
00:03:43,752 --> 00:03:46,514
implicitement par le langage à balises

87
00:03:46,911 --> 00:03:49,671
et que la bonne abstraction, c'est l'arbre qui est derrière.

88
00:03:50,085 --> 00:03:52,135
On a un arbre qui est sous-jacent.

89
00:03:52,235 --> 00:03:53,861
Pareil pour les expressions arithmétiques.

90
00:03:54,221 --> 00:03:56,332
Ou alors on peut avoir un arbre explicite,

91
00:03:56,665 --> 00:03:59,153
quand par exemple vous prenez une origine dans un graphe

92
00:03:59,253 --> 00:04:00,640
et vous regardez les chemins

93
00:04:00,740 --> 00:04:03,892
pour aller de cette origine à n'importe quel point du graphe.

94
00:04:04,236 --> 00:04:06,214
Donc on a les deux formes

95
00:04:06,620 --> 00:04:08,216
et souvent on est amené

96
00:04:08,316 --> 00:04:10,302
à jongler d'une forme à l'autre,

97
00:04:10,609 --> 00:04:12,383
travailler sur un arbre implicite

98
00:04:12,947 --> 00:04:14,952
en sachant derrière que c'est un arbre.

99
00:04:16,556 --> 00:04:19,164
On a différentes façons d'aborder les arbres.

100
00:04:19,264 --> 00:04:21,388
Une première qui est de dire que

101
00:04:21,488 --> 00:04:23,814
un arbre, c'est un graphe particulier

102
00:04:24,119 --> 00:04:27,273
donc c'est un graphe qui est connexe et sans cycle.

103
00:04:29,551 --> 00:04:30,992
Ici, la définition qu'on aurait

104
00:04:31,092 --> 00:04:36,724
c'est connexe et sans cycle.

105
00:04:42,943 --> 00:04:44,160
Mais cette définition,

106
00:04:44,260 --> 00:04:46,329
qui est une définition plutôt de structure,

107
00:04:47,077 --> 00:04:48,797
à partir d'une propriété,

108
00:04:48,897 --> 00:04:50,347
n'est pas forcément utile

109
00:04:50,692 --> 00:04:53,281
lorsque l'on va, je dirais,

110
00:04:53,381 --> 00:04:55,273
utiliser des arbres en informatique.

111
00:04:56,886 --> 00:04:59,862
Donc en informatique de façon générale,

112
00:05:00,223 --> 00:05:02,179
ce n'est pas tout à fait vrai partout,

113
00:05:02,279 --> 00:05:03,986
on verra des exemples dans lesquels ce n'est pas vrai,

114
00:05:04,374 --> 00:05:06,651
on parle d'un arbre qui est enraciné

115
00:05:07,005 --> 00:05:08,518
c'est-à-dire que

116
00:05:09,577 --> 00:05:11,722
on va se donner une racine

117
00:05:12,440 --> 00:05:14,520
qui est un nœud particulier de l'arbre.

118
00:05:14,849 --> 00:05:18,616
Et donc ce nœud, que j'appelle racine,

119
00:05:20,767 --> 00:05:22,473
pour cet arbre-là, c'est celui-là,

120
00:05:23,745 --> 00:05:24,484
celui-là,

121
00:05:25,716 --> 00:05:26,946
ou celui-là,

122
00:05:27,046 --> 00:05:28,624
et évidemment, le dernier aussi,

123
00:05:29,277 --> 00:05:31,436
ce nœud racine

124
00:05:31,748 --> 00:05:34,377
va avoir une étiquette en général

125
00:05:34,477 --> 00:05:36,833
donc un identifiant, ou une clé,

126
00:05:36,933 --> 00:05:39,769
de l'information qui peut être rattachée à ce nœud,

127
00:05:39,992 --> 00:05:42,231
et puis, il va avoir des sous-arbres.

128
00:05:42,631 --> 00:05:44,198
Ici, on a plusieurs sous-arbres,

129
00:05:44,298 --> 00:05:45,535
on a trois sous-arbres,

130
00:05:47,139 --> 00:05:49,579
ici, on en a un autre et ici, on en a un autre,

131
00:05:50,456 --> 00:05:52,853
qui sont enracinés eux-mêmes

132
00:05:53,308 --> 00:05:57,376
dont le père serait l'élément qui est au-dessus,

133
00:05:59,383 --> 00:06:01,921
et qui sont eux-mêmes constitués de nœuds, et cætera.

134
00:06:02,309 --> 00:06:04,990
Donc ici la définition que l'on fait,

135
00:06:05,090 --> 00:06:06,674
c'est une définition récursive.

136
00:06:15,115 --> 00:06:18,474
Un arbre, c'est constitué d'un nœud

137
00:06:18,574 --> 00:06:20,509
et de sous-arbres

138
00:06:20,609 --> 00:06:21,609
qui sont eux-mêmes des arbres.

139
00:06:22,670 --> 00:06:24,592
C'est cette définition-là

140
00:06:24,692 --> 00:06:27,078
qui est sans doute la plus intéressante

141
00:06:27,178 --> 00:06:28,813
et la plus facile à manipuler.

142
00:06:29,632 --> 00:06:31,441
Après on a un certain nombre de termes

143
00:06:31,821 --> 00:06:35,129
par exemple l'arité d'un nœud.

144
00:06:35,462 --> 00:06:38,568
Le terme arité correspond au nombre de fils,

145
00:06:38,668 --> 00:06:40,515
de sous-arbres,

146
00:06:40,784 --> 00:06:42,076
qui sont portés par le nœud.

147
00:06:42,436 --> 00:06:44,132
Si par exemple je regarde

148
00:06:45,048 --> 00:06:46,207
cet exemple-là,

149
00:06:46,533 --> 00:06:48,477
l'arité de ce nœud-là, c'est 3,

150
00:06:48,945 --> 00:06:50,740
l'arité de ce nœud, c'est 2,

151
00:06:50,840 --> 00:06:52,589
l'arité de ce nœud, c'est 0,

152
00:06:53,801 --> 00:06:57,476
0, 0, 1 et 0.

153
00:06:58,135 --> 00:07:00,044
Donc ça, c'est le terme arité,

154
00:07:00,144 --> 00:07:02,024
qui vous permet de savoir effectivement

155
00:07:02,124 --> 00:07:04,138
combien il y a de sous-arbres

156
00:07:04,741 --> 00:07:06,002
qui sont associés à un nœud.

157
00:07:06,900 --> 00:07:08,275
Évidemment, une feuille,

158
00:07:08,375 --> 00:07:09,898
c'est un nœud d'arité 0,

159
00:07:09,998 --> 00:07:11,637
ça, tout le monde s'en souvient,

160
00:07:11,737 --> 00:07:15,839
ici, j'ai quatre feuilles.

161
00:07:21,566 --> 00:07:23,009
À l'opposé des feuilles,

162
00:07:23,978 --> 00:07:26,336
on a les nœuds internes.

163
00:07:26,632 --> 00:07:27,830
Donc les nœuds internes,

164
00:07:28,078 --> 00:07:29,694
c'est ceux qui ne sont pas des feuilles,

165
00:07:30,352 --> 00:07:33,807
c'est ceux dont les sous-arbres

166
00:07:34,030 --> 00:07:37,286
ne sont pas vides en fait.

167
00:07:38,125 --> 00:07:39,587
Donc un nœud qui n'est pas une feuille,

168
00:07:39,687 --> 00:07:41,460
c'est un nœud qu'on appelle un nœud interne.

169
00:07:41,812 --> 00:07:43,215
Le fils d'un nœud,

170
00:07:43,315 --> 00:07:49,398
par exemple, C est le fils du nœud D,

171
00:07:49,749 --> 00:07:53,678
E est le fils du nœud D également,

172
00:07:54,219 --> 00:07:56,247
donc là, ça permet d'avoir une relation

173
00:07:56,769 --> 00:07:57,859
qui s'appelle fils.

174
00:07:58,275 --> 00:07:59,696
Alors attention,

175
00:08:00,565 --> 00:08:02,062
cette relation fils,

176
00:08:02,307 --> 00:08:04,917
un nœud peut avoir plusieurs fils.

177
00:08:06,107 --> 00:08:08,412
Et puis on a la notion de  père ou parent.

178
00:08:08,819 --> 00:08:10,383
Dans ce cas-là,

179
00:08:10,483 --> 00:08:12,165
le père, c'est le nœud,

180
00:08:12,462 --> 00:08:13,473
s'il existe,

181
00:08:13,791 --> 00:08:14,840
dont il est un fils.

182
00:08:14,940 --> 00:08:16,858
Ici, par exemple,

183
00:08:16,958 --> 00:08:19,209
le père de G,

184
00:08:19,909 --> 00:08:21,003
c'est B.

185
00:08:25,065 --> 00:08:27,929
Alors évidemment, il y  a un seul nœud

186
00:08:28,029 --> 00:08:29,951
dans un arbre enraciné qui n'a pas de père,

187
00:08:30,361 --> 00:08:31,683
c'est la racine.

188
00:08:31,783 --> 00:08:33,609
Donc ici, ma racine,

189
00:08:34,809 --> 00:08:37,857
ici, n'a pas de père.

190
00:08:38,556 --> 00:08:41,336
C'est une façon de parcourir

191
00:08:41,436 --> 00:08:42,649
ou d'explorer le graphe,

192
00:08:43,748 --> 00:08:45,622
souvent on va avoir des algorithmes

193
00:08:45,722 --> 00:08:47,499
dans lesquels, c'est tant que j'ai un père,

194
00:08:47,599 --> 00:08:48,511
je fais quelque chose.

195
00:08:51,592 --> 00:08:53,415
Qu'est-ce que c'est qu'un chemin ?

196
00:08:53,515 --> 00:08:55,348
Un chemin, c'est une suite de nœuds

197
00:08:55,448 --> 00:08:57,058
dont chacun est le fils du précédent.

198
00:08:57,571 --> 00:08:59,543
Par exemple ici, j'ai un chemin

199
00:09:02,218 --> 00:09:09,417
qui est le chemin A, D, E.

200
00:09:10,905 --> 00:09:12,008
Ce chemin-là,

201
00:09:13,046 --> 00:09:15,220
il comporte trois sommets

202
00:09:15,534 --> 00:09:17,770
et deux arêtes,

203
00:09:18,228 --> 00:09:19,743
donc deux relations père

204
00:09:20,269 --> 00:09:21,512
ou deux relations fils

205
00:09:23,173 --> 00:09:24,541
La racine évidemment,

206
00:09:24,641 --> 00:09:25,790
c'est ce que j'ai dit avant,

207
00:09:25,890 --> 00:09:27,483
c'est le nœud qui n'a pas de parent.

208
00:09:28,244 --> 00:09:30,225
Et puis on va essayer de définir quelque chose

209
00:09:30,325 --> 00:09:32,483
qui s'appelle la profondeur ou le niveau,

210
00:09:32,806 --> 00:09:34,363
qui est la longueur du chemin

211
00:09:34,463 --> 00:09:35,820
qui nous relie à la racine.

212
00:09:35,920 --> 00:09:38,069
Alors là, on va avoir un petit problème

213
00:09:38,641 --> 00:09:39,853
de vocabulaire,

214
00:09:40,115 --> 00:09:42,752
parce que, en fonction des ouvrages d'algorithmique

215
00:09:42,852 --> 00:09:44,091
et des choix,

216
00:09:44,387 --> 00:09:47,024
vous aurez des définitions différentes.

217
00:09:47,645 --> 00:09:50,344
Donc si on se base sur la longueur des chemins,

218
00:09:50,673 --> 00:09:54,422
ici, on va avoir le niveau 1,

219
00:09:56,035 --> 00:09:59,611
ici, on va avoir le niveau 0,

220
00:10:00,857 --> 00:10:02,906
et ici, on va avoir le niveau 2.

221
00:10:04,445 --> 00:10:05,872
Ça, ça nous permet

222
00:10:06,195 --> 00:10:08,312
de définir effectivement

223
00:10:08,412 --> 00:10:11,369
des nœuds par rapport à la distance à la racine,

224
00:10:11,903 --> 00:10:14,836
et la distance en termes de nombre d'arcs ou d'arêtes

225
00:10:14,936 --> 00:10:16,839
à traverser pour retourner à la racine.

226
00:10:17,918 --> 00:10:19,799
Et donc, on peut définir ainsi

227
00:10:19,899 --> 00:10:22,471
la hauteur ici

228
00:10:24,583 --> 00:10:28,499
qui correspond au niveau maximal d'un nœud,

229
00:10:28,599 --> 00:10:31,225
et si on définit la hauteur de cette façon-là,

230
00:10:31,452 --> 00:10:33,549
ici, la hauteur serait égale à 2.

231
00:10:37,200 --> 00:10:39,208
Et là, c'est là où il y a un petit souci,

232
00:10:39,552 --> 00:10:41,210
parce que, dans certains ouvrages,

233
00:10:41,310 --> 00:10:42,882
on dit qu'ici, la hauteur, c'est 3.

234
00:10:42,982 --> 00:10:45,420
En fait, il faut prendre une convention

235
00:10:45,520 --> 00:10:47,479
dans laquelle on dit effectivement

236
00:10:47,579 --> 00:10:48,984
qu'est-ce que l'on regarde

237
00:10:49,084 --> 00:10:51,169
comme notion de hauteur,

238
00:10:52,056 --> 00:10:55,673
et si on compte les arêtes qui sont traversées

239
00:10:55,773 --> 00:10:58,216
ou si on compte les nœuds

240
00:10:58,316 --> 00:10:59,944
qui sont sur le chemin.

241
00:11:00,406 --> 00:11:05,173
Alors ces deux notations-là,

242
00:11:05,273 --> 00:11:07,300
c'est exactement la même chose

243
00:11:07,400 --> 00:11:10,099
que ce que vous avez pour les ascenseurs et les étages

244
00:11:11,173 --> 00:11:13,951
dans le monde américain par exemple

245
00:11:14,051 --> 00:11:16,488
où le rez-de-chaussée, c'est l'étage n°1,

246
00:11:16,702 --> 00:11:18,553
c'est le 1 sur l'ascenseur,

247
00:11:18,653 --> 00:11:19,876
et le 2, et cætera

248
00:11:20,206 --> 00:11:22,178
et où vous avez donc une différence

249
00:11:22,278 --> 00:11:25,281
entre compter le nombre de sols sur lesquels vous pouvez être

250
00:11:25,381 --> 00:11:26,652
ou compter le nombre d'étages

251
00:11:26,752 --> 00:11:28,953
c'est-à-dire ce qui sépare deux sols.

252
00:11:29,641 --> 00:11:33,227
Donc de façon je dirais un peu générale,

253
00:11:33,673 --> 00:11:35,375
si on est en informatique,

254
00:11:35,475 --> 00:11:39,182
il faudra dire exactement ce qu'on entend par hauteur.

255
00:11:39,424 --> 00:11:41,421
Pour moi, la hauteur d'un arbre,

256
00:11:41,521 --> 00:11:42,698
c'est le nombre d'arcs

257
00:11:42,798 --> 00:11:44,761
qui permettent d'aller de la racine

258
00:11:45,540 --> 00:11:48,358
jusqu'à la feuille la plus profonde.

259
00:11:50,936 --> 00:11:53,033
Donc un arbre qui n'a qu'un seul nœud

260
00:11:53,133 --> 00:11:54,377
aura la hauteur 0,

261
00:11:56,630 --> 00:11:58,652
mais si j'ai un arbre qui n'a pas de nœud,

262
00:11:58,752 --> 00:12:00,318
ce qui est tout à fait possible

263
00:12:00,418 --> 00:12:02,204
parce qu'un arbre, ça va être une collection

264
00:12:02,580 --> 00:12:04,583
d'éléments, et donc on va avoir un arbre vide,

265
00:12:05,071 --> 00:12:07,275
dans ce cas-là, c'est une convention

266
00:12:08,076 --> 00:12:09,402
que l'on peut utiliser,

267
00:12:10,024 --> 00:12:12,062
il faut bien comprendre que ce sont des conventions,

268
00:12:15,449 --> 00:12:20,397
on peut dire que c'est par exemple de hauteur -1.

269
00:12:24,314 --> 00:12:26,200
Alors il faudra être très attentif

270
00:12:26,461 --> 00:12:27,958
quand vous lirez des énoncés.

271
00:12:28,389 --> 00:12:31,561
Sur certains documents d'Eduscol,

272
00:12:31,661 --> 00:12:34,007
la hauteur d'un arbre réduit à un nœud

273
00:12:34,107 --> 00:12:35,749
est donnée comme étant égale à 1.

274
00:12:36,158 --> 00:12:38,590
On est tout le temps en train de jongler

275
00:12:38,690 --> 00:12:40,977
et donc il faut définir précisément

276
00:12:41,077 --> 00:12:44,260
et rappeler quelle est la définition de hauteur que l'on utilise.

277
00:12:44,884 --> 00:12:47,372
À titre personnel, je préfère cette notation

278
00:12:47,714 --> 00:12:49,473
parce qu'on verra le lien

279
00:12:49,573 --> 00:12:51,470
entre le nombre de nœuds dans un arbre

280
00:12:51,570 --> 00:12:54,010
et la hauteur qui est sans doute plus simple

281
00:12:54,110 --> 00:12:56,075
pour savoir le nombre de nœuds

282
00:12:56,175 --> 00:12:57,474
que l'on a à une certaine hauteur,

283
00:12:57,574 --> 00:12:59,671
on aura une relation simple

284
00:12:59,771 --> 00:13:01,584
entre le niveau et le nombre de nœuds

285
00:13:01,684 --> 00:13:02,794
à un certain niveau.




  - Dire qu’une requête est déclarative, c’est dire que (indiquer les phrases correctes) :

    .. eqt:: optimReq1

        A) :eqt:`I` La requête ne définit pas précisément le résultat.
        #) :eqt:`C`  La requête ne dit pas comment calculer le résultat.
        #) :eqt:`C` La requête est indépendante de l’organisation des données.
        #) :eqt:`I`  La requête est une expression de besoin en langage naturel.

  - Un plan d’exécution, c’est

    .. eqt:: optimReq2

        A)  :eqt:`I` Un programme choisi parmi un ensemble fixe et pré-défini de programmes proposés par le système.
        #)  :eqt:`C` Un programme produit à la volée par le système pour chaque requête.
        #)  :eqt:`C` Un arbre d’opérateurs communicants entre eux.

  - L’optimisation de requêtes, c’est

    .. eqt:: optimReq3

       A)   :eqt:`I` Modifier une requête SQL pour qu’elle soit la plus efficace possible.
       #)   :eqt:`I` Structurer les données pour qu’elles soient adaptées aux requêtes soumises.
       #)  :eqt:`C` Choisir, pour chaque requête, la meilleure manière de l’exécuter.

  - Quelles sont les affirmations vraies parmi les suivantes\,? (1 pt)

   .. eqt:: optimReq3

        A) :eqt:`C` Le choix d'un plan d'exécution dépend de la mémoire RAM disponible.
        #) :eqt:`I` Le choix d'un plan d'exécution dépend de la forme de la requête SQL
        #) :eqt:`C` Le choix d'un plan d'exécution dépend de l'existence d'index
        #) :eqt:`I` Le choix d'un plan d'exécution dépend du langage de programmation utilisé.

## Types abstraits de données : introduction à la séquence 1/10

  1. **Introduction à la séquence**
  2. Types abstraits en algorithmique
  3. Pourquoi, pour qui ? 
  4. La pile
  5. La file
  6. Arbres - définition, propriétés, comptage part1 & part 2 
  7. Arbres binaires 
  8. Parcours d'arbre
  9. Feuilles étiquetées

L'objectif de la séquence est d'aborder de manière élémentaire les notions de types abstraits de données pour aboutir à l'examen de la notion d'objet en Python. On regardera ces notions du côté algorithmique : comment j'exprime un algorithme en ayant une abstraction qui permette de manipuler les données.

**Dans une première partie (videos 1 à 5) :**

- On regarde les types simples rapides
- On se pose la question qu'est-ce qu'un type abstrait de données, comment on le spécifie
- Présentation de structures séquentielles, en particulier le tableau et la liste
- Présentation de types plus compliqués comme la pile et la file, la file à priorité

**Dans une deuxième partie (videos 6 à 10) :**

Nous abordons la structure d'arbres et le concept d'arbre qui se poursuivra sur plusieures sequences, en demarrant dans cette sequence 4 par **les arbres binaires simples** et **les arbres binaires de recherche**

[![Vidéo 1 B3-M1-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S4-V1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S4-V1.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S4/B3-M1-S4-video1.srt" target="_blank">Sous-titre de la vidéo</a>





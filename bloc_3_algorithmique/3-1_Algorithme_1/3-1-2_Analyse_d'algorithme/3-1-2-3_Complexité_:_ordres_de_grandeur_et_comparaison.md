## Complexité d'algorithme : ordres de grandeur et comparaison des algorithmes 

1. Complexité d'algorithme : introduction et schéma algorithmique
2. Calcul de la complexité d'un algorithme
3. **Complexité : ordres de grandeur et comparaison des algorithmes** (2 vidéos)
4. Analyse de la complexité d'un problème. Conclusion
5. Preuve d'algorithme

[![Vidéo 3 part 1 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1e.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1e.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3a.srt" target="_blank">Sous-titre de la vidéo</a>

[![Vidéo 3 part 2 B3-M1-S2 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B3-M1-S2-video1f.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B3-M1-S2-video1f.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B3-M1/B3-M1-S2/B3-M1-S2-video3b.srt" target="_blank">Sous-titre de la vidéo</a>


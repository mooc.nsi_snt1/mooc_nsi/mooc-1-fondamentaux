====================================
Notion de complexité et grand *O*
====================================


Notion de complexité et de O()
==============================

Vidéo 1 : Introduction à la complexité et à la notion de O()
-------------------------------------------------------------

[Face caméra]

Bonjour à toutes et à tous,

Nous allons ici voir comment évaluer l'afficacité d'un programme.
Plus précisément le menu d'aujourd'hui est de voir les notions de
base de complexité d'un algorithme ou d'un programme.

C'est le terme utilisé pour parler d'efficacité d'un programme.

Montrer qu'une version d'un programme est plus *efficace* qu'une autre
n'est, a priori, pas aisé. La succession ou l'imbrication des tests
et des boucles et la multitude des possibilités fait qu'il n'est
généralement pas possible ou raisonnable d'évaluer l'efficacité de
l'algorithme pour chaque cas possible.

Dans ce chapitre, nous donnerons un aperçu des techniques et méthodes
d'estimation de l'efficacité d'un algorithme. En particulier nous définirons
la notation *grand O* qui permettra de classer les algorithmes selon
leur type d'efficacité sans devoir détailler le nombre exact d'instructions
exécutées par l'algorithme.

Nous ne traiterons donc pas ici de nouveaux concepts algorithmiques,
mais bien de la façon d'analyser programmes et algorithmes.

Commençons d'abord par motiver pourquoi la notion de complexité est importante.

[slides + 
Motivation : Efficacité d'un algorithme
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Généralement, il existe des tas de méthodes et d'algorithmes pour
résoudre un même problème. Il faut alors en choisir un qui soit
évidemment correct, mais aussi si possible simple et efficace.

Si l'algorithme doit être mis en oeuvre sur un nombre limité de
données avec peu de calculs, cet algorithme doit de préférence être
simple, facile à concevoir de façon correcte.

Si, par contre, un algorithme est fréquemment exécuté pour une masse
importante de données, il doit être le plus efficace possible.

Au lieu de parler de l'efficacité d'un algorithme, on parlera
généralement de la notion opposée, à savoir, de la complexité d'un
algorithme.

La complexité d'un algorithme ou d'un programme peut être mesurée de diverses
façons. Généralement, le temps d'exécution est la mesure principale de
la complexité d'un algorithme. 

D'autres mesures sont possibles dont:

  - la quantité d'espace mémoire occupée par le programme et en
    particulier par ses variables;
  - la quantité d'espace disque nécessaire pour que le
    programme s'exécute;
  - la quantité d'information qui doit être transférée (par lecture ou
    écriture) entre le programme et les disques ou entre le programme et des serveurs externes via un réseau;
  -  ...

Nous n'allons pas considérer de programme qui échange un grand nombre
d'informations avec des disques ou un autre ordinateur. 

Pour illustrer nos propos nous analyserons le **temps d'exécution** d'un programme pour évaluer
sa complexité, même si la mesure de l'espace mémoire occupé par les variables est
un autre critère qui est également souvent utilisé.

De façon générale, la complexité d'un algorithme pour un ensemble de
ressources donné est une mesure de la quantité de ces ressources
utilisées par cet algorithme.

Le critère que nous utiliserons dans la suite est un nombre d'actions
élémentaires exécutées.  Nous prendrons soin, avant toute chose, de
préciser le type d'actions prises en compte.

Nous verrons que, généralement, la complexité d'un algorithme est exprimée par
une fonction mathématique qui dépend de la taille du problème à résoudre.

[Vidéo 2]

Paramètres du programme
^^^^^^^^^^^^^^^^^^^^^^^

Le travail nécessaire pour qu'un programme s'exécute dépend de la 'taille'
du jeu de données fourni. 

Par exemple, si nous voulons analyser l'efficacité d'une fonction ``indice_min(s)`` qui
recherche, de façon piétone, l'indice de l'élément de valeur minimale
dans une liste ``s`` à ``n`` valeurs,

.. code-block:: python
		
    def indice_min(s):                     # 1
        meilleur_indice = 0                # 2
	for i in range(1, len(s)):         # 3
	    if s[i] < s[meilleur_indice]:  # 4
	        meilleur_indice = i        # 5
	return meilleur_indice             # 6

il est intuitivement normal que cet algorithme prenne un temps
proportionnel à la taille ``n`` de la liste à traiter.

``n`` est donc la taille du problème de recherche que l'on veut
résoudre.

Un programme qui fait des traitements dans un tableau de dimension
``m x n`` aura un temps d'exécution qui sera probablement, en fonction du
travail effectué, propositionnel aux deux dimensions donc aux deux
paramètres ``m`` et ``n``, et ainsi de suite.

``n``, ``m`` ... sont donc les paramètres du calcul de l'efficacité.


Les approches possibles
^^^^^^^^^^^^^^^^^^^^^^^^^

Imaginons que nous voulions comparer plusieurs programmes qui résolvent le
même problème algorithmique, disons sur un tableau.  La paramètre du problème est
donc la taille - appelons la ``n`` - du tableau.

**Approche pragmatique**


L'utilisateur peut chronométrer le temps moyen que mettent chacun des
trois codes à s'éxécuter.  Evidemment ce temps dépendra de
l'ordinateur et de la taille ``n`` du tableau et éventuellement le
travail de comparaison devrait être refait pour chaque nouvelle
configuration d'ordinateur que l'on voudrait tester.

Fort laborieux tout ça !

**Approche par calcul**

Pour obtenir des résultats qui restent valables quel que soit
l'ordinateur utilisé, on peut réaliser des calculs donnant une
approximation dans une unité qui soit indépendante du processeur
utilisé.

On peut, par exemple, faire l'approximation que chaque assignation de donnée simple ou 
test élémentaire représente une unité de temps d'exécution.

La complexité dépend donc fortement de l'unité prise qui doit être clairement
précisée.

Avec nos hypothèses, pour notre code

.. code-block:: python
		
    def indice_min(s):                     # 1
        meilleur_indice = 0                # 2
	for i in range(1, len(s)):         # 3
	    if s[i] < s[meilleur_indice]:  # 4
	        meilleur_indice = i        # 5
	return meilleur_indice             # 6

chaque exécution des instructions des lignes 1, 2, 5 et 6
prennent chacune une unité de temps d'exécution. 

La boucle *for* de la ligne 3 à 5 s'exécute *n-1* fois.

Le test de la ligne 4 prend une unité; il est effectué *n-1* fois et donc la ligne 4 utilisera au total *n-1* unités.

L'assignation de la ligne 5 prend une unité. Elle n'est effectuée que lorsque le test du ``if`` est vérifié, ce qui dépends des valeurs précises dans ``s``.

Si l'on ne désire pas uniquement faire une seule mesure sur un jeu de
données bien précis, il faut donc expliciter si l'on désire connaître le temps
d'exécution dans *le meilleur des cas*  *T_min(n)*, *le pire des cas* *T_MAX(n))*, ou 
*en moyenne*  *T_moyen(n)*. Ici:

 - *T_min(n)* est donné dans le cas où le test du ``if`` n'est jamais
    vérifié, ce qui signifie que le minimum se trouve à la première
    composante. 

 - *T_MAX(n)* est donné dans le cas où le test du ``if`` est à chaque
    fois vérifié, ce qui correspond au cas où toutes les valeurs de
    ``s`` sont distinctes et triées en ordre décroissant. 

Le calcul de *T_moyen(n)* est généralement beaucoup plus difficile à
effectuer et dépend souvent d'hypothèses sur les données à traiter.

Bien compliqué tout ça !

N'oublions pas que  *T_min(n)*, *T_MAX(n)* et  *T_moyen(n)* sont des fonctions qui dépendent de la taille des données et donc du paramètre ``n``.

Les informaticiens sont donc retournés vers les mathématiciens pour voir ce qu'ils proposaient d'autres.  La suite explique quel a été leur réponse.

[vidéo 3]
Le grand *O*
================

En bref, il est souvent très difficile de
calculer le temps moyen (ou maximal) d'exécution d'un algorithme.  Ces calculs
précis sont, de plus, inutiles puisqu'ils se basent sur des
approximations grossières du temps d'exécution des actions
élémentaires; évaluations qui ne tiennent pas non plus compte, par
exemple, de l'ordinateur et du compilateur ou de l'interpréteur utilisés.

Une évaluation du temps d'exécution d'un algorithme
ne devient généralement intéressante que lorsque la taille du jeu de
données, c'est-à-dire de *n* dans notre exemple précédent, est grande. En effet
pour *n* petit, les algorithmes s'exécutent généralement très rapidement.

Un élément souvent fort important est l'évolution du temps d'exécution quand la taille du problème (et donc ici du paramètre ``n``) augmente.

Supposons que le nombre  maximal d'instructions élémentaires de 4 algorithmes soit respectivement de

- *A = 100.n*, 
- *B = 15.n^2*
- *C = n^4*
- *D = 2^n*.

La table suivante donne les valeurs de complexité pour *n =
1,10,100,1000, 10^6* et *10^9*

Un bref calcul montre qu'en un jour, si je n'ai pas de soucis d'espace mémoire, je peux résoudre sur mon ordinateur personnel, avec l'algorithme

  -  *A* , un problème pour *n*  valant approximativement *1000000000000* 
  -  *B* , un problème pour *n*  valant approximativement *10000000* 
  -  *C* , un problème pour *n*  valant approximativement *3000*
  -  *D* , un problème pour *n*  valant approximativement *50*

et donc, sauf pour de petites valeurs de *n*, on va clairement préferrer *A* puis *B* puis *C* puis *D*.

On préfère donc donner un ordre de grandeur permettant d'avoir une
idée du type d'algorithme.

Pour cela, on utilise la notation grand *O* définie ci-dessous, qui permet:

  - de déterminer si un algorithme a une chance de s'exécuter pour un
    *n* donné,
  - connaissant le temps d'exécution pour un *n* donné, de faire une
    approximation de ce temps pour une autre valeur de *n*.

Définition
-----------

La notation grand *O* est définie comme suit:

.. centered::   Définition (*O*) : Une complexité *T(n)* est dite en grand *O*  de *f(n)* (en *O(f(n))* s'il existe un entier *N* et une constante *c > 0* tels que pour tout entier *n > N* nous avons *T(n) <=  c.f(n)*

Cette définition permet d'effectuer de nombreuses simplifications dans les
calculs. En effet de cette définition il résulte que:

  * Les facteurs constants ne sont pas importants. En effet, si par
    exemple *T(n)=n* ou si *T(n)=100n*, *T(n)* est toujours en *O(n)*
  * Les termes d'ordres inférieurs sont négligeables. Ainsi si *T(n) =
    n^3 + 4n^2 + 20n + 100*, on peut voir que pour *n > N = 5*

    -  *n^3  >  4n^2* 
    -  *n^3  >  20n* 
    -  *n^3  >  100*

et donc pour *n>5*, *T(n) = n^3 + 4n^2 + 20.n + 100 < 4n^3*. 

De ce fait *T(n)* est en *O(n^3)*.

Notons que le *O* donne une borne supérieure; en l'utilisant, on joue donc la carte de la sécurité.


[Video 4]
Calcul du grand *O*
-----------------------

Classes de complexité
.....................

D'abord une remarque évidente : si on peut calculer qu'un algorithme a une complexité en *O(n^2 )*, il n'est pas
intéressant, même si cela est trivialement vrai, d'exprimer le fait que *T(n)*
est en *O(n^3 )*, *O(n^4 )*, .... 

Lorsque l'on évalue le grand *O* d'une complexité, c'est la meilleure
estimation "simple" que l'on cherche à obtenir. 

Ainsi on classe généralement les complexités d'algorithmes selon leur grand
*O* comme donné par la table suivante :

=============   ====================
*O*             Classe d'algorithmes
=============   ====================
*O(1)*          constant 
*O(log n)*      logarithmique 
*O(n)*          linéaire 
*O(n log n)*    *n log n* 
*O(n^2 )*       quadratique 
*O(n^3 )*       cubique
\.\.\.          toutes les complexités polynomiales successives
*O(2^n )*       exponentiel en base 2 
*O(3^n )*       exponentiel en base 3
 \.\.\.          
*O(n^n )*       exponentiel en base n 
 \.\.\.          
=============   ====================

**Classes de complexité**

et ainsi de suite.

Remarquons qu'en vertu du fait que *pour tout a,b* positif : log_a n = (log_a b).(log_b n)
 
et comme *log_a b* est une constante pour *a* et *b* fixés, si *T(n)* a une complexité en
*O(log_a n)* il est également en *O(log_b n)*. (la base
du logarithme n'a donc pas d'importance pour exprimer un grand *O*).  Notons
encore que ceci n'est pas vrai pour la base des complexités exponentielles.


[Vidéo 5]

Problèmes indécidables
----------------------

Notons aussi que certains problèmes n'ont pas d'algorithmes pour les résoudre.

En théorie de la calculabilité, ces problèmes sont dit indécidables.

Le problème indécidable souvent mentionné est `le problème l'arrêt` :
on peut démontrer qu'il n'y a donc pas de méthode générale d'analyse
statique capable de déterminer si un programme boucle indéfiniment ou
non.

L'important dans cet énoncé est de bien comprendre quels sont les
données du problème.  Le problème de l'arrêt demande de construire une
**méthode générale**. Donc ce n'est qu'après construction de cette méthode
que celle-ci recevra comme donnée un programme, et qu'elle devra
répondre si oui ou non ce programme se termine toujours quelque soit
les données qu'on lui fournit.

Ce n'est pas équivalent à recevoir un programme et ensuite trouver une
**méthode spécifique** à ce programme pour déterminer s'il se termine toujours ou non.

Plus d'information sur les problèmes indécidables peuvent être trouvés dans divers endroits

https://fr.wikipedia.org/wiki/Problème_de_l%27arrêt

https://fr.wikipedia.org/wiki/Décidabilité

La calculabilité est le domaine qui étudie ces aspects de décidabilité et de complexité (voir Pierre Wolper, Introduction à la calculabilité, 3e éd., Dunod, 2006 (ISBN 978-2100499816))


[Vidéo 6]

Donnons ici un petit nombre de règles simples permettant d'évaluer la
complexité d'un algorithme.  Ces règles ne donnent en général qu'une
surapproximation parfois grossière; une estimation plus précise devant
tenir compte du fonctionnement de l'algorithme.

Règles de calcul du grand O
..............................

Règle sur l'unité : 

=======================================================================

.. admonition:: Règle 1 (unité)

 Il faut clairement définir l'unité utilisée et les éléments pris en
 compte.  Ainsi si l'on tient compte des tests élémentaires, des
 assignations, des lectures de données et des écritures de résultats,
 une assignation à une variable simple, une instruction input ou print
 d'une donnée simple, ou l'évaluation d'une expression (ou d'une
 condition) simple ne donnant pas lieu à l'exécution de fonctions,
 prend un temps constant fixé et est donc en *O(1)*.

 **Attention** : l'assignation à des objets plus complexes ou des
 input ou print de données plus complexes (comme des séquences) peut
 ne plus être en *O(1)* si leur taille dépend de la taille du problème
 (*n*).

=======================================================================

Règle de la séquence : 

=======================================================================

.. admonition:: Règle 2 (séquence)

  Une séquence formée d'un traitement 1 suivit d'un traitement 2
  dont les complexités respectives sont en  O(f_1(n)) et
   *O(f_2(n))* alors
  le traitement 1 suivi du traitement 2 prend *T_1(n) + T_2(n)* et
  est en

  aura une complexité en
  
  *O(f_1(n) + f_2(n)) = O (max(f_1(n) , f_2(n)))*
  
  où max prend la fonction qui croît le plus vite c'est-à-dire de plus grand
  ''ordre''.

=======================================================================

Par exemple si traitement 1 est en *O(n^2 )* et traitement 2 est en *O(n)*,
la séquence des 2 est en *O(n^2 + n) = O(n^2)*
 
En particulier une séquence d'instructions simples ne manipulant que des objets simples et ne faisant aucun appel
à une fonction ne dépend pas de *n* et est donc en *O(1)*.


Règle du if : 

=======================================================================

.. admonition:: Règle 3 (if)

  Pour un ``if condition: instruction_1 else: instruction_2`` 

  où 

  - ``instruction_1`` est en *O(f_1(n))* 
  - ``instruction_2`` est en *O(f_2(n))* 
  - l'évaluation de la ``condition`` est en *O(g(n))*
  
  suivant le test, le ``if`` sera en *O(max(f_1(n),g(n)))* ou en
  *O(max(f_2(n),g(n)))* et peut être borné par:
  
  *O(max(f_1(n), f_2(n), g(n)))* 

=======================================================================

Notons que souvent *g(n)* est en *O(1)*.

Cette règle peut être généralisée pour une instruction ``if`` ayant une
ou des parties ``elsif``.

Règle du while : 

=======================================================================

.. admonition:: Règle 4 (while)

  Pour un ``while``, sachant que le corps de la boucle est en
  *O(f_1(n))* et que l'évaluation de la condition est en *O(f_2(n))*,
  si on a une fonction en *O(g(n))* qui donne une borne supérieure du
  nombre de fois que le corps sera exécuté, alors le ``while`` est en
  *O(f(n).g(n))* avec *f(n) = max(f_1(n),f_2(n))*.

=======================================================================

Règle du for : 

=======================================================================

.. admonition:: Règle 5 (for)

  La règle pour une boucle ``for``, est similaire à celle du ``while``, avec l'évaluation de la condition (*O(f_2(n))*) généralement en *O(1)*. On a donc une complexité en *O(f_1(n).g(n))*

=======================================================================

Par exemple :

.. code-block:: python
   
    for i in range(n):
       print(i)

Se traduit en:

.. code-block:: python
   
    i=0
    while i < n: 
       print(i)
       i=i+1

La fonction iter() appliquée sur une séquence ou un range, permet de pouvoir ensuite utiliser la fonction next() sur l'objet produit.

Par exemple :

.. code-block:: python
   
  st = "bonjour"  
  for i in st:
       print(i)

se traduit en:

.. code-block:: python
   
    st = "bonjour"
    i = iter(st)
    s=next(i)
    while s != None:
       print(s)
       s=next(i)


Règle de la fonction : 

=======================================================================

.. admonition:: Règle 6 (fonction)

  Si l'évaluation de la complexité d'une fonction est en *O(f(n))*
  pour un paramètre de taille *n*, L'appel à cette fonction avec un
  paramètre de taille *g(n)* donnera une complexité en *f(g(n))*
  correspondant à la complexité du traitement de cette fonction pour
  les paramètres effectifs donnés.

=======================================================================

Par exemple :

pour le code :

.. code-block:: python
   
    n = int(input())
    ma_liste = [random(1,100) for _ in range(n*n)]
    mon_min = min(ma_liste)

sachant que la fonctionn ``min`` a une complexité en ``O(n)``, et que
ma_liste a une taille effective de ``n^2``éléments, on obtient une
complexité pour l'instruction ``mon_min = min(ma_liste)`` (ainsi que
pour l'instruction ```ma_liste = [random(1,100) for _ in range(n*n)]`` en ``O(n^2)``.



On peut raffiner ces calculs, selon que l'on essaye de trouver une
complexité minimale, moyenne ou maximale.  Ce raffinement se situera
dans le nombre de fois qu'une instruction sera répétée, ayant par
exemple certaines hypothèses sur la probabilité que certaines
conditions de ``if`` ou de boucles sont vérifiées.

[Textes et quiz]

Application des règles de calcul
================================

Ayant ces règles d'évaluation du grand *O* pour chaque type d'instruction, le
calcul de la complexité d'un algorithme se fait en partant des complexités des
instructions simples, et en calculant, de proche en proche, les complexités des
instructions non simples à partir des résultats déjà calculés. Ces calculs
procèdent en quelque sorte par des regroupements en suivant la structure de
l'algorithme.

Complexité de la recherche du minimum
--------------------------------------------------

Prenons l'exemple de la recherche du minimum c'est-à-dire du
traitement suivant (après traduction en utilisant un ``while`` en ayant ``n = len(s)``:


.. code-block:: python

   res = 0                    #1
   i = 1                      #2
   while i < n     :          #3  
       if s[i] < s[res]:      #4   
           res = i            #5
       i = i+1                #6
   return res                 #7

Nous supposons que chaque instruction simple est prise en compte pour
le calcul de la complexité.

La structure du code de tout programme, peut être décomposé sous forme
d'*arbre syntaxique* comme donné par la figure suivante où chaque
*noeud* *représente* une instruction *Python* simple ou composée.

.. image:: images/rechmin.png
   :align: center


.. centered:: Décomposition d'un traitement en suivant la structure

Les noeuds sans *fils*, appelés les *feuilles* de l'arbre,
correspondant aux instructions 1,
2, 5, 6 et 7, représentent des instructions d'assignations simples ou
``return`` qui, d'après la règle 1, sont en *O(1)*.

De ce fait, d'après la règle 3, le noeud 4-5 correspondant à
l'instruction ``if`` est en *O(1)*.

D'après la règle 2, la complexité de la séquence d'instructions 4-6
est en *O(1)*.

On peut maintenant évaluer la complexité du ``while`` (noeud 3-6),
il s'exécute *n-1* fois et la complexité du corps de la boucle est,
comme on vient de le voir, en *O(1)*. Donc d'après la règle 4, la
complexité du ``while`` est en *O(n)*.

Finalement, d'après la règle 2, la complexité de tout le traitement est en
*O(1+1+n+1)* qui peut être simplifié par *O(n)*.

.. Note::

  - Si, par exemple, un algorithme *A* est en *O(n)* et un algorithme
    *B* est en *O(n^2)*, *A* est 'meilleur', en terme, de complexité
    que *B*.   
   
    Par contre si *A* et *B* sont tous les deux en *O(f(n))*, il faut
    détailler le calcul de la complexité pour déterminer lequel est
    plus complexe que l'autre.

  - D'après la définition du *O*, l'algorithme de recherche du
    minimum est en *O(n)* et donc trivialement également en *O(n^2)*,
    *O(n^3)*, ..., *O(n^n)*.  Lorsque l'on cherche la complexité d'un
    algorithme, on essaye bien évidemment de donner la valeur la plus
    précise (Exemple: *O(n)* pour la recherche du minimum)

  - Rappelons que l'on peut évaluer plusieurs complexités
    différentes.  Par exemple, pour les algorithmes de tris de liste,
    on peut regarder la complexité en terme de nombre d'instructions ou
    seulement regarder les instructions qui effectuent des déplacements
    d'éléments (le déplacement d'un élément peut prendre beaucoup plus
    de temps que les autres instructions).  Certains tris sont en
    *O(n^2)* si l'on regarde toutes les instructions, mais en *O(n)* en
    terme de déplacement d'informations.

Complexité de la recherche séquentielle et dichotomique
--------------------------------------------------------

Il est facile de voir que l'algorithme de **recherche séquentielle** est
également en *O(n)* où *n* est la longueur de la liste et en supposant
que les éléments sont testés en *O(1)*.

Pour calculer la complexité de la **recherche dichotomique**, en
utilisant les règles vues précédemment, on peut assez rapidement
observer que la complexité moyenne ou maximale en nombre d'actions
élémentaires de cet algorithme est en *O(f(n))* où *f(n)* est le
nombre respectivement moyen *(f_moyen(n))* ou maximum *(f_max(n))*
de fois que la boucle ``while`` est exécutée.

Pour évaluer *f_max(n)*, commençons par analyser le comportement de
l'algorithme. Nous pouvons tout d'abord remarquer qu'à chaque tour de boucle,
au pire des cas, l'intervalle de recherche est divisé par deux, quelle que soit
la valeur de ``x``. 

La figure suivante donne une évolution possible de l'intervalle de
recherche dans ``s`` pour une liste à 100 éléments en supposant que ``x`` n'est
pas dans ``s`` mais que ``s[40] < x < s[41]``

.. image:: images/fdicho.png
   :align: center

.. centered:: Evolution de l'intervalle de recherche lors d'une recherche dichotomique

Le ``while`` s'exécute donc au maximum 7 fois. De façon générale on peut
voir que:

.. image:: images/eqdicho.png
   :align: center

et donc que *f_max(n) <= \log_2(n) + 1* 

La recherche dichotomique a donc une complexité maximale en *O(ln n)*.
On peut calculer que la complexité moyenne (en supposant ou non que
``x`` est dans ``s``) est également en *O(ln n)*.  

Complexité du tri par sélection
--------------------------------------------------------

Pour calculer la complexité maximale du tri par sélection,
décomposons l'algorithme sous forme d'arbre en suivant sa structure; ceci est
donné en figure suivante:

.. image:: images/decsel.png
   :align: center

.. centered:: Décomposition du tri par sélection

En supposant que toutes les instructions et les traitements de
passages de paramètres interviennent dans ce calcul, en suivant les
règles pour calculer cette complexité, nous obtenons :

 -  ``1, 5, 8, 11`` et ``13`` , sont en *O(1)*
 -  le ``if`` (``10-11``) est en *O(1)*
 -  le ``for`` (``9-11``) imbriqué est en *O(n-i)* et peut être borné
    par *O(n)*
 -  le ``for`` global (``6-13``) est en *O(n^2)*
 -  la procédure ``tri_selection`` (``1-13``) est en *O(n^2)*


Complexité du tri par insertion
--------------------------------------------------------

En analysant l'algorithme de tri par insertion, on peut voir que sa
complexité maximale est également en *O(n^2)*.  On peut voir que la
complexité minimale est rencontrée dans le cas où la liste est déjà
triée et vaut *O(n)*


Complexité du tri Bulle
--------------------------------------------------------

Ici aussi la complexité maximale est en *O(n^2)*.  La complexité
minimale est également en *O(n^2)*.

Complexité du tri par énumération
--------------------------------------------------------

Si *m < n*, la complexité moyenne et maximale de ce tri est en *O(n)* ce qui
est remarquable.

Quelques exemples intéressants
-------------------------------

Les exemples suivants viennent d'interrogation ou d'examens précédents où l'on considère que m,l,n sont des paramètres entiers positifs.

 .. code-block:: python

   i = 1
   while i < n**3 :
      i=i*2

 .. code-block:: python

   for i in range(n):
     for j in range(m):
        for k in range(l):
            print('hello')

 .. code-block:: python

   i = 2
   while i < n:
      i=i*i


Complexité des méthodes de manipulation de séquences
====================================================

Voir le site wiki python pour plus de détails:  

http://wiki.python.org/moin/TimeComplexity

On suppose que *n* est le nombre d'éléments dans la liste et *k* la
valeur (ex: longueur) du paramètre.

La complexité moyenne suppose que les paramètres sont générés
uniformément de façon aléatoire.


Complexité des opérations sur les listes
----------------------------------------

De façon interne, une liste est représentée sous forme de vecteur où
les composantes sont contiguës en mémoire et avec, en moyenne, de la
place libre pour allonger la liste.  La pire opération est donc une
insertion ou suppression en début de liste car toutes les composantes
doivent bouger.  Malheureusement, au pire des cas, un simple
``s.append(x)`` peut demander de recopier toute la liste s'il n'y a
plus de place libre juste après la liste ``s``.


=============== ==================  ====================
Opération       Complexité moyenne  Complexité maximale
=============== ==================  ====================
Copy            O(n)                O(n)
Append          O(1)                O(n)
Insert          O(n)                O(n) 
Get Item        O(1)                O(1) 
Set Item        O(1)                O(1)
Delete Item     O(n)                O(n)
Iteration       O(n)                O(n) 
Get Slice       O(k)                O(k) 
Del Slice       O(n)                O(n)
Set Slice       O(k+n)              O(n+k) 
Extend          O(k)                O(n+k)
Sort            O(n log n)          O(n log n)
Multiply        O(nk)               O(nk) 
x in s          O(n)                O(n)
min(s), max(s)  O(n)                O(n)
len(s)          O(1)                O(1) 
=============== ==================  ====================

Complexité des opérations sur les ensembles
-----------------------------------------------

L'implémentation des ensembles est similaire à celle des dictionnaires.

================== ====================  =======================
Opération          Complexité moyenne    Complexité maximale
================== ====================  =======================
x in s             O(1)                  O(n)        
s | t              O(len(s)+len(t))      O(len(s) * len(t))        
s & t              O(max(len(s),len(t))  O(len(s) * len(t))        
s - t              O(max(len(s),len(t))  O(len(s) * len(t))        
s ^ t              O(max(len(s),len(t))  O(len(s) * len(t))        
================== ====================  =======================

Complexité des opérations sur les dictionnaires
------------------------------------------------

Avec l'interpréteur que nous utilisons,  un dictionnaire Python est stocké dans une table (zone mémoire contiguë).
Pour simplifier supposant que c'est une table à ``MAX`` composantes (par exemple pour un petit dictionnaire ``MAX = 100``.

L'accès à une composante du dictionnaire (Get Item) se fait par hashing (``hash(cle) % 100``) qui calcule un indice entre ``0`` et ``MAX-1``.
En moyenne cette indice correspond à l'emplacement dans la table où l'élément (clé et valeur) va être stocké.
Cette opération (hashing, modulo et accès à la table) prend un temps constant ``O(1)``.

La complexité moyenne suppose que les collisions sont rares avec la fonction de hashage (ce qui est généralement la cas).

Le cas moyen suppose que les paramètres sont choisis uniformément aléatoirement parmi les clés possibles.

Malheureusement, il peut y avoir des collisions.  Au pire des cas,
pour un dictionnaire de ``n``éléments, l'accès à un élément aura
``n-1`` collisions plus le dernier hashing et prend un temps (complexité maximale) en
``O(n)``.

En terme de complexité, placer un élément (Set Item) revient à y accéder suivit du stockage de l'élément qui est en ``O(1)`` en moyenne et ``O(n)`` en complexité maximal, soit la même complexité que l'accès à un élément.

================ ====================  ====================
Opération        Complexité moyenne    Complexité maximale
================ ====================  ====================
Copy             O(n)                  O(n)         
Get Item         O(1)                  O(n)         
Set Item         O(1)                  O(n)         
Delete Item      O(1)                  O(n)         
Iteration        O(n)                  O(n)         
================ ====================  ====================

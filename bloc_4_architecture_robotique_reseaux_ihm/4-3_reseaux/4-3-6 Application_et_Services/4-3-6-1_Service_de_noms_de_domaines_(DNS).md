# Application et Services 1/3: Service de noms de domaines (DNS)

1. **Service de noms de domaines (DNS)**
2. HTTP, FTP, SMTP
3. NTP

####

- **Geoffrey Vaquette**.  Ce chapitre 6 traite des services applicatifs au sein d'un réseau. Nous nous attarderons sur quelques services associés à la couche 7 du modèle OSI et analyserons leur fonctionnement afin d'en comprendre les rôles et les protocoles qui leur sont associés. Pour rappel, la couche 7 contient les protocoles de plus haut niveau lors de l'accès au réseau. C'est à ce niveau que les données utilisateur sont contenues et transmises. Nous commencerons par un protocole d'exploitation qu'est le DNS, puis nous verrons un protocole orienté vers l'envoi de pages Internet HTTP. Un protocole pour l'envoi de mail, le SMTP et enfin FTP pour l'envoi de fichiers. Nous verrons dans la dernière partie le protocole NTP pour la synchronisation d'horloge. Commençons tout de suite par le protocole DNS. 

[![Vidéo 1 B4-M3-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S6_video1_DNS.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S6_video1_DNS.mp4)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-script-video1.md" target="_blank">Transcription vidéo 1</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-diapos.pdf" target="_blank">Supports de présentation des vidéos 1 à 3</a>



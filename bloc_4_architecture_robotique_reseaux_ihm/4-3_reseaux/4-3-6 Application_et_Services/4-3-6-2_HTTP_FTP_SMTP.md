# Application et Services 2/3: HTTP, FTP, SMTP

1. Service de noms de domaines (DNS)
2. **HTTP, FTP, SMTP**
3. NTP

####

- **Geoffrey Vaquette** détaille dans cette vidéo les trois protocoles de la couche 7 que sont le HTTP, le FTP et le SMTP.

[![Vidéo 2 B4-M3-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S6_video2_HTTP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S6_video2_HTTP.mp4)


#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-script-video2.md" target="_blank">Transcription vidéo 2</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-diapos.pdf" target="_blank">Supports de présentation des vidéos 1 à 3</a>

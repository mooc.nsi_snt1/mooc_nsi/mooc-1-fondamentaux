# Application et Services 3/3: NTP

1. Service de noms de domaines (DNS)
2. HTTP, FTP, SMTP
3. **NTP**

####
- 
**Anthony Juton**.  La vidéo aborde le protocole NTP, Network Time Protocol, le protocole de synchronisation d'horloge des appareils informatiques.

[![Vidéo 3 B4-M3-S6 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S6_video3_NTP.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S6_video3_NTP.mp4)

### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-script-video3.md" target="_blank">Transcription vidéo 3</a>

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S6-diapos.pdf" target="_blank">Supports de présentation des vidéos 1 à 3</a>
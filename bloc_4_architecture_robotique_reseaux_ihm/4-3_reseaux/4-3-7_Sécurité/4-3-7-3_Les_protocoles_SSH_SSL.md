# Sécurité 3/5: Les protocoles SSH/SSL

1. Firewall / Proxy / DMZ
2. Chiffrement
3. **Les protocoles SSH/SSL**
4. VPN
5. VLAN

####

- **Anne Josiane Kouam** présente les protocoles standardisés SSH et SSL qui sont grandement utilisés dans le monde d'aujourd'hui et qui sont basés sur des méthodes de chiffrement.

[![Vidéo 3 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video3-SSH_et_SSL.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video3-SSH_et_SSL.mp4)

 #### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-script-video3.md" target="_blank">Transcription de la vidéo</a> 

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-video3-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>

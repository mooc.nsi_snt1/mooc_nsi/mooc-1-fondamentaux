# Sécurité 4/5: VPN

1. Firewall / Proxy / DMZ
2. Chiffrement
3. Les protocoles SSH/SSL
4. **VPN**
5. VLAN

####

- **Anne Josiane Kouam** aborde dans la video le réseau VPN ou réseau privé virtuel, qui est notamment un élément essentiel de sécurité des réseaux d'entreprise dans la mise en œuvre du télétravail. 

[![Vidéo 4 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video4-VPN.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video4-VPN.mp4)

 #### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-script-video4.md" target="_blank">Transcription de la vidéo</a> 

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-video4-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>

# Sécurité 1/5: sécurité du réseau local - Firewall / Proxy / DMZ

1. **Firewall / Proxy / DMZ**
2. Chiffrement
3. Les protocoles SSH/SSL
4. VPN
5. VLAN

####

- **Anthony Juton**. Présentation du Module Sécurité qui a  pour objectif d'Introduire les principes et quelques outils de sécurité des réseaux informatiques. 

[![Vidéo 1 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video1_SecuResLocal.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video1_SecuResLocal.mp4)

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-video1-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>





# Sécurité 5/5: VLAN

1. Firewall / Proxy / DMZ
2. Chiffrement
3. Les protocoles SSH/SSL
4. VPN
5. **VLAN**

####

- **Anthony Juton**.  L'isolation des réseaux est un élément important de la sécurité. Nous présentons Le VLAN (Virtual Local Area Network) qui est un de ces outils qui permettent l'isolation d'un réseau.

[![Vidéo 5 B4-M3-S7 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M3-S7_video5_VLAN.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M3-S7_video5_VLAN.mp4)

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M3/B4-M3-S7-video5-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>


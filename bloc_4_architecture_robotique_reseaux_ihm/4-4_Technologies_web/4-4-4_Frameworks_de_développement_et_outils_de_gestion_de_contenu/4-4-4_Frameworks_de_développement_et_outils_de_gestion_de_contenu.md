# Frameworks de développement et outils de gestion de contenu

1. Introduction à HTML 5
2. Déploiement d'un site web
3. Interactions et gestion dynamique dans une page web
4. **Frameworks de développement et outils de gestion de contenu**

####

* **Rodrigue Chakode**. Ouverture sur les CMS (Wordpress et autres...) et les framework MVC (Symphony et Django/Flask)
 
[![Vidéo 1 B4-M4-S4 ](https://files.inria.fr/LearningLab_public/C045TV/img/NSI-B4-M4-S4-video1.png)](https://files.inria.fr/LearningLab_public/C045TV/NSI-B4-M4-S4-video1.mp4 )

## Supports de présentation (diapos)

#### <a href="https://gitlab.com/mooc-nsi-snt/mooc-1-fondamentaux/-/raw/master/__supports-de-cours/B4-M4/NSI-B4-M4-S4-diapos.pdf" target="_blank">Supports de présentation de la vidéo</a>



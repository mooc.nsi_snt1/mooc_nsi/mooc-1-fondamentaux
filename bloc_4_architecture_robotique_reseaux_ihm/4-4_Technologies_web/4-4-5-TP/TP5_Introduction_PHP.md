# TP5 - Introduction à PHP (120 mn)

Le langage PHP (PHP: Hypertext Preprocessor) est un langage interprété libre et gratuit, multiplateforme, assez proche dans sa syntaxe du langage C. Il est interprété sur le serveur pour générer dynamiquement des pages web. Il est extrêmement populaire dans le secteur du web. Le site https://w3techs.com/ indique que 78% des sites web ayant un langage de programmation côté serveur utilisent php.

Le serveur web doit disposer d'un interpréteur php. Les pages web comportent du code php. En fonction notamment des réponses de l'utilisateur aux formulaires des pages web, le serveur va générer des pages HTML. On parle de site web dynamique. 

Très souvent, le langage php est associé à une base de données (mySQL, mariaDB, PostgreSQL...)

<img src="image-30.png" alt="PHP" width="600px;">


Ce TP n'est qu'une introduction à PHP dont l'objectif est de comprendre comment fonctionne PHP.

Ce TP utilise toujours le serveur web apache, associé à un interpréteur PHP et une base de données MariaDB : LAMP. Le serveur web nginx permet les mêmes fonctionnalités.
LAMP (Linux Apache MySQL PHP), comme WAMP (Windows…) et MAMP (Macintosh…) est l’association des outils suivants : 
* Un système d’exploitation pour l’accès aux ressources processeurs, mémoire, réseau,
* Un serveur web Apache,
* Un interpréteur PHP pour générer dynamiquement les pages HTML
* Un gestionnaire de base de données MySQL ou MariaDB pour stocker et exploiter les données du site web.

## Les formulaires

Les formulaires permettent d’envoyer des informations de l’utilisateur vers le site web.

### Méthode GET

Ajouter un formulaire dans une page web avec les balises :
* `<form name=“nom_formulaire” method=“GET”>`
* `</form>`

Dans ce formulaire, ajouter des champs.
Un champ texte avec :
	`<input type="text" name ="champ_nom" value ="Valeur_par_defaut">`
Un bouton pour envoyer le texte 
	`<input type="submit" value=“ENVOYER”>`


<img src="image-31.png" alt="php 2" width="600px;">



Là encore, une description plus complète des possibilités offertes (les listes déroulantes, les boutons radios et les cases à cocher notamment) par les formulaires est disponible sur https://www.w3schools.com/.

Si on lance un analyseur réseau (Wireshark), on s'aperçoit que la réponse au formulaire circule dans la requête HTTP.

<img src="image-32.png" alt="php 3" width="600px;">


### Méthode POST

Il est possible de créer un formulaire similaire, avec method=“POST”.

<img src="image-33.png" alt="methode Post" width="600px;">

On observe alors sur wireshark que la réponse au formulaire n'est pas transporté dans la requête, mais dans le champ de données associé.

<img src="image-34.png" alt="methode Post réponse au formulaire" width="600px;">

## Premières lignes de PHP

### Installation d’un interpréteur PHP sur le serveur apache 

Sur Linux, l’interpréteur PHP pour apache s’installe avec les lignes suivantes :

* sudo apt install php libapache2-mod-php

Il faut ensuite redémarrer le serveur apache : 

* sudo systemctl restart apache2.service

Les pages php doivent être sur le serveur (dans le dossier /var/www/html) pour être interprété, il n’est plus possible de tester depuis le dossier personnel.
Pour tester le bon fonctionnement de l'interpréteur php, il suffit de copier dans le serveur une page test.php avec `<?php phpinfo() ; ?>` et de l’appeler via un navigateur web : 127.0.0.1/test.php

<img src="image-35.png" alt="tester PHP 1" width="600px;">

<img src="image-36.png" alt="tester PHP 2" width="600px;">

La page générée par la commande phpinfo() donne les informations de version et indique les plugins installés.

Le code php, très inspiré du langage C, est inséré dans une page html entre une balise ouvrante `<?php` et une balise fermante `?>`. La page reçoit alors l'extension .php pour signifier qu'elle doit passer par l'interpréteur php.
Le fichier .php contient donc à la fois du langage html et du code php.

Pour déboguer le code, noter que les messages d’erreur PHP sont dans le fichier /var/log/apache2/error.log

<img src="image-39.png" alt="messages erreur PHP" width="600px;">


### Echo

La fonction echo, la plus simple, permet de créer une ligne de texte HTML avec une instruction PHP :
`<?php echo 'Sur le mur du ptit Arthur'; ?>`

Ajouter la création de texte via php dans le fichier index.html, le renommer index.php, (et supprimer le fichier index.html) et afficher :

* d'une part le code source php présent sur le serveur
* d'autre part la source du fichier reçu par le navigateur à l'adresse 127.0.0.1

<img src="image-38.png" alt="fonction echo PHP" width="600px;">


On peut ainsi voir que, si le fichier index.php contient bien des instructions php, le fichier reçu par le navigateur ne contient que du code html. L'interpréteur php a généré une page html, que le serveur a envoyé au client.

### L’horodatage

En PHP, les variables, qui n’ont pas besoin d’être déclarées, sont repérées par un $ au début de leur nom.

Le temps est souvent utilisé sous forme d’un entier en informatique. Sous Unix, le timestamp (horodatage) est un entier correspondant au nombre de secondes écoulées depuis le 1er janvier 1970, temps UTC.
Pour connaître le timestamp actuel en PHP, on utilise la fonction time (qui n'a besoin d'aucun paramètre) et il est possible de créer le timestamp d'une date particulière avec mktime : 


* `$timestamp_actuel = time();`
* `$timestamp = mktime(heure, minutes, secondes, mois, jour, an, heure d'hiver);`

Inversement, on peut reconstruire une date à partir d’un timestamp $mon_timestamp. Par exemple, si on choisit $mon_timestamp = 0, on peut trouver l'origine du timestamp.


* `$jour = date('d',$mon_timestamp);`
* `$mois = date('m',$mon_timestamp);`
* `$annee = date('Y',$mon_timestamp);`
* `$heure = date('H',$mon_timestamp);`
* `$minute = date('i',$mon_timestamp);`
* `$seconde = date(‘sa’,$mon_timestamp);`

Afficher l’heure et la date actuelle, ainsi que le timestamp du moment. Vérifier que la date affichée correspond bien au principe de l’horodatage UNIX expliqué ci-dessus.
Afficher l’heure et la date correspondant à la création de la première version de ce TP ($mon_timestamp = 1291407502).

<img src="image-37.png" alt="ftimestamp PHP" width="600px;">


Noter dans l'en-tête la ligne `<meta http-equiv="Refresh" content="1">` qui permet de demander le rafraichissement périodique de la page chaque seconde. Attention, rafraîchir la page trop rapidement ne rend pas l'usage des formulaires aisé.

### Utilisation des variables issues du formulaire

L’intérêt d’un site web dynamique est de pouvoir tenir compte des envois de l’utilisateur pour générer des pages web adaptées.
Le retour du formulaire est obtenu via $_POST['nom_du_champ']. Comme cette variable est vide tant que l'utilisateur n'a pas répondu, il est bon de vérifier son état avant de l'utiliser, comme le montre le code suivant.

<img src="image-40.png" alt="variables formulaire PHP" width="600px;">


Noter que php permet d’utiliser des structures de contrôle if else, mais aussi switch / case, while et for.
De nombreuses autres possibilités du langage php sont décrites sur le site http://www.w3schools.com.

## Connexion de PHP à une base de données

Pour gérer les données du site web, il est couramment fait usage d’une base de données. Les deux système de gestion de bases de données (SGDB) libres les plus simples pour une association avec PHP sont MySQL et MariaDB (continuation libre de MySQL depuis son rachat par Oracle).
SQL (Standard Query Langage) est le langage universellement utilisé pour créer et gérer la plupart des bases de données.

### Installation de MariaDB et importation d'une base de test

La ligne suivante permet d'installer le système de gestion de bases de données (SGBD) MariaDB : 

* sudo apt install mariadb-server

Celle-ci installe ensuite le paquet avec les fonctions PHP d’interaction avec les bases de données :

* sudo apt install php-mysql

Enfin, on redémarre le serveur apache pour qu’il prenne en compte l’ajout du paquet php-mysql.

* sudo systemctl restart apache2.service 

Le serveur de bases de données installé, créer un compte client pour le serveur web :

* sudo apt install mariadb-client

Se connecter à la base de données qui a son propre système de gestion des utilisateurs et des autorisations :

* sudo mariadb

En cas de souci de connexion, forcer le démarrage du serveur de base de données :

* sudo service mariadb start

A l'invite de commande du SGBD (`MariaDB[(none)] >`) créer une base de données :
* create DATABASE TPINTROWEB;

Créer un utilisateur client_tpweb avec le mot de passe client_tpweb_pwd, lui donner les droits sur la base TPINTROWEB et mettre à jour les privilèges :

* CREATE USER client_tpweb IDENTIFIED BY 'client_tpweb_pwd';
* GRANT ALL PRIVILEGES ON TPINTROWEB.* TO client_tpweb;
* FLUSH PRIVILEGES;
* exit

Pour faciliter l'utilisation d'une base de données, quelques tables d'une base de données très simple sont fournies. On les importe de la manière suivante : 

* mariadb -u client_tpweb -p TPINTROWEB < /chemin_personnel/tpm2.sql

Le mot de passe demandé est celui de client_tpweb.

Vérifier le contenu de la base de données, en se connectant, en affichant la liste des tables et en affichant le contenu de la table INDIVIDUS. 

* mariadb -u client_tpweb -p  TPINTROWEB
* show tables;
* select * from INDIVIDUS;


<img src="image-41.png" alt="mariaDB" width="600px;">


### phpmyAdmin

L’outil phpMyAdmin est bien utile pour gérer de manière graphique les bases de données et la configuration de PHP.
PhpMyAdmin est une page PHP qui permet d’interagir avec MariaDB ou MySQL : créer une base de données, créer des tables, tester des requêtes.

La ligne suivante permet de l’installer :
* sudo apt install phpmyadmin php-mbstring php-zip php-gd php-json php-curl

Les options par défaut sont correctes. Il faut choisir un mot de passe sûr.

Une fois phpmyadmin installé, redémarrer apache2 :

* sudo systemctl restart apache2

phpmyadmin est alors disponible via un navigateur web à adresse_serveur/phpmyadmin.

Si phpmyadmin ne s’affiche pas, modifier légèrement la configuration de apache, par exemple avec l'éditeur de fichier texte __nano__ :

* sudo nano /etc/apache2/apache2.conf

Ajouter la ligne suivante à la fin du fichier :

* Include /etc/phpmyadmin/apache.conf

Enregistrer et quitter (CTRL+X et Yes ou Oui sur nano) puis redémarrer le serveur apache :

* sudo systemctl restart apache2

<img src="image-43.png" alt="restart serveur apache" width="600px;">


On se connecte à la base TPINTROWEB avec les login/mot de passe du client propriétaire de la base de données : client_tpweb et client_tpweb_pwd dans cet exemple.

<img src="image-42.png" alt="serveur apache" width="600px;">

L’onglet __Structure__ permet de voir les différentes tables et de les modifier. L’onglet __SQL__ permet d’exécuter des requêtes SQL, ce qui est bien utile avant de les intégrer dans un fichier PHP.

### Connexion PHP - SQL

La fonction PHP mysqli_connect(..) permet d’établir une connexion avec une base de données. Elle renvoie une variable contenant les données de connexion et prend en argument : 

* L’adresse du serveur de base de données, localhost ici,
* Le nom de l’utilisateur mariaDB, client_tpweb ici,
* Le mot de passe de cet utilisateur, client_tpweb_pwd ici,
* Le nom de la base de données, TPINTROWEB ici

<img src="image-44.png" alt="connexion PHP SQL" width="600px;">

Tester la connexion.

### Requêtes SQL en PHP

La fonction PHP __mysqli_query(...)__ reçoit en argument la variable connexion et une chaîne de caractères contenant la requête. Elle renvoie la table des réponses.
La fonction __mysqli_fetch_array(…)__ permet de renvoyer ligne par ligne les réponses.
Enfin la fonction __mysqli_close()__ ferme la connexion à la base de données.

<img src="image-45.png" alt="requete SQL en PHP" width="600px;">


Il est bien entendu possible d'utiliser la réponse au formulaire champ_nom (de méthode POST) pour obtenir la réponse à une requête sélective. Attention à bien utiliser les " et les ' pour créer la requête avec des variable 


<img src="image-46.png" alt="requete SQL en PHP" width="600px;">


Afficher la liste des films et le rôle joué par les acteurs dont le prénom est cité dans le formulaire champ_prenom.


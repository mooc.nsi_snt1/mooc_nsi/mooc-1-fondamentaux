# Introduction aux TPs technologies web - Installation du serveur (60 mn)

## Objectif

L'objectif des travaux pratiques de ce module est de mettre en oeuvre pour les comprendre **les principales technologies web : HTML (et les feuilles de style css), PHP (et ses liens avec les bases de données SQL) et javascript**.
Il ne s'agit pas de former des développeurs web, ce qui explique l'impasse faite sur les systèmes de gestion de contenu (Wordpress, Django, Flask...)
A la fin des TPs, l'apprenant sera capable de développer un site web utilisant quelques fonctionnalités de ces technologies et de le mettre en ligne.
L’apprenant intéressé pourra ensuite s’appuyer sur cette première introduction pour approfondir ce sujet à l’aide des nombreux tutoriels disponibles sur internet (openclassroom, w3schools...).

## Matériel utilisé

Le serveur web utilisé pendant les TPs est une machine virtuelle Linux Ubuntu 20.04.
C'est transposable très simplement sur une machine physique Linux moderne ou sur une Raspberry Pi (quelques mots seront ajoutés si besoin).
Les outils (Apache, MariaDB, PHP) fonctionnent également sur une machine Windows ou Mac et le support sur les forums est très important si besoin. Les sujets de TPs n'ont juste pas été testés sur ces systèmes.
La partie suivante n'est donc pas indispensable, mais recommandée pour suivre les TPs sans difficulté de configuration de logiciels.

## Installation d'une machine virtuelle Linux (45 mn)

La machine physique est nommée ici machine hôté et la machine virtuelle est parfois nommée VM (pour Virtual Machine).

### Création de la machine sur Virtual Box

On commence par créer une machine sur virtual box :
* Télécharger Virtual Box : https://www.virtualbox.org/ Cela fonctionne sur Windows, Mac et Linux.
* Installer et Exécuter Virtual Box.
* Cliquer sur Machine > Nouvelle... et choisir Type : Linux et Version : Ubuntu 64 bits.


<img src="image.png" alt="un service proposé par le système d'exploitation" width="500px;">

> un service proposé par le système d'exploitation

* Affecter au moins 2 Go de mémoire (cela dépend de la machine hôte, 4 Go c'est mieux).

<img src="image-1.png" alt="un service proposé par le système d'exploitation" width="500px;">

* Ajouter un disque dur virtuel, type VDI avec un stockage dynamiquement alloué, et une limite à 20 Go dans la mesure du possible.

<img src="image-2.png" alt="un service proposé par le système d'exploitation" width="500px;">

<img src="image-3.png" alt="un service proposé par le système d'exploitation" width="500px;">

<img src="image-4.png" alt="un service proposé par le système d'exploitation" width="500px;">

<img src="image-5.png" alt="un service proposé par le système d'exploitation" width="500px;">


* Cliquer sur Créer.

### Configuration de la machine

* Pour ajouter 2 fonctionnalités à la machine virtuelle (VM pour virtual Machine), cliquer sur le bouton configuration : 

<img src="image-6.png" alt="configuration VM 1" width="500px;">

* Dans l'onglet Général > Avancé, autoriser les copier/coller et le glisser/déposer de la machine hôte vers la machine virtuelle, c'est très pratique en développement.

<img src="image-7.png" alt="configuration VM 2" width="500px;">

* Dans l'onglet Dossiers partagés, ajouter un dossier en indiquant le chemin sur la machine hôte et le chemin sur la machine destination /home/nom_utilisateur/dossierpartage par exemple.

<img src="image-8.png" alt="configuration VM 3" width="500px;">

* Enfin, pour autoriser l'utilisation des ports USB par la machine virtuelle, il faut, sous Linux uniquement, ajouter l'utilisateur au groupe vbox avec la ligne de commande suivante :

`sudo usermod -G vboxusers -a ajuton`



### Installation de Linux sur la machine

La machine étant créée, il faut lui installer un système d'exploitation. Pour cela, télécharger l'image du disque d'installation d'Ubuntu sur https://www.ubuntu-fr.org/download/.
* Démarrer la machine virtuelle créée ci-dessus

<img src="image-11.png" alt="installation de Linux sur VM" width="500px;">



* Dans la fenêtre de choix du disque de démarrage, ajouter, grâce à l'icône jaune à droite, le chemin vers le fichier iso téléchargé.

<img src="image-10.png" alt="installation de Linux sur VM" width="500px;">

* Suivre ensuite les instructions d'installation de Ubuntu, les propositions par défaut étant souvent bien adaptées. La machine virtuelle n'ayant accès qu'à son propre espace, il est normal, et sans risque, que l'installeur propose d'effacer l'ensemble du disque.

* Une fois l'installation terminée, redémarrer la machine et se logguer.

* Passer les fenêtres de démarrage. Autoriser les mises à jour lorsqu'elles sont proposées.

* Pour augmenter la résolution, cliquer droit sur le fond du bureau et choisir configuration d'affichage.

* Pour un bon fonctionnement du dossier partagé et du copier/coller entre machines hôte et virtuelle, ajouter les additions invités en cliquant Périphériques > Insérer l'image CD des additions invités... puis lancer et mettre son mot de passe à chaque que fois que c'est demandé. Redémarrer la machine ensuite et vérifier que le dossier partagé apparaît et que le copier/coller est possible.

### Remarques

Si jamais les additions invitées ne s'installent pas (la machine virtuelle indique qu'elle ne trouve pas le "disque"), il est possible de télécharger le disque directement depuis la machine virtuelle : 
* wget http://download.virtualbox.org/virtualbox/6.1.26/VBoxGuestAdditions_6.1.26.iso
* sudo mount -o loop VBoxGuestAdditions_6.1.26.iso /media/nom_utilisateur
* cd /media/nom_utilisateur/
* sudo ./VBoxLinuxAdditions.run 
Il faut redémarrer la machine virtuelle ensuite.

Si le dossier partagé apparaît mais n'est pas accessible à l'utilisateur (permission refusée), il faut ajouter l'utilisateur dans le groupe des utilisateurs de ce dossier :
* sudo usermod -a -G vboxsf nom_utilisateur
Il faut alors redémarrer la machine virtuelle pour que l'utilisateur ait ses nouveaux droits au redémarrage.
Si jamais l'accès n'est toujours pas possible, on peut forcer le changement des droits du dossier : 
* sudo chmod +770 dossierpartage

<img src="image-16.png" alt="installation de Linux sur VM" width="500px;">








